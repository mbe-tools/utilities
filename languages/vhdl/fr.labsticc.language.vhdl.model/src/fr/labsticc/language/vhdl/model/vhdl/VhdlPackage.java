/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import java.lang.String;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlFactory
 * @model kind="package"
 * @generated
 */
public interface VhdlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "vhdl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.labsticc.fr/language/vhdl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "vhdl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VhdlPackage eINSTANCE = fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IdentifiedElementImpl <em>Identified Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IdentifiedElementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIdentifiedElement()
	 * @generated
	 */
	int IDENTIFIED_ELEMENT = 143;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT__ID = 0;

	/**
	 * The number of structural features of the '<em>Identified Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Identified Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.VhdlFileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlFileImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getVhdlFile()
	 * @generated
	 */
	int VHDL_FILE = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VHDL_FILE__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Design</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VHDL_FILE__DESIGN = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VHDL_FILE_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VHDL_FILE_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.DesignUnitImpl <em>Design Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.DesignUnitImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getDesignUnit()
	 * @generated
	 */
	int DESIGN_UNIT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Library</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT__LIBRARY = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Use</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT__USE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT__UNIT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Design Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Design Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_UNIT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NamedElementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 144;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.DeclarationImpl <em>Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.DeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getDeclaration()
	 * @generated
	 */
	int DECLARATION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.UseClauseImpl <em>Use Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.UseClauseImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUseClause()
	 * @generated
	 */
	int USE_CLAUSE = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CLAUSE__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CLAUSE__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Use</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CLAUSE__USE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Use Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CLAUSE_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Use Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CLAUSE_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.LibraryUnitImpl <em>Library Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.LibraryUnitImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getLibraryUnit()
	 * @generated
	 */
	int LIBRARY_UNIT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_UNIT__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_UNIT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_UNIT__DECLARATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Library Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_UNIT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Library Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_UNIT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.TypeDefinitionImpl <em>Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.TypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getTypeDefinition()
	 * @generated
	 */
	int TYPE_DEFINITION = 113;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DEFINITION__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DEFINITION_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DEFINITION_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AccessTypeDefinitionImpl <em>Access Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AccessTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAccessTypeDefinition()
	 * @generated
	 */
	int ACCESS_TYPE_DEFINITION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_DEFINITION__TYPE = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Access Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Access Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ChoiceImpl <em>Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ChoiceImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getChoice()
	 * @generated
	 */
	int CHOICE = 13;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NameElementImpl <em>Name Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NameElementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNameElement()
	 * @generated
	 */
	int NAME_ELEMENT = 5;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ArchitectureImpl <em>Architecture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ArchitectureImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getArchitecture()
	 * @generated
	 */
	int ARCHITECTURE = 7;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.StatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.StatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getStatement()
	 * @generated
	 */
	int STATEMENT = 8;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NatureDefinitionImpl <em>Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNatureDefinition()
	 * @generated
	 */
	int NATURE_DEFINITION = 68;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.CompositeNatureDefinitionImpl <em>Composite Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.CompositeNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getCompositeNatureDefinition()
	 * @generated
	 */
	int COMPOSITE_NATURE_DEFINITION = 30;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ArrayNatureDefinitionImpl <em>Array Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ArrayNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getArrayNatureDefinition()
	 * @generated
	 */
	int ARRAY_NATURE_DEFINITION = 9;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.CompositeTypeDefinitionImpl <em>Composite Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.CompositeTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getCompositeTypeDefinition()
	 * @generated
	 */
	int COMPOSITE_TYPE_DEFINITION = 31;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ArrayTypeDefinitionImpl <em>Array Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ArrayTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getArrayTypeDefinition()
	 * @generated
	 */
	int ARRAY_TYPE_DEFINITION = 10;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AssertionStatementImpl <em>Assertion Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AssertionStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAssertionStatement()
	 * @generated
	 */
	int ASSERTION_STATEMENT = 11;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_ELEMENT__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_ELEMENT__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_ELEMENT__SIGNATURE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Name Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_ELEMENT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Name Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_ELEMENT_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__ID = LIBRARY_UNIT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__NAME = LIBRARY_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__DECLARATION = LIBRARY_UNIT__DECLARATION;

	/**
	 * The feature id for the '<em><b>Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__OF = LIBRARY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE__STATEMENT = LIBRARY_UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_FEATURE_COUNT = LIBRARY_UNIT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Architecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARCHITECTURE_OPERATION_COUNT = LIBRARY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__ID = NAMED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DEFINITION__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DEFINITION_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DEFINITION_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_NATURE_DEFINITION__ID = NATURE_DEFINITION__ID;

	/**
	 * The number of structural features of the '<em>Composite Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_NATURE_DEFINITION_FEATURE_COUNT = NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composite Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_NATURE_DEFINITION_OPERATION_COUNT = NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_NATURE_DEFINITION__ID = COMPOSITE_NATURE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_NATURE_DEFINITION__NATURE = COMPOSITE_NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Array Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_NATURE_DEFINITION_FEATURE_COUNT = COMPOSITE_NATURE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Array Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_NATURE_DEFINITION_OPERATION_COUNT = COMPOSITE_NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The number of structural features of the '<em>Composite Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composite Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_DEFINITION__ID = COMPOSITE_TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_DEFINITION__TYPE = COMPOSITE_TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Array Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_DEFINITION_FEATURE_COUNT = COMPOSITE_TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Array Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_DEFINITION_OPERATION_COUNT = COMPOSITE_TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__CONDITION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Report</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__REPORT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__SEVERITY = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Assertion Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Assertion Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.OpenImpl <em>Open</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.OpenImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getOpen()
	 * @generated
	 */
	int OPEN = 14;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPEN__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPEN__NAME = EXPRESSION__NAME;

	/**
	 * The number of structural features of the '<em>Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPEN_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPEN_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AttributeDeclarationImpl <em>Attribute Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AttributeDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAttributeDeclaration()
	 * @generated
	 */
	int ATTRIBUTE_DECLARATION = 15;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attribute Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AttributeSpecificationImpl <em>Attribute Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AttributeSpecificationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAttributeSpecification()
	 * @generated
	 */
	int ATTRIBUTE_SPECIFICATION = 16;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION__ENTITY = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION__CLASS = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION__IS = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Attribute Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Attribute Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SPECIFICATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NameListImpl <em>Name List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NameListImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNameList()
	 * @generated
	 */
	int NAME_LIST = 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_LIST__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_LIST__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Name List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_LIST_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Name List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_LIST_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationItemImpl <em>Configuration Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationItemImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConfigurationItem()
	 * @generated
	 */
	int CONFIGURATION_ITEM = 38;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_ITEM__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Configuration Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_ITEM_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Configuration Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_ITEM_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BlockConfigurationImpl <em>Block Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BlockConfigurationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBlockConfiguration()
	 * @generated
	 */
	int BLOCK_CONFIGURATION = 18;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION__ID = CONFIGURATION_ITEM__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION__NAME = CONFIGURATION_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Use</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION__USE = CONFIGURATION_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Item</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION__ITEM = CONFIGURATION_ITEM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Block Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION_FEATURE_COUNT = CONFIGURATION_ITEM_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Block Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_CONFIGURATION_OPERATION_COUNT = CONFIGURATION_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BlockStatementImpl <em>Block Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BlockStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBlockStatement()
	 * @generated
	 */
	int BLOCK_STATEMENT = 19;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__GUARD = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__GENERIC = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__GENERIC_MAP = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__PORT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__PORT_MAP = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__DECLARATION = STATEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Block Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Block Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BreakElementImpl <em>Break Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BreakElementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBreakElement()
	 * @generated
	 */
	int BREAK_ELEMENT = 20;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT__USE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Arrow</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT__ARROW = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Break Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Break Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_ELEMENT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BreakStatementImpl <em>Break Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BreakStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBreakStatement()
	 * @generated
	 */
	int BREAK_STATEMENT = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Break</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__BREAK = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__WHEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sensitivity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT__SENSITIVITY = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Break Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Break Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BREAK_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.CaseStatementImpl <em>Case Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.CaseStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getCaseStatement()
	 * @generated
	 */
	int CASE_STATEMENT = 22;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__CASE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT__WHEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Case Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Case Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.CaseAlternativeImpl <em>Case Alternative</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.CaseAlternativeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getCaseAlternative()
	 * @generated
	 */
	int CASE_ALTERNATIVE = 23;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_ALTERNATIVE__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_ALTERNATIVE__CHOICE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_ALTERNATIVE__STATEMENT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Case Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_ALTERNATIVE_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Case Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASE_ALTERNATIVE_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ComponentConfigurationImpl <em>Component Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ComponentConfigurationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getComponentConfiguration()
	 * @generated
	 */
	int COMPONENT_CONFIGURATION = 24;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__ID = CONFIGURATION_ITEM__ID;

	/**
	 * The feature id for the '<em><b>List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__LIST = CONFIGURATION_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__COMPONENT = CONFIGURATION_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__ENTITY = CONFIGURATION_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__CONFIGURATION = CONFIGURATION_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__GENERIC_MAP = CONFIGURATION_ITEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__PORT_MAP = CONFIGURATION_ITEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION__BLOCK = CONFIGURATION_ITEM_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Component Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION_FEATURE_COUNT = CONFIGURATION_ITEM_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Component Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_CONFIGURATION_OPERATION_COUNT = CONFIGURATION_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ComponentDeclarationImpl <em>Component Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ComponentDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getComponentDeclaration()
	 * @generated
	 */
	int COMPONENT_DECLARATION = 25;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION__GENERIC = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION__PORT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Component Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Component Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.InstantiationStatementImpl <em>Instantiation Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.InstantiationStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getInstantiationStatement()
	 * @generated
	 */
	int INSTANTIATION_STATEMENT = 26;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT__GENERIC_MAP = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT__PORT_MAP = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATION_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ComponentInstantiationStatementImpl <em>Component Instantiation Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ComponentInstantiationStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getComponentInstantiationStatement()
	 * @generated
	 */
	int COMPONENT_INSTANTIATION_STATEMENT = 27;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT__ID = INSTANTIATION_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT__NAME = INSTANTIATION_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT__GENERIC_MAP = INSTANTIATION_STATEMENT__GENERIC_MAP;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT__PORT_MAP = INSTANTIATION_STATEMENT__PORT_MAP;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT__COMPONENT = INSTANTIATION_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT_FEATURE_COUNT = INSTANTIATION_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Component Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANTIATION_STATEMENT_OPERATION_COUNT = INSTANTIATION_STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.EntityInstantiationStatementImpl <em>Entity Instantiation Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.EntityInstantiationStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getEntityInstantiationStatement()
	 * @generated
	 */
	int ENTITY_INSTANTIATION_STATEMENT = 28;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT__ID = INSTANTIATION_STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT__NAME = INSTANTIATION_STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT__GENERIC_MAP = INSTANTIATION_STATEMENT__GENERIC_MAP;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT__PORT_MAP = INSTANTIATION_STATEMENT__PORT_MAP;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT__ENTITY = INSTANTIATION_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Entity Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT_FEATURE_COUNT = INSTANTIATION_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Entity Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_INSTANTIATION_STATEMENT_OPERATION_COUNT = INSTANTIATION_STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationInstantiationStatementImpl <em>Configuration Instantiation Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationInstantiationStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConfigurationInstantiationStatement()
	 * @generated
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT = 29;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT__CONFIGURATION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT__GENERIC_MAP = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT__PORT_MAP = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Configuration Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Configuration Instantiation Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_INSTANTIATION_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SequentialSignalAssignmentStatementImpl <em>Sequential Signal Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SequentialSignalAssignmentStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT = 32;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Guarded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__GUARDED = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__DELAY = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Waveform</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT__WAVEFORM = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Sequential Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Sequential Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalSignalAssignmentStatementImpl <em>Conditional Signal Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalSignalAssignmentStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT = 33;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Guarded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__GUARDED = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__DELAY = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Waveform</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT__WAVEFORM = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Conditional Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Conditional Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SelectedSignalAssignmentStatementImpl <em>Selected Signal Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SelectedSignalAssignmentStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT = 34;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__EXPRESSION = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Guarded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__GUARDED = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__DELAY = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Waveform</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT__WAVEFORM = STATEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Selected Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Selected Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTED_SIGNAL_ASSIGNMENT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimpleSimultaneousStatementImpl <em>Simple Simultaneous Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimpleSimultaneousStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimpleSimultaneousStatement()
	 * @generated
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT = 35;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT__LEFT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT__RIGHT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tolerance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT__TOLERANCE = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Simple Simultaneous Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Simple Simultaneous Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SIMULTANEOUS_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalWaveformImpl <em>Conditional Waveform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConditionalWaveformImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConditionalWaveform()
	 * @generated
	 */
	int CONDITIONAL_WAVEFORM = 36;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_WAVEFORM__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Waveform</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_WAVEFORM__WAVEFORM = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_WAVEFORM__CHOICE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conditional Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_WAVEFORM_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Conditional Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_WAVEFORM_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationDeclarationImpl <em>Configuration Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConfigurationDeclaration()
	 * @generated
	 */
	int CONFIGURATION_DECLARATION = 37;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION__ID = LIBRARY_UNIT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION__NAME = LIBRARY_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION__DECLARATION = LIBRARY_UNIT__DECLARATION;

	/**
	 * The feature id for the '<em><b>Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION__OF = LIBRARY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION__BLOCK = LIBRARY_UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Configuration Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION_FEATURE_COUNT = LIBRARY_UNIT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Configuration Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_DECLARATION_OPERATION_COUNT = LIBRARY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl <em>Configuration Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConfigurationSpecificationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConfigurationSpecification()
	 * @generated
	 */
	int CONFIGURATION_SPECIFICATION = 39;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>List</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__LIST = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__COMPONENT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__ENTITY = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__CONFIGURATION = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__GENERIC_MAP = DECLARATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION__PORT_MAP = DECLARATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Configuration Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Configuration Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_SPECIFICATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.DelayMechanismImpl <em>Delay Mechanism</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.DelayMechanismImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getDelayMechanism()
	 * @generated
	 */
	int DELAY_MECHANISM = 40;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_MECHANISM__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Delay Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_MECHANISM_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Delay Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_MECHANISM_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RejectMechanismImpl <em>Reject Mechanism</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RejectMechanismImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRejectMechanism()
	 * @generated
	 */
	int REJECT_MECHANISM = 41;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT_MECHANISM__ID = DELAY_MECHANISM__ID;

	/**
	 * The feature id for the '<em><b>Reject</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT_MECHANISM__REJECT = DELAY_MECHANISM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reject Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT_MECHANISM_FEATURE_COUNT = DELAY_MECHANISM_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reject Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REJECT_MECHANISM_OPERATION_COUNT = DELAY_MECHANISM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.TransportImpl <em>Transport</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.TransportImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getTransport()
	 * @generated
	 */
	int TRANSPORT = 42;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSPORT__ID = DELAY_MECHANISM__ID;

	/**
	 * The number of structural features of the '<em>Transport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSPORT_FEATURE_COUNT = DELAY_MECHANISM_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Transport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSPORT_OPERATION_COUNT = DELAY_MECHANISM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.DisconnectionSpecificationImpl <em>Disconnection Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.DisconnectionSpecificationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getDisconnectionSpecification()
	 * @generated
	 */
	int DISCONNECTION_SPECIFICATION = 43;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Disconnect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION__DISCONNECT = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION__TYPE = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>After</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION__AFTER = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Disconnection Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Disconnection Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONNECTION_SPECIFICATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ElementDeclarationImpl <em>Element Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ElementDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getElementDeclaration()
	 * @generated
	 */
	int ELEMENT_DECLARATION = 44;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_DECLARATION__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_DECLARATION__IDENTIFIER = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_DECLARATION__TYPE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Element Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_DECLARATION_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Element Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_DECLARATION_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.EntityDeclarationImpl <em>Entity Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.EntityDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getEntityDeclaration()
	 * @generated
	 */
	int ENTITY_DECLARATION = 45;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__ID = LIBRARY_UNIT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__NAME = LIBRARY_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__DECLARATION = LIBRARY_UNIT__DECLARATION;

	/**
	 * The feature id for the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__GENERIC = LIBRARY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__PORT = LIBRARY_UNIT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION__STATEMENT = LIBRARY_UNIT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Entity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION_FEATURE_COUNT = LIBRARY_UNIT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Entity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_DECLARATION_OPERATION_COUNT = LIBRARY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.EnumerationTypeDefinitionImpl <em>Enumeration Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.EnumerationTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getEnumerationTypeDefinition()
	 * @generated
	 */
	int ENUMERATION_TYPE_DEFINITION = 46;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_DEFINITION__LITERAL = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumeration Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enumeration Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ExitStatementImpl <em>Exit Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ExitStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getExitStatement()
	 * @generated
	 */
	int EXIT_STATEMENT = 47;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Exit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT__EXIT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT__WHEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Exit Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Exit Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXIT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.FileDeclarationImpl <em>File Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.FileDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getFileDeclaration()
	 * @generated
	 */
	int FILE_DECLARATION = 48;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Open</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION__OPEN = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION__IS = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>File Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>File Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.FileTypeDefinitionImpl <em>File Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.FileTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getFileTypeDefinition()
	 * @generated
	 */
	int FILE_TYPE_DEFINITION = 49;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>File</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_DEFINITION__FILE = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>File Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>File Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GenerateStatementImpl <em>Generate Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GenerateStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGenerateStatement()
	 * @generated
	 */
	int GENERATE_STATEMENT = 50;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Scheme</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT__SCHEME = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT__DECLARATION = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Generate Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Generate Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATE_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GenerationSchemeImpl <em>Generation Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GenerationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGenerationScheme()
	 * @generated
	 */
	int GENERATION_SCHEME = 51;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_SCHEME__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_SCHEME_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATION_SCHEME_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ForGenerationSchemeImpl <em>For Generation Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ForGenerationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getForGenerationScheme()
	 * @generated
	 */
	int FOR_GENERATION_SCHEME = 52;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_GENERATION_SCHEME__ID = GENERATION_SCHEME__ID;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_GENERATION_SCHEME__VARIABLE = GENERATION_SCHEME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_GENERATION_SCHEME__IN = GENERATION_SCHEME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>For Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_GENERATION_SCHEME_FEATURE_COUNT = GENERATION_SCHEME_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>For Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_GENERATION_SCHEME_OPERATION_COUNT = GENERATION_SCHEME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IfGenerationSchemeImpl <em>If Generation Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IfGenerationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIfGenerationScheme()
	 * @generated
	 */
	int IF_GENERATION_SCHEME = 53;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_GENERATION_SCHEME__ID = GENERATION_SCHEME__ID;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_GENERATION_SCHEME__CONDITION = GENERATION_SCHEME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>If Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_GENERATION_SCHEME_FEATURE_COUNT = GENERATION_SCHEME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>If Generation Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_GENERATION_SCHEME_OPERATION_COUNT = GENERATION_SCHEME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GenericsImpl <em>Generics</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GenericsImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGenerics()
	 * @generated
	 */
	int GENERICS = 54;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERICS__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERICS__DECLARATION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Generics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERICS_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Generics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERICS_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GenericMapsImpl <em>Generic Maps</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GenericMapsImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGenericMaps()
	 * @generated
	 */
	int GENERIC_MAPS = 55;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_MAPS__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Generic</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_MAPS__GENERIC = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Generic Maps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_MAPS_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Generic Maps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_MAPS_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GroupDeclarationImpl <em>Group Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GroupDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGroupDeclaration()
	 * @generated
	 */
	int GROUP_DECLARATION = 56;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION__IS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Member</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION__MEMBER = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Group Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Group Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.GroupTemplateDeclarationImpl <em>Group Template Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.GroupTemplateDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getGroupTemplateDeclaration()
	 * @generated
	 */
	int GROUP_TEMPLATE_DECLARATION = 57;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TEMPLATE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TEMPLATE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TEMPLATE_DECLARATION__ENTRY = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Group Template Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TEMPLATE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Group Template Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP_TEMPLATE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IfStatementImpl <em>If Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IfStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIfStatement()
	 * @generated
	 */
	int IF_STATEMENT = 58;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Test</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__TEST = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IfStatementTestImpl <em>If Statement Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IfStatementTestImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIfStatementTest()
	 * @generated
	 */
	int IF_STATEMENT_TEST = 59;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_TEST__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_TEST__CONDITION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_TEST__STATEMENT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>If Statement Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_TEST_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>If Statement Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_TEST_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConstraintImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 60;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__RANGE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.DiscreteRangeImpl <em>Discrete Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.DiscreteRangeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getDiscreteRange()
	 * @generated
	 */
	int DISCRETE_RANGE = 61;

	/**
	 * The number of structural features of the '<em>Discrete Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCRETE_RANGE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Discrete Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCRETE_RANGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.LoopStatementImpl <em>Loop Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.LoopStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getLoopStatement()
	 * @generated
	 */
	int LOOP_STATEMENT = 62;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Iteration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__ITERATION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IterationSchemeImpl <em>Iteration Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IterationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIterationScheme()
	 * @generated
	 */
	int ITERATION_SCHEME = 63;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_SCHEME__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_SCHEME_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_SCHEME_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.WhileIterationSchemeImpl <em>While Iteration Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.WhileIterationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getWhileIterationScheme()
	 * @generated
	 */
	int WHILE_ITERATION_SCHEME = 64;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ITERATION_SCHEME__ID = ITERATION_SCHEME__ID;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ITERATION_SCHEME__CONDITION = ITERATION_SCHEME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>While Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ITERATION_SCHEME_FEATURE_COUNT = ITERATION_SCHEME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>While Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ITERATION_SCHEME_OPERATION_COUNT = ITERATION_SCHEME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ForIterationSchemeImpl <em>For Iteration Scheme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ForIterationSchemeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getForIterationScheme()
	 * @generated
	 */
	int FOR_ITERATION_SCHEME = 65;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ITERATION_SCHEME__ID = ITERATION_SCHEME__ID;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ITERATION_SCHEME__VARIABLE = ITERATION_SCHEME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ITERATION_SCHEME__IN = ITERATION_SCHEME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>For Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ITERATION_SCHEME_FEATURE_COUNT = ITERATION_SCHEME_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>For Iteration Scheme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ITERATION_SCHEME_OPERATION_COUNT = ITERATION_SCHEME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AttributeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 66;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NatureDeclarationImpl <em>Nature Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NatureDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNatureDeclaration()
	 * @generated
	 */
	int NATURE_DECLARATION = 67;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DECLARATION__IS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Nature Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Nature Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NATURE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NextStatementImpl <em>Next Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NextStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNextStatement()
	 * @generated
	 */
	int NEXT_STATEMENT = 69;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Next</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT__NEXT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT__WHEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Next Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Next Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PackageBodyImpl <em>Package Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PackageBodyImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPackageBody()
	 * @generated
	 */
	int PACKAGE_BODY = 70;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_BODY__ID = LIBRARY_UNIT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_BODY__NAME = LIBRARY_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_BODY__DECLARATION = LIBRARY_UNIT__DECLARATION;

	/**
	 * The number of structural features of the '<em>Package Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_BODY_FEATURE_COUNT = LIBRARY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Package Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_BODY_OPERATION_COUNT = LIBRARY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PackageDeclarationImpl <em>Package Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PackageDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPackageDeclaration()
	 * @generated
	 */
	int PACKAGE_DECLARATION = 71;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_DECLARATION__ID = LIBRARY_UNIT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_DECLARATION__NAME = LIBRARY_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_DECLARATION__DECLARATION = LIBRARY_UNIT__DECLARATION;

	/**
	 * The number of structural features of the '<em>Package Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_DECLARATION_FEATURE_COUNT = LIBRARY_UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Package Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_DECLARATION_OPERATION_COUNT = LIBRARY_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionImpl <em>Physical Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPhysicalTypeDefinition()
	 * @generated
	 */
	int PHYSICAL_TYPE_DEFINITION = 72;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION__RANGE = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Primary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION__PRIMARY = TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Secondary</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION__SECONDARY = TYPE_DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Physical Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Physical Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionSecondaryImpl <em>Physical Type Definition Secondary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PhysicalTypeDefinitionSecondaryImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPhysicalTypeDefinitionSecondary()
	 * @generated
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY = 73;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY__NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY__OF = 2;

	/**
	 * The number of structural features of the '<em>Physical Type Definition Secondary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Physical Type Definition Secondary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_TYPE_DEFINITION_SECONDARY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PortsImpl <em>Ports</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PortsImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPorts()
	 * @generated
	 */
	int PORTS = 74;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTS__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTS__DECLARATION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ports</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTS_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ports</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTS_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PortMapsImpl <em>Port Maps</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PortMapsImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPortMaps()
	 * @generated
	 */
	int PORT_MAPS = 75;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_MAPS__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_MAPS__PORT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port Maps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_MAPS_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Port Maps</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_MAPS_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ProcedureCallStatementImpl <em>Procedure Call Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ProcedureCallStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getProcedureCallStatement()
	 * @generated
	 */
	int PROCEDURE_CALL_STATEMENT = 76;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT__PROCEDURE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Procedure Call Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Procedure Call Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ProcessStatementImpl <em>Process Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ProcessStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getProcessStatement()
	 * @generated
	 */
	int PROCESS_STATEMENT = 77;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Postponed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__POSTPONED = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sensitivity</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__SENSITIVITY = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__DECLARATION = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Process Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Process Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESS_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AllocatorImpl <em>Allocator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AllocatorImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAllocator()
	 * @generated
	 */
	int ALLOCATOR = 78;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATOR__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATOR__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Allocate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATOR__ALLOCATE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Allocator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATOR_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Allocator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOCATOR_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RecordNatureDefinitionImpl <em>Record Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RecordNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRecordNatureDefinition()
	 * @generated
	 */
	int RECORD_NATURE_DEFINITION = 79;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_DEFINITION__ID = COMPOSITE_NATURE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Record</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_DEFINITION__RECORD = COMPOSITE_NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Record Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_DEFINITION_FEATURE_COUNT = COMPOSITE_NATURE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Record Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_DEFINITION_OPERATION_COUNT = COMPOSITE_NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RecordNatureElementImpl <em>Record Nature Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RecordNatureElementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRecordNatureElement()
	 * @generated
	 */
	int RECORD_NATURE_ELEMENT = 80;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_ELEMENT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_ELEMENT__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_ELEMENT__NATURE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Record Nature Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_ELEMENT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Record Nature Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_NATURE_ELEMENT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RecordTypeDefinitionImpl <em>Record Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RecordTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRecordTypeDefinition()
	 * @generated
	 */
	int RECORD_TYPE_DEFINITION = 81;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_DEFINITION__ID = COMPOSITE_TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_DEFINITION__DECLARATION = COMPOSITE_TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Record Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_DEFINITION_FEATURE_COUNT = COMPOSITE_TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Record Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECORD_TYPE_DEFINITION_OPERATION_COUNT = COMPOSITE_TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ReportStatementImpl <em>Report Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ReportStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getReportStatement()
	 * @generated
	 */
	int REPORT_STATEMENT = 82;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Report</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT__REPORT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT__SEVERITY = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Report Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Report Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ReturnStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getReturnStatement()
	 * @generated
	 */
	int RETURN_STATEMENT = 83;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Return</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__RETURN = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Return Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Return Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ScalarNatureDefinitionImpl <em>Scalar Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ScalarNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getScalarNatureDefinition()
	 * @generated
	 */
	int SCALAR_NATURE_DEFINITION = 84;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION__ID = NATURE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION__NAME = NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Across</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION__ACROSS = NATURE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Through</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION__THROUGH = NATURE_DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Scalar Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION_FEATURE_COUNT = NATURE_DEFINITION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Scalar Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALAR_NATURE_DEFINITION_OPERATION_COUNT = NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NullStatementImpl <em>Null Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NullStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNullStatement()
	 * @generated
	 */
	int NULL_STATEMENT = 85;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Null Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Null Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SignalAssignmentStatementImpl <em>Signal Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SignalAssignmentStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSignalAssignmentStatement()
	 * @generated
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT = 86;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT__DELAY = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Waveform</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT__WAVEFORM = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Signal Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_ASSIGNMENT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.VariableAssignmentStatementImpl <em>Variable Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VariableAssignmentStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getVariableAssignmentStatement()
	 * @generated
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT = 87;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT__INITIAL = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Variable Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Variable Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SignalDeclarationImpl <em>Signal Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SignalDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSignalDeclaration()
	 * @generated
	 */
	int SIGNAL_DECLARATION = 88;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__KIND = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__INITIAL = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION__MODE = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Signal Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Signal Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.VariableDeclarationImpl <em>Variable Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VariableDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getVariableDeclaration()
	 * @generated
	 */
	int VARIABLE_DECLARATION = 89;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__SHARED = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__INITIAL = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__MODE = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Variable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Variable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConstantDeclarationImpl <em>Constant Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConstantDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConstantDeclaration()
	 * @generated
	 */
	int CONSTANT_DECLARATION = 90;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION__INITIAL = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constant Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Constant Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SignatureImpl <em>Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SignatureImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSignature()
	 * @generated
	 */
	int SIGNATURE = 91;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Return</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__RETURN = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousAlternativeImpl <em>Simultaneous Alternative</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousAlternativeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimultaneousAlternative()
	 * @generated
	 */
	int SIMULTANEOUS_ALTERNATIVE = 92;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_ALTERNATIVE__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_ALTERNATIVE__CHOICE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statememt</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_ALTERNATIVE__STATEMEMT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simultaneous Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_ALTERNATIVE_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simultaneous Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_ALTERNATIVE_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousCaseStatementImpl <em>Simultaneous Case Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousCaseStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimultaneousCaseStatement()
	 * @generated
	 */
	int SIMULTANEOUS_CASE_STATEMENT = 93;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Case</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT__CASE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT__WHEN = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simultaneous Case Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simultaneous Case Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_CASE_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementImpl <em>Simultaneous If Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimultaneousIfStatement()
	 * @generated
	 */
	int SIMULTANEOUS_IF_STATEMENT = 94;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Test</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT__TEST = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statememt</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT__STATEMEMT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simultaneous If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simultaneous If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementTestImpl <em>Simultaneous If Statement Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousIfStatementTestImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimultaneousIfStatementTest()
	 * @generated
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST = 95;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST__CONDITION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statememt</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST__STATEMEMT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simultaneous If Statement Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simultaneous If Statement Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_IF_STATEMENT_TEST_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousProceduralStatementImpl <em>Simultaneous Procedural Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousProceduralStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSimultaneousProceduralStatement()
	 * @generated
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT = 96;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT__DECLARATION = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT__STATEMENT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simultaneous Procedural Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simultaneous Procedural Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULTANEOUS_PROCEDURAL_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SourceAspectImpl <em>Source Aspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SourceAspectImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSourceAspect()
	 * @generated
	 */
	int SOURCE_ASPECT = 97;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_ASPECT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The number of structural features of the '<em>Source Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_ASPECT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Source Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_ASPECT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SpectrumImpl <em>Spectrum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SpectrumImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSpectrum()
	 * @generated
	 */
	int SPECTRUM = 98;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECTRUM__ID = SOURCE_ASPECT__ID;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECTRUM__LEFT = SOURCE_ASPECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECTRUM__RIGHT = SOURCE_ASPECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Spectrum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECTRUM_FEATURE_COUNT = SOURCE_ASPECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Spectrum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECTRUM_OPERATION_COUNT = SOURCE_ASPECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NoiseImpl <em>Noise</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NoiseImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNoise()
	 * @generated
	 */
	int NOISE = 99;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOISE__ID = SOURCE_ASPECT__ID;

	/**
	 * The feature id for the '<em><b>Noise</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOISE__NOISE = SOURCE_ASPECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Noise</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOISE_FEATURE_COUNT = SOURCE_ASPECT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Noise</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOISE_OPERATION_COUNT = SOURCE_ASPECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.QuantityDeclarationImpl <em>Quantity Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.QuantityDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getQuantityDeclaration()
	 * @generated
	 */
	int QUANTITY_DECLARATION = 100;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The number of structural features of the '<em>Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.FreeQuantityDeclarationImpl <em>Free Quantity Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.FreeQuantityDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getFreeQuantityDeclaration()
	 * @generated
	 */
	int FREE_QUANTITY_DECLARATION = 101;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION__ID = QUANTITY_DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION__NAME = QUANTITY_DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION__TYPE = QUANTITY_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION__QUANTITY = QUANTITY_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Free Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION_FEATURE_COUNT = QUANTITY_DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Free Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_QUANTITY_DECLARATION_OPERATION_COUNT = QUANTITY_DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BranchQuantityDeclarationImpl <em>Branch Quantity Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BranchQuantityDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBranchQuantityDeclaration()
	 * @generated
	 */
	int BRANCH_QUANTITY_DECLARATION = 102;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__ID = QUANTITY_DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__NAME = QUANTITY_DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Across</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__ACROSS = QUANTITY_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Through</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__THROUGH = QUANTITY_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__LEFT = QUANTITY_DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION__RIGHT = QUANTITY_DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Branch Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION_FEATURE_COUNT = QUANTITY_DECLARATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Branch Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_QUANTITY_DECLARATION_OPERATION_COUNT = QUANTITY_DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.QuantityAspectImpl <em>Quantity Aspect</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.QuantityAspectImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getQuantityAspect()
	 * @generated
	 */
	int QUANTITY_ASPECT = 103;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tolerance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT__TOLERANCE = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT__EXPRESSION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Quantity Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Quantity Aspect</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTITY_ASPECT_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SourceQuantityDeclarationImpl <em>Source Quantity Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SourceQuantityDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSourceQuantityDeclaration()
	 * @generated
	 */
	int SOURCE_QUANTITY_DECLARATION = 104;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION__ID = QUANTITY_DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION__NAME = QUANTITY_DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION__TYPE = QUANTITY_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION__SOURCE = QUANTITY_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Source Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION_FEATURE_COUNT = QUANTITY_DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Source Quantity Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_QUANTITY_DECLARATION_OPERATION_COUNT = QUANTITY_DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.LimitSpecificationImpl <em>Limit Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.LimitSpecificationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getLimitSpecification()
	 * @generated
	 */
	int LIMIT_SPECIFICATION = 105;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Limit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION__LIMIT = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION__TYPE = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION__VALUE = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Limit Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Limit Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMIT_SPECIFICATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SubnatureDeclarationImpl <em>Subnature Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SubnatureDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSubnatureDeclaration()
	 * @generated
	 */
	int SUBNATURE_DECLARATION = 106;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBNATURE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBNATURE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBNATURE_DECLARATION__NATURE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Subnature Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBNATURE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Subnature Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBNATURE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SubprogramDeclarationImpl <em>Subprogram Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SubprogramDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSubprogramDeclaration()
	 * @generated
	 */
	int SUBPROGRAM_DECLARATION = 107;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION__SPECIFICATION = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION__DECLARATION = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION__STATEMENT = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Subprogram Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Subprogram Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SubprogramSpecificationImpl <em>Subprogram Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SubprogramSpecificationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSubprogramSpecification()
	 * @generated
	 */
	int SUBPROGRAM_SPECIFICATION = 108;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION__ID = IDENTIFIED_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION__NAME = IDENTIFIED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION__DECLARATION = IDENTIFIED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Purity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION__PURITY = IDENTIFIED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Return</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION__RETURN = IDENTIFIED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Subprogram Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION_FEATURE_COUNT = IDENTIFIED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Subprogram Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBPROGRAM_SPECIFICATION_OPERATION_COUNT = IDENTIFIED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SubtypeDeclarationImpl <em>Subtype Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SubtypeDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSubtypeDeclaration()
	 * @generated
	 */
	int SUBTYPE_DECLARATION = 109;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTYPE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTYPE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTYPE_DECLARATION__TYPE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Subtype Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTYPE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Subtype Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTYPE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AliasDeclarationImpl <em>Alias Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AliasDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAliasDeclaration()
	 * @generated
	 */
	int ALIAS_DECLARATION = 110;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Alias</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION__ALIAS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION__IS = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION__SIGNATURE = DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Alias Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Alias Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALIAS_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.TerminalDeclarationImpl <em>Terminal Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.TerminalDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getTerminalDeclaration()
	 * @generated
	 */
	int TERMINAL_DECLARATION = 111;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERMINAL_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERMINAL_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERMINAL_DECLARATION__NATURE = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Terminal Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERMINAL_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Terminal Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERMINAL_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.TypeDeclarationImpl <em>Type Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.TypeDeclarationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getTypeDeclaration()
	 * @generated
	 */
	int TYPE_DECLARATION = 112;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECLARATION__ID = DECLARATION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECLARATION__NAME = DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECLARATION__IS = DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECLARATION_FEATURE_COUNT = DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Type Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_DECLARATION_OPERATION_COUNT = DECLARATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RangeTypeDefinitionImpl <em>Range Type Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RangeTypeDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRangeTypeDefinition()
	 * @generated
	 */
	int RANGE_TYPE_DEFINITION = 114;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION__ID = TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION__LEFT = TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION__DIRECTION = TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION__RIGHT = TYPE_DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Range Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION_FEATURE_COUNT = TYPE_DEFINITION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Range Type Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_TYPE_DEFINITION_OPERATION_COUNT = TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.UnconstrainedArrayDefinitionImpl <em>Unconstrained Array Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.UnconstrainedArrayDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUnconstrainedArrayDefinition()
	 * @generated
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION = 115;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION__ID = ARRAY_TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION__TYPE = ARRAY_TYPE_DEFINITION__TYPE;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION__INDEX = ARRAY_TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unconstrained Array Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION_FEATURE_COUNT = ARRAY_TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Unconstrained Array Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_ARRAY_DEFINITION_OPERATION_COUNT = ARRAY_TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.UnconstrainedNatureDefinitionImpl <em>Unconstrained Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.UnconstrainedNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUnconstrainedNatureDefinition()
	 * @generated
	 */
	int UNCONSTRAINED_NATURE_DEFINITION = 116;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_NATURE_DEFINITION__ID = ARRAY_NATURE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_NATURE_DEFINITION__NATURE = ARRAY_NATURE_DEFINITION__NATURE;

	/**
	 * The feature id for the '<em><b>Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_NATURE_DEFINITION__INDEX = ARRAY_NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unconstrained Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_NATURE_DEFINITION_FEATURE_COUNT = ARRAY_NATURE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Unconstrained Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONSTRAINED_NATURE_DEFINITION_OPERATION_COUNT = ARRAY_NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConstrainedArrayDefinitionImpl <em>Constrained Array Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConstrainedArrayDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConstrainedArrayDefinition()
	 * @generated
	 */
	int CONSTRAINED_ARRAY_DEFINITION = 117;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_ARRAY_DEFINITION__ID = ARRAY_TYPE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_ARRAY_DEFINITION__TYPE = ARRAY_TYPE_DEFINITION__TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_ARRAY_DEFINITION__CONSTRAINT = ARRAY_TYPE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constrained Array Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_ARRAY_DEFINITION_FEATURE_COUNT = ARRAY_TYPE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constrained Array Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_ARRAY_DEFINITION_OPERATION_COUNT = ARRAY_TYPE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ConstrainedNatureDefinitionImpl <em>Constrained Nature Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ConstrainedNatureDefinitionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getConstrainedNatureDefinition()
	 * @generated
	 */
	int CONSTRAINED_NATURE_DEFINITION = 118;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_NATURE_DEFINITION__ID = ARRAY_NATURE_DEFINITION__ID;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_NATURE_DEFINITION__NATURE = ARRAY_NATURE_DEFINITION__NATURE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_NATURE_DEFINITION__CONSTRAINT = ARRAY_NATURE_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constrained Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_NATURE_DEFINITION_FEATURE_COUNT = ARRAY_NATURE_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constrained Nature Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINED_NATURE_DEFINITION_OPERATION_COUNT = ARRAY_NATURE_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.WaitStatementImpl <em>Wait Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.WaitStatementImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getWaitStatement()
	 * @generated
	 */
	int WAIT_STATEMENT = 119;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT__ID = STATEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT__NAME = STATEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Sensitivity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT__SENSITIVITY = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Until</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT__UNTIL = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Time</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT__TIME = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Wait Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Wait Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_STATEMENT_OPERATION_COUNT = STATEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NullImpl <em>Null</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NullImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getNull()
	 * @generated
	 */
	int NULL = 120;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL__NAME = EXPRESSION__NAME;

	/**
	 * The number of structural features of the '<em>Null</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Null</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ValueImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 121;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__NUMBER = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AggregateImpl <em>Aggregate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AggregateImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAggregate()
	 * @generated
	 */
	int AGGREGATE = 122;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The feature id for the '<em><b>Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE__ELEMENT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aggregate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aggregate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATE_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AssociationImpl <em>Association</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AssociationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAssociation()
	 * @generated
	 */
	int ASSOCIATION = 124;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AllImpl <em>All</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AllImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAll()
	 * @generated
	 */
	int ALL = 123;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>All</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>All</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Choice</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__CHOICE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.OthersImpl <em>Others</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.OthersImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getOthers()
	 * @generated
	 */
	int OTHERS = 125;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHERS__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHERS__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHERS__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Others</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHERS_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Others</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OTHERS_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.LogicalExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getLogicalExpression()
	 * @generated
	 */
	int LOGICAL_EXPRESSION = 126;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Logical Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Logical Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RelationalExpressionImpl <em>Relational Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RelationalExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRelationalExpression()
	 * @generated
	 */
	int RELATIONAL_EXPRESSION = 127;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Relational Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Relational Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.ShiftExpressionImpl <em>Shift Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.ShiftExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getShiftExpression()
	 * @generated
	 */
	int SHIFT_EXPRESSION = 128;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Shift Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Shift Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.AddingExpressionImpl <em>Adding Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.AddingExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAddingExpression()
	 * @generated
	 */
	int ADDING_EXPRESSION = 129;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Adding Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Adding Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDING_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.MultiplyingExpressionImpl <em>Multiplying Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.MultiplyingExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getMultiplyingExpression()
	 * @generated
	 */
	int MULTIPLYING_EXPRESSION = 130;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Multiplying Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Multiplying Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLYING_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.PowerExpressionImpl <em>Power Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.PowerExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getPowerExpression()
	 * @generated
	 */
	int POWER_EXPRESSION = 131;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Power Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Power Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.UnaryExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 132;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.SignExpressionImpl <em>Sign Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.SignExpressionImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSignExpression()
	 * @generated
	 */
	int SIGN_EXPRESSION = 133;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Sign</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION__SIGN = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sign Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Sign Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGN_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.RangeImpl <em>Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.RangeImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRange()
	 * @generated
	 */
	int RANGE = 134;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__LEFT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__DIRECTION = NAME_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE__RIGHT = NAME_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANGE_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.NameImpl <em>Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.NameImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getName_()
	 * @generated
	 */
	int NAME = 135;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__ELEMENT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.StringImpl <em>String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.StringImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getString()
	 * @generated
	 */
	int STRING = 137;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.CharacterImpl <em>Character</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.CharacterImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getCharacter()
	 * @generated
	 */
	int CHARACTER = 138;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IndicationImpl <em>Indication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IndicationImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIndication()
	 * @generated
	 */
	int INDICATION = 140;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.WaveformImpl <em>Waveform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.WaveformImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getWaveform()
	 * @generated
	 */
	int WAVEFORM = 141;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.BitStringImpl <em>Bit String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.BitStringImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getBitString()
	 * @generated
	 */
	int BIT_STRING = 136;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_STRING__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_STRING__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_STRING__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Bit String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_STRING_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bit String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_STRING_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Character</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Character</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.IdentifierImpl <em>Identifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.IdentifierImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getIdentifier()
	 * @generated
	 */
	int IDENTIFIER = 139;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__ID = NAME_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__NAME = NAME_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER__SIGNATURE = NAME_ELEMENT__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_FEATURE_COUNT = NAME_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIER_OPERATION_COUNT = NAME_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Mark</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__MARK = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__CONSTRAINT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tolerance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__TOLERANCE = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Across</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION__ACROSS = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Indication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Indication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDICATION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM__NAME = EXPRESSION__NAME;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>After</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM__AFTER = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAVEFORM_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.impl.UnitValueImpl <em>Unit Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.UnitValueImpl
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUnitValue()
	 * @generated
	 */
	int UNIT_VALUE = 142;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE__ID = VALUE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE__NAME = VALUE__NAME;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE__NUMBER = VALUE__NUMBER;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE__UNIT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unit Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Unit Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.SignalKind <em>Signal Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalKind
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSignalKind()
	 * @generated
	 */
	int SIGNAL_KIND = 145;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.RangeDirection <em>Range Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeDirection
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRangeDirection()
	 * @generated
	 */
	int RANGE_DIRECTION = 146;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.Mode <em>Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.Mode
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getMode()
	 * @generated
	 */
	int MODE = 147;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryOperator <em>Unary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 148;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingOperator <em>Multiplying Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getMultiplyingOperator()
	 * @generated
	 */
	int MULTIPLYING_OPERATOR = 149;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftOperator <em>Shift Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getShiftOperator()
	 * @generated
	 */
	int SHIFT_OPERATOR = 150;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalOperator <em>Relational Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getRelationalOperator()
	 * @generated
	 */
	int RELATIONAL_OPERATOR = 151;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalOperator <em>Logical Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getLogicalOperator()
	 * @generated
	 */
	int LOGICAL_OPERATOR = 152;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.AddingOperator <em>Adding Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingOperator
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getAddingOperator()
	 * @generated
	 */
	int ADDING_OPERATOR = 153;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.Sign <em>Sign</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.Sign
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getSign()
	 * @generated
	 */
	int SIGN = 154;

	/**
	 * The meta object id for the '{@link fr.labsticc.language.vhdl.model.vhdl.EntityClass <em>Entity Class</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityClass
	 * @see fr.labsticc.language.vhdl.model.vhdl.impl.VhdlPackageImpl#getEntityClass()
	 * @generated
	 */
	int ENTITY_CLASS = 155;


	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.VhdlFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlFile
	 * @generated
	 */
	EClass getVhdlFile();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.VhdlFile#getDesign <em>Design</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Design</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlFile#getDesign()
	 * @see #getVhdlFile()
	 * @generated
	 */
	EReference getVhdlFile_Design();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit <em>Design Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Unit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DesignUnit
	 * @generated
	 */
	EClass getDesignUnit();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getLibrary <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Library</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getLibrary()
	 * @see #getDesignUnit()
	 * @generated
	 */
	EAttribute getDesignUnit_Library();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Use</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUse()
	 * @see #getDesignUnit()
	 * @generated
	 */
	EReference getDesignUnit_Use();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DesignUnit#getUnit()
	 * @see #getDesignUnit()
	 * @generated
	 */
	EReference getDesignUnit_Unit();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.UseClause <em>Use Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Clause</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UseClause
	 * @generated
	 */
	EClass getUseClause();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.UseClause#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Use</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UseClause#getUse()
	 * @see #getUseClause()
	 * @generated
	 */
	EReference getUseClause_Use();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.LibraryUnit <em>Library Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library Unit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LibraryUnit
	 * @generated
	 */
	EClass getLibraryUnit();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.LibraryUnit#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LibraryUnit#getDeclaration()
	 * @see #getLibraryUnit()
	 * @generated
	 */
	EReference getLibraryUnit_Declaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition <em>Access Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition
	 * @generated
	 */
	EClass getAccessTypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition#getType()
	 * @see #getAccessTypeDefinition()
	 * @generated
	 */
	EReference getAccessTypeDefinition_Type();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NameElement <em>Name Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameElement
	 * @generated
	 */
	EClass getNameElement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.NameElement#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameElement#getSignature()
	 * @see #getNameElement()
	 * @generated
	 */
	EReference getNameElement_Signature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Declaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Declaration
	 * @generated
	 */
	EClass getDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Architecture <em>Architecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Architecture</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Architecture
	 * @generated
	 */
	EClass getArchitecture();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Architecture#getOf <em>Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Of</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Architecture#getOf()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Of();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Architecture#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Architecture#getStatement()
	 * @see #getArchitecture()
	 * @generated
	 */
	EReference getArchitecture_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Statement
	 * @generated
	 */
	EClass getStatement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition <em>Array Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition
	 * @generated
	 */
	EClass getArrayNatureDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayNatureDefinition#getNature()
	 * @see #getArrayNatureDefinition()
	 * @generated
	 */
	EReference getArrayNatureDefinition_Nature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition <em>Array Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition
	 * @generated
	 */
	EClass getArrayTypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ArrayTypeDefinition#getType()
	 * @see #getArrayTypeDefinition()
	 * @generated
	 */
	EReference getArrayTypeDefinition_Type();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement <em>Assertion Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement
	 * @generated
	 */
	EClass getAssertionStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getCondition()
	 * @see #getAssertionStatement()
	 * @generated
	 */
	EReference getAssertionStatement_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getReport <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Report</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getReport()
	 * @see #getAssertionStatement()
	 * @generated
	 */
	EReference getAssertionStatement_Report();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Severity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#getSeverity()
	 * @see #getAssertionStatement()
	 * @generated
	 */
	EReference getAssertionStatement_Severity();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AssertionStatement#isPostponed()
	 * @see #getAssertionStatement()
	 * @generated
	 */
	EAttribute getAssertionStatement_Postponed();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Choice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choice</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Choice
	 * @generated
	 */
	EClass getChoice();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Open <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Open</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Open
	 * @generated
	 */
	EClass getOpen();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration <em>Attribute Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration
	 * @generated
	 */
	EClass getAttributeDeclaration();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration#getType()
	 * @see #getAttributeDeclaration()
	 * @generated
	 */
	EAttribute getAttributeDeclaration_Type();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification <em>Attribute Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification
	 * @generated
	 */
	EClass getAttributeSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getEntity()
	 * @see #getAttributeSpecification()
	 * @generated
	 */
	EReference getAttributeSpecification_Entity();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getClass_()
	 * @see #getAttributeSpecification()
	 * @generated
	 */
	EAttribute getAttributeSpecification_Class();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getIs()
	 * @see #getAttributeSpecification()
	 * @generated
	 */
	EReference getAttributeSpecification_Is();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NameList <em>Name List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name List</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameList
	 * @generated
	 */
	EClass getNameList();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.NameList#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NameList#getName()
	 * @see #getNameList()
	 * @generated
	 */
	EReference getNameList_Name();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration <em>Block Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Configuration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration
	 * @generated
	 */
	EClass getBlockConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getName()
	 * @see #getBlockConfiguration()
	 * @generated
	 */
	EReference getBlockConfiguration_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Use</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getUse()
	 * @see #getBlockConfiguration()
	 * @generated
	 */
	EReference getBlockConfiguration_Use();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Item</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration#getItem()
	 * @see #getBlockConfiguration()
	 * @generated
	 */
	EReference getBlockConfiguration_Item();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement <em>Block Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement
	 * @generated
	 */
	EClass getBlockStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGuard()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_Guard();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGeneric()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_Generic();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGenericMap <em>Generic Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGenericMap()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_GenericMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPort()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_Port();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPortMap <em>Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPortMap()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_PortMap();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getDeclaration()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getStatement()
	 * @see #getBlockStatement()
	 * @generated
	 */
	EReference getBlockStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement <em>Break Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Break Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakElement
	 * @generated
	 */
	EClass getBreakElement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakElement#getName()
	 * @see #getBreakElement()
	 * @generated
	 */
	EReference getBreakElement_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getUse <em>Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Use</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakElement#getUse()
	 * @see #getBreakElement()
	 * @generated
	 */
	EReference getBreakElement_Use();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getArrow <em>Arrow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Arrow</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakElement#getArrow()
	 * @see #getBreakElement()
	 * @generated
	 */
	EReference getBreakElement_Arrow();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BreakStatement <em>Break Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Break Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakStatement
	 * @generated
	 */
	EClass getBreakStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getBreak <em>Break</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Break</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getBreak()
	 * @see #getBreakStatement()
	 * @generated
	 */
	EReference getBreakStatement_Break();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getWhen()
	 * @see #getBreakStatement()
	 * @generated
	 */
	EReference getBreakStatement_When();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getSensitivity <em>Sensitivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sensitivity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BreakStatement#getSensitivity()
	 * @see #getBreakStatement()
	 * @generated
	 */
	EReference getBreakStatement_Sensitivity();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.CaseStatement <em>Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseStatement
	 * @generated
	 */
	EClass getCaseStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.CaseStatement#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Case</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseStatement#getCase()
	 * @see #getCaseStatement()
	 * @generated
	 */
	EReference getCaseStatement_Case();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.CaseStatement#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>When</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseStatement#getWhen()
	 * @see #getCaseStatement()
	 * @generated
	 */
	EReference getCaseStatement_When();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.CaseAlternative <em>Case Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Case Alternative</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseAlternative
	 * @generated
	 */
	EClass getCaseAlternative();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.CaseAlternative#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Choice</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseAlternative#getChoice()
	 * @see #getCaseAlternative()
	 * @generated
	 */
	EReference getCaseAlternative_Choice();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.CaseAlternative#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CaseAlternative#getStatement()
	 * @see #getCaseAlternative()
	 * @generated
	 */
	EReference getCaseAlternative_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration <em>Component Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Configuration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration
	 * @generated
	 */
	EClass getComponentConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getList <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>List</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getList()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_List();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Component</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getComponent()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_Component();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getEntity()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_Entity();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getConfiguration()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getGenericMap <em>Generic Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getGenericMap()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_GenericMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getPortMap <em>Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getPortMap()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_PortMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration#getBlock()
	 * @see #getComponentConfiguration()
	 * @generated
	 */
	EReference getComponentConfiguration_Block();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration <em>Component Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration
	 * @generated
	 */
	EClass getComponentDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getGeneric()
	 * @see #getComponentDeclaration()
	 * @generated
	 */
	EReference getComponentDeclaration_Generic();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration#getPort()
	 * @see #getComponentDeclaration()
	 * @generated
	 */
	EReference getComponentDeclaration_Port();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement <em>Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instantiation Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement
	 * @generated
	 */
	EClass getInstantiationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement#getGenericMap <em>Generic Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement#getGenericMap()
	 * @see #getInstantiationStatement()
	 * @generated
	 */
	EReference getInstantiationStatement_GenericMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement#getPortMap <em>Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.InstantiationStatement#getPortMap()
	 * @see #getInstantiationStatement()
	 * @generated
	 */
	EReference getInstantiationStatement_PortMap();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement <em>Component Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instantiation Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement
	 * @generated
	 */
	EClass getComponentInstantiationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Component</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement#getComponent()
	 * @see #getComponentInstantiationStatement()
	 * @generated
	 */
	EReference getComponentInstantiationStatement_Component();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement <em>Entity Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Instantiation Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement
	 * @generated
	 */
	EClass getEntityInstantiationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement#getEntity()
	 * @see #getEntityInstantiationStatement()
	 * @generated
	 */
	EReference getEntityInstantiationStatement_Entity();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement <em>Configuration Instantiation Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration Instantiation Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement
	 * @generated
	 */
	EClass getConfigurationInstantiationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getConfiguration()
	 * @see #getConfigurationInstantiationStatement()
	 * @generated
	 */
	EReference getConfigurationInstantiationStatement_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getGenericMap <em>Generic Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getGenericMap()
	 * @see #getConfigurationInstantiationStatement()
	 * @generated
	 */
	EReference getConfigurationInstantiationStatement_GenericMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getPortMap <em>Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement#getPortMap()
	 * @see #getConfigurationInstantiationStatement()
	 * @generated
	 */
	EReference getConfigurationInstantiationStatement_PortMap();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition <em>Composite Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CompositeNatureDefinition
	 * @generated
	 */
	EClass getCompositeNatureDefinition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition <em>Composite Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.CompositeTypeDefinition
	 * @generated
	 */
	EClass getCompositeTypeDefinition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement <em>Sequential Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Signal Assignment Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement
	 * @generated
	 */
	EClass getSequentialSignalAssignmentStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#isPostponed()
	 * @see #getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getSequentialSignalAssignmentStatement_Postponed();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getTarget()
	 * @see #getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSequentialSignalAssignmentStatement_Target();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#isGuarded <em>Guarded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guarded</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#isGuarded()
	 * @see #getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getSequentialSignalAssignmentStatement_Guarded();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getDelay()
	 * @see #getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSequentialSignalAssignmentStatement_Delay();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getWaveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement#getWaveform()
	 * @see #getSequentialSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSequentialSignalAssignmentStatement_Waveform();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement <em>Conditional Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Signal Assignment Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement
	 * @generated
	 */
	EClass getConditionalSignalAssignmentStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isPostponed()
	 * @see #getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getConditionalSignalAssignmentStatement_Postponed();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getTarget()
	 * @see #getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	EReference getConditionalSignalAssignmentStatement_Target();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isGuarded <em>Guarded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guarded</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#isGuarded()
	 * @see #getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getConditionalSignalAssignmentStatement_Guarded();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getDelay()
	 * @see #getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	EReference getConditionalSignalAssignmentStatement_Delay();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getWaveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement#getWaveform()
	 * @see #getConditionalSignalAssignmentStatement()
	 * @generated
	 */
	EReference getConditionalSignalAssignmentStatement_Waveform();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement <em>Selected Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Selected Signal Assignment Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement
	 * @generated
	 */
	EClass getSelectedSignalAssignmentStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#isPostponed()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getSelectedSignalAssignmentStatement_Postponed();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getExpression()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSelectedSignalAssignmentStatement_Expression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getTarget()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSelectedSignalAssignmentStatement_Target();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#isGuarded <em>Guarded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guarded</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#isGuarded()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EAttribute getSelectedSignalAssignmentStatement_Guarded();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getDelay()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSelectedSignalAssignmentStatement_Delay();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getWaveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement#getWaveform()
	 * @see #getSelectedSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSelectedSignalAssignmentStatement_Waveform();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement <em>Simple Simultaneous Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Simultaneous Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement
	 * @generated
	 */
	EClass getSimpleSimultaneousStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getLeft()
	 * @see #getSimpleSimultaneousStatement()
	 * @generated
	 */
	EReference getSimpleSimultaneousStatement_Left();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getRight()
	 * @see #getSimpleSimultaneousStatement()
	 * @generated
	 */
	EReference getSimpleSimultaneousStatement_Right();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getTolerance <em>Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tolerance</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement#getTolerance()
	 * @see #getSimpleSimultaneousStatement()
	 * @generated
	 */
	EReference getSimpleSimultaneousStatement_Tolerance();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform <em>Conditional Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform
	 * @generated
	 */
	EClass getConditionalWaveform();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform#getWaveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform#getWaveform()
	 * @see #getConditionalWaveform()
	 * @generated
	 */
	EReference getConditionalWaveform_Waveform();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Choice</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform#getChoice()
	 * @see #getConditionalWaveform()
	 * @generated
	 */
	EReference getConditionalWaveform_Choice();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration <em>Configuration Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration
	 * @generated
	 */
	EClass getConfigurationDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getOf <em>Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Of</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getOf()
	 * @see #getConfigurationDeclaration()
	 * @generated
	 */
	EReference getConfigurationDeclaration_Of();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Block</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getBlock()
	 * @see #getConfigurationDeclaration()
	 * @generated
	 */
	EReference getConfigurationDeclaration_Block();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem <em>Configuration Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration Item</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationItem
	 * @generated
	 */
	EClass getConfigurationItem();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification <em>Configuration Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification
	 * @generated
	 */
	EClass getConfigurationSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getList <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>List</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getList()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_List();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Component</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getComponent()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_Component();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getEntity()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_Entity();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getConfiguration()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getGenericMap <em>Generic Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getGenericMap()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_GenericMap();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getPortMap <em>Port Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Map</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification#getPortMap()
	 * @see #getConfigurationSpecification()
	 * @generated
	 */
	EReference getConfigurationSpecification_PortMap();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.DelayMechanism <em>Delay Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delay Mechanism</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DelayMechanism
	 * @generated
	 */
	EClass getDelayMechanism();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RejectMechanism <em>Reject Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reject Mechanism</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RejectMechanism
	 * @generated
	 */
	EClass getRejectMechanism();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RejectMechanism#getReject <em>Reject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reject</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RejectMechanism#getReject()
	 * @see #getRejectMechanism()
	 * @generated
	 */
	EReference getRejectMechanism_Reject();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Transport <em>Transport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transport</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Transport
	 * @generated
	 */
	EClass getTransport();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification <em>Disconnection Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disconnection Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification
	 * @generated
	 */
	EClass getDisconnectionSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getDisconnect <em>Disconnect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disconnect</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getDisconnect()
	 * @see #getDisconnectionSpecification()
	 * @generated
	 */
	EReference getDisconnectionSpecification_Disconnect();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getType()
	 * @see #getDisconnectionSpecification()
	 * @generated
	 */
	EReference getDisconnectionSpecification_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getAfter <em>After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>After</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification#getAfter()
	 * @see #getDisconnectionSpecification()
	 * @generated
	 */
	EReference getDisconnectionSpecification_After();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration <em>Element Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration
	 * @generated
	 */
	EClass getElementDeclaration();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Identifier</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getIdentifier()
	 * @see #getElementDeclaration()
	 * @generated
	 */
	EAttribute getElementDeclaration_Identifier();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration#getType()
	 * @see #getElementDeclaration()
	 * @generated
	 */
	EReference getElementDeclaration_Type();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration <em>Entity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration
	 * @generated
	 */
	EClass getEntityDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Generic</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getGeneric()
	 * @see #getEntityDeclaration()
	 * @generated
	 */
	EReference getEntityDeclaration_Generic();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getPort()
	 * @see #getEntityDeclaration()
	 * @generated
	 */
	EReference getEntityDeclaration_Port();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration#getStatement()
	 * @see #getEntityDeclaration()
	 * @generated
	 */
	EReference getEntityDeclaration_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition <em>Enumeration Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition
	 * @generated
	 */
	EClass getEnumerationTypeDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literal</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition#getLiteral()
	 * @see #getEnumerationTypeDefinition()
	 * @generated
	 */
	EReference getEnumerationTypeDefinition_Literal();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ExitStatement <em>Exit Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exit Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ExitStatement
	 * @generated
	 */
	EClass getExitStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ExitStatement#getExit <em>Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ExitStatement#getExit()
	 * @see #getExitStatement()
	 * @generated
	 */
	EAttribute getExitStatement_Exit();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ExitStatement#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ExitStatement#getWhen()
	 * @see #getExitStatement()
	 * @generated
	 */
	EReference getExitStatement_When();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration <em>File Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileDeclaration
	 * @generated
	 */
	EClass getFileDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getType()
	 * @see #getFileDeclaration()
	 * @generated
	 */
	EReference getFileDeclaration_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getOpen <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Open</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getOpen()
	 * @see #getFileDeclaration()
	 * @generated
	 */
	EReference getFileDeclaration_Open();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getIs()
	 * @see #getFileDeclaration()
	 * @generated
	 */
	EReference getFileDeclaration_Is();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition <em>File Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition
	 * @generated
	 */
	EClass getFileTypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition#getFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>File</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition#getFile()
	 * @see #getFileTypeDefinition()
	 * @generated
	 */
	EReference getFileTypeDefinition_File();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.GenerateStatement <em>Generate Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generate Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerateStatement
	 * @generated
	 */
	EClass getGenerateStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getScheme <em>Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getScheme()
	 * @see #getGenerateStatement()
	 * @generated
	 */
	EReference getGenerateStatement_Scheme();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getDeclaration()
	 * @see #getGenerateStatement()
	 * @generated
	 */
	EReference getGenerateStatement_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerateStatement#getStatement()
	 * @see #getGenerateStatement()
	 * @generated
	 */
	EReference getGenerateStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.GenerationScheme <em>Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generation Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenerationScheme
	 * @generated
	 */
	EClass getGenerationScheme();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme <em>For Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Generation Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme
	 * @generated
	 */
	EClass getForGenerationScheme();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme#getVariable()
	 * @see #getForGenerationScheme()
	 * @generated
	 */
	EAttribute getForGenerationScheme_Variable();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme#getIn()
	 * @see #getForGenerationScheme()
	 * @generated
	 */
	EReference getForGenerationScheme_In();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme <em>If Generation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Generation Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme
	 * @generated
	 */
	EClass getIfGenerationScheme();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme#getCondition()
	 * @see #getIfGenerationScheme()
	 * @generated
	 */
	EReference getIfGenerationScheme_Condition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Generics <em>Generics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generics</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Generics
	 * @generated
	 */
	EClass getGenerics();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Generics#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Generics#getDeclaration()
	 * @see #getGenerics()
	 * @generated
	 */
	EReference getGenerics_Declaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.GenericMaps <em>Generic Maps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Maps</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenericMaps
	 * @generated
	 */
	EClass getGenericMaps();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.GenericMaps#getGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Generic</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GenericMaps#getGeneric()
	 * @see #getGenericMaps()
	 * @generated
	 */
	EReference getGenericMaps_Generic();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration <em>Group Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration
	 * @generated
	 */
	EClass getGroupDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration#getIs()
	 * @see #getGroupDeclaration()
	 * @generated
	 */
	EReference getGroupDeclaration_Is();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration#getMember <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Member</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration#getMember()
	 * @see #getGroupDeclaration()
	 * @generated
	 */
	EReference getGroupDeclaration_Member();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration <em>Group Template Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group Template Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration
	 * @generated
	 */
	EClass getGroupTemplateDeclaration();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Entry</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration#getEntry()
	 * @see #getGroupTemplateDeclaration()
	 * @generated
	 */
	EAttribute getGroupTemplateDeclaration_Entry();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatement
	 * @generated
	 */
	EClass getIfStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatement#getTest <em>Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatement#getTest()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_Test();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatement#getStatement()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatementTest <em>If Statement Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Statement Test</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatementTest
	 * @generated
	 */
	EClass getIfStatementTest();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatementTest#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatementTest#getCondition()
	 * @see #getIfStatementTest()
	 * @generated
	 */
	EReference getIfStatementTest_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.IfStatementTest#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IfStatementTest#getStatement()
	 * @see #getIfStatementTest()
	 * @generated
	 */
	EReference getIfStatementTest_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Constraint#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Range</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Constraint#getRange()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_Range();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.DiscreteRange <em>Discrete Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discrete Range</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.DiscreteRange
	 * @generated
	 */
	EClass getDiscreteRange();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.LoopStatement <em>Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LoopStatement
	 * @generated
	 */
	EClass getLoopStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LoopStatement#getIteration <em>Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iteration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LoopStatement#getIteration()
	 * @see #getLoopStatement()
	 * @generated
	 */
	EReference getLoopStatement_Iteration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.LoopStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LoopStatement#getStatement()
	 * @see #getLoopStatement()
	 * @generated
	 */
	EReference getLoopStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.IterationScheme <em>Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iteration Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IterationScheme
	 * @generated
	 */
	EClass getIterationScheme();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme <em>While Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Iteration Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme
	 * @generated
	 */
	EClass getWhileIterationScheme();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme#getCondition()
	 * @see #getWhileIterationScheme()
	 * @generated
	 */
	EReference getWhileIterationScheme_Condition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme <em>For Iteration Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Iteration Scheme</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme
	 * @generated
	 */
	EClass getForIterationScheme();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getVariable()
	 * @see #getForIterationScheme()
	 * @generated
	 */
	EAttribute getForIterationScheme_Variable();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme#getIn()
	 * @see #getForIterationScheme()
	 * @generated
	 */
	EReference getForIterationScheme_In();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration <em>Nature Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nature Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration
	 * @generated
	 */
	EClass getNatureDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration#getIs()
	 * @see #getNatureDeclaration()
	 * @generated
	 */
	EReference getNatureDeclaration_Is();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NatureDefinition <em>Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NatureDefinition
	 * @generated
	 */
	EClass getNatureDefinition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NextStatement <em>Next Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Next Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NextStatement
	 * @generated
	 */
	EClass getNextStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.NextStatement#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Next</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NextStatement#getNext()
	 * @see #getNextStatement()
	 * @generated
	 */
	EAttribute getNextStatement_Next();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.NextStatement#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NextStatement#getWhen()
	 * @see #getNextStatement()
	 * @generated
	 */
	EReference getNextStatement_When();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PackageBody <em>Package Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package Body</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PackageBody
	 * @generated
	 */
	EClass getPackageBody();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration <em>Package Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration
	 * @generated
	 */
	EClass getPackageDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition <em>Physical Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition
	 * @generated
	 */
	EClass getPhysicalTypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Range</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getRange()
	 * @see #getPhysicalTypeDefinition()
	 * @generated
	 */
	EReference getPhysicalTypeDefinition_Range();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getPrimary <em>Primary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Primary</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getPrimary()
	 * @see #getPhysicalTypeDefinition()
	 * @generated
	 */
	EAttribute getPhysicalTypeDefinition_Primary();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getSecondary <em>Secondary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Secondary</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition#getSecondary()
	 * @see #getPhysicalTypeDefinition()
	 * @generated
	 */
	EReference getPhysicalTypeDefinition_Secondary();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary <em>Physical Type Definition Secondary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Type Definition Secondary</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary
	 * @generated
	 */
	EClass getPhysicalTypeDefinitionSecondary();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getName()
	 * @see #getPhysicalTypeDefinitionSecondary()
	 * @generated
	 */
	EAttribute getPhysicalTypeDefinitionSecondary_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getNumber()
	 * @see #getPhysicalTypeDefinitionSecondary()
	 * @generated
	 */
	EAttribute getPhysicalTypeDefinitionSecondary_Number();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getOf <em>Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Of</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary#getOf()
	 * @see #getPhysicalTypeDefinitionSecondary()
	 * @generated
	 */
	EReference getPhysicalTypeDefinitionSecondary_Of();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Ports <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ports</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Ports
	 * @generated
	 */
	EClass getPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Ports#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Ports#getDeclaration()
	 * @see #getPorts()
	 * @generated
	 */
	EReference getPorts_Declaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PortMaps <em>Port Maps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Maps</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PortMaps
	 * @generated
	 */
	EClass getPortMaps();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.PortMaps#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PortMaps#getPort()
	 * @see #getPortMaps()
	 * @generated
	 */
	EReference getPortMaps_Port();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement <em>Procedure Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Call Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement
	 * @generated
	 */
	EClass getProcedureCallStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Procedure</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement#getProcedure()
	 * @see #getProcedureCallStatement()
	 * @generated
	 */
	EReference getProcedureCallStatement_Procedure();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement#isPostponed()
	 * @see #getProcedureCallStatement()
	 * @generated
	 */
	EAttribute getProcedureCallStatement_Postponed();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement <em>Process Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement
	 * @generated
	 */
	EClass getProcessStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#isPostponed <em>Postponed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postponed</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#isPostponed()
	 * @see #getProcessStatement()
	 * @generated
	 */
	EAttribute getProcessStatement_Postponed();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getSensitivity <em>Sensitivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Sensitivity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getSensitivity()
	 * @see #getProcessStatement()
	 * @generated
	 */
	EAttribute getProcessStatement_Sensitivity();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getDeclaration()
	 * @see #getProcessStatement()
	 * @generated
	 */
	EReference getProcessStatement_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ProcessStatement#getStatement()
	 * @see #getProcessStatement()
	 * @generated
	 */
	EReference getProcessStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Allocator <em>Allocator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Allocator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Allocator
	 * @generated
	 */
	EClass getAllocator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Allocator#getAllocate <em>Allocate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Allocate</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Allocator#getAllocate()
	 * @see #getAllocator()
	 * @generated
	 */
	EReference getAllocator_Allocate();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition <em>Record Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition
	 * @generated
	 */
	EClass getRecordNatureDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition#getRecord <em>Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Record</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition#getRecord()
	 * @see #getRecordNatureDefinition()
	 * @generated
	 */
	EReference getRecordNatureDefinition_Record();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement <em>Record Nature Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Nature Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement
	 * @generated
	 */
	EClass getRecordNatureElement();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getName()
	 * @see #getRecordNatureElement()
	 * @generated
	 */
	EAttribute getRecordNatureElement_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement#getNature()
	 * @see #getRecordNatureElement()
	 * @generated
	 */
	EReference getRecordNatureElement_Nature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition <em>Record Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Record Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition
	 * @generated
	 */
	EClass getRecordTypeDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition#getDeclaration()
	 * @see #getRecordTypeDefinition()
	 * @generated
	 */
	EReference getRecordTypeDefinition_Declaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ReportStatement <em>Report Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Report Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReportStatement
	 * @generated
	 */
	EClass getReportStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ReportStatement#getReport <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Report</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReportStatement#getReport()
	 * @see #getReportStatement()
	 * @generated
	 */
	EReference getReportStatement_Report();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ReportStatement#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Severity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReportStatement#getSeverity()
	 * @see #getReportStatement()
	 * @generated
	 */
	EReference getReportStatement_Severity();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReturnStatement
	 * @generated
	 */
	EClass getReturnStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ReturnStatement#getReturn <em>Return</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ReturnStatement#getReturn()
	 * @see #getReturnStatement()
	 * @generated
	 */
	EReference getReturnStatement_Return();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition <em>Scalar Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scalar Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition
	 * @generated
	 */
	EClass getScalarNatureDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getName()
	 * @see #getScalarNatureDefinition()
	 * @generated
	 */
	EReference getScalarNatureDefinition_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getAcross <em>Across</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Across</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getAcross()
	 * @see #getScalarNatureDefinition()
	 * @generated
	 */
	EReference getScalarNatureDefinition_Across();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getThrough <em>Through</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Through</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition#getThrough()
	 * @see #getScalarNatureDefinition()
	 * @generated
	 */
	EReference getScalarNatureDefinition_Through();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NullStatement <em>Null Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NullStatement
	 * @generated
	 */
	EClass getNullStatement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement <em>Signal Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Assignment Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement
	 * @generated
	 */
	EClass getSignalAssignmentStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getTarget()
	 * @see #getSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSignalAssignmentStatement_Target();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getDelay()
	 * @see #getSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSignalAssignmentStatement_Delay();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getWaveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement#getWaveform()
	 * @see #getSignalAssignmentStatement()
	 * @generated
	 */
	EReference getSignalAssignmentStatement_Waveform();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement <em>Variable Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Assignment Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement
	 * @generated
	 */
	EClass getVariableAssignmentStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement#getTarget()
	 * @see #getVariableAssignmentStatement()
	 * @generated
	 */
	EReference getVariableAssignmentStatement_Target();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement#getInitial()
	 * @see #getVariableAssignmentStatement()
	 * @generated
	 */
	EReference getVariableAssignmentStatement_Initial();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration <em>Signal Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration
	 * @generated
	 */
	EClass getSignalDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getType()
	 * @see #getSignalDeclaration()
	 * @generated
	 */
	EReference getSignalDeclaration_Type();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getKind()
	 * @see #getSignalDeclaration()
	 * @generated
	 */
	EAttribute getSignalDeclaration_Kind();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getInitial()
	 * @see #getSignalDeclaration()
	 * @generated
	 */
	EReference getSignalDeclaration_Initial();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getMode()
	 * @see #getSignalDeclaration()
	 * @generated
	 */
	EAttribute getSignalDeclaration_Mode();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration <em>Variable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration
	 * @generated
	 */
	EClass getVariableDeclaration();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#isShared <em>Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shared</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#isShared()
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	EAttribute getVariableDeclaration_Shared();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getType()
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	EReference getVariableDeclaration_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getInitial()
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	EReference getVariableDeclaration_Initial();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration#getMode()
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	EAttribute getVariableDeclaration_Mode();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration <em>Constant Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration
	 * @generated
	 */
	EClass getConstantDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration#getType()
	 * @see #getConstantDeclaration()
	 * @generated
	 */
	EReference getConstantDeclaration_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration#getInitial()
	 * @see #getConstantDeclaration()
	 * @generated
	 */
	EReference getConstantDeclaration_Initial();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Signature
	 * @generated
	 */
	EClass getSignature();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Signature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Signature#getName()
	 * @see #getSignature()
	 * @generated
	 */
	EReference getSignature_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Signature#getReturn <em>Return</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Signature#getReturn()
	 * @see #getSignature()
	 * @generated
	 */
	EReference getSignature_Return();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative <em>Simultaneous Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simultaneous Alternative</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative
	 * @generated
	 */
	EClass getSimultaneousAlternative();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Choice</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative#getChoice()
	 * @see #getSimultaneousAlternative()
	 * @generated
	 */
	EReference getSimultaneousAlternative_Choice();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative#getStatememt <em>Statememt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statememt</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative#getStatememt()
	 * @see #getSimultaneousAlternative()
	 * @generated
	 */
	EReference getSimultaneousAlternative_Statememt();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement <em>Simultaneous Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simultaneous Case Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement
	 * @generated
	 */
	EClass getSimultaneousCaseStatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Case</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getCase()
	 * @see #getSimultaneousCaseStatement()
	 * @generated
	 */
	EReference getSimultaneousCaseStatement_Case();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>When</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement#getWhen()
	 * @see #getSimultaneousCaseStatement()
	 * @generated
	 */
	EReference getSimultaneousCaseStatement_When();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement <em>Simultaneous If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simultaneous If Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement
	 * @generated
	 */
	EClass getSimultaneousIfStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement#getTest <em>Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Test</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement#getTest()
	 * @see #getSimultaneousIfStatement()
	 * @generated
	 */
	EReference getSimultaneousIfStatement_Test();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement#getStatememt <em>Statememt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statememt</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement#getStatememt()
	 * @see #getSimultaneousIfStatement()
	 * @generated
	 */
	EReference getSimultaneousIfStatement_Statememt();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest <em>Simultaneous If Statement Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simultaneous If Statement Test</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest
	 * @generated
	 */
	EClass getSimultaneousIfStatementTest();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest#getCondition()
	 * @see #getSimultaneousIfStatementTest()
	 * @generated
	 */
	EReference getSimultaneousIfStatementTest_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest#getStatememt <em>Statememt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statememt</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest#getStatememt()
	 * @see #getSimultaneousIfStatementTest()
	 * @generated
	 */
	EReference getSimultaneousIfStatementTest_Statememt();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement <em>Simultaneous Procedural Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simultaneous Procedural Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement
	 * @generated
	 */
	EClass getSimultaneousProceduralStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement#getDeclaration()
	 * @see #getSimultaneousProceduralStatement()
	 * @generated
	 */
	EReference getSimultaneousProceduralStatement_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement#getStatement()
	 * @see #getSimultaneousProceduralStatement()
	 * @generated
	 */
	EReference getSimultaneousProceduralStatement_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SourceAspect <em>Source Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Aspect</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceAspect
	 * @generated
	 */
	EClass getSourceAspect();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Spectrum <em>Spectrum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Spectrum</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Spectrum
	 * @generated
	 */
	EClass getSpectrum();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Spectrum#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Spectrum#getLeft()
	 * @see #getSpectrum()
	 * @generated
	 */
	EReference getSpectrum_Left();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Spectrum#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Spectrum#getRight()
	 * @see #getSpectrum()
	 * @generated
	 */
	EReference getSpectrum_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Noise <em>Noise</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Noise</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Noise
	 * @generated
	 */
	EClass getNoise();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Noise#getNoise <em>Noise</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Noise</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Noise#getNoise()
	 * @see #getNoise()
	 * @generated
	 */
	EReference getNoise_Noise();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration <em>Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantity Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityDeclaration
	 * @generated
	 */
	EClass getQuantityDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration <em>Free Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Quantity Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration
	 * @generated
	 */
	EClass getFreeQuantityDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getType()
	 * @see #getFreeQuantityDeclaration()
	 * @generated
	 */
	EReference getFreeQuantityDeclaration_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getQuantity <em>Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Quantity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration#getQuantity()
	 * @see #getFreeQuantityDeclaration()
	 * @generated
	 */
	EReference getFreeQuantityDeclaration_Quantity();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration <em>Branch Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Branch Quantity Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration
	 * @generated
	 */
	EClass getBranchQuantityDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getAcross <em>Across</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Across</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getAcross()
	 * @see #getBranchQuantityDeclaration()
	 * @generated
	 */
	EReference getBranchQuantityDeclaration_Across();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getThrough <em>Through</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Through</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getThrough()
	 * @see #getBranchQuantityDeclaration()
	 * @generated
	 */
	EReference getBranchQuantityDeclaration_Through();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getLeft()
	 * @see #getBranchQuantityDeclaration()
	 * @generated
	 */
	EReference getBranchQuantityDeclaration_Left();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration#getRight()
	 * @see #getBranchQuantityDeclaration()
	 * @generated
	 */
	EReference getBranchQuantityDeclaration_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityAspect <em>Quantity Aspect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantity Aspect</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityAspect
	 * @generated
	 */
	EClass getQuantityAspect();

	/**
	 * Returns the meta object for the attribute list '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getName()
	 * @see #getQuantityAspect()
	 * @generated
	 */
	EAttribute getQuantityAspect_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getTolerance <em>Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tolerance</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getTolerance()
	 * @see #getQuantityAspect()
	 * @generated
	 */
	EReference getQuantityAspect_Tolerance();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.QuantityAspect#getExpression()
	 * @see #getQuantityAspect()
	 * @generated
	 */
	EReference getQuantityAspect_Expression();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration <em>Source Quantity Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Quantity Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration
	 * @generated
	 */
	EClass getSourceQuantityDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration#getType()
	 * @see #getSourceQuantityDeclaration()
	 * @generated
	 */
	EReference getSourceQuantityDeclaration_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration#getSource()
	 * @see #getSourceQuantityDeclaration()
	 * @generated
	 */
	EReference getSourceQuantityDeclaration_Source();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.LimitSpecification <em>Limit Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Limit Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LimitSpecification
	 * @generated
	 */
	EClass getLimitSpecification();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getLimit <em>Limit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Limit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getLimit()
	 * @see #getLimitSpecification()
	 * @generated
	 */
	EReference getLimitSpecification_Limit();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getType()
	 * @see #getLimitSpecification()
	 * @generated
	 */
	EReference getLimitSpecification_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LimitSpecification#getValue()
	 * @see #getLimitSpecification()
	 * @generated
	 */
	EReference getLimitSpecification_Value();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration <em>Subnature Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subnature Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration
	 * @generated
	 */
	EClass getSubnatureDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration#getNature()
	 * @see #getSubnatureDeclaration()
	 * @generated
	 */
	EReference getSubnatureDeclaration_Nature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration <em>Subprogram Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subprogram Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration
	 * @generated
	 */
	EClass getSubprogramDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getSpecification()
	 * @see #getSubprogramDeclaration()
	 * @generated
	 */
	EReference getSubprogramDeclaration_Specification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getDeclaration()
	 * @see #getSubprogramDeclaration()
	 * @generated
	 */
	EReference getSubprogramDeclaration_Declaration();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration#getStatement()
	 * @see #getSubprogramDeclaration()
	 * @generated
	 */
	EReference getSubprogramDeclaration_Statement();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification <em>Subprogram Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subprogram Specification</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification
	 * @generated
	 */
	EClass getSubprogramSpecification();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getName()
	 * @see #getSubprogramSpecification()
	 * @generated
	 */
	EAttribute getSubprogramSpecification_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getDeclaration()
	 * @see #getSubprogramSpecification()
	 * @generated
	 */
	EReference getSubprogramSpecification_Declaration();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getPurity <em>Purity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Purity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getPurity()
	 * @see #getSubprogramSpecification()
	 * @generated
	 */
	EAttribute getSubprogramSpecification_Purity();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getReturn <em>Return</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification#getReturn()
	 * @see #getSubprogramSpecification()
	 * @generated
	 */
	EReference getSubprogramSpecification_Return();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration <em>Subtype Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subtype Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration
	 * @generated
	 */
	EClass getSubtypeDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration#getType()
	 * @see #getSubtypeDeclaration()
	 * @generated
	 */
	EReference getSubtypeDeclaration_Type();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration <em>Alias Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alias Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration
	 * @generated
	 */
	EClass getAliasDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getAlias <em>Alias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alias</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getAlias()
	 * @see #getAliasDeclaration()
	 * @generated
	 */
	EReference getAliasDeclaration_Alias();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getIs()
	 * @see #getAliasDeclaration()
	 * @generated
	 */
	EReference getAliasDeclaration_Is();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getSignature()
	 * @see #getAliasDeclaration()
	 * @generated
	 */
	EReference getAliasDeclaration_Signature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration <em>Terminal Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Terminal Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration
	 * @generated
	 */
	EClass getTerminalDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nature</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration#getNature()
	 * @see #getTerminalDeclaration()
	 * @generated
	 */
	EReference getTerminalDeclaration_Nature();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration <em>Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Declaration</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration
	 * @generated
	 */
	EClass getTypeDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration#getIs <em>Is</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Is</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration#getIs()
	 * @see #getTypeDeclaration()
	 * @generated
	 */
	EReference getTypeDeclaration_Is();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.TypeDefinition <em>Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.TypeDefinition
	 * @generated
	 */
	EClass getTypeDefinition();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition <em>Range Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range Type Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition
	 * @generated
	 */
	EClass getRangeTypeDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getLeft()
	 * @see #getRangeTypeDefinition()
	 * @generated
	 */
	EReference getRangeTypeDefinition_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getDirection()
	 * @see #getRangeTypeDefinition()
	 * @generated
	 */
	EAttribute getRangeTypeDefinition_Direction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition#getRight()
	 * @see #getRangeTypeDefinition()
	 * @generated
	 */
	EReference getRangeTypeDefinition_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unconstrained Array Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition
	 * @generated
	 */
	EClass getUnconstrainedArrayDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition#getIndex()
	 * @see #getUnconstrainedArrayDefinition()
	 * @generated
	 */
	EReference getUnconstrainedArrayDefinition_Index();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition <em>Unconstrained Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unconstrained Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition
	 * @generated
	 */
	EClass getUnconstrainedNatureDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Index</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition#getIndex()
	 * @see #getUnconstrainedNatureDefinition()
	 * @generated
	 */
	EReference getUnconstrainedNatureDefinition_Index();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition <em>Constrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constrained Array Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition
	 * @generated
	 */
	EClass getConstrainedArrayDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraint</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition#getConstraint()
	 * @see #getConstrainedArrayDefinition()
	 * @generated
	 */
	EReference getConstrainedArrayDefinition_Constraint();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition <em>Constrained Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constrained Nature Definition</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition
	 * @generated
	 */
	EClass getConstrainedNatureDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraint</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition#getConstraint()
	 * @see #getConstrainedNatureDefinition()
	 * @generated
	 */
	EReference getConstrainedNatureDefinition_Constraint();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement <em>Wait Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait Statement</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WaitStatement
	 * @generated
	 */
	EClass getWaitStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getSensitivity <em>Sensitivity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sensitivity</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getSensitivity()
	 * @see #getWaitStatement()
	 * @generated
	 */
	EReference getWaitStatement_Sensitivity();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getUntil <em>Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Until</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getUntil()
	 * @see #getWaitStatement()
	 * @generated
	 */
	EReference getWaitStatement_Until();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Time</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.WaitStatement#getTime()
	 * @see #getWaitStatement()
	 * @generated
	 */
	EReference getWaitStatement_Time();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Null <em>Null</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Null
	 * @generated
	 */
	EClass getNull();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.Value#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Value#getNumber()
	 * @see #getValue()
	 * @generated
	 */
	EAttribute getValue_Number();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Aggregate <em>Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aggregate</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Aggregate
	 * @generated
	 */
	EClass getAggregate();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Aggregate#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Aggregate#getElement()
	 * @see #getAggregate()
	 * @generated
	 */
	EReference getAggregate_Element();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Association <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Association
	 * @generated
	 */
	EClass getAssociation();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Association#getChoice <em>Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Choice</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Association#getChoice()
	 * @see #getAssociation()
	 * @generated
	 */
	EReference getAssociation_Choice();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Association#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Association#getExpression()
	 * @see #getAssociation()
	 * @generated
	 */
	EReference getAssociation_Expression();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.All <em>All</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>All</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.All
	 * @generated
	 */
	EClass getAll();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Others <em>Others</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Others</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Others
	 * @generated
	 */
	EClass getOthers();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalExpression <em>Logical Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalExpression
	 * @generated
	 */
	EClass getLogicalExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getLeft()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EReference getLogicalExpression_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getOperator()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EAttribute getLogicalExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalExpression#getRight()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EReference getLogicalExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalExpression <em>Relational Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relational Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalExpression
	 * @generated
	 */
	EClass getRelationalExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getLeft()
	 * @see #getRelationalExpression()
	 * @generated
	 */
	EReference getRelationalExpression_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getOperator()
	 * @see #getRelationalExpression()
	 * @generated
	 */
	EAttribute getRelationalExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalExpression#getRight()
	 * @see #getRelationalExpression()
	 * @generated
	 */
	EReference getRelationalExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression <em>Shift Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shift Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftExpression
	 * @generated
	 */
	EClass getShiftExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getLeft()
	 * @see #getShiftExpression()
	 * @generated
	 */
	EReference getShiftExpression_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getOperator()
	 * @see #getShiftExpression()
	 * @generated
	 */
	EAttribute getShiftExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getRight()
	 * @see #getShiftExpression()
	 * @generated
	 */
	EReference getShiftExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.AddingExpression <em>Adding Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adding Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingExpression
	 * @generated
	 */
	EClass getAddingExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getLeft()
	 * @see #getAddingExpression()
	 * @generated
	 */
	EReference getAddingExpression_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getOperator()
	 * @see #getAddingExpression()
	 * @generated
	 */
	EAttribute getAddingExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingExpression#getRight()
	 * @see #getAddingExpression()
	 * @generated
	 */
	EReference getAddingExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression <em>Multiplying Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplying Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression
	 * @generated
	 */
	EClass getMultiplyingExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getLeft()
	 * @see #getMultiplyingExpression()
	 * @generated
	 */
	EReference getMultiplyingExpression_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getOperator()
	 * @see #getMultiplyingExpression()
	 * @generated
	 */
	EAttribute getMultiplyingExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression#getRight()
	 * @see #getMultiplyingExpression()
	 * @generated
	 */
	EReference getMultiplyingExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.PowerExpression <em>Power Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Power Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PowerExpression
	 * @generated
	 */
	EClass getPowerExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.PowerExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PowerExpression#getLeft()
	 * @see #getPowerExpression()
	 * @generated
	 */
	EReference getPowerExpression_Left();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.PowerExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.PowerExpression#getRight()
	 * @see #getPowerExpression()
	 * @generated
	 */
	EReference getPowerExpression_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryExpression#getOperator()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EAttribute getUnaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryExpression#getExpression()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Expression();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.SignExpression <em>Sign Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sign Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignExpression
	 * @generated
	 */
	EClass getSignExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.SignExpression#getSign <em>Sign</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sign</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignExpression#getSign()
	 * @see #getSignExpression()
	 * @generated
	 */
	EAttribute getSignExpression_Sign();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.SignExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignExpression#getExpression()
	 * @see #getSignExpression()
	 * @generated
	 */
	EReference getSignExpression_Expression();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Range <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Range</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Range
	 * @generated
	 */
	EClass getRange();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Range#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Range#getLeft()
	 * @see #getRange()
	 * @generated
	 */
	EReference getRange_Left();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.Range#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Range#getDirection()
	 * @see #getRange()
	 * @generated
	 */
	EAttribute getRange_Direction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Range#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Range#getRight()
	 * @see #getRange()
	 * @generated
	 */
	EReference getRange_Right();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Name
	 * @generated
	 */
	EClass getName_();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Name#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Name#getElement()
	 * @see #getName_()
	 * @generated
	 */
	EReference getName_Element();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.String
	 * @generated
	 */
	EClass getString();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Character <em>Character</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Character</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Character
	 * @generated
	 */
	EClass getCharacter();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifier</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Identifier
	 * @generated
	 */
	EClass getIdentifier();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Indication <em>Indication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Indication</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication
	 * @generated
	 */
	EClass getIndication();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.labsticc.language.vhdl.model.vhdl.Indication#getMark <em>Mark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mark</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication#getMark()
	 * @see #getIndication()
	 * @generated
	 */
	EReference getIndication_Mark();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Indication#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraint</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication#getConstraint()
	 * @see #getIndication()
	 * @generated
	 */
	EReference getIndication_Constraint();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Indication#getTolerance <em>Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tolerance</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication#getTolerance()
	 * @see #getIndication()
	 * @generated
	 */
	EReference getIndication_Tolerance();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Indication#getAcross <em>Across</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Across</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Indication#getAcross()
	 * @see #getIndication()
	 * @generated
	 */
	EReference getIndication_Across();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.Waveform <em>Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Waveform</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Waveform
	 * @generated
	 */
	EClass getWaveform();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Waveform#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Waveform#getExpression()
	 * @see #getWaveform()
	 * @generated
	 */
	EReference getWaveform_Expression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.Waveform#getAfter <em>After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>After</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Waveform#getAfter()
	 * @see #getWaveform()
	 * @generated
	 */
	EReference getWaveform_After();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.BitString <em>Bit String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bit String</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.BitString
	 * @generated
	 */
	EClass getBitString();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.UnitValue <em>Unit Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unit Value</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnitValue
	 * @generated
	 */
	EClass getUnitValue();

	/**
	 * Returns the meta object for the containment reference '{@link fr.labsticc.language.vhdl.model.vhdl.UnitValue#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Unit</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnitValue#getUnit()
	 * @see #getUnitValue()
	 * @generated
	 */
	EReference getUnitValue_Unit();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement <em>Identified Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identified Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement
	 * @generated
	 */
	EClass getIdentifiedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.IdentifiedElement#getId()
	 * @see #getIdentifiedElement()
	 * @generated
	 */
	EAttribute getIdentifiedElement_Id();

	/**
	 * Returns the meta object for class '{@link fr.labsticc.language.vhdl.model.vhdl.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.labsticc.language.vhdl.model.vhdl.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.SignalKind <em>Signal Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signal Kind</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalKind
	 * @generated
	 */
	EEnum getSignalKind();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.RangeDirection <em>Range Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Range Direction</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RangeDirection
	 * @generated
	 */
	EEnum getRangeDirection();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.Mode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mode</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Mode
	 * @generated
	 */
	EEnum getMode();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.UnaryOperator
	 * @generated
	 */
	EEnum getUnaryOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.MultiplyingOperator <em>Multiplying Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplying Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.MultiplyingOperator
	 * @generated
	 */
	EEnum getMultiplyingOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftOperator <em>Shift Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Shift Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftOperator
	 * @generated
	 */
	EEnum getShiftOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.RelationalOperator <em>Relational Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Relational Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.RelationalOperator
	 * @generated
	 */
	EEnum getRelationalOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.LogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.LogicalOperator
	 * @generated
	 */
	EEnum getLogicalOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.AddingOperator <em>Adding Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Adding Operator</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.AddingOperator
	 * @generated
	 */
	EEnum getAddingOperator();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.Sign <em>Sign</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Sign</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Sign
	 * @generated
	 */
	EEnum getSign();

	/**
	 * Returns the meta object for enum '{@link fr.labsticc.language.vhdl.model.vhdl.EntityClass <em>Entity Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Entity Class</em>'.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityClass
	 * @generated
	 */
	EEnum getEntityClass();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VhdlFactory getVhdlFactory();

} //VhdlPackage
