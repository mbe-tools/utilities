/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Nature Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getCompositeNatureDefinition()
 * @model
 * @generated
 */
public interface CompositeNatureDefinition extends NatureDefinition {
} // CompositeNatureDefinition
