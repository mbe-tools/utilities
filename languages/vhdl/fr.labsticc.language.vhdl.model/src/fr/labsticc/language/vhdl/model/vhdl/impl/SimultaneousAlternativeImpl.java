/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Choice;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative;
import fr.labsticc.language.vhdl.model.vhdl.Statement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simultaneous Alternative</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousAlternativeImpl#getChoice <em>Choice</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.SimultaneousAlternativeImpl#getStatememt <em>Statememt</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimultaneousAlternativeImpl extends IdentifiedElementImpl implements SimultaneousAlternative {
	/**
	 * The cached value of the '{@link #getChoice() <em>Choice</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoice()
	 * @generated
	 * @ordered
	 */
	protected EList<Choice> choice;

	/**
	 * The cached value of the '{@link #getStatememt() <em>Statememt</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatememt()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statememt;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimultaneousAlternativeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getSimultaneousAlternative();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Choice> getChoice() {
		if (choice == null) {
			choice = new EObjectContainmentEList<Choice>(Choice.class, this, VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE);
		}
		return choice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Statement> getStatememt() {
		if (statememt == null) {
			statememt = new EObjectContainmentEList<Statement>(Statement.class, this, VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT);
		}
		return statememt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE:
				return ((InternalEList<?>)getChoice()).basicRemove(otherEnd, msgs);
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT:
				return ((InternalEList<?>)getStatememt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE:
				return getChoice();
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT:
				return getStatememt();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE:
				getChoice().clear();
				getChoice().addAll((Collection<? extends Choice>)newValue);
				return;
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT:
				getStatememt().clear();
				getStatememt().addAll((Collection<? extends Statement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE:
				getChoice().clear();
				return;
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT:
				getStatememt().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__CHOICE:
				return choice != null && !choice.isEmpty();
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE__STATEMEMT:
				return statememt != null && !statememt.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SimultaneousAlternativeImpl
