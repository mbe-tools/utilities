/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getKind <em>Kind</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getInitial <em>Initial</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getMode <em>Mode</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSignalDeclaration()
 * @model
 * @generated
 */
public interface SignalDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSignalDeclaration_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.language.vhdl.model.vhdl.SignalKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalKind
	 * @see #setKind(SignalKind)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSignalDeclaration_Kind()
	 * @model
	 * @generated
	 */
	SignalKind getKind();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.SignalKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(SignalKind value);

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' containment reference.
	 * @see #setInitial(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSignalDeclaration_Initial()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInitial();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getInitial <em>Initial</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' containment reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(Expression value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.language.vhdl.model.vhdl.Mode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Mode
	 * @see #setMode(Mode)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getSignalDeclaration_Mode()
	 * @model
	 * @generated
	 */
	Mode getMode();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.Mode
	 * @see #getMode()
	 * @generated
	 */
	void setMode(Mode value);

} // SignalDeclaration
