/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constrained Array Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition#getConstraint <em>Constraint</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConstrainedArrayDefinition()
 * @model
 * @generated
 */
public interface ConstrainedArrayDefinition extends ArrayTypeDefinition {
	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' containment reference.
	 * @see #setConstraint(Constraint)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConstrainedArrayDefinition_Constraint()
	 * @model containment="true"
	 * @generated
	 */
	Constraint getConstraint();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition#getConstraint <em>Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint</em>' containment reference.
	 * @see #getConstraint()
	 * @generated
	 */
	void setConstraint(Constraint value);

} // ConstrainedArrayDefinition
