/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getType <em>Type</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getOpen <em>Open</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getIs <em>Is</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileDeclaration()
 * @model
 * @generated
 */
public interface FileDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileDeclaration_Type()
	 * @model containment="true"
	 * @generated
	 */
	Expression getType();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Expression value);

	/**
	 * Returns the value of the '<em><b>Open</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Open</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Open</em>' containment reference.
	 * @see #setOpen(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileDeclaration_Open()
	 * @model containment="true"
	 * @generated
	 */
	Expression getOpen();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getOpen <em>Open</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Open</em>' containment reference.
	 * @see #getOpen()
	 * @generated
	 */
	void setOpen(Expression value);

	/**
	 * Returns the value of the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is</em>' containment reference.
	 * @see #setIs(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getFileDeclaration_Is()
	 * @model containment="true"
	 * @generated
	 */
	Expression getIs();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.FileDeclaration#getIs <em>Is</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is</em>' containment reference.
	 * @see #getIs()
	 * @generated
	 */
	void setIs(Expression value);

} // FileDeclaration
