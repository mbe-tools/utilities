/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getTypeDefinition()
 * @model
 * @generated
 */
public interface TypeDefinition extends IdentifiedElement {
} // TypeDefinition
