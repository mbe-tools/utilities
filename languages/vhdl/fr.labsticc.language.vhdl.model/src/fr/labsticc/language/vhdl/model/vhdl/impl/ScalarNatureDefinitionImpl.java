/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.Expression;
import fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scalar Nature Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ScalarNatureDefinitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ScalarNatureDefinitionImpl#getAcross <em>Across</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.impl.ScalarNatureDefinitionImpl#getThrough <em>Through</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScalarNatureDefinitionImpl extends NatureDefinitionImpl implements ScalarNatureDefinition {
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected Expression name;

	/**
	 * The cached value of the '{@link #getAcross() <em>Across</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcross()
	 * @generated
	 * @ordered
	 */
	protected Expression across;

	/**
	 * The cached value of the '{@link #getThrough() <em>Through</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThrough()
	 * @generated
	 * @ordered
	 */
	protected Expression through;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScalarNatureDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VhdlPackage.eINSTANCE.getScalarNatureDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetName(Expression newName, NotificationChain msgs) {
		Expression oldName = name;
		name = newName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__NAME, oldName, newName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(Expression newName) {
		if (newName != name) {
			NotificationChain msgs = null;
			if (name != null)
				msgs = ((InternalEObject)name).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__NAME, null, msgs);
			if (newName != null)
				msgs = ((InternalEObject)newName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__NAME, null, msgs);
			msgs = basicSetName(newName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__NAME, newName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getAcross() {
		return across;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcross(Expression newAcross, NotificationChain msgs) {
		Expression oldAcross = across;
		across = newAcross;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS, oldAcross, newAcross);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcross(Expression newAcross) {
		if (newAcross != across) {
			NotificationChain msgs = null;
			if (across != null)
				msgs = ((InternalEObject)across).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS, null, msgs);
			if (newAcross != null)
				msgs = ((InternalEObject)newAcross).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS, null, msgs);
			msgs = basicSetAcross(newAcross, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS, newAcross, newAcross));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getThrough() {
		return through;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThrough(Expression newThrough, NotificationChain msgs) {
		Expression oldThrough = through;
		through = newThrough;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH, oldThrough, newThrough);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThrough(Expression newThrough) {
		if (newThrough != through) {
			NotificationChain msgs = null;
			if (through != null)
				msgs = ((InternalEObject)through).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH, null, msgs);
			if (newThrough != null)
				msgs = ((InternalEObject)newThrough).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH, null, msgs);
			msgs = basicSetThrough(newThrough, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH, newThrough, newThrough));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VhdlPackage.SCALAR_NATURE_DEFINITION__NAME:
				return basicSetName(null, msgs);
			case VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS:
				return basicSetAcross(null, msgs);
			case VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH:
				return basicSetThrough(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VhdlPackage.SCALAR_NATURE_DEFINITION__NAME:
				return getName();
			case VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS:
				return getAcross();
			case VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH:
				return getThrough();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VhdlPackage.SCALAR_NATURE_DEFINITION__NAME:
				setName((Expression)newValue);
				return;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS:
				setAcross((Expression)newValue);
				return;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH:
				setThrough((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VhdlPackage.SCALAR_NATURE_DEFINITION__NAME:
				setName((Expression)null);
				return;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS:
				setAcross((Expression)null);
				return;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH:
				setThrough((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VhdlPackage.SCALAR_NATURE_DEFINITION__NAME:
				return name != null;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__ACROSS:
				return across != null;
			case VhdlPackage.SCALAR_NATURE_DEFINITION__THROUGH:
				return through != null;
		}
		return super.eIsSet(featureID);
	}

} //ScalarNatureDefinitionImpl
