/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Terminal Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration#getNature <em>Nature</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getTerminalDeclaration()
 * @model
 * @generated
 */
public interface TerminalDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature</em>' containment reference.
	 * @see #setNature(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getTerminalDeclaration_Nature()
	 * @model containment="true"
	 * @generated
	 */
	Expression getNature();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration#getNature <em>Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' containment reference.
	 * @see #getNature()
	 * @generated
	 */
	void setNature(Expression value);

} // TerminalDeclaration
