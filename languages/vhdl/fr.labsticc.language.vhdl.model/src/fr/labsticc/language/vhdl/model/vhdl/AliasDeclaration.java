/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alias Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getAlias <em>Alias</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getIs <em>Is</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getSignature <em>Signature</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAliasDeclaration()
 * @model
 * @generated
 */
public interface AliasDeclaration extends Declaration {
	/**
	 * Returns the value of the '<em><b>Alias</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alias</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alias</em>' containment reference.
	 * @see #setAlias(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAliasDeclaration_Alias()
	 * @model containment="true"
	 * @generated
	 */
	Expression getAlias();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getAlias <em>Alias</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alias</em>' containment reference.
	 * @see #getAlias()
	 * @generated
	 */
	void setAlias(Expression value);

	/**
	 * Returns the value of the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is</em>' containment reference.
	 * @see #setIs(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAliasDeclaration_Is()
	 * @model containment="true"
	 * @generated
	 */
	Expression getIs();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getIs <em>Is</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is</em>' containment reference.
	 * @see #getIs()
	 * @generated
	 */
	void setIs(Expression value);

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' containment reference.
	 * @see #setSignature(Signature)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAliasDeclaration_Signature()
	 * @model containment="true"
	 * @generated
	 */
	Signature getSignature();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration#getSignature <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' containment reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(Signature value);

} // AliasDeclaration
