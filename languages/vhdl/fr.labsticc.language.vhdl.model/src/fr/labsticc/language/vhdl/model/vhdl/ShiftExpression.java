/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Shift Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getLeft <em>Left</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getShiftExpression()
 * @model
 * @generated
 */
public interface ShiftExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getShiftExpression_Left()
	 * @model containment="true"
	 * @generated
	 */
	Expression getLeft();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Expression value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.language.vhdl.model.vhdl.ShiftOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftOperator
	 * @see #setOperator(ShiftOperator)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getShiftExpression_Operator()
	 * @model
	 * @generated
	 */
	ShiftOperator getOperator();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.ShiftOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ShiftOperator value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getShiftExpression_Right()
	 * @model containment="true"
	 * @generated
	 */
	Expression getRight();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ShiftExpression#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Expression value);

} // ShiftExpression
