/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Instantiation Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement#getEntity <em>Entity</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityInstantiationStatement()
 * @model
 * @generated
 */
public interface EntityInstantiationStatement extends InstantiationStatement {
	/**
	 * Returns the value of the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' containment reference.
	 * @see #setEntity(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getEntityInstantiationStatement_Entity()
	 * @model containment="true"
	 * @generated
	 */
	Expression getEntity();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement#getEntity <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' containment reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(Expression value);

} // EntityInstantiationStatement
