/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Break Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getName <em>Name</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getUse <em>Use</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getArrow <em>Arrow</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBreakElement()
 * @model
 * @generated
 */
public interface BreakElement extends IdentifiedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference.
	 * @see #setName(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBreakElement_Name()
	 * @model containment="true"
	 * @generated
	 */
	Expression getName();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getName <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' containment reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(Expression value);

	/**
	 * Returns the value of the '<em><b>Use</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use</em>' containment reference.
	 * @see #setUse(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBreakElement_Use()
	 * @model containment="true"
	 * @generated
	 */
	Expression getUse();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getUse <em>Use</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use</em>' containment reference.
	 * @see #getUse()
	 * @generated
	 */
	void setUse(Expression value);

	/**
	 * Returns the value of the '<em><b>Arrow</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arrow</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arrow</em>' containment reference.
	 * @see #setArrow(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBreakElement_Arrow()
	 * @model containment="true"
	 * @generated
	 */
	Expression getArrow();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BreakElement#getArrow <em>Arrow</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Arrow</em>' containment reference.
	 * @see #getArrow()
	 * @generated
	 */
	void setArrow(Expression value);

} // BreakElement
