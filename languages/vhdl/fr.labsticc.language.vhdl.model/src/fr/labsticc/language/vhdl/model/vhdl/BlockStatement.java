/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGuard <em>Guard</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGeneric <em>Generic</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGenericMap <em>Generic Map</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPort <em>Port</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPortMap <em>Port Map</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getDeclaration <em>Declaration</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getStatement <em>Statement</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement()
 * @model
 * @generated
 */
public interface BlockStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guard</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' containment reference.
	 * @see #setGuard(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_Guard()
	 * @model containment="true"
	 * @generated
	 */
	Expression getGuard();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGuard <em>Guard</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' containment reference.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(Expression value);

	/**
	 * Returns the value of the '<em><b>Generic</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic</em>' containment reference.
	 * @see #setGeneric(Generics)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_Generic()
	 * @model containment="true"
	 * @generated
	 */
	Generics getGeneric();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGeneric <em>Generic</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic</em>' containment reference.
	 * @see #getGeneric()
	 * @generated
	 */
	void setGeneric(Generics value);

	/**
	 * Returns the value of the '<em><b>Generic Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Map</em>' containment reference.
	 * @see #setGenericMap(GenericMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_GenericMap()
	 * @model containment="true"
	 * @generated
	 */
	GenericMaps getGenericMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getGenericMap <em>Generic Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Map</em>' containment reference.
	 * @see #getGenericMap()
	 * @generated
	 */
	void setGenericMap(GenericMaps value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference.
	 * @see #setPort(Ports)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_Port()
	 * @model containment="true"
	 * @generated
	 */
	Ports getPort();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPort <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' containment reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Ports value);

	/**
	 * Returns the value of the '<em><b>Port Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Map</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Map</em>' containment reference.
	 * @see #setPortMap(PortMaps)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_PortMap()
	 * @model containment="true"
	 * @generated
	 */
	PortMaps getPortMap();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.BlockStatement#getPortMap <em>Port Map</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Map</em>' containment reference.
	 * @see #getPortMap()
	 * @generated
	 */
	void setPortMap(PortMaps value);

	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Declaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_Declaration()
	 * @model containment="true"
	 * @generated
	 */
	EList<Declaration> getDeclaration();

	/**
	 * Returns the value of the '<em><b>Statement</b></em>' containment reference list.
	 * The list contents are of type {@link fr.labsticc.language.vhdl.model.vhdl.Statement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement</em>' containment reference list.
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getBlockStatement_Statement()
	 * @model containment="true"
	 * @generated
	 */
	EList<Statement> getStatement();

} // BlockStatement
