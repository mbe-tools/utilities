/**
 */
package fr.labsticc.language.vhdl.model.vhdl.impl;

import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VhdlPackageImpl extends EPackageImpl implements VhdlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "vhdl.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vhdlFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass libraryUnitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass declarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass architectureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arrayTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertionStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass openEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass breakElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass breakStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseAlternativeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instantiationStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstantiationStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityInstantiationStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationInstantiationStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialSignalAssignmentStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionalSignalAssignmentStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selectedSignalAssignmentStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleSimultaneousStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionalWaveformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delayMechanismEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rejectMechanismEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass disconnectionSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exitStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generateStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forGenerationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifGenerationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericMapsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groupTemplateDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifStatementTestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass discreteRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loopStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iterationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whileIterationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forIterationSchemeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass natureDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass natureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nextStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageBodyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalTypeDefinitionSecondaryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portMapsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureCallStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass allocatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordNatureElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recordTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reportStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass returnStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalarNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nullStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalAssignmentStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableAssignmentStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simultaneousAlternativeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simultaneousCaseStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simultaneousIfStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simultaneousIfStatementTestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simultaneousProceduralStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceAspectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass spectrumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass noiseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantityDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass freeQuantityDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass branchQuantityDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantityAspectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceQuantityDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass limitSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subnatureDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subprogramDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subprogramSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subtypeDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aliasDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass terminalDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeTypeDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unconstrainedArrayDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unconstrainedNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constrainedArrayDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constrainedNatureDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass waitStatementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nullEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aggregateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass associationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass allEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass othersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relationalExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shiftExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addingExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplyingExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass powerExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass characterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass indicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass waveformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bitStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifiedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signalKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rangeDirectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum modeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unaryOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum multiplyingOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum shiftOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum relationalOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum logicalOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum addingOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum entityClassEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VhdlPackageImpl() {
		super(eNS_URI, VhdlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VhdlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static VhdlPackage init() {
		if (isInited) return (VhdlPackage)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI);

		// Obtain or create and register package
		VhdlPackageImpl theVhdlPackage = (VhdlPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VhdlPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VhdlPackageImpl());

		isInited = true;

		// Load packages
		theVhdlPackage.loadPackage();

		// Fix loaded packages
		theVhdlPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theVhdlPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VhdlPackage.eNS_URI, theVhdlPackage);
		return theVhdlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVhdlFile() {
		if (vhdlFileEClass == null) {
			vhdlFileEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(0);
		}
		return vhdlFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVhdlFile_Design() {
        return (EReference)getVhdlFile().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignUnit() {
		if (designUnitEClass == null) {
			designUnitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(1);
		}
		return designUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDesignUnit_Library() {
        return (EAttribute)getDesignUnit().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignUnit_Use() {
        return (EReference)getDesignUnit().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignUnit_Unit() {
        return (EReference)getDesignUnit().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseClause() {
		if (useClauseEClass == null) {
			useClauseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(2);
		}
		return useClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseClause_Use() {
        return (EReference)getUseClause().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLibraryUnit() {
		if (libraryUnitEClass == null) {
			libraryUnitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(3);
		}
		return libraryUnitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLibraryUnit_Declaration() {
        return (EReference)getLibraryUnit().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessTypeDefinition() {
		if (accessTypeDefinitionEClass == null) {
			accessTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(4);
		}
		return accessTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessTypeDefinition_Type() {
        return (EReference)getAccessTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameElement() {
		if (nameElementEClass == null) {
			nameElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(5);
		}
		return nameElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameElement_Signature() {
        return (EReference)getNameElement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeclaration() {
		if (declarationEClass == null) {
			declarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(6);
		}
		return declarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArchitecture() {
		if (architectureEClass == null) {
			architectureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(7);
		}
		return architectureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArchitecture_Of() {
        return (EReference)getArchitecture().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArchitecture_Statement() {
        return (EReference)getArchitecture().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatement() {
		if (statementEClass == null) {
			statementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(8);
		}
		return statementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayNatureDefinition() {
		if (arrayNatureDefinitionEClass == null) {
			arrayNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(9);
		}
		return arrayNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayNatureDefinition_Nature() {
        return (EReference)getArrayNatureDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArrayTypeDefinition() {
		if (arrayTypeDefinitionEClass == null) {
			arrayTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(10);
		}
		return arrayTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArrayTypeDefinition_Type() {
        return (EReference)getArrayTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertionStatement() {
		if (assertionStatementEClass == null) {
			assertionStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(11);
		}
		return assertionStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertionStatement_Condition() {
        return (EReference)getAssertionStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertionStatement_Report() {
        return (EReference)getAssertionStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertionStatement_Severity() {
        return (EReference)getAssertionStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAssertionStatement_Postponed() {
        return (EAttribute)getAssertionStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpression() {
		if (expressionEClass == null) {
			expressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(12);
		}
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoice() {
		if (choiceEClass == null) {
			choiceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(13);
		}
		return choiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOpen() {
		if (openEClass == null) {
			openEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(14);
		}
		return openEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeDeclaration() {
		if (attributeDeclarationEClass == null) {
			attributeDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(15);
		}
		return attributeDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeDeclaration_Type() {
        return (EAttribute)getAttributeDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeSpecification() {
		if (attributeSpecificationEClass == null) {
			attributeSpecificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(16);
		}
		return attributeSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeSpecification_Entity() {
        return (EReference)getAttributeSpecification().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttributeSpecification_Class() {
        return (EAttribute)getAttributeSpecification().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeSpecification_Is() {
        return (EReference)getAttributeSpecification().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameList() {
		if (nameListEClass == null) {
			nameListEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(17);
		}
		return nameListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameList_Name() {
        return (EReference)getNameList().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlockConfiguration() {
		if (blockConfigurationEClass == null) {
			blockConfigurationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(18);
		}
		return blockConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockConfiguration_Name() {
        return (EReference)getBlockConfiguration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockConfiguration_Use() {
        return (EReference)getBlockConfiguration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockConfiguration_Item() {
        return (EReference)getBlockConfiguration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlockStatement() {
		if (blockStatementEClass == null) {
			blockStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(19);
		}
		return blockStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_Guard() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_Generic() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_GenericMap() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_Port() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_PortMap() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_Declaration() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlockStatement_Statement() {
        return (EReference)getBlockStatement().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBreakElement() {
		if (breakElementEClass == null) {
			breakElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(20);
		}
		return breakElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakElement_Name() {
        return (EReference)getBreakElement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakElement_Use() {
        return (EReference)getBreakElement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakElement_Arrow() {
        return (EReference)getBreakElement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBreakStatement() {
		if (breakStatementEClass == null) {
			breakStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(21);
		}
		return breakStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakStatement_Break() {
        return (EReference)getBreakStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakStatement_When() {
        return (EReference)getBreakStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBreakStatement_Sensitivity() {
        return (EReference)getBreakStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseStatement() {
		if (caseStatementEClass == null) {
			caseStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(22);
		}
		return caseStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseStatement_Case() {
        return (EReference)getCaseStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseStatement_When() {
        return (EReference)getCaseStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaseAlternative() {
		if (caseAlternativeEClass == null) {
			caseAlternativeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(23);
		}
		return caseAlternativeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseAlternative_Choice() {
        return (EReference)getCaseAlternative().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaseAlternative_Statement() {
        return (EReference)getCaseAlternative().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentConfiguration() {
		if (componentConfigurationEClass == null) {
			componentConfigurationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(24);
		}
		return componentConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_List() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_Component() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_Entity() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_Configuration() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_GenericMap() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_PortMap() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentConfiguration_Block() {
        return (EReference)getComponentConfiguration().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentDeclaration() {
		if (componentDeclarationEClass == null) {
			componentDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(25);
		}
		return componentDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentDeclaration_Generic() {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentDeclaration_Port() {
        return (EReference)getComponentDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstantiationStatement() {
		if (instantiationStatementEClass == null) {
			instantiationStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(26);
		}
		return instantiationStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstantiationStatement_GenericMap() {
        return (EReference)getInstantiationStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstantiationStatement_PortMap() {
        return (EReference)getInstantiationStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstantiationStatement() {
		if (componentInstantiationStatementEClass == null) {
			componentInstantiationStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(27);
		}
		return componentInstantiationStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstantiationStatement_Component() {
        return (EReference)getComponentInstantiationStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntityInstantiationStatement() {
		if (entityInstantiationStatementEClass == null) {
			entityInstantiationStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(28);
		}
		return entityInstantiationStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityInstantiationStatement_Entity() {
        return (EReference)getEntityInstantiationStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationInstantiationStatement() {
		if (configurationInstantiationStatementEClass == null) {
			configurationInstantiationStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(29);
		}
		return configurationInstantiationStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationInstantiationStatement_Configuration() {
        return (EReference)getConfigurationInstantiationStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationInstantiationStatement_GenericMap() {
        return (EReference)getConfigurationInstantiationStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationInstantiationStatement_PortMap() {
        return (EReference)getConfigurationInstantiationStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeNatureDefinition() {
		if (compositeNatureDefinitionEClass == null) {
			compositeNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(30);
		}
		return compositeNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeTypeDefinition() {
		if (compositeTypeDefinitionEClass == null) {
			compositeTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(31);
		}
		return compositeTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialSignalAssignmentStatement() {
		if (sequentialSignalAssignmentStatementEClass == null) {
			sequentialSignalAssignmentStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(32);
		}
		return sequentialSignalAssignmentStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSequentialSignalAssignmentStatement_Postponed() {
        return (EAttribute)getSequentialSignalAssignmentStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequentialSignalAssignmentStatement_Target() {
        return (EReference)getSequentialSignalAssignmentStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSequentialSignalAssignmentStatement_Guarded() {
        return (EAttribute)getSequentialSignalAssignmentStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequentialSignalAssignmentStatement_Delay() {
        return (EReference)getSequentialSignalAssignmentStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequentialSignalAssignmentStatement_Waveform() {
        return (EReference)getSequentialSignalAssignmentStatement().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionalSignalAssignmentStatement() {
		if (conditionalSignalAssignmentStatementEClass == null) {
			conditionalSignalAssignmentStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(33);
		}
		return conditionalSignalAssignmentStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConditionalSignalAssignmentStatement_Postponed() {
        return (EAttribute)getConditionalSignalAssignmentStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalSignalAssignmentStatement_Target() {
        return (EReference)getConditionalSignalAssignmentStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConditionalSignalAssignmentStatement_Guarded() {
        return (EAttribute)getConditionalSignalAssignmentStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalSignalAssignmentStatement_Delay() {
        return (EReference)getConditionalSignalAssignmentStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalSignalAssignmentStatement_Waveform() {
        return (EReference)getConditionalSignalAssignmentStatement().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSelectedSignalAssignmentStatement() {
		if (selectedSignalAssignmentStatementEClass == null) {
			selectedSignalAssignmentStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(34);
		}
		return selectedSignalAssignmentStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSelectedSignalAssignmentStatement_Postponed() {
        return (EAttribute)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectedSignalAssignmentStatement_Expression() {
        return (EReference)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectedSignalAssignmentStatement_Target() {
        return (EReference)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSelectedSignalAssignmentStatement_Guarded() {
        return (EAttribute)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectedSignalAssignmentStatement_Delay() {
        return (EReference)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelectedSignalAssignmentStatement_Waveform() {
        return (EReference)getSelectedSignalAssignmentStatement().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleSimultaneousStatement() {
		if (simpleSimultaneousStatementEClass == null) {
			simpleSimultaneousStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(35);
		}
		return simpleSimultaneousStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleSimultaneousStatement_Left() {
        return (EReference)getSimpleSimultaneousStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleSimultaneousStatement_Right() {
        return (EReference)getSimpleSimultaneousStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimpleSimultaneousStatement_Tolerance() {
        return (EReference)getSimpleSimultaneousStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionalWaveform() {
		if (conditionalWaveformEClass == null) {
			conditionalWaveformEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(36);
		}
		return conditionalWaveformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalWaveform_Waveform() {
        return (EReference)getConditionalWaveform().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConditionalWaveform_Choice() {
        return (EReference)getConditionalWaveform().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationDeclaration() {
		if (configurationDeclarationEClass == null) {
			configurationDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(37);
		}
		return configurationDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationDeclaration_Of() {
        return (EReference)getConfigurationDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationDeclaration_Block() {
        return (EReference)getConfigurationDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationItem() {
		if (configurationItemEClass == null) {
			configurationItemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(38);
		}
		return configurationItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationSpecification() {
		if (configurationSpecificationEClass == null) {
			configurationSpecificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(39);
		}
		return configurationSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_List() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_Component() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_Entity() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_Configuration() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_GenericMap() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfigurationSpecification_PortMap() {
        return (EReference)getConfigurationSpecification().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDelayMechanism() {
		if (delayMechanismEClass == null) {
			delayMechanismEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(40);
		}
		return delayMechanismEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRejectMechanism() {
		if (rejectMechanismEClass == null) {
			rejectMechanismEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(41);
		}
		return rejectMechanismEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRejectMechanism_Reject() {
        return (EReference)getRejectMechanism().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransport() {
		if (transportEClass == null) {
			transportEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(42);
		}
		return transportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDisconnectionSpecification() {
		if (disconnectionSpecificationEClass == null) {
			disconnectionSpecificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(43);
		}
		return disconnectionSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDisconnectionSpecification_Disconnect() {
        return (EReference)getDisconnectionSpecification().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDisconnectionSpecification_Type() {
        return (EReference)getDisconnectionSpecification().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDisconnectionSpecification_After() {
        return (EReference)getDisconnectionSpecification().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementDeclaration() {
		if (elementDeclarationEClass == null) {
			elementDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(44);
		}
		return elementDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElementDeclaration_Identifier() {
        return (EAttribute)getElementDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementDeclaration_Type() {
        return (EReference)getElementDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntityDeclaration() {
		if (entityDeclarationEClass == null) {
			entityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(45);
		}
		return entityDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDeclaration_Generic() {
        return (EReference)getEntityDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDeclaration_Port() {
        return (EReference)getEntityDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEntityDeclaration_Statement() {
        return (EReference)getEntityDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationTypeDefinition() {
		if (enumerationTypeDefinitionEClass == null) {
			enumerationTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(46);
		}
		return enumerationTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumerationTypeDefinition_Literal() {
        return (EReference)getEnumerationTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExitStatement() {
		if (exitStatementEClass == null) {
			exitStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(47);
		}
		return exitStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExitStatement_Exit() {
        return (EAttribute)getExitStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExitStatement_When() {
        return (EReference)getExitStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFileDeclaration() {
		if (fileDeclarationEClass == null) {
			fileDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(48);
		}
		return fileDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFileDeclaration_Type() {
        return (EReference)getFileDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFileDeclaration_Open() {
        return (EReference)getFileDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFileDeclaration_Is() {
        return (EReference)getFileDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFileTypeDefinition() {
		if (fileTypeDefinitionEClass == null) {
			fileTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(49);
		}
		return fileTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFileTypeDefinition_File() {
        return (EReference)getFileTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerateStatement() {
		if (generateStatementEClass == null) {
			generateStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(50);
		}
		return generateStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerateStatement_Scheme() {
        return (EReference)getGenerateStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerateStatement_Declaration() {
        return (EReference)getGenerateStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerateStatement_Statement() {
        return (EReference)getGenerateStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerationScheme() {
		if (generationSchemeEClass == null) {
			generationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(51);
		}
		return generationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForGenerationScheme() {
		if (forGenerationSchemeEClass == null) {
			forGenerationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(52);
		}
		return forGenerationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getForGenerationScheme_Variable() {
        return (EAttribute)getForGenerationScheme().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForGenerationScheme_In() {
        return (EReference)getForGenerationScheme().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfGenerationScheme() {
		if (ifGenerationSchemeEClass == null) {
			ifGenerationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(53);
		}
		return ifGenerationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfGenerationScheme_Condition() {
        return (EReference)getIfGenerationScheme().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerics() {
		if (genericsEClass == null) {
			genericsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(54);
		}
		return genericsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerics_Declaration() {
        return (EReference)getGenerics().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenericMaps() {
		if (genericMapsEClass == null) {
			genericMapsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(55);
		}
		return genericMapsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenericMaps_Generic() {
        return (EReference)getGenericMaps().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroupDeclaration() {
		if (groupDeclarationEClass == null) {
			groupDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(56);
		}
		return groupDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroupDeclaration_Is() {
        return (EReference)getGroupDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroupDeclaration_Member() {
        return (EReference)getGroupDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroupTemplateDeclaration() {
		if (groupTemplateDeclarationEClass == null) {
			groupTemplateDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(57);
		}
		return groupTemplateDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGroupTemplateDeclaration_Entry() {
        return (EAttribute)getGroupTemplateDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfStatement() {
		if (ifStatementEClass == null) {
			ifStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(58);
		}
		return ifStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfStatement_Test() {
        return (EReference)getIfStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfStatement_Statement() {
        return (EReference)getIfStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfStatementTest() {
		if (ifStatementTestEClass == null) {
			ifStatementTestEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(59);
		}
		return ifStatementTestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfStatementTest_Condition() {
        return (EReference)getIfStatementTest().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfStatementTest_Statement() {
        return (EReference)getIfStatementTest().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstraint() {
		if (constraintEClass == null) {
			constraintEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(60);
		}
		return constraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstraint_Range() {
        return (EReference)getConstraint().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiscreteRange() {
		if (discreteRangeEClass == null) {
			discreteRangeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(61);
		}
		return discreteRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLoopStatement() {
		if (loopStatementEClass == null) {
			loopStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(62);
		}
		return loopStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoopStatement_Iteration() {
        return (EReference)getLoopStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLoopStatement_Statement() {
        return (EReference)getLoopStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIterationScheme() {
		if (iterationSchemeEClass == null) {
			iterationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(63);
		}
		return iterationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhileIterationScheme() {
		if (whileIterationSchemeEClass == null) {
			whileIterationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(64);
		}
		return whileIterationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileIterationScheme_Condition() {
        return (EReference)getWhileIterationScheme().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForIterationScheme() {
		if (forIterationSchemeEClass == null) {
			forIterationSchemeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(65);
		}
		return forIterationSchemeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getForIterationScheme_Variable() {
        return (EAttribute)getForIterationScheme().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForIterationScheme_In() {
        return (EReference)getForIterationScheme().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttribute() {
		if (attributeEClass == null) {
			attributeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(66);
		}
		return attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNatureDeclaration() {
		if (natureDeclarationEClass == null) {
			natureDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(67);
		}
		return natureDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNatureDeclaration_Is() {
        return (EReference)getNatureDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNatureDefinition() {
		if (natureDefinitionEClass == null) {
			natureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(68);
		}
		return natureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNextStatement() {
		if (nextStatementEClass == null) {
			nextStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(69);
		}
		return nextStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNextStatement_Next() {
        return (EAttribute)getNextStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextStatement_When() {
        return (EReference)getNextStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageBody() {
		if (packageBodyEClass == null) {
			packageBodyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(70);
		}
		return packageBodyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageDeclaration() {
		if (packageDeclarationEClass == null) {
			packageDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(71);
		}
		return packageDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalTypeDefinition() {
		if (physicalTypeDefinitionEClass == null) {
			physicalTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(72);
		}
		return physicalTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalTypeDefinition_Range() {
        return (EReference)getPhysicalTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalTypeDefinition_Primary() {
        return (EAttribute)getPhysicalTypeDefinition().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalTypeDefinition_Secondary() {
        return (EReference)getPhysicalTypeDefinition().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhysicalTypeDefinitionSecondary() {
		if (physicalTypeDefinitionSecondaryEClass == null) {
			physicalTypeDefinitionSecondaryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(73);
		}
		return physicalTypeDefinitionSecondaryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalTypeDefinitionSecondary_Name() {
        return (EAttribute)getPhysicalTypeDefinitionSecondary().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhysicalTypeDefinitionSecondary_Number() {
        return (EAttribute)getPhysicalTypeDefinitionSecondary().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhysicalTypeDefinitionSecondary_Of() {
        return (EReference)getPhysicalTypeDefinitionSecondary().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPorts() {
		if (portsEClass == null) {
			portsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(74);
		}
		return portsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPorts_Declaration() {
        return (EReference)getPorts().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortMaps() {
		if (portMapsEClass == null) {
			portMapsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(75);
		}
		return portMapsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortMaps_Port() {
        return (EReference)getPortMaps().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureCallStatement() {
		if (procedureCallStatementEClass == null) {
			procedureCallStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(76);
		}
		return procedureCallStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureCallStatement_Procedure() {
        return (EReference)getProcedureCallStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcedureCallStatement_Postponed() {
        return (EAttribute)getProcedureCallStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessStatement() {
		if (processStatementEClass == null) {
			processStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(77);
		}
		return processStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessStatement_Postponed() {
        return (EAttribute)getProcessStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessStatement_Sensitivity() {
        return (EAttribute)getProcessStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessStatement_Declaration() {
        return (EReference)getProcessStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessStatement_Statement() {
        return (EReference)getProcessStatement().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAllocator() {
		if (allocatorEClass == null) {
			allocatorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(78);
		}
		return allocatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAllocator_Allocate() {
        return (EReference)getAllocator().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordNatureDefinition() {
		if (recordNatureDefinitionEClass == null) {
			recordNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(79);
		}
		return recordNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordNatureDefinition_Record() {
        return (EReference)getRecordNatureDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordNatureElement() {
		if (recordNatureElementEClass == null) {
			recordNatureElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(80);
		}
		return recordNatureElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRecordNatureElement_Name() {
        return (EAttribute)getRecordNatureElement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordNatureElement_Nature() {
        return (EReference)getRecordNatureElement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecordTypeDefinition() {
		if (recordTypeDefinitionEClass == null) {
			recordTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(81);
		}
		return recordTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecordTypeDefinition_Declaration() {
        return (EReference)getRecordTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReportStatement() {
		if (reportStatementEClass == null) {
			reportStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(82);
		}
		return reportStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReportStatement_Report() {
        return (EReference)getReportStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReportStatement_Severity() {
        return (EReference)getReportStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReturnStatement() {
		if (returnStatementEClass == null) {
			returnStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(83);
		}
		return returnStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReturnStatement_Return() {
        return (EReference)getReturnStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalarNatureDefinition() {
		if (scalarNatureDefinitionEClass == null) {
			scalarNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(84);
		}
		return scalarNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalarNatureDefinition_Name() {
        return (EReference)getScalarNatureDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalarNatureDefinition_Across() {
        return (EReference)getScalarNatureDefinition().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalarNatureDefinition_Through() {
        return (EReference)getScalarNatureDefinition().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNullStatement() {
		if (nullStatementEClass == null) {
			nullStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(85);
		}
		return nullStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalAssignmentStatement() {
		if (signalAssignmentStatementEClass == null) {
			signalAssignmentStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(86);
		}
		return signalAssignmentStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalAssignmentStatement_Target() {
        return (EReference)getSignalAssignmentStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalAssignmentStatement_Delay() {
        return (EReference)getSignalAssignmentStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalAssignmentStatement_Waveform() {
        return (EReference)getSignalAssignmentStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableAssignmentStatement() {
		if (variableAssignmentStatementEClass == null) {
			variableAssignmentStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(87);
		}
		return variableAssignmentStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableAssignmentStatement_Target() {
        return (EReference)getVariableAssignmentStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableAssignmentStatement_Initial() {
        return (EReference)getVariableAssignmentStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalDeclaration() {
		if (signalDeclarationEClass == null) {
			signalDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(88);
		}
		return signalDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalDeclaration_Type() {
        return (EReference)getSignalDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDeclaration_Kind() {
        return (EAttribute)getSignalDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalDeclaration_Initial() {
        return (EReference)getSignalDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDeclaration_Mode() {
        return (EAttribute)getSignalDeclaration().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableDeclaration() {
		if (variableDeclarationEClass == null) {
			variableDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(89);
		}
		return variableDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableDeclaration_Shared() {
        return (EAttribute)getVariableDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableDeclaration_Type() {
        return (EReference)getVariableDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableDeclaration_Initial() {
        return (EReference)getVariableDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVariableDeclaration_Mode() {
        return (EAttribute)getVariableDeclaration().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstantDeclaration() {
		if (constantDeclarationEClass == null) {
			constantDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(90);
		}
		return constantDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstantDeclaration_Type() {
        return (EReference)getConstantDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstantDeclaration_Initial() {
        return (EReference)getConstantDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignature() {
		if (signatureEClass == null) {
			signatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(91);
		}
		return signatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignature_Name() {
        return (EReference)getSignature().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignature_Return() {
        return (EReference)getSignature().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimultaneousAlternative() {
		if (simultaneousAlternativeEClass == null) {
			simultaneousAlternativeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(92);
		}
		return simultaneousAlternativeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousAlternative_Choice() {
        return (EReference)getSimultaneousAlternative().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousAlternative_Statememt() {
        return (EReference)getSimultaneousAlternative().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimultaneousCaseStatement() {
		if (simultaneousCaseStatementEClass == null) {
			simultaneousCaseStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(93);
		}
		return simultaneousCaseStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousCaseStatement_Case() {
        return (EReference)getSimultaneousCaseStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousCaseStatement_When() {
        return (EReference)getSimultaneousCaseStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimultaneousIfStatement() {
		if (simultaneousIfStatementEClass == null) {
			simultaneousIfStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(94);
		}
		return simultaneousIfStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousIfStatement_Test() {
        return (EReference)getSimultaneousIfStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousIfStatement_Statememt() {
        return (EReference)getSimultaneousIfStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimultaneousIfStatementTest() {
		if (simultaneousIfStatementTestEClass == null) {
			simultaneousIfStatementTestEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(95);
		}
		return simultaneousIfStatementTestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousIfStatementTest_Condition() {
        return (EReference)getSimultaneousIfStatementTest().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousIfStatementTest_Statememt() {
        return (EReference)getSimultaneousIfStatementTest().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimultaneousProceduralStatement() {
		if (simultaneousProceduralStatementEClass == null) {
			simultaneousProceduralStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(96);
		}
		return simultaneousProceduralStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousProceduralStatement_Declaration() {
        return (EReference)getSimultaneousProceduralStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimultaneousProceduralStatement_Statement() {
        return (EReference)getSimultaneousProceduralStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSourceAspect() {
		if (sourceAspectEClass == null) {
			sourceAspectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(97);
		}
		return sourceAspectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpectrum() {
		if (spectrumEClass == null) {
			spectrumEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(98);
		}
		return spectrumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSpectrum_Left() {
        return (EReference)getSpectrum().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSpectrum_Right() {
        return (EReference)getSpectrum().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNoise() {
		if (noiseEClass == null) {
			noiseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(99);
		}
		return noiseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNoise_Noise() {
        return (EReference)getNoise().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantityDeclaration() {
		if (quantityDeclarationEClass == null) {
			quantityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(100);
		}
		return quantityDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFreeQuantityDeclaration() {
		if (freeQuantityDeclarationEClass == null) {
			freeQuantityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(101);
		}
		return freeQuantityDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFreeQuantityDeclaration_Type() {
        return (EReference)getFreeQuantityDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFreeQuantityDeclaration_Quantity() {
        return (EReference)getFreeQuantityDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBranchQuantityDeclaration() {
		if (branchQuantityDeclarationEClass == null) {
			branchQuantityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(102);
		}
		return branchQuantityDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBranchQuantityDeclaration_Across() {
        return (EReference)getBranchQuantityDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBranchQuantityDeclaration_Through() {
        return (EReference)getBranchQuantityDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBranchQuantityDeclaration_Left() {
        return (EReference)getBranchQuantityDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBranchQuantityDeclaration_Right() {
        return (EReference)getBranchQuantityDeclaration().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantityAspect() {
		if (quantityAspectEClass == null) {
			quantityAspectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(103);
		}
		return quantityAspectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuantityAspect_Name() {
        return (EAttribute)getQuantityAspect().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityAspect_Tolerance() {
        return (EReference)getQuantityAspect().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getQuantityAspect_Expression() {
        return (EReference)getQuantityAspect().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSourceQuantityDeclaration() {
		if (sourceQuantityDeclarationEClass == null) {
			sourceQuantityDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(104);
		}
		return sourceQuantityDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSourceQuantityDeclaration_Type() {
        return (EReference)getSourceQuantityDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSourceQuantityDeclaration_Source() {
        return (EReference)getSourceQuantityDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLimitSpecification() {
		if (limitSpecificationEClass == null) {
			limitSpecificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(105);
		}
		return limitSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLimitSpecification_Limit() {
        return (EReference)getLimitSpecification().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLimitSpecification_Type() {
        return (EReference)getLimitSpecification().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLimitSpecification_Value() {
        return (EReference)getLimitSpecification().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubnatureDeclaration() {
		if (subnatureDeclarationEClass == null) {
			subnatureDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(106);
		}
		return subnatureDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubnatureDeclaration_Nature() {
        return (EReference)getSubnatureDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubprogramDeclaration() {
		if (subprogramDeclarationEClass == null) {
			subprogramDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(107);
		}
		return subprogramDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubprogramDeclaration_Specification() {
        return (EReference)getSubprogramDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubprogramDeclaration_Declaration() {
        return (EReference)getSubprogramDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubprogramDeclaration_Statement() {
        return (EReference)getSubprogramDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubprogramSpecification() {
		if (subprogramSpecificationEClass == null) {
			subprogramSpecificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(108);
		}
		return subprogramSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubprogramSpecification_Name() {
        return (EAttribute)getSubprogramSpecification().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubprogramSpecification_Declaration() {
        return (EReference)getSubprogramSpecification().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSubprogramSpecification_Purity() {
        return (EAttribute)getSubprogramSpecification().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubprogramSpecification_Return() {
        return (EReference)getSubprogramSpecification().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubtypeDeclaration() {
		if (subtypeDeclarationEClass == null) {
			subtypeDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(109);
		}
		return subtypeDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubtypeDeclaration_Type() {
        return (EReference)getSubtypeDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAliasDeclaration() {
		if (aliasDeclarationEClass == null) {
			aliasDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(110);
		}
		return aliasDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAliasDeclaration_Alias() {
        return (EReference)getAliasDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAliasDeclaration_Is() {
        return (EReference)getAliasDeclaration().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAliasDeclaration_Signature() {
        return (EReference)getAliasDeclaration().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTerminalDeclaration() {
		if (terminalDeclarationEClass == null) {
			terminalDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(111);
		}
		return terminalDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTerminalDeclaration_Nature() {
        return (EReference)getTerminalDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeDeclaration() {
		if (typeDeclarationEClass == null) {
			typeDeclarationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(112);
		}
		return typeDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeDeclaration_Is() {
        return (EReference)getTypeDeclaration().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeDefinition() {
		if (typeDefinitionEClass == null) {
			typeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(113);
		}
		return typeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeTypeDefinition() {
		if (rangeTypeDefinitionEClass == null) {
			rangeTypeDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(114);
		}
		return rangeTypeDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeTypeDefinition_Left() {
        return (EReference)getRangeTypeDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRangeTypeDefinition_Direction() {
        return (EAttribute)getRangeTypeDefinition().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeTypeDefinition_Right() {
        return (EReference)getRangeTypeDefinition().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnconstrainedArrayDefinition() {
		if (unconstrainedArrayDefinitionEClass == null) {
			unconstrainedArrayDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(115);
		}
		return unconstrainedArrayDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnconstrainedArrayDefinition_Index() {
        return (EReference)getUnconstrainedArrayDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnconstrainedNatureDefinition() {
		if (unconstrainedNatureDefinitionEClass == null) {
			unconstrainedNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(116);
		}
		return unconstrainedNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnconstrainedNatureDefinition_Index() {
        return (EReference)getUnconstrainedNatureDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstrainedArrayDefinition() {
		if (constrainedArrayDefinitionEClass == null) {
			constrainedArrayDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(117);
		}
		return constrainedArrayDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstrainedArrayDefinition_Constraint() {
        return (EReference)getConstrainedArrayDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstrainedNatureDefinition() {
		if (constrainedNatureDefinitionEClass == null) {
			constrainedNatureDefinitionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(118);
		}
		return constrainedNatureDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstrainedNatureDefinition_Constraint() {
        return (EReference)getConstrainedNatureDefinition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWaitStatement() {
		if (waitStatementEClass == null) {
			waitStatementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(119);
		}
		return waitStatementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWaitStatement_Sensitivity() {
        return (EReference)getWaitStatement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWaitStatement_Until() {
        return (EReference)getWaitStatement().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWaitStatement_Time() {
        return (EReference)getWaitStatement().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNull() {
		if (nullEClass == null) {
			nullEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(120);
		}
		return nullEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValue() {
		if (valueEClass == null) {
			valueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(121);
		}
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValue_Number() {
        return (EAttribute)getValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAggregate() {
		if (aggregateEClass == null) {
			aggregateEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(133);
		}
		return aggregateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAggregate_Element() {
        return (EReference)getAggregate().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssociation() {
		if (associationEClass == null) {
			associationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(135);
		}
		return associationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociation_Choice() {
        return (EReference)getAssociation().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociation_Expression() {
        return (EReference)getAssociation().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAll() {
		if (allEClass == null) {
			allEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(134);
		}
		return allEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOthers() {
		if (othersEClass == null) {
			othersEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(136);
		}
		return othersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogicalExpression() {
		if (logicalExpressionEClass == null) {
			logicalExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(137);
		}
		return logicalExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalExpression_Left() {
        return (EReference)getLogicalExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogicalExpression_Operator() {
        return (EAttribute)getLogicalExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogicalExpression_Right() {
        return (EReference)getLogicalExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelationalExpression() {
		if (relationalExpressionEClass == null) {
			relationalExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(138);
		}
		return relationalExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelationalExpression_Left() {
        return (EReference)getRelationalExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRelationalExpression_Operator() {
        return (EAttribute)getRelationalExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRelationalExpression_Right() {
        return (EReference)getRelationalExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShiftExpression() {
		if (shiftExpressionEClass == null) {
			shiftExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(139);
		}
		return shiftExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShiftExpression_Left() {
        return (EReference)getShiftExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShiftExpression_Operator() {
        return (EAttribute)getShiftExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShiftExpression_Right() {
        return (EReference)getShiftExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddingExpression() {
		if (addingExpressionEClass == null) {
			addingExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(140);
		}
		return addingExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddingExpression_Left() {
        return (EReference)getAddingExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAddingExpression_Operator() {
        return (EAttribute)getAddingExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddingExpression_Right() {
        return (EReference)getAddingExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplyingExpression() {
		if (multiplyingExpressionEClass == null) {
			multiplyingExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(141);
		}
		return multiplyingExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplyingExpression_Left() {
        return (EReference)getMultiplyingExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiplyingExpression_Operator() {
        return (EAttribute)getMultiplyingExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplyingExpression_Right() {
        return (EReference)getMultiplyingExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPowerExpression() {
		if (powerExpressionEClass == null) {
			powerExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(142);
		}
		return powerExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPowerExpression_Left() {
        return (EReference)getPowerExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPowerExpression_Right() {
        return (EReference)getPowerExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnaryExpression() {
		if (unaryExpressionEClass == null) {
			unaryExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(143);
		}
		return unaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnaryExpression_Operator() {
        return (EAttribute)getUnaryExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnaryExpression_Expression() {
        return (EReference)getUnaryExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignExpression() {
		if (signExpressionEClass == null) {
			signExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(144);
		}
		return signExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignExpression_Sign() {
        return (EAttribute)getSignExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignExpression_Expression() {
        return (EReference)getSignExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRange() {
		if (rangeEClass == null) {
			rangeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(145);
		}
		return rangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRange_Left() {
        return (EReference)getRange().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRange_Direction() {
        return (EAttribute)getRange().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRange_Right() {
        return (EReference)getRange().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getName_() {
		if (nameEClass == null) {
			nameEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(146);
		}
		return nameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getName_Element() {
        return (EReference)getName_().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getString() {
		if (stringEClass == null) {
			stringEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(148);
		}
		return stringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCharacter() {
		if (characterEClass == null) {
			characterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(149);
		}
		return characterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifier() {
		if (identifierEClass == null) {
			identifierEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(150);
		}
		return identifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIndication() {
		if (indicationEClass == null) {
			indicationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(151);
		}
		return indicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIndication_Mark() {
        return (EReference)getIndication().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIndication_Constraint() {
        return (EReference)getIndication().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIndication_Tolerance() {
        return (EReference)getIndication().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIndication_Across() {
        return (EReference)getIndication().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWaveform() {
		if (waveformEClass == null) {
			waveformEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(152);
		}
		return waveformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWaveform_Expression() {
        return (EReference)getWaveform().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWaveform_After() {
        return (EReference)getWaveform().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBitString() {
		if (bitStringEClass == null) {
			bitStringEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(147);
		}
		return bitStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnitValue() {
		if (unitValueEClass == null) {
			unitValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(153);
		}
		return unitValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnitValue_Unit() {
        return (EReference)getUnitValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifiedElement() {
		if (identifiedElementEClass == null) {
			identifiedElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(154);
		}
		return identifiedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiedElement_Id() {
        return (EAttribute)getIdentifiedElement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		if (namedElementEClass == null) {
			namedElementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(155);
		}
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
        return (EAttribute)getNamedElement().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSignalKind() {
		if (signalKindEEnum == null) {
			signalKindEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(122);
		}
		return signalKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRangeDirection() {
		if (rangeDirectionEEnum == null) {
			rangeDirectionEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(123);
		}
		return rangeDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMode() {
		if (modeEEnum == null) {
			modeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(124);
		}
		return modeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUnaryOperator() {
		if (unaryOperatorEEnum == null) {
			unaryOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(125);
		}
		return unaryOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMultiplyingOperator() {
		if (multiplyingOperatorEEnum == null) {
			multiplyingOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(126);
		}
		return multiplyingOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getShiftOperator() {
		if (shiftOperatorEEnum == null) {
			shiftOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(127);
		}
		return shiftOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRelationalOperator() {
		if (relationalOperatorEEnum == null) {
			relationalOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(128);
		}
		return relationalOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLogicalOperator() {
		if (logicalOperatorEEnum == null) {
			logicalOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(129);
		}
		return logicalOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAddingOperator() {
		if (addingOperatorEEnum == null) {
			addingOperatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(130);
		}
		return addingOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSign() {
		if (signEEnum == null) {
			signEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(131);
		}
		return signEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEntityClass() {
		if (entityClassEEnum == null) {
			entityClassEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(VhdlPackage.eNS_URI).getEClassifiers().get(132);
		}
		return entityClassEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VhdlFactory getVhdlFactory() {
		return (VhdlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("fr.labsticc.language.vhdl.model.vhdl." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //VhdlPackageImpl
