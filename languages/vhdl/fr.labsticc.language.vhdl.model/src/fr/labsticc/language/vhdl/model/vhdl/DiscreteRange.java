/**
 */
package fr.labsticc.language.vhdl.model.vhdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discrete Range</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getDiscreteRange()
 * @model abstract="true"
 * @generated
 */
public interface DiscreteRange extends EObject {
} // DiscreteRange
