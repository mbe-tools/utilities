/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getOf <em>Of</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getBlock <em>Block</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationDeclaration()
 * @model
 * @generated
 */
public interface ConfigurationDeclaration extends LibraryUnit {
	/**
	 * Returns the value of the '<em><b>Of</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Of</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Of</em>' containment reference.
	 * @see #setOf(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationDeclaration_Of()
	 * @model containment="true"
	 * @generated
	 */
	Expression getOf();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getOf <em>Of</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Of</em>' containment reference.
	 * @see #getOf()
	 * @generated
	 */
	void setOf(Expression value);

	/**
	 * Returns the value of the '<em><b>Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' containment reference.
	 * @see #setBlock(BlockConfiguration)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getConfigurationDeclaration_Block()
	 * @model containment="true"
	 * @generated
	 */
	BlockConfiguration getBlock();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration#getBlock <em>Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block</em>' containment reference.
	 * @see #getBlock()
	 * @generated
	 */
	void setBlock(BlockConfiguration value);

} // ConfigurationDeclaration
