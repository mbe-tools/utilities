/**
 */
package fr.labsticc.language.vhdl.model.vhdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getEntity <em>Entity</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getClass_ <em>Class</em>}</li>
 *   <li>{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getIs <em>Is</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAttributeSpecification()
 * @model
 * @generated
 */
public interface AttributeSpecification extends Declaration {
	/**
	 * Returns the value of the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' containment reference.
	 * @see #setEntity(NameList)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAttributeSpecification_Entity()
	 * @model containment="true"
	 * @generated
	 */
	NameList getEntity();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getEntity <em>Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' containment reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(NameList value);

	/**
	 * Returns the value of the '<em><b>Class</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.labsticc.language.vhdl.model.vhdl.EntityClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityClass
	 * @see #setClass(EntityClass)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAttributeSpecification_Class()
	 * @model
	 * @generated
	 */
	EntityClass getClass_();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getClass_ <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' attribute.
	 * @see fr.labsticc.language.vhdl.model.vhdl.EntityClass
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(EntityClass value);

	/**
	 * Returns the value of the '<em><b>Is</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is</em>' containment reference.
	 * @see #setIs(Expression)
	 * @see fr.labsticc.language.vhdl.model.vhdl.VhdlPackage#getAttributeSpecification_Is()
	 * @model containment="true"
	 * @generated
	 */
	Expression getIs();

	/**
	 * Sets the value of the '{@link fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification#getIs <em>Is</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is</em>' containment reference.
	 * @see #getIs()
	 * @generated
	 */
	void setIs(Expression value);

} // AttributeSpecification
