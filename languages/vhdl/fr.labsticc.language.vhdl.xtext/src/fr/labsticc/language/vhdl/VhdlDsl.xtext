/**
 * VHDL AMS grammar
 * 
 * @author mickael.lanoe@univ-ubs.fr
 */
grammar fr.labsticc.language.vhdl.model.Vhdl 
hidden(WS, SL_COMMENT) 

import "platform:/resource/fr.labsticc.language.vhdl.model/model/Vhdl.ecore"

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

//generate vhdl "http://www.labsticc.fr/language/vhdl"

VhdlFile: 
  ( design += DesignUnit )*
;
 
DesignUnit: 
  ( 
      'library' library += Identifier ( ',' library += Identifier )* ';'
    | 'use' use += Name ( ',' use += Name )* ';'
  )*
   
  unit = LibraryUnit
;

UseClause: 
  'use' use += Name ( ',' use += Name )* ';'
;

LibraryUnit: 
    Architecture 
  | PackageBody
  | EntityDeclaration 
  | ConfigurationDeclaration 
  | PackageDeclaration
;

/*
 * access_type_definition: ACCESS subtype_indication;
 */
AccessTypeDefinition:
  'access' type = SubtypeIndication
;

/*
 * aggregate = "(" element_association { "," element_association } ")".
 *
 * Associations with elements' simple names are allowed in record aggregates only.
 * Associations with simple expressions or discrete ranges as choices are allowed only in array aggregates.
 * Each element of the value defined by an aggregate must be represented once and only once in the aggregate.
 * Aggregates containing the single element association must always be specified 
 * using named association in order to distinguish them from parenthesized expressions.
 * The others choice can be only the last in an aggregate.
 */
Aggregate returns NameElement:
  {Aggregate} '(' (element += Parameter ( ',' element += Parameter )*) ? ')'
;

BlockDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | SignalDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | ComponentDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | ConfigurationSpecification
  | DisconnectionSpecification
  | LimitSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
  | NatureDeclaration
  | SubnatureDeclaration
  | QuantityDeclaration
  | TerminalDeclaration
;

ConfigurationDeclarativeItem returns Declaration:
    UseClause
  | AttributeSpecification
  | GroupDeclaration
;

EntityDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | SignalDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | DisconnectionSpecification
  | LimitSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
  | NatureDeclaration
  | SubnatureDeclaration
  | QuantityDeclaration
  | TerminalDeclaration
;

PackageBodyDeclarativeItem returns Declaration: 
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
;

PackageDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | SignalDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | ComponentDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | DisconnectionSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
  | NatureDeclaration
  | SubnatureDeclaration
  | TerminalDeclaration
;

ProceduralDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | VariableDeclaration
  | AliasDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
;

ProcessDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
;

SubprogramDeclarativeItem returns Declaration:
    SubprogramDeclaration
  | TypeDeclaration
  | SubtypeDeclaration
  | ConstantDeclaration
  | VariableDeclaration
  | FileDeclaration
  | AliasDeclaration
  | AttributeDeclaration
  | AttributeSpecification
  | UseClause
  | GroupTemplateDeclaration
  | GroupDeclaration
;

/*
 * architecture identifier of entity_name is
 *    [ declarations]
 * begin -- optional
 *    [ statements]
 * end architecture identifier ;  
 */
Architecture:
  'architecture' name = Identifier 'of' of = Name 'is'
    ( declaration += BlockDeclarativeItem )*
  'begin'
    ( statement += ArchitectureStatement )*
  'end' ( 'architecture' )? ( Identifier )? ';'
;

ArchitectureStatement returns Statement: 
    ConfigurationInstantiationStatement
  | BlockStatement
  | ProcessStatement
  | ConcurrentAssertionStatement
  | SimultaneousProceduralStatement
  | SimultaneousCaseStatement
  | SimultaneousIfStatement
  | ConcurrentBreakStatement
  | EntityInstantiationStatement
  | ConcurrentProcedureCallStatement
  | SequentialSignalAssignmentStatement
  | ConditionalSignalAssignmentStatement
  | SelectedSignalAssignmentStatement
  | ComponentInstantiationStatement
  | GenerateStatement
  | SimpleSimultaneousStatement
;
  
ArrayNatureDefinition:
    UnconstrainedNatureDefinition
  | ConstrainedNatureDefinition
;

ArrayTypeDefinition:
    UnconstrainedArrayDefinition
  | ConstrainedArrayDefinition
;

Label:
	Identifier
;

/*
 * [ label: ] assert boolean_condition [ report string ] [ severity name ] ;
 */
AssertionStatement: 
  ( name = Label ':' )? 'assert' condition = Expression 
  ( 'report' report = Expression )? ( 'severity' severity = Expression )? ';'
;

/*
 * [ label: ] [postponed] assert boolean_condition [ report string ] [ severity name ] ;
 */
ConcurrentAssertionStatement returns AssertionStatement: 
  ( name = Label ':' )? ( postponed ?= 'postponed' )? 'assert' condition = Expression 
  ( 'report' report = Expression )? ( 'severity' severity = Expression )? ';'
;

Parameter returns Expression:
    {Association} choice += Choice ( '|' choice += Choice )* '=>' ( expression = Expression | 'open')
  | Expression 
  | Open
;

Choice: 
    RangeSpecification
  | SubtypeIndication
  | SimpleExpression
  | Others
;

Open:
  {Open}
    name = 'open'
;

/*
 * attribute identifier : type_mark ;
 */
AttributeDeclaration:
  'attribute' name = Identifier ':' type = Identifier ';'
;


/*
 * attribute_specification = "attribute" attribute_designator "of" entity_specification "is" expression ";"
 *  entity_specification = entity_name_list ":" entity_class
 */
AttributeSpecification:
  'attribute' name = AttributeDesignator 'of' 
  entity = EntityNameList ':' class = EntityClass 'is' is = Expression ';'
;

/*
 * entity_name_list = entity_designator { "," entity_designator }
 *                  | "others"
 *                  | "all"
 */
EntityNameList returns NameList:
  name += EntityDesignator  ( ',' name += EntityDesignator )*
;

InstantiationList returns NameList:
    name += IdentifierName ( ',' name += IdentifierName )*
  | name += Others
  | name += All
;

EntityDesignator returns NameElement:
    IdentifierName (signature = Signature)?
  | CharacterName (signature = Signature)?
  | StringName (signature = Signature)?
  | Others
  | All
;

BlockConfiguration:
  'for' name = Name
    ( use += UseClause )*
    ( item += ConfigurationItem )*
  'end' 'for' ';'
;

/*
 * label : block [ ( guard expression ) ] [ is ]
 *            [ generic clause [ generic map aspect ; ] ]
 *            [ port clause [ port map aspect ; ] ]
 *            [ block declarative items ]
 *         begin
 *            concurrent statements
 *         end block [ label ] ;
 */
BlockStatement:
  name = Label ':' 'block' ( '(' guard = Expression ')' )? ( 'is' )?
    ( generic = Generics ( genericMap = GenericMaps ';' )? )?
    ( port = Ports ( portMap = PortMaps ';' )? )?
    ( declaration += BlockDeclarativeItem )* 'begin'
    ( statement += ArchitectureStatement )* 
  'end' 'block' ( Identifier )? ';'
;

BreakElement:
  ( 'for' name = Name 'use' )? use = Name '=>' arrow = Expression
;

BreakStatement:
  {BreakStatement}
    ( name = Label ':' )? 'break' ( break += BreakElement ( ',' break += BreakElement )* )? ( 'when' when = Expression )? ';'
;

ConcurrentBreakStatement returns BreakStatement:
  {BreakStatement}
    ( name = Label ':' )? 'break' ( break += BreakElement ( ',' break += BreakElement )* )? ( 'on' sensitivity += Name ( ',' sensitivity += Name )* )?
    ( 'when' when = Expression )? ';'
;


/*
 * [ label: ] case  expression  is
 *              when choice1 =>
 *                 sequence-of-statements
 *              when choice2 =>            \_ optional
 *                 sequence-of-statements  /
 *              ...
 *
 *              when others =>             \_ optional if all choices covered
 *                 sequence-of-statements  /
 *            end case [ label ] ;
 */
CaseStatement:
  ( name = Label ':' )? 'case' case = Expression 'is'
      ( when += CaseAlternative )+
    'end' 'case' ( Identifier )? ';'
;

CaseAlternative:
  'when' choice += Choice ( '|' choice += Choice )* '=>' ( statement += SequentialStatement )*
;

ComponentConfiguration:
  'for' list = InstantiationList ':' component = Name
    ( 
      ('use' (
      	'entity' entity = Name |
      	'configuration' configuration = Name |
      	'open' )
      )? ( genericMap = GenericMaps )? ( portMap = PortMaps )? ';'
    )?
    ( block = BlockConfiguration )?
  'end' 'for' ';'
;

All returns NameElement:
  {All}
	name = 'all'
;

Others returns NameElement:
  {Others}
	name = 'others'
;

/*
 * component component_name is
 *    generic ( generic_variable_declarations ) ; -- optional
 *    port ( input_and_output_variable_declarations ) ;
 * end component component_name ;
 */
ComponentDeclaration:
  'component' name = Identifier ( 'is' )?
    ( generic = Generics )?
    ( port = Ports )?
  'end' 'component' ( Identifier )? ';'
;

/*
 * part_name: entity  library_name.entity_name(architecture_name)
 *            port map ( actual arguments ) ; 
 *
 *            optional (architecture_name)
 * part_name: component_name
 *            port map ( actual arguments ) ;
 */
ComponentInstantiationStatement:
  name = Label ':' ( 'component' )? component = Name 
  ( genericMap = GenericMaps )? ( portMap = PortMaps )? ';'
;

EntityInstantiationStatement:
  name = Label ':' 'entity' entity = Name 
  ( genericMap = GenericMaps )? ( portMap = PortMaps )? ';'
;

ConfigurationInstantiationStatement:
  name = Label ':' 'configuration' configuration = Name 
  ( genericMap = GenericMaps )? ( portMap = PortMaps )? ';'
;

CompositeNatureDefinition:
    ArrayNatureDefinition
  | RecordNatureDefinition
;

CompositeTypeDefinition:
    ArrayTypeDefinition
  | RecordTypeDefinition
;

/*
 * [ label : ] sequential signal assignment statement
 *
 * target "<=" [ "guarded" ] [ delay_mechanism ] waveform ";".
 */
SequentialSignalAssignmentStatement:
  ( name = Label ':' )? ( postponed ?= 'postponed' )?
  target = Target '<=' ( guarded ?= 'guarded' )? ( delay = DelayMechanism )? 
   ('unaffected' | waveform += Waveform ( ',' waveform += Waveform )*) ';'
;

/*
 * [ label : ] [ postponed ] conditional_signal_assignment_statement ;
 *
 * target <= waveform when choice; -- choice is a boolean expression
 * target <= waveform when choice else waveform;
 */
ConditionalSignalAssignmentStatement:
  ( name = Label ':' )? ( postponed ?= 'postponed' )?
   target = Target '<=' ( guarded ?= 'guarded' )? ( delay = DelayMechanism )? 
   waveform += ConditionalWaveform ('else' waveform += ConditionalWaveform)* ';'
;

/*
 * [ label : ] [ postponed ] selected_signal_assignment_statement ;
 *
 * with expression select target <=
 *            waveform when choice [, waveform when choice ] ;
 */
SelectedSignalAssignmentStatement:
  ( name = Label ':' )? ( postponed ?= 'postponed' )?
    'with' expression = Expression 'select' target = Target '<=' 
  ( guarded ?= 'guarded' )? ( delay = DelayMechanism )? waveform += ConditionalWaveform 
  ( ',' waveform += ConditionalWaveform )* ';'
;

SimpleSimultaneousStatement:
  ( name = Label ':' )?
    left = SimpleExpression '==' right = SimpleExpression ( 'tolerance' tolerance = Expression )? ';'
;


ConditionalWaveform:
  ('unaffected' | waveform += Waveform ( ',' waveform += Waveform )*) 'when' choice += Choice ( '|' choice += Choice )*
;

ConfigurationDeclaration:
	'configuration' name = Identifier 'of' of = Name 'is'
	( declaration += ConfigurationDeclarativeItem )*
	block = BlockConfiguration
	'end' ( 'configuration' )? ( Identifier )? ';'
;

ConfigurationItem:
    BlockConfiguration
  | ComponentConfiguration
;

ConfigurationSpecification:
    'for' list = InstantiationList ':' 
    component = Name 
    ( 'use' ( 
      'entity' entity = Name |
      'configuration' configuration = Name |
      'open' ) 	
    )? ( genericMap = GenericMaps )? ( portMap = PortMaps )? ';'
;

DelayMechanism:
    Transport
  | RejectMechanism
;

RejectMechanism:
  {RejectMechanism}
    ('reject' reject = Expression)? 'inertial'
;

Transport:
  {Transport}
    'transport'
;

/*
 * disconnection_specification = "disconnect" guarded_signal_specification "after" time_expression ";".
 *
 * guarded_signal_specification = guarded_signal_list ":" type_mark.
 */
DisconnectionSpecification:
    'disconnect' disconnect = NameList ':' type = Name 'after' after = Expression ';'
;

NameList:
  name += Name ( ',' name += Name )*
;

ElementDeclaration:
    identifier += Identifier ( ',' identifier += Identifier )* ':' type = SubtypeIndication ';'
;

EntityDeclaration: 
  'entity' name = Identifier 'is' 
      ( generic = Generics )?
      ( port = Ports )?
      ( declaration += EntityDeclarativeItem )*
    ( 'begin' 
      ( statement += EntityStatement )*
    )?
    'end' ( 'entity' )? ( Identifier )? ';'
;

EntityStatement returns Statement:
    ConcurrentAssertionStatement
  | ProcessStatement
  | ConcurrentProcedureCallStatement
;

EnumerationTypeDefinition:
    '(' literal += EnumerationLiteral 
    ( ',' literal += EnumerationLiteral )* ')'
;

/*
 * [ label: ] exit [ label2 ] [ when condition ] ;
 */
ExitStatement:
  {ExitStatement}
    ( name = Label ':' )? 'exit' ( exit = Identifier )? ( 'when' when = Expression )? ';'
;

/*
 * Addition returns Expression:
 * Multiplication ({Addition.left=current} '+' right=Multiplication)*;
 */
Expression:
  Relation ({LogicalExpression.left=current} operator=LogicalOperator right=Relation)*
;

Relation returns Expression:
  ShiftExpression ({RelationalExpression.left=current} operator = RelationalOperator right = ShiftExpression)?
;

ShiftExpression returns Expression: 
  SimpleExpression ({ShiftExpression.left=current} operator=ShiftOperator right=SimpleExpression)?
;
SimpleExpression returns Expression: 
    TermExpression ({AddingExpression.left=current} operator = AddingOperator right = TermExpression)*
  | SignTermExpression ({AddingExpression.left=current} operator = AddingOperator right = TermExpression)*
;

SignTermExpression returns Expression:
  SignFactor ({MultiplyingExpression.left=current} operator = MultiplyingOperator right = Factor)*
;

TermExpression returns Expression:
  Factor ({MultiplyingExpression.left=current} operator = MultiplyingOperator right = Factor)*
;

Factor returns Expression: 
    Primary ({PowerExpression.left=current} '**' right = Primary) ?
  | UnaryExpression
;

UnaryExpression returns Expression:
  {UnaryExpression}operator = UnaryOperator expression = Primary
;


SignFactor returns Expression: 
    SignPrimary ({PowerExpression.left=current} '**' right = Primary) ?
  | SignUnaryExpression
;


SignUnaryExpression returns Expression:
  {SignExpression}sign = Sign expression = UnaryExpression
;

SignPrimary returns Expression:
  {SignExpression}sign = Sign expression = Primary	
;

Primary returns Expression: 
    Allocator
  | Name
  | Null
  | BitStringName
  | CharacterName
  | ValueExpression
  | Aggregate
;

FileDeclaration:
    'file' 
      name = CommaIdentifier 
    ':' type = SubtypeIndication ( ( 'open' open = Expression )? 'is' is = Expression )? ';'
;


InterfaceFileDeclaration returns FileDeclaration:
    'file' 
      name = CommaIdentifier 
    ':' type = SubtypeIndication
;

FileTypeDefinition:
    'file' 'of' file = Name
;

/*
 * label: for variable in range generate    -- label required
 *            block declarative items  \__ optional   
 *         begin                       /
 *            concurrent statements          -- using variable
 *         end generate label ;
 *
 *  label: if condition generate            -- label required
 *            block declarative items  \__ optional   
 *         begin                       /
 *            concurrent statements
 *         end generate label ;
 */
GenerateStatement:
  name = Label ':' scheme = GenerationScheme 
    'generate'
      ( ( declaration += BlockDeclarativeItem )* 'begin' )?
      ( statement += ArchitectureStatement )*
    'end' 'generate' ( Identifier )? ';'
;
    
GenerationScheme:
   ForGenerationScheme
 | IfGenerationScheme
;

ForGenerationScheme:
  'for' variable = Identifier 'in' in = DiscreteRange
;

IfGenerationScheme:
  'if' condition = Expression
;

Generics: 
    'generic' 
      '(' declaration += GenericDeclaration ( ';' declaration += GenericDeclaration )* ')' ';'
;

GenericMaps:
    'generic' 'map' '(' generic += Parameter ( ',' generic += Parameter )* ')'
;

/*
 * group identifier : group_template_name ( group_member [, group member] ) ;
 */
GroupDeclaration: 
    'group' name = Identifier ':' is = Name
    '(' member += GroupConstituent ( ',' member += GroupConstituent )* ')' ';'
;

GroupConstituent returns Expression:
  Name
 | CharacterName
;

GroupTemplateDeclaration:
    'group' name = Identifier 'is' '(' 
       entry += EntityClass ( '<>' )? 
       ( ',' entry += EntityClass ( '<>' )? )* 
     ')' ';'
;

/*
 * [ label: ] if  condition1  then
 *                 sequence-of-statements
 *             elsif  condition2  then      \_ optional
 *                  sequence-of-statements  /
 *             elsif  condition3  then      \_ optional
 *                  sequence-of-statements  /
 *             ...
 * 
 *             else                         \_ optional
 *                  sequence-of-statements  /
 *             end if [ label ] ;
 */
IfStatement:
    ( name = Label ':' )? 'if' test += IfStatementTest
    ( 'elsif' test += IfStatementTest )* 
    ( 'else' ( statement += SequentialStatement )* )?
    'end' 'if' ( Identifier )? ';'
;

IfStatementTest: 
  condition = Expression 'then' ( statement += SequentialStatement )*
;

Constraint:
    RangeConstraint
  | IndexConstraint
;

RangeConstraint returns Constraint:
  'range' range += Range
;

Range returns Expression:
   RangeSpecification
 | SimpleExpression
;

RangeSpecification returns NameElement:
  {Range} left = SimpleExpression direction = RangeDirection right = SimpleExpression
;

IndexConstraint returns Constraint:
  '(' range += DiscreteRange ( ',' range += DiscreteRange )* ')'
;

/*
 * discrete_range = discrete_subtype_indication
 *	              | range
 */
DiscreteRange:
    RangeSpecification
  | SubtypeIndication
;

GenericDeclaration returns Declaration: 
    InterfaceConstantDeclaration
;

PortDeclaration returns Declaration: 
    InterfaceSignalDeclaration
;

FunctionParameterDeclaration returns Declaration: 
    InterfaceConstantDeclaration
  | InterfaceSignalDeclaration
  | InterfaceVariableDeclaration
  | InterfaceFileDeclaration
;

ProcedureParameterDeclaration returns Declaration: 
    InterfaceVariableDeclaration
  | InterfaceSignalDeclaration
  | InterfaceConstantDeclaration
  | InterfaceFileDeclaration
;



/*
InterfaceQuantityDeclaration returns QuantityDeclaration:
    'quantity' 
      name += Identifier ( ',' name += Identifier )* 
    ':'  (direction = Direction )? type = SubtypeIndication ( ':=' quantity = Expression )?
;

InterfaceTerminalDeclaration returns TerminalDeclaration:
    'terminal'
      name += Identifier ( ',' name += Identifier )* 
    ':' nature = SubnatureIndication
;*/

/*
 * [ label: ] loop
 *                  sequence-of-statements -- use exit statement to get out
 *             end loop [ label ] ;
 * 
 *  [ label: ] for variable in range loop
 *                  sequence-of-statements
 *             end loop [ label ] ;
 * 
 *  [ label: ] while  condition  loop
 *                  sequence-of-statements
 *             end loop [ label ] ;
 */
LoopStatement:
  {LoopStatement}
    ( name = Label ':' )? ( iteration = IterationScheme )?
    'loop'
    ( statement += SequentialStatement )* 
    'end' 'loop' ( Identifier )? ';'
  ;

IterationScheme:
    WhileIterationScheme
  | ForIterationScheme
;

WhileIterationScheme:
  'while' condition = Expression
;

ForIterationScheme:
  'for' variable = Identifier 'in' in = DiscreteRange
;

Name returns Expression:
   {Name} element += NamePrefix element += NameSuffix (element += NameSuffix)*
 | NamePrefix 
//   {Name} element += NamePrefix (element += NameSuffix)*
;

NamePrefix returns NameElement:
    IdentifierName
  | StringName
  | All
  | Others
;

StringName returns NameElement:
  {String}name = STRING
;

CharacterName returns NameElement:
  {Character}name = CHAR
;

IdentifierName returns NameElement:
  {Identifier}name = Identifier
;

SelectedName returns NameElement:
  '.' (
      IdentifierName
    | CharacterName
    | StringName
    | All
  )
;

/* 
 * prefixed operator:
 *   SelectedName: prefix = Prefix '.' suffix = Suffix
 *   AttributeName: prefix = Prefix (signature = Signature )? '\'' AttributeDesignator ( '(' expression = Expression ')' )?	
 *   IndexedName: prefix = Prefix '(' index += Expression ( ',' index += Expression )* ')'
 *   SliceName: prefix = Prefix '(' range = DiscreteRange ')'
 */
NameSuffix returns NameElement:
    SelectedName
  | AttributeName
  | Aggregate
  | SliceName
;

SliceName returns NameElement:
	'(' RangeSpecification ')'
;

AttributeName returns Attribute:
  (signature = Signature )? name = ATTRIBUTE
;

AttributeDesignator:
    Identifier
  | 'range'
  | 'across'
  | 'through'
  | 'reference'
  | 'tolerance'
;
 
NatureDeclaration:
    'nature' name = Identifier 'is' is = NatureDefinition ';'
;

NatureDefinition:
    ScalarNatureDefinition
  | CompositeNatureDefinition
;

/*
 * [ label: ] next [ label2 ] [ when condition ] ;
 */
NextStatement:
  {NextStatement}
    ( name = Label ':' )? 'next' ( next = Identifier )? ( 'when' when = Expression )? ';'
;

/*
 * package body identifier is
 *     [ declarations, see allowed list below ]
 *  end package body identifier ;
 */
PackageBody:
    'package' 'body' name = Identifier 'is'
      ( declaration += PackageBodyDeclarativeItem )*
    'end' ( 'package' 'body' )? ( Identifier )? ';'
;

/*
 * package identifier is
 *     [ declarations, see allowed list below ]
 * end package identifier ;
 */
PackageDeclaration:
    'package' name = Identifier 'is'
      ( declaration += PackageDeclarativeItem )*
    'end' ( 'package' )? ( Identifier )? ';'
;

/*
 *  range left_bound to right_bound
 *  units
 *    primary_unit_name
 *    (secondary_unit_name = number primary_unit_name)*
 * end units type_name
 */
PhysicalTypeDefinition:
    range = RangeConstraint 'units' 
      primary = Identifier ';'
      ( secondary += PhysicalTypeDefinitionSecondary )* 
    'end' 'units' ( Identifier )?
;

PhysicalTypeDefinitionSecondary:
  name = Identifier '=' ( number = ABSTRACT_LITERAL )? of = Name ';'
;
  
Ports: 
  'port' '(' declaration += PortDeclaration ( ';' declaration += PortDeclaration )* ')' ';'
;

PortMaps:
  'port' 'map' '(' port += Parameter ( ',' port += Parameter )* ')'
;

ProcedureCallStatement:
  ( name = Label ':' )? 
    procedure = Name  ';'
;

ConcurrentProcedureCallStatement returns ProcedureCallStatement: 
  ( name = Label ':' )? postponed ?= 'postponed' 
    procedure = Name ';'
;

/*
 * label : process [ ( sensitivity_list ) ] [ is ]
 *            [ process_declarative_items ]
 *         begin
 *            sequential statements
 *         end process [ label ] ;
 */
ProcessStatement:
  {ProcessStatement}
    ( name = Label ':' )? ( postponed ?= 'postponed' )? 
    'process' 
      ( '(' sensitivity += Identifier ( ',' sensitivity += Identifier )* ')' )? 
      ( 'is' )?
      ( declaration += ProcessDeclarativeItem )*
    'begin'
      ( statement += SequentialStatement )* 
    'end' ( 'postponed' )? 'process' ( Identifier )? ';'
;

/*
 * allocator = "new" subtype_indication
 *	         | "new" qualified_expression
 */
Allocator:
   'new' allocate = SubtypeIndication
;

RecordNatureDefinition:
    'record' 
      ( record += RecordNatureElement )+
    'end' 'record' ( Identifier )?
;

RecordNatureElement:
    name += Identifier ( ',' name += Identifier )* ':' nature = SubnatureIndication
;

RecordTypeDefinition:
    'record' ( declaration += ElementDeclaration )+
    'end' 'record' ( Identifier )?
;

/*
 * [ label: ] report string [ severity name ] ;
 */
ReportStatement:
  ( name = Label ':' )? 'report' report = Expression ( 'severity' severity = Expression )? ';'
;

/*
 * [ label: ] return [ expression ] ;
 */
ReturnStatement:
  {ReturnStatement}
    ( name = Label ':' )? 'return' ( return = Expression )? ';'
;

ScalarNatureDefinition:
    name = Name 'across' across = Name 'through' through = Name 'reference'
;

SequentialStatement returns Statement:
    WaitStatement
  | AssertionStatement
  | ReportStatement
  | IfStatement
  | CaseStatement
  | LoopStatement
  | NextStatement
  | ExitStatement
  | ReturnStatement
  | NullStatement
  | BreakStatement
  | VariableAssignmentStatement
  | ProcedureCallStatement
  | SignalAssignmentStatement
;

NullStatement:
  {NullStatement}
    ( name = Label ':' )? 'null' ';'
;

/*
 * [ label: ] target <= [ delay_mechanism ] waveform ;
 */
SignalAssignmentStatement:
  ( name = Label ':' )? target = Target '<=' 
  ( delay = DelayMechanism )? 
   ('unaffected' | waveform += Waveform ( ',' waveform += Waveform )*) 
    ';'
;

/*
 * [ label: ] target := expression ;
 */
VariableAssignmentStatement:
  ( name = Label ':' )? target = Target ':=' initial = Expression ';'
;

SignalDeclaration:
  'signal' name = CommaIdentifier ':'
  type = SubtypeIndication ( kind = SignalKind )?  ( ':=' initial = Expression )? ';'
;

VariableDeclaration:
  ( ( shared ?= 'shared' ) )? 'variable' name = CommaIdentifier 
    ':' type = SubtypeIndication ( ':=' initial = Expression )? ';'
;

ConstantDeclaration:
  'constant' name = CommaIdentifier ':' type = SubtypeIndication
    ( ':=' initial = Expression )? ';'
;
  
InterfaceSignalDeclaration returns SignalDeclaration:
  ('signal')? name = CommaIdentifier ':' ( mode = Mode )?
  type = SubtypeIndication ( kind = SignalKind )?  ( ':=' initial = Expression )?
;

InterfaceVariableDeclaration returns VariableDeclaration:
  ( 'variable' )? name = CommaIdentifier ':' ( mode = Mode )?
   type = SubtypeIndication  ( ':=' initial = Expression )?
;

InterfaceConstantDeclaration returns ConstantDeclaration:
  ( 'constant' )?  name = CommaIdentifier ':' ( 'in' )?
   type = SubtypeIndication  ( ':=' initial = Expression )?
;
  
/*
 * signature = [ [ type_mark { "," type_mark } ] [ "return" type_mark ] ].
 */
Signature:
  {Signature}
    '[' ( name += Name ( ',' name += Name )* )? ( 'return' return = Name )? ']'
;

SimultaneousAlternative:
  'when' choice += Choice ( '|' choice += Choice )* '=>' ( statememt += SimultaneousStatement )*
;

SimultaneousCaseStatement:
    ( name = Label ':' )? 'case' case = Expression 'use'
    ( when += SimultaneousAlternative )+ 
    'end' 'case' ( Identifier )? ';'
;

SimultaneousIfStatement:
  ( name = Label ':' )? 'if' test += SimultaneousIfStatementTest 'use'
    ( 'elsif' test += SimultaneousIfStatementTest )*
    ( 'else' ( statememt += SimultaneousStatement )* )?
    'end' 'use' ( Identifier )? ';'
;
  
SimultaneousIfStatementTest:
    condition = Expression 'use' ( statememt += SimultaneousStatement )*
;

SimultaneousProceduralStatement:
  {SimultaneousProceduralStatement}
    ( name = Label ':' )? 'procedural' ( 'is' )?
    ( declaration += ProceduralDeclarativeItem )* 'begin'
    ( statement += SequentialStatement )* 
    'end' 'procedural' ( Identifier )? ';'
;

SimultaneousStatement returns Statement:
    SimpleSimultaneousStatement
  | SimultaneousIfStatement
  | SimultaneousCaseStatement
  | SimultaneousProceduralStatement
  | NullStatement
;

SourceAspect:
   Spectrum
 | Noise
;

Spectrum:
  'spectrum' left = SimpleExpression ',' right = SimpleExpression
;

Noise:
  'noise' noise = SimpleExpression
;

QuantityDeclaration:
    FreeQuantityDeclaration
  | BranchQuantityDeclaration
  | SourceQuantityDeclaration
;

FreeQuantityDeclaration:
    'quantity' 
      name = CommaIdentifier 
    ':' type = SubtypeIndication ( ':=' quantity = Expression )? ';'
;

BranchQuantityDeclaration:
  'quantity' ( across = QuantityAspect 'across')?
    ( through = QuantityAspect 'through')? left = Name ( 'to' right = Name )? ';'
;

/* 
 * across_aspect: identifier_list ( tolerance_aspect )? ( VARASGN expression )? ACROSS;
 */
QuantityAspect:
  name += Identifier ( ',' name += Identifier )* ( 'tolerance' tolerance = Expression )? 
  ( ':=' expression = Expression )?
;

SourceQuantityDeclaration:
    'quantity' 
      name = CommaIdentifier 
    ':' type = SubtypeIndication source = SourceAspect ';'
;

LimitSpecification:
  'limit' limit = NameList ':' type = Name 'with' value = Expression ';'
;

SubnatureDeclaration: 
  'subnature' name = Identifier 'is' nature = SubnatureIndication ';'
;

SubprogramDeclaration:
    specification = SubprogramSpecification ('is'
      ( declaration += SubprogramDeclarativeItem )*
    'begin'
    ( statement += SequentialStatement )*
    'end' ( 'procedure' | 'function' )? ( Designator )?) ? ';'
;

SubprogramSpecification:
    'procedure' name = Designator ( '(' declaration += ProcedureParameterDeclaration ( ';' declaration += ProcedureParameterDeclaration )* ')' )?
  | purity = ( 'pure' | 'impure' )? 'function' name = Designator ( '(' declaration += FunctionParameterDeclaration ( ';' declaration += FunctionParameterDeclaration )* ')' )? 'return' return = Name
;

SubtypeDeclaration:
  'subtype' name = Identifier 'is' type = SubtypeIndication ';'
;

AliasDeclaration:
  'alias' name = Designator ( ':' alias = AliasIndication )? 'is'
    is = Name ( signature = Signature )? ';'
;

AliasIndication returns Expression:
    Name {Indication.mark+=current} mark += Name constraint = Constraint ('tolerance' tolerance = Expression )?
  | Name {Indication.mark+=current} mark += Name ('tolerance' tolerance = Expression )?
  | Name {Indication.mark+=current} constraint = Constraint ('tolerance' tolerance = Expression ('across' across = Expression 'through')?)?
  | Name ({Indication.mark+=current} 'tolerance' tolerance = Expression ('across' across = Expression 'through')?)?  
;

/*
 * NAME_PATTERN (NAME_PATTERN)? = [ resolution_indication ] type_mark [ constraint ].
 */
SubtypeIndication returns Expression:
    Name {Indication.mark+=current} mark += Name constraint = Constraint ('tolerance' tolerance = Expression )?
  | Name {Indication.mark+=current} mark += Name ('tolerance' tolerance = Expression )?
  | Name {Indication.mark+=current} constraint = Constraint ('tolerance' tolerance = Expression )?
  | Name ({Indication.mark+=current} 'tolerance' tolerance = Expression )?
;

SubnatureIndication returns Expression:
    Name {Indication.mark+=current} constraint = IndexConstraint ('tolerance' tolerance = Expression 'across' across = Expression 'through')?
  | Name ({Indication.mark+=current} 'tolerance' tolerance = Expression 'across' across = Expression 'through')?
;

Target returns Expression:
    Name
  | Aggregate
;

TerminalDeclaration:
    'terminal' 
      name = CommaIdentifier 
    ':' nature = SubnatureIndication ';'
;

TypeDeclaration:
    'type' name = Identifier ( 'is' is = TypeDefinition )? ';'
;
 
TypeDefinition:
    PhysicalTypeDefinition
  | EnumerationTypeDefinition
  | CompositeTypeDefinition
  | AccessTypeDefinition
  | FileTypeDefinition
  | RangeTypeDefinition
;

RangeTypeDefinition:
   {RangeTypeDefinition} 'range' left = SimpleExpression direction = RangeDirection right = SimpleExpression
 | {RangeTypeDefinition} 'range' '<>'
;

UnconstrainedArrayDefinition:
    'array' 
      '(' 
        index += Name 'range' '<>' ( ',' index += Name 'range' '<>' )* 
      ')' 
    'of' type = SubtypeIndication
;

UnconstrainedNatureDefinition:
    'array' 
      '(' 
        index += Name 'range' '<>' ( ',' index += Name 'range' '<>' )* 
      ')' 
    'of' nature = SubnatureIndication
;

ConstrainedArrayDefinition:
    'array' constraint = IndexConstraint 'of' type = SubtypeIndication
;

ConstrainedNatureDefinition:
    'array' constraint = IndexConstraint 'of' nature = SubnatureIndication
;

/*
 * wait until condition;
 * wait on signal_list;
 * wait for time;
 * wait;
 */
WaitStatement:
  {WaitStatement}
    ( name = Label ':' )? 'wait' ( 'on' sensitivity += Name ( ',' sensitivity += Name )* )? 
    ( 'until' until = Expression )? ( 'for' time = Expression )? ';'
;

/*
 * value_expression [ after time_expression ]
 */
Waveform returns Expression: 
  Expression ( {Waveform.expression=current} 'after' after = Expression )?
;

Null:
  {Null}
    name = 'null'
;

BitStringName returns NameElement:
  {BitString}name = BIT_STRING_LITERAL
;

ValueExpression returns Value: 
    {UnitValue} number = ABSTRACT_LITERAL unit = Name
  | {Value} number = ABSTRACT_LITERAL
;

EnumerationLiteral returns NameElement:
    IdentifierName
  | CharacterName
;

enum SignalKind:
    REGISTER = 'register' 
  | BUS      = 'bus'
;

enum RangeDirection:
    TO     = 'to'
  | DOWNTO = 'downto'
;

/*
enum Direction:
    IN  = 'in'
  | OUT = 'out'
;
*/

enum Mode:
    IN      = 'in'
  | OUT     = 'out'
  | INOUT   = 'inout'
  | BUFFER  = 'buffer'
  | LINKAGE = 'linkage'
;

enum UnaryOperator:
    ABS = 'abs'
  | NOT = 'not'
;

enum MultiplyingOperator:
    MUL = '*'
  | DIV = '/'
  | MOD = 'mod'
  | REM = 'rem'
;

enum ShiftOperator:
    SLL = 'sll'
  | SRL = 'srl'
  | SLA = 'sla'
  | SRA = 'sra'
  | ROL = 'rol'
  | ROR = 'ror'
;

enum RelationalOperator:
    EQ          = '='
  | NEQ         = '/='
  | LOWERTHAN   = '<'
  | LE          = '<='
  | GREATERTHAN = '>'
  | GE          = '>='
;

enum LogicalOperator:
    AND  = 'and'
  | OR   = 'or'
  | NAND = 'nand'
  | NOR  = 'nor'
  | XOR  = 'xor'
  | XNOR = 'xnor'
;

enum AddingOperator:
    PLUS      = '+'
  | MINUS     = '-'
  | AMPERSAND = '&'
;

enum Sign:
    PLUS = '+'
  | MINUS = '-'
;

enum EntityClass:
    ENTITY        = 'entity'
  | ARCHITECTURE  = 'architecture'
  | CONFIGURATION = 'configuration'
  | PROCEDURE     = 'procedure'
  | FUNCTION      = 'function'
  | PACKAGE       = 'package'
  | TYPE          = 'type'
  | SUBTYPE       = 'subtype'
  | CONSTANT      = 'constant'
  | SIGNAL        = 'signal'
  | VARIABLE      = 'variable'
  | COMPONENT     = 'component'
  | LABEL         = 'label'
  | LITERAL       = 'literal'
  | UNITS         = 'units'
  | GROUP         = 'group'
  | FILE          = 'file'
  | NATURE        = 'nature'
  | SUBNATURE     = 'subnature'
  | QUANTITY      = 'quantity'
  | TERMINAL      = 'terminal'
;

Designator:
    Identifier
  | STRING
;


CommaIdentifier:
	Identifier (',' Identifier)*
;

Identifier:
    ID
  | EXTENDED_IDENTIFIER
;

terminal ABSTRACT_LITERAL: 
  INTEGER_FRAGMENT (
    ( "." INTEGER_FRAGMENT )? 
      | "#" BASED_INTEGER_FRAGMENT ( "." BASED_INTEGER_FRAGMENT )? "#"
    ) ( EXPONENT_FRAGMENT )?
;

terminal BIT_STRING_LITERAL: ( INTEGER_FRAGMENT )? BASE_SPECIFIER_FRAGMENT STRING;

terminal ID:
  '^'?LETTER_FRAGMENT ( '_' | LETTER_OR_DIGIT_FRAGMENT )*
;

terminal EXTENDED_IDENTIFIER: 
  '\\' ( '\\' ('b'|'t'|'n'|'f'|'r'|'"'|"'"|'\\') | !('\\' | '\r' | '\n') )+ '\\'
;

terminal STRING : 
  '"' ( '\\' ('b'|'t'|'n'|'f'|'r'|'"'|"'"|'\\') | !('\\' | '"' | '\r' | '\n') )* '"';

terminal CHAR:
    "'a'" | "'b'" | "'c'"  | "'d'" | "'e'" | "'f'" | "'g'" | "'h'" | "'i'" 
  | "'j'" | "'k'" | "'l'"  | "'m'" | "'n'" | "'o'" | "'p'" | "'q'" | "'r'" 
  | "'s'" | "'t'" | "'u'"  | "'v'" | "'w'" | "'x'" | "'y'" | "'z'" | "'A'" 
  | "'B'" | "'C'" | "'D'"  | "'E'" | "'F'" | "'G'" | "'H'" | "'I'" | "'J'" 
  | "'K'" | "'L'" | "'M'"  | "'N'" | "'O'" | "'P'" | "'Q'" | "'R'" | "'S'" 
  | "'T'" | "'U'" | "'V'"  | "'W'" | "'X'" | "'Y'" | "'Z'" | "'0'" | "'1'"
  | "'2'" | "'3'" | "'4'"  | "'5'" | "'6'" | "'7'" | "'8'" | "'9'" | "'&'" 
  | "'''" | "'('" | "')'"  | "'+'" | "','" | "'-'" | "'.'" | "'/'" | "':'" 
  | "';'" | "'<'" | "'='"  | "'>'" | "'|'" | "' '" | "'#'" | "'['" | "']'" 
  | "'_'" | "'*'" | "'\"'" | "'!'" | "'$'" | "'%'" | "'@'" | "'?'" | "'^'" 
  | "'`'" | "'{'" | "'}'"  | "'~'" | "' '" | "'¡'" | "'¢'" | "'£'" | "'€'"
  | "'¥'" | "'Š'" | "'§'"  | "'š'" | "'©'" | "'ª'" | "'«'" | "'¬'" | "''" 
  | "'®'" | "'¯'" | "'°'"  | "'±'" | "'²'" | "'³'" | "'Ž'" | "'µ'" | "'¶'"
  | "'·'" | "'ž'" | "'¹'"  | "'º'" | "'»'" | "'Œ'" | "'œ'" | "'Ÿ'" | "'¿'"
  | "'À'" | "'Á'" | "'Â'"  | "'Ã'" | "'Ä'" | "'Å'" | "'Æ'" | "'Ç'" | "'È'" 
  | "'É'" | "'Ê'" | "'Ë'"  | "'Ì'" | "'Í'" | "'Î'" | "'Ï'" | "'Ð'" | "'Ñ'"
  | "'Ò'" | "'Ó'" | "'Ô'"  | "'Õ'" | "'Ö'" | "'×'" | "'Ø'" | "'Ù'" | "'Ú'" 
  | "'Û'" | "'Ü'" | "'Ý'"  | "'Þ'" | "'ß'" | "'à'" | "'á'" | "'â'" | "'ã'" 
  | "'ä'" | "'å'" | "'æ'"  | "'ç'" | "'è'" | "'é'" | "'ê'" | "'ë'" | "'ì'"
  | "'í'" | "'î'" | "'ï'"  | "'ð'" | "'ñ'" | "'ò'" | "'ó'" | "'ô'" | "'õ'"
  | "'ö'" | "'÷'" | "'ø'"  | "'ù'" | "'ú'" | "'û'" | "'ü'" | "'ý'" | "'þ'" 
  | "'ÿ'"
;

terminal ATTRIBUTE:
  "'"LETTER_FRAGMENT ( '_' | LETTER_OR_DIGIT_FRAGMENT )*
;

terminal SL_COMMENT: '--' !('\n'|'\r')* ('\r'? '\n')? ;
terminal WS: (' ' | '\t' | '\r' | '\n')+;

terminal fragment BASED_INTEGER_FRAGMENT: 
  LETTER_OR_DIGIT_FRAGMENT ( '_' | LETTER_OR_DIGIT_FRAGMENT )*
;

terminal fragment LETTER_OR_DIGIT_FRAGMENT: LETTER_FRAGMENT | DIGIT_FRAGMENT;

terminal fragment LETTER_FRAGMENT:
    ('a'..'z' )| ('A'..'Z')
;

terminal fragment BASE_SPECIFIER_FRAGMENT: 
    "B"
  | "O"
  | "X"
  | "UB"
  | "UO"
  | "UX"
  | "SB"
  | "SO"
  | "SX"
  | "D"
  | "b"
  | "o"
  | "x"
  | "ub"
  | "uo"
  | "ux"
  | "sb"
  | "so"
  | "sx"
  | "d"
;

terminal fragment EXPONENT_FRAGMENT:
  ("E" | "e") ( "+" | "-" )? INTEGER_FRAGMENT
;

terminal fragment INTEGER_FRAGMENT: 
  DIGIT_FRAGMENT ( '_' | DIGIT_FRAGMENT )*
;

terminal fragment DIGIT_FRAGMENT: '0'..'9';

