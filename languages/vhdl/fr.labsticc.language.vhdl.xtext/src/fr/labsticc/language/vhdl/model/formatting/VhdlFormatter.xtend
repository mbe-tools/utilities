/*
 * generated by Xtext
 */
package fr.labsticc.language.vhdl.model.formatting

import com.google.inject.Inject
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig
import fr.labsticc.language.vhdl.model.services.VhdlGrammarAccess

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
public class VhdlFormatter extends AbstractDeclarativeFormatter {
	
	@Inject extension VhdlGrammarAccess
	
	override protected configureFormatting(FormattingConfig c) {
		c.setAutoLinewrap(120);
		
		for(dot: findKeywords('.')) {
			c.setNoLinewrap().around(dot)
			c.setNoSpace().around(dot)
		}
		
		for(quote: findKeywords('\'')) {
			c.setNoLinewrap().around(quote)
			c.setNoSpace().around(quote)
		}		
		
		for (semi : findKeywords(';')) {
			c.setNoSpace().before(semi);
			c.setLinewrap().after(semi);
		}

		for (begin : findKeywords("begin")) {
		      c.setLinewrap().around(begin);
		}
		
		for (is : findKeywords("is")) {
		      c.setLinewrap().after(is);
		}

		for (end : findKeywords("end")) {
		      c.setLinewrap().before(end);
		}

		for (then : findKeywords("then")) {
			c.setIndentationIncrement().after(then)
			c.setLinewrap().after(then)
		}
		
		for (loop : findKeywords("loop")) {
			c.setIndentationIncrement().after(loop)
			c.setLinewrap().after(loop)
		}
		
		// entity declaration
		c.setLinewrap(2).before(getEntityDeclarationAccess().getEntityKeyword_0());
		
		// architecture		
		c.setLinewrap(2).before(getArchitectureAccess().getArchitectureKeyword_0());		
		c.setIndentationIncrement().after(getArchitectureAccess().getIsKeyword_4());
		c.setIndentationDecrement().before(getArchitectureAccess().getBeginKeyword_6());
		c.setIndentationIncrement().after(getArchitectureAccess().getBeginKeyword_6());
		c.setIndentationDecrement().before(getArchitectureAccess().getEndKeyword_8());
		
		/* ...
		 * TAB port map (
		 * TAB TAB ...
		 * TAB TAB ...
		 * TAB )
		 * ... 
		 */
		c.setLinewrap().before(getPortMapsAccess().getPortKeyword_0());		
		c.setIndentationIncrement().before(getPortMapsAccess().getPortKeyword_0());	
		c.setLinewrap().after(getPortMapsAccess().getLeftParenthesisKeyword_2());		
		c.setIndentationIncrement().after(getPortMapsAccess().getLeftParenthesisKeyword_2());
		c.setNoSpace().before(getPortMapsAccess().getCommaKeyword_4_0());
		c.setLinewrap().after(getPortMapsAccess().getCommaKeyword_4_0());
		c.setLinewrap().before(getPortMapsAccess().getRightParenthesisKeyword_5());		
		c.setIndentationDecrement().before(getPortMapsAccess().getRightParenthesisKeyword_5());		
		c.setIndentationDecrement().after(getPortMapsAccess().getRightParenthesisKeyword_5());		

		/* ...
		 * TAB generic map (
		 * TAB TAB ...
		 * TAB TAB ...
		 * TAB )
		 * ... 
		 */
		c.setLinewrap().before(getGenericMapsAccess().getGenericKeyword_0());		
		c.setIndentationIncrement().before(getGenericMapsAccess().getGenericKeyword_0());	
		c.setLinewrap().after(getGenericMapsAccess().getLeftParenthesisKeyword_2());		
		c.setIndentationIncrement().after(getGenericMapsAccess().getLeftParenthesisKeyword_2());
		c.setNoSpace().before(getGenericMapsAccess().getCommaKeyword_4_0());
		c.setLinewrap().after(getGenericMapsAccess().getCommaKeyword_4_0());
		c.setLinewrap().before(getGenericMapsAccess().getRightParenthesisKeyword_5());		
		c.setIndentationDecrement().before(getGenericMapsAccess().getRightParenthesisKeyword_5());		
		c.setIndentationDecrement().after(getGenericMapsAccess().getRightParenthesisKeyword_5());		

		/* ...
		 * TAB port (
		 * TAB TAB ...
		 * TAB TAB ...
		 * TAB );
		 * ... 
		 */
		c.setLinewrap().before(getPortsAccess().getPortKeyword_0());		
		c.setIndentationIncrement().before(getPortsAccess().getPortKeyword_0());	
		c.setLinewrap().after(getPortsAccess().getLeftParenthesisKeyword_1());		
		c.setIndentationIncrement().after(getPortsAccess().getLeftParenthesisKeyword_1());
		c.setLinewrap().before(getPortsAccess().getRightParenthesisKeyword_4());		
		c.setIndentationDecrement().before(getPortsAccess().getRightParenthesisKeyword_4());		
		c.setIndentationDecrement().after(getPortsAccess().getRightParenthesisKeyword_4());		
					
		/* ...
		 * TAB generic (
		 * TAB TAB ...
		 * TAB TAB ...
		 * TAB );
		 * ...
		 */
		c.setLinewrap().before(getGenericsAccess().getGenericKeyword_0());		
		c.setIndentationIncrement().before(getGenericsAccess().getGenericKeyword_0());
		c.setLinewrap().after(getGenericsAccess().getLeftParenthesisKeyword_1());		
		c.setIndentationIncrement().after(getGenericsAccess().getLeftParenthesisKeyword_1());		
		c.setLinewrap().before(getGenericsAccess().getRightParenthesisKeyword_4());		
		c.setIndentationDecrement().before(getGenericsAccess().getRightParenthesisKeyword_4());		
		c.setIndentationDecrement().after(getGenericsAccess().getRightParenthesisKeyword_4());		
		
		/* component */
		c.setLinewrap(2).before(getComponentDeclarationAccess().getComponentKeyword_0());
			
		/* label : entity */
		c.setLinewrap(2).before(getEntityInstantiationStatementAccess().getNameAssignment_0());

		/*
		 * label : process [ ( sensitivity_list ) ] [ is ]
		 *            [ process_declarative_items ]
		 *         begin
		 *            sequential statements
		 *         end process [ label ] ;
		 */
//		c.setLinewrap().after(getProcessStatementAccess().getLeftParenthesisKeyword_4_0());		
//		c.setIndentationIncrement().after(getProcessStatementAccess().getLeftParenthesisKeyword_4_0());		
//		c.setLinewrap().before(getProcessStatementAccess().getRightParenthesisKeyword_4_3());		
//		c.setIndentationDecrement().before(getProcessStatementAccess().getRightParenthesisKeyword_4_3());		
		c.setIndentationIncrement().after(getProcessStatementAccess().getBeginKeyword_7());
		c.setIndentationDecrement().before(getProcessStatementAccess().getEndKeyword_9());

		/*
		 * [ label: ] case  expression  is
		 *              when choice1 =>
		 *                 sequence-of-statements
		 *              when choice2 =>            \_ optional
		 *                 sequence-of-statements  /
		 *              ...
		 *
		 *              when others =>             \_ optional if all choices covered
		 *                 sequence-of-statements  /
		 *            end case [ label ] ;
		 */
		c.setIndentationIncrement().after(getCaseStatementAccess().getIsKeyword_3());
		c.setIndentationIncrement().after(getCaseStatementAccess().getIsKeyword_3()); // twice
		c.setIndentationDecrement().before(getCaseAlternativeAccess().getWhenKeyword_0());
		c.setIndentationIncrement().after(getCaseAlternativeAccess().getEqualsSignGreaterThanSignKeyword_3());
		c.setLinewrap().after(getCaseAlternativeAccess().getEqualsSignGreaterThanSignKeyword_3());
		c.setIndentationDecrement().before(getCaseStatementAccess().getEndKeyword_5());
		c.setIndentationDecrement().before(getCaseStatementAccess().getEndKeyword_5()); // twice
		

		c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
//		c.setLinewrap(0, 1, 2).before(ML_COMMENTRule)
//		c.setLinewrap(0, 1, 1).after(ML_COMMENTRule)
	}
}
