package fr.labsticc.language.vhdl.model.serializer;

import com.google.inject.Inject;
import fr.labsticc.language.vhdl.model.services.VhdlGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.GroupAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public abstract class AbstractVhdlSyntacticSequencer extends AbstractSyntacticSequencer {

	protected VhdlGrammarAccess grammarAccess;
	protected AbstractElementAlias match_Architecture_ArchitectureKeyword_9_q;
	protected AbstractElementAlias match_Architecture_IdentifierParserRuleCall_10_q;
	protected AbstractElementAlias match_BlockStatement_IdentifierParserRuleCall_12_q;
	protected AbstractElementAlias match_BlockStatement_IsKeyword_4_q;
	protected AbstractElementAlias match_CaseStatement_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_ComponentConfiguration___UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q;
	protected AbstractElementAlias match_ComponentConfiguration_____UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q_SemicolonKeyword_4_3__q;
	protected AbstractElementAlias match_ComponentDeclaration_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_ComponentDeclaration_IsKeyword_2_q;
	protected AbstractElementAlias match_ComponentInstantiationStatement_ComponentKeyword_2_q;
	protected AbstractElementAlias match_ConfigurationDeclaration_ConfigurationKeyword_8_q;
	protected AbstractElementAlias match_ConfigurationDeclaration_IdentifierParserRuleCall_9_q;
	protected AbstractElementAlias match_ConfigurationSpecification___UseKeyword_4_0_OpenKeyword_4_1_2__q;
	protected AbstractElementAlias match_EntityDeclaration_BeginKeyword_6_0_q;
	protected AbstractElementAlias match_EntityDeclaration_EntityKeyword_8_q;
	protected AbstractElementAlias match_EntityDeclaration_IdentifierParserRuleCall_9_q;
	protected AbstractElementAlias match_GenerateStatement_BeginKeyword_4_1_q;
	protected AbstractElementAlias match_GenerateStatement_IdentifierParserRuleCall_8_q;
	protected AbstractElementAlias match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_5_q;
	protected AbstractElementAlias match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_6_2_q;
	protected AbstractElementAlias match_IfStatement_ElseKeyword_4_0_q;
	protected AbstractElementAlias match_IfStatement_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_InterfaceConstantDeclaration_ConstantKeyword_0_q;
	protected AbstractElementAlias match_InterfaceConstantDeclaration_InKeyword_3_q;
	protected AbstractElementAlias match_InterfaceSignalDeclaration_SignalKeyword_0_q;
	protected AbstractElementAlias match_InterfaceVariableDeclaration_VariableKeyword_0_q;
	protected AbstractElementAlias match_LoopStatement_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_PackageBody_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_PackageBody___PackageKeyword_6_0_BodyKeyword_6_1__q;
	protected AbstractElementAlias match_PackageDeclaration_IdentifierParserRuleCall_6_q;
	protected AbstractElementAlias match_PackageDeclaration_PackageKeyword_5_q;
	protected AbstractElementAlias match_PhysicalTypeDefinition_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_ProcessStatement_IdentifierParserRuleCall_12_q;
	protected AbstractElementAlias match_ProcessStatement_IsKeyword_5_q;
	protected AbstractElementAlias match_ProcessStatement_PostponedKeyword_10_q;
	protected AbstractElementAlias match_RecordNatureDefinition_IdentifierParserRuleCall_4_q;
	protected AbstractElementAlias match_RecordTypeDefinition_IdentifierParserRuleCall_4_q;
	protected AbstractElementAlias match_SimultaneousCaseStatement_IdentifierParserRuleCall_7_q;
	protected AbstractElementAlias match_SimultaneousIfStatement_ElseKeyword_5_0_q;
	protected AbstractElementAlias match_SimultaneousIfStatement_IdentifierParserRuleCall_8_q;
	protected AbstractElementAlias match_SimultaneousProceduralStatement_IdentifierParserRuleCall_9_q;
	protected AbstractElementAlias match_SimultaneousProceduralStatement_IsKeyword_3_q;
	protected AbstractElementAlias match_SubprogramDeclaration_DesignatorParserRuleCall_1_6_q;
	protected AbstractElementAlias match_SubprogramDeclaration___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q;
	protected AbstractElementAlias match_SubprogramDeclaration___IsKeyword_1_0_BeginKeyword_1_2_EndKeyword_1_4___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q_DesignatorParserRuleCall_1_6_q__q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (VhdlGrammarAccess) access;
		match_Architecture_ArchitectureKeyword_9_q = new TokenAlias(false, true, grammarAccess.getArchitectureAccess().getArchitectureKeyword_9());
		match_Architecture_IdentifierParserRuleCall_10_q = new TokenAlias(false, true, grammarAccess.getArchitectureAccess().getIdentifierParserRuleCall_10());
		match_BlockStatement_IdentifierParserRuleCall_12_q = new TokenAlias(false, true, grammarAccess.getBlockStatementAccess().getIdentifierParserRuleCall_12());
		match_BlockStatement_IsKeyword_4_q = new TokenAlias(false, true, grammarAccess.getBlockStatementAccess().getIsKeyword_4());
		match_CaseStatement_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getCaseStatementAccess().getIdentifierParserRuleCall_7());
		match_ComponentConfiguration___UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getComponentConfigurationAccess().getUseKeyword_4_0_0()), new TokenAlias(false, false, grammarAccess.getComponentConfigurationAccess().getOpenKeyword_4_0_1_2()));
		match_ComponentConfiguration_____UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q_SemicolonKeyword_4_3__q = new GroupAlias(false, true, new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getComponentConfigurationAccess().getUseKeyword_4_0_0()), new TokenAlias(false, false, grammarAccess.getComponentConfigurationAccess().getOpenKeyword_4_0_1_2())), new TokenAlias(false, false, grammarAccess.getComponentConfigurationAccess().getSemicolonKeyword_4_3()));
		match_ComponentDeclaration_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getComponentDeclarationAccess().getIdentifierParserRuleCall_7());
		match_ComponentDeclaration_IsKeyword_2_q = new TokenAlias(false, true, grammarAccess.getComponentDeclarationAccess().getIsKeyword_2());
		match_ComponentInstantiationStatement_ComponentKeyword_2_q = new TokenAlias(false, true, grammarAccess.getComponentInstantiationStatementAccess().getComponentKeyword_2());
		match_ConfigurationDeclaration_ConfigurationKeyword_8_q = new TokenAlias(false, true, grammarAccess.getConfigurationDeclarationAccess().getConfigurationKeyword_8());
		match_ConfigurationDeclaration_IdentifierParserRuleCall_9_q = new TokenAlias(false, true, grammarAccess.getConfigurationDeclarationAccess().getIdentifierParserRuleCall_9());
		match_ConfigurationSpecification___UseKeyword_4_0_OpenKeyword_4_1_2__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getConfigurationSpecificationAccess().getUseKeyword_4_0()), new TokenAlias(false, false, grammarAccess.getConfigurationSpecificationAccess().getOpenKeyword_4_1_2()));
		match_EntityDeclaration_BeginKeyword_6_0_q = new TokenAlias(false, true, grammarAccess.getEntityDeclarationAccess().getBeginKeyword_6_0());
		match_EntityDeclaration_EntityKeyword_8_q = new TokenAlias(false, true, grammarAccess.getEntityDeclarationAccess().getEntityKeyword_8());
		match_EntityDeclaration_IdentifierParserRuleCall_9_q = new TokenAlias(false, true, grammarAccess.getEntityDeclarationAccess().getIdentifierParserRuleCall_9());
		match_GenerateStatement_BeginKeyword_4_1_q = new TokenAlias(false, true, grammarAccess.getGenerateStatementAccess().getBeginKeyword_4_1());
		match_GenerateStatement_IdentifierParserRuleCall_8_q = new TokenAlias(false, true, grammarAccess.getGenerateStatementAccess().getIdentifierParserRuleCall_8());
		match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_5_q = new TokenAlias(false, true, grammarAccess.getGroupTemplateDeclarationAccess().getLessThanSignGreaterThanSignKeyword_5());
		match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_6_2_q = new TokenAlias(false, true, grammarAccess.getGroupTemplateDeclarationAccess().getLessThanSignGreaterThanSignKeyword_6_2());
		match_IfStatement_ElseKeyword_4_0_q = new TokenAlias(false, true, grammarAccess.getIfStatementAccess().getElseKeyword_4_0());
		match_IfStatement_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getIfStatementAccess().getIdentifierParserRuleCall_7());
		match_InterfaceConstantDeclaration_ConstantKeyword_0_q = new TokenAlias(false, true, grammarAccess.getInterfaceConstantDeclarationAccess().getConstantKeyword_0());
		match_InterfaceConstantDeclaration_InKeyword_3_q = new TokenAlias(false, true, grammarAccess.getInterfaceConstantDeclarationAccess().getInKeyword_3());
		match_InterfaceSignalDeclaration_SignalKeyword_0_q = new TokenAlias(false, true, grammarAccess.getInterfaceSignalDeclarationAccess().getSignalKeyword_0());
		match_InterfaceVariableDeclaration_VariableKeyword_0_q = new TokenAlias(false, true, grammarAccess.getInterfaceVariableDeclarationAccess().getVariableKeyword_0());
		match_LoopStatement_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getLoopStatementAccess().getIdentifierParserRuleCall_7());
		match_PackageBody_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getPackageBodyAccess().getIdentifierParserRuleCall_7());
		match_PackageBody___PackageKeyword_6_0_BodyKeyword_6_1__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getPackageBodyAccess().getPackageKeyword_6_0()), new TokenAlias(false, false, grammarAccess.getPackageBodyAccess().getBodyKeyword_6_1()));
		match_PackageDeclaration_IdentifierParserRuleCall_6_q = new TokenAlias(false, true, grammarAccess.getPackageDeclarationAccess().getIdentifierParserRuleCall_6());
		match_PackageDeclaration_PackageKeyword_5_q = new TokenAlias(false, true, grammarAccess.getPackageDeclarationAccess().getPackageKeyword_5());
		match_PhysicalTypeDefinition_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getPhysicalTypeDefinitionAccess().getIdentifierParserRuleCall_7());
		match_ProcessStatement_IdentifierParserRuleCall_12_q = new TokenAlias(false, true, grammarAccess.getProcessStatementAccess().getIdentifierParserRuleCall_12());
		match_ProcessStatement_IsKeyword_5_q = new TokenAlias(false, true, grammarAccess.getProcessStatementAccess().getIsKeyword_5());
		match_ProcessStatement_PostponedKeyword_10_q = new TokenAlias(false, true, grammarAccess.getProcessStatementAccess().getPostponedKeyword_10());
		match_RecordNatureDefinition_IdentifierParserRuleCall_4_q = new TokenAlias(false, true, grammarAccess.getRecordNatureDefinitionAccess().getIdentifierParserRuleCall_4());
		match_RecordTypeDefinition_IdentifierParserRuleCall_4_q = new TokenAlias(false, true, grammarAccess.getRecordTypeDefinitionAccess().getIdentifierParserRuleCall_4());
		match_SimultaneousCaseStatement_IdentifierParserRuleCall_7_q = new TokenAlias(false, true, grammarAccess.getSimultaneousCaseStatementAccess().getIdentifierParserRuleCall_7());
		match_SimultaneousIfStatement_ElseKeyword_5_0_q = new TokenAlias(false, true, grammarAccess.getSimultaneousIfStatementAccess().getElseKeyword_5_0());
		match_SimultaneousIfStatement_IdentifierParserRuleCall_8_q = new TokenAlias(false, true, grammarAccess.getSimultaneousIfStatementAccess().getIdentifierParserRuleCall_8());
		match_SimultaneousProceduralStatement_IdentifierParserRuleCall_9_q = new TokenAlias(false, true, grammarAccess.getSimultaneousProceduralStatementAccess().getIdentifierParserRuleCall_9());
		match_SimultaneousProceduralStatement_IsKeyword_3_q = new TokenAlias(false, true, grammarAccess.getSimultaneousProceduralStatementAccess().getIsKeyword_3());
		match_SubprogramDeclaration_DesignatorParserRuleCall_1_6_q = new TokenAlias(false, true, grammarAccess.getSubprogramDeclarationAccess().getDesignatorParserRuleCall_1_6());
		match_SubprogramDeclaration___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q = new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getFunctionKeyword_1_5_1()), new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getProcedureKeyword_1_5_0()));
		match_SubprogramDeclaration___IsKeyword_1_0_BeginKeyword_1_2_EndKeyword_1_4___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q_DesignatorParserRuleCall_1_6_q__q = new GroupAlias(false, true, new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getIsKeyword_1_0()), new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getBeginKeyword_1_2()), new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getEndKeyword_1_4()), new AlternativeAlias(false, true, new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getFunctionKeyword_1_5_1()), new TokenAlias(false, false, grammarAccess.getSubprogramDeclarationAccess().getProcedureKeyword_1_5_0())), new TokenAlias(false, true, grammarAccess.getSubprogramDeclarationAccess().getDesignatorParserRuleCall_1_6()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if(ruleCall.getRule() == grammarAccess.getDesignatorRule())
			return getDesignatorToken(semanticObject, ruleCall, node);
		else if(ruleCall.getRule() == grammarAccess.getIdentifierRule())
			return getIdentifierToken(semanticObject, ruleCall, node);
		return "";
	}
	
	/**
	 * Designator:
	 *     Identifier
	 *   | STRING
	 * ;
	 */
	protected String getDesignatorToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "";
	}
	
	/**
	 * Identifier:
	 *     ID
	 *   | EXTENDED_IDENTIFIER
	 * ;
	 */
	protected String getIdentifierToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		if (node != null)
			return getTokenText(node);
		return "";
	}
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_Architecture_ArchitectureKeyword_9_q.equals(syntax))
				emit_Architecture_ArchitectureKeyword_9_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Architecture_IdentifierParserRuleCall_10_q.equals(syntax))
				emit_Architecture_IdentifierParserRuleCall_10_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_BlockStatement_IdentifierParserRuleCall_12_q.equals(syntax))
				emit_BlockStatement_IdentifierParserRuleCall_12_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_BlockStatement_IsKeyword_4_q.equals(syntax))
				emit_BlockStatement_IsKeyword_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_CaseStatement_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_CaseStatement_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ComponentConfiguration___UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q.equals(syntax))
				emit_ComponentConfiguration___UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ComponentConfiguration_____UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q_SemicolonKeyword_4_3__q.equals(syntax))
				emit_ComponentConfiguration_____UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q_SemicolonKeyword_4_3__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ComponentDeclaration_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_ComponentDeclaration_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ComponentDeclaration_IsKeyword_2_q.equals(syntax))
				emit_ComponentDeclaration_IsKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ComponentInstantiationStatement_ComponentKeyword_2_q.equals(syntax))
				emit_ComponentInstantiationStatement_ComponentKeyword_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ConfigurationDeclaration_ConfigurationKeyword_8_q.equals(syntax))
				emit_ConfigurationDeclaration_ConfigurationKeyword_8_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ConfigurationDeclaration_IdentifierParserRuleCall_9_q.equals(syntax))
				emit_ConfigurationDeclaration_IdentifierParserRuleCall_9_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ConfigurationSpecification___UseKeyword_4_0_OpenKeyword_4_1_2__q.equals(syntax))
				emit_ConfigurationSpecification___UseKeyword_4_0_OpenKeyword_4_1_2__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_EntityDeclaration_BeginKeyword_6_0_q.equals(syntax))
				emit_EntityDeclaration_BeginKeyword_6_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_EntityDeclaration_EntityKeyword_8_q.equals(syntax))
				emit_EntityDeclaration_EntityKeyword_8_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_EntityDeclaration_IdentifierParserRuleCall_9_q.equals(syntax))
				emit_EntityDeclaration_IdentifierParserRuleCall_9_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GenerateStatement_BeginKeyword_4_1_q.equals(syntax))
				emit_GenerateStatement_BeginKeyword_4_1_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GenerateStatement_IdentifierParserRuleCall_8_q.equals(syntax))
				emit_GenerateStatement_IdentifierParserRuleCall_8_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_5_q.equals(syntax))
				emit_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_5_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_6_2_q.equals(syntax))
				emit_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_6_2_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_IfStatement_ElseKeyword_4_0_q.equals(syntax))
				emit_IfStatement_ElseKeyword_4_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_IfStatement_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_IfStatement_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_InterfaceConstantDeclaration_ConstantKeyword_0_q.equals(syntax))
				emit_InterfaceConstantDeclaration_ConstantKeyword_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_InterfaceConstantDeclaration_InKeyword_3_q.equals(syntax))
				emit_InterfaceConstantDeclaration_InKeyword_3_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_InterfaceSignalDeclaration_SignalKeyword_0_q.equals(syntax))
				emit_InterfaceSignalDeclaration_SignalKeyword_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_InterfaceVariableDeclaration_VariableKeyword_0_q.equals(syntax))
				emit_InterfaceVariableDeclaration_VariableKeyword_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_LoopStatement_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_LoopStatement_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PackageBody_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_PackageBody_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PackageBody___PackageKeyword_6_0_BodyKeyword_6_1__q.equals(syntax))
				emit_PackageBody___PackageKeyword_6_0_BodyKeyword_6_1__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PackageDeclaration_IdentifierParserRuleCall_6_q.equals(syntax))
				emit_PackageDeclaration_IdentifierParserRuleCall_6_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PackageDeclaration_PackageKeyword_5_q.equals(syntax))
				emit_PackageDeclaration_PackageKeyword_5_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PhysicalTypeDefinition_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_PhysicalTypeDefinition_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ProcessStatement_IdentifierParserRuleCall_12_q.equals(syntax))
				emit_ProcessStatement_IdentifierParserRuleCall_12_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ProcessStatement_IsKeyword_5_q.equals(syntax))
				emit_ProcessStatement_IsKeyword_5_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_ProcessStatement_PostponedKeyword_10_q.equals(syntax))
				emit_ProcessStatement_PostponedKeyword_10_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_RecordNatureDefinition_IdentifierParserRuleCall_4_q.equals(syntax))
				emit_RecordNatureDefinition_IdentifierParserRuleCall_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_RecordTypeDefinition_IdentifierParserRuleCall_4_q.equals(syntax))
				emit_RecordTypeDefinition_IdentifierParserRuleCall_4_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SimultaneousCaseStatement_IdentifierParserRuleCall_7_q.equals(syntax))
				emit_SimultaneousCaseStatement_IdentifierParserRuleCall_7_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SimultaneousIfStatement_ElseKeyword_5_0_q.equals(syntax))
				emit_SimultaneousIfStatement_ElseKeyword_5_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SimultaneousIfStatement_IdentifierParserRuleCall_8_q.equals(syntax))
				emit_SimultaneousIfStatement_IdentifierParserRuleCall_8_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SimultaneousProceduralStatement_IdentifierParserRuleCall_9_q.equals(syntax))
				emit_SimultaneousProceduralStatement_IdentifierParserRuleCall_9_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SimultaneousProceduralStatement_IsKeyword_3_q.equals(syntax))
				emit_SimultaneousProceduralStatement_IsKeyword_3_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SubprogramDeclaration_DesignatorParserRuleCall_1_6_q.equals(syntax))
				emit_SubprogramDeclaration_DesignatorParserRuleCall_1_6_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SubprogramDeclaration___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q.equals(syntax))
				emit_SubprogramDeclaration___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_SubprogramDeclaration___IsKeyword_1_0_BeginKeyword_1_2_EndKeyword_1_4___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q_DesignatorParserRuleCall_1_6_q__q.equals(syntax))
				emit_SubprogramDeclaration___IsKeyword_1_0_BeginKeyword_1_2_EndKeyword_1_4___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q_DesignatorParserRuleCall_1_6_q__q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     'architecture'?
	 */
	protected void emit_Architecture_ArchitectureKeyword_9_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_Architecture_IdentifierParserRuleCall_10_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_BlockStatement_IdentifierParserRuleCall_12_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'is'?
	 */
	protected void emit_BlockStatement_IsKeyword_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_CaseStatement_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('use' 'open')?
	 */
	protected void emit_ComponentConfiguration___UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     (('use' 'open')? ';')?
	 */
	protected void emit_ComponentConfiguration_____UseKeyword_4_0_0_OpenKeyword_4_0_1_2__q_SemicolonKeyword_4_3__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_ComponentDeclaration_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'is'?
	 */
	protected void emit_ComponentDeclaration_IsKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'component'?
	 */
	protected void emit_ComponentInstantiationStatement_ComponentKeyword_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'configuration'?
	 */
	protected void emit_ConfigurationDeclaration_ConfigurationKeyword_8_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_ConfigurationDeclaration_IdentifierParserRuleCall_9_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('use' 'open')?
	 */
	protected void emit_ConfigurationSpecification___UseKeyword_4_0_OpenKeyword_4_1_2__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'begin'?
	 */
	protected void emit_EntityDeclaration_BeginKeyword_6_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'entity'?
	 */
	protected void emit_EntityDeclaration_EntityKeyword_8_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_EntityDeclaration_IdentifierParserRuleCall_9_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'begin'?
	 */
	protected void emit_GenerateStatement_BeginKeyword_4_1_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_GenerateStatement_IdentifierParserRuleCall_8_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '<>'?
	 */
	protected void emit_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_5_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '<>'?
	 */
	protected void emit_GroupTemplateDeclaration_LessThanSignGreaterThanSignKeyword_6_2_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'else'?
	 */
	protected void emit_IfStatement_ElseKeyword_4_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_IfStatement_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'constant'?
	 */
	protected void emit_InterfaceConstantDeclaration_ConstantKeyword_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'in'?
	 */
	protected void emit_InterfaceConstantDeclaration_InKeyword_3_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'signal'?
	 */
	protected void emit_InterfaceSignalDeclaration_SignalKeyword_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'variable'?
	 */
	protected void emit_InterfaceVariableDeclaration_VariableKeyword_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_LoopStatement_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_PackageBody_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('package' 'body')?
	 */
	protected void emit_PackageBody___PackageKeyword_6_0_BodyKeyword_6_1__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_PackageDeclaration_IdentifierParserRuleCall_6_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'package'?
	 */
	protected void emit_PackageDeclaration_PackageKeyword_5_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_PhysicalTypeDefinition_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_ProcessStatement_IdentifierParserRuleCall_12_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'is'?
	 */
	protected void emit_ProcessStatement_IsKeyword_5_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'postponed'?
	 */
	protected void emit_ProcessStatement_PostponedKeyword_10_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_RecordNatureDefinition_IdentifierParserRuleCall_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_RecordTypeDefinition_IdentifierParserRuleCall_4_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_SimultaneousCaseStatement_IdentifierParserRuleCall_7_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'else'?
	 */
	protected void emit_SimultaneousIfStatement_ElseKeyword_5_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_SimultaneousIfStatement_IdentifierParserRuleCall_8_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Identifier?
	 */
	protected void emit_SimultaneousProceduralStatement_IdentifierParserRuleCall_9_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'is'?
	 */
	protected void emit_SimultaneousProceduralStatement_IsKeyword_3_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     Designator?
	 */
	protected void emit_SubprogramDeclaration_DesignatorParserRuleCall_1_6_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('procedure' | 'function')?
	 */
	protected void emit_SubprogramDeclaration___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     ('is' 'begin' 'end' ('procedure' | 'function')? Designator?)?
	 */
	protected void emit_SubprogramDeclaration___IsKeyword_1_0_BeginKeyword_1_2_EndKeyword_1_4___FunctionKeyword_1_5_1_or_ProcedureKeyword_1_5_0__q_DesignatorParserRuleCall_1_6_q__q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
