package fr.labsticc.language.vhdl.model.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import fr.labsticc.language.vhdl.model.services.VhdlGrammarAccess;
import fr.labsticc.language.vhdl.model.vhdl.AccessTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.AddingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Aggregate;
import fr.labsticc.language.vhdl.model.vhdl.AliasDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.All;
import fr.labsticc.language.vhdl.model.vhdl.Allocator;
import fr.labsticc.language.vhdl.model.vhdl.Architecture;
import fr.labsticc.language.vhdl.model.vhdl.AssertionStatement;
import fr.labsticc.language.vhdl.model.vhdl.Association;
import fr.labsticc.language.vhdl.model.vhdl.Attribute;
import fr.labsticc.language.vhdl.model.vhdl.AttributeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.AttributeSpecification;
import fr.labsticc.language.vhdl.model.vhdl.BitString;
import fr.labsticc.language.vhdl.model.vhdl.BlockConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.BlockStatement;
import fr.labsticc.language.vhdl.model.vhdl.BranchQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.BreakElement;
import fr.labsticc.language.vhdl.model.vhdl.BreakStatement;
import fr.labsticc.language.vhdl.model.vhdl.CaseAlternative;
import fr.labsticc.language.vhdl.model.vhdl.CaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.ComponentConfiguration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ComponentInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConditionalWaveform;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ConstantDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ConstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.Constraint;
import fr.labsticc.language.vhdl.model.vhdl.DesignUnit;
import fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification;
import fr.labsticc.language.vhdl.model.vhdl.ElementDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.EntityInstantiationStatement;
import fr.labsticc.language.vhdl.model.vhdl.EnumerationTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ExitStatement;
import fr.labsticc.language.vhdl.model.vhdl.FileDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.FileTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.ForGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.ForIterationScheme;
import fr.labsticc.language.vhdl.model.vhdl.FreeQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GenerateStatement;
import fr.labsticc.language.vhdl.model.vhdl.GenericMaps;
import fr.labsticc.language.vhdl.model.vhdl.Generics;
import fr.labsticc.language.vhdl.model.vhdl.GroupDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.GroupTemplateDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Identifier;
import fr.labsticc.language.vhdl.model.vhdl.IfGenerationScheme;
import fr.labsticc.language.vhdl.model.vhdl.IfStatement;
import fr.labsticc.language.vhdl.model.vhdl.IfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.Indication;
import fr.labsticc.language.vhdl.model.vhdl.LimitSpecification;
import fr.labsticc.language.vhdl.model.vhdl.LogicalExpression;
import fr.labsticc.language.vhdl.model.vhdl.LoopStatement;
import fr.labsticc.language.vhdl.model.vhdl.MultiplyingExpression;
import fr.labsticc.language.vhdl.model.vhdl.Name;
import fr.labsticc.language.vhdl.model.vhdl.NameList;
import fr.labsticc.language.vhdl.model.vhdl.NatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.NextStatement;
import fr.labsticc.language.vhdl.model.vhdl.Noise;
import fr.labsticc.language.vhdl.model.vhdl.Null;
import fr.labsticc.language.vhdl.model.vhdl.NullStatement;
import fr.labsticc.language.vhdl.model.vhdl.Open;
import fr.labsticc.language.vhdl.model.vhdl.Others;
import fr.labsticc.language.vhdl.model.vhdl.PackageBody;
import fr.labsticc.language.vhdl.model.vhdl.PackageDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.PhysicalTypeDefinitionSecondary;
import fr.labsticc.language.vhdl.model.vhdl.PortMaps;
import fr.labsticc.language.vhdl.model.vhdl.Ports;
import fr.labsticc.language.vhdl.model.vhdl.PowerExpression;
import fr.labsticc.language.vhdl.model.vhdl.ProcedureCallStatement;
import fr.labsticc.language.vhdl.model.vhdl.ProcessStatement;
import fr.labsticc.language.vhdl.model.vhdl.QuantityAspect;
import fr.labsticc.language.vhdl.model.vhdl.Range;
import fr.labsticc.language.vhdl.model.vhdl.RangeTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RecordNatureElement;
import fr.labsticc.language.vhdl.model.vhdl.RecordTypeDefinition;
import fr.labsticc.language.vhdl.model.vhdl.RejectMechanism;
import fr.labsticc.language.vhdl.model.vhdl.RelationalExpression;
import fr.labsticc.language.vhdl.model.vhdl.ReportStatement;
import fr.labsticc.language.vhdl.model.vhdl.ReturnStatement;
import fr.labsticc.language.vhdl.model.vhdl.ScalarNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.SelectedSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SequentialSignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.ShiftExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignExpression;
import fr.labsticc.language.vhdl.model.vhdl.SignalAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.SignalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Signature;
import fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousAlternative;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousCaseStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatement;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousIfStatementTest;
import fr.labsticc.language.vhdl.model.vhdl.SimultaneousProceduralStatement;
import fr.labsticc.language.vhdl.model.vhdl.SourceQuantityDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Spectrum;
import fr.labsticc.language.vhdl.model.vhdl.SubnatureDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.SubprogramSpecification;
import fr.labsticc.language.vhdl.model.vhdl.SubtypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.TerminalDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.Transport;
import fr.labsticc.language.vhdl.model.vhdl.TypeDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.UnaryExpression;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedArrayDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnconstrainedNatureDefinition;
import fr.labsticc.language.vhdl.model.vhdl.UnitValue;
import fr.labsticc.language.vhdl.model.vhdl.UseClause;
import fr.labsticc.language.vhdl.model.vhdl.Value;
import fr.labsticc.language.vhdl.model.vhdl.VariableAssignmentStatement;
import fr.labsticc.language.vhdl.model.vhdl.VariableDeclaration;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFile;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;
import fr.labsticc.language.vhdl.model.vhdl.WaitStatement;
import fr.labsticc.language.vhdl.model.vhdl.Waveform;
import fr.labsticc.language.vhdl.model.vhdl.WhileIterationScheme;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;

@SuppressWarnings("all")
public abstract class AbstractVhdlSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private VhdlGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == VhdlPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case VhdlPackage.ACCESS_TYPE_DEFINITION:
				if(context == grammarAccess.getAccessTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_AccessTypeDefinition(context, (AccessTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ADDING_EXPRESSION:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_SimpleExpression(context, (AddingExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0()) {
					sequence_SimpleExpression_AddingExpression_0_1_0(context, (AddingExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_1_1_0()) {
					sequence_SimpleExpression_AddingExpression_1_1_0(context, (AddingExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.AGGREGATE:
				if(context == grammarAccess.getAggregateRule() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Aggregate(context, (Aggregate) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ALIAS_DECLARATION:
				if(context == grammarAccess.getAliasDeclarationRule() ||
				   context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_AliasDeclaration(context, (AliasDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ALL:
				if(context == grammarAccess.getAliasIndicationRule() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getAllRule() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getEntityDesignatorRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getNameRule() ||
				   context == grammarAccess.getNamePrefixRule() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSelectedNameRule() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getSubnatureIndicationRule() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_1_1_0() ||
				   context == grammarAccess.getSubtypeIndicationRule() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_All(context, (All) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ALLOCATOR:
				if(context == grammarAccess.getAllocatorRule() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Allocator(context, (Allocator) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ARCHITECTURE:
				if(context == grammarAccess.getArchitectureRule() ||
				   context == grammarAccess.getLibraryUnitRule()) {
					sequence_Architecture(context, (Architecture) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ASSERTION_STATEMENT:
				if(context == grammarAccess.getAssertionStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_AssertionStatement(context, (AssertionStatement) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getConcurrentAssertionStatementRule() ||
				   context == grammarAccess.getEntityStatementRule()) {
					sequence_ConcurrentAssertionStatement(context, (AssertionStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ASSOCIATION:
				if(context == grammarAccess.getParameterRule()) {
					sequence_Parameter(context, (Association) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ATTRIBUTE:
				if(context == grammarAccess.getAttributeNameRule() ||
				   context == grammarAccess.getNameSuffixRule()) {
					sequence_AttributeName(context, (Attribute) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ATTRIBUTE_DECLARATION:
				if(context == grammarAccess.getAttributeDeclarationRule() ||
				   context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_AttributeDeclaration(context, (AttributeDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ATTRIBUTE_SPECIFICATION:
				if(context == grammarAccess.getAttributeSpecificationRule() ||
				   context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getConfigurationDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_AttributeSpecification(context, (AttributeSpecification) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BIT_STRING:
				if(context == grammarAccess.getBitStringNameRule() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_BitStringName(context, (BitString) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BLOCK_CONFIGURATION:
				if(context == grammarAccess.getBlockConfigurationRule() ||
				   context == grammarAccess.getConfigurationItemRule()) {
					sequence_BlockConfiguration(context, (BlockConfiguration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BLOCK_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getBlockStatementRule()) {
					sequence_BlockStatement(context, (BlockStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BRANCH_QUANTITY_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getBranchQuantityDeclarationRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getQuantityDeclarationRule()) {
					sequence_BranchQuantityDeclaration(context, (BranchQuantityDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BREAK_ELEMENT:
				if(context == grammarAccess.getBreakElementRule()) {
					sequence_BreakElement(context, (BreakElement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.BREAK_STATEMENT:
				if(context == grammarAccess.getBreakStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_BreakStatement(context, (BreakStatement) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getConcurrentBreakStatementRule()) {
					sequence_ConcurrentBreakStatement(context, (BreakStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CASE_ALTERNATIVE:
				if(context == grammarAccess.getCaseAlternativeRule()) {
					sequence_CaseAlternative(context, (CaseAlternative) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CASE_STATEMENT:
				if(context == grammarAccess.getCaseStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_CaseStatement(context, (CaseStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CHARACTER:
				if(context == grammarAccess.getCharacterNameRule() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getEnumerationLiteralRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSelectedNameRule() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_CharacterName(context, (fr.labsticc.language.vhdl.model.vhdl.Character) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getEntityDesignatorRule()) {
					sequence_CharacterName_EntityDesignator(context, (fr.labsticc.language.vhdl.model.vhdl.Character) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.COMPONENT_CONFIGURATION:
				if(context == grammarAccess.getComponentConfigurationRule() ||
				   context == grammarAccess.getConfigurationItemRule()) {
					sequence_ComponentConfiguration(context, (ComponentConfiguration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.COMPONENT_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getComponentDeclarationRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule()) {
					sequence_ComponentDeclaration(context, (ComponentDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.COMPONENT_INSTANTIATION_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getComponentInstantiationStatementRule()) {
					sequence_ComponentInstantiationStatement(context, (ComponentInstantiationStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONDITIONAL_SIGNAL_ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getConditionalSignalAssignmentStatementRule()) {
					sequence_ConditionalSignalAssignmentStatement(context, (ConditionalSignalAssignmentStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONDITIONAL_WAVEFORM:
				if(context == grammarAccess.getConditionalWaveformRule()) {
					sequence_ConditionalWaveform(context, (ConditionalWaveform) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONFIGURATION_DECLARATION:
				if(context == grammarAccess.getConfigurationDeclarationRule() ||
				   context == grammarAccess.getLibraryUnitRule()) {
					sequence_ConfigurationDeclaration(context, (ConfigurationDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONFIGURATION_INSTANTIATION_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getConfigurationInstantiationStatementRule()) {
					sequence_ConfigurationInstantiationStatement(context, (ConfigurationInstantiationStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONFIGURATION_SPECIFICATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getConfigurationSpecificationRule()) {
					sequence_ConfigurationSpecification(context, (ConfigurationSpecification) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONSTANT_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getConstantDeclarationRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_ConstantDeclaration(context, (ConstantDeclaration) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getFunctionParameterDeclarationRule() ||
				   context == grammarAccess.getGenericDeclarationRule() ||
				   context == grammarAccess.getInterfaceConstantDeclarationRule() ||
				   context == grammarAccess.getProcedureParameterDeclarationRule()) {
					sequence_InterfaceConstantDeclaration(context, (ConstantDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONSTRAINED_ARRAY_DEFINITION:
				if(context == grammarAccess.getArrayTypeDefinitionRule() ||
				   context == grammarAccess.getCompositeTypeDefinitionRule() ||
				   context == grammarAccess.getConstrainedArrayDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_ConstrainedArrayDefinition(context, (ConstrainedArrayDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONSTRAINED_NATURE_DEFINITION:
				if(context == grammarAccess.getArrayNatureDefinitionRule() ||
				   context == grammarAccess.getCompositeNatureDefinitionRule() ||
				   context == grammarAccess.getConstrainedNatureDefinitionRule() ||
				   context == grammarAccess.getNatureDefinitionRule()) {
					sequence_ConstrainedNatureDefinition(context, (ConstrainedNatureDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.CONSTRAINT:
				if(context == grammarAccess.getConstraintRule()) {
					sequence_Constraint_IndexConstraint_RangeConstraint(context, (Constraint) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getIndexConstraintRule()) {
					sequence_IndexConstraint(context, (Constraint) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getRangeConstraintRule()) {
					sequence_RangeConstraint(context, (Constraint) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.DESIGN_UNIT:
				if(context == grammarAccess.getDesignUnitRule()) {
					sequence_DesignUnit(context, (DesignUnit) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.DISCONNECTION_SPECIFICATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getDisconnectionSpecificationRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule()) {
					sequence_DisconnectionSpecification(context, (DisconnectionSpecification) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ELEMENT_DECLARATION:
				if(context == grammarAccess.getElementDeclarationRule()) {
					sequence_ElementDeclaration(context, (ElementDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ENTITY_DECLARATION:
				if(context == grammarAccess.getEntityDeclarationRule() ||
				   context == grammarAccess.getLibraryUnitRule()) {
					sequence_EntityDeclaration(context, (EntityDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ENTITY_INSTANTIATION_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getEntityInstantiationStatementRule()) {
					sequence_EntityInstantiationStatement(context, (EntityInstantiationStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.ENUMERATION_TYPE_DEFINITION:
				if(context == grammarAccess.getEnumerationTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_EnumerationTypeDefinition(context, (EnumerationTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.EXIT_STATEMENT:
				if(context == grammarAccess.getExitStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_ExitStatement(context, (ExitStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.FILE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getFileDeclarationRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_FileDeclaration(context, (FileDeclaration) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getFunctionParameterDeclarationRule() ||
				   context == grammarAccess.getInterfaceFileDeclarationRule() ||
				   context == grammarAccess.getProcedureParameterDeclarationRule()) {
					sequence_InterfaceFileDeclaration(context, (FileDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.FILE_TYPE_DEFINITION:
				if(context == grammarAccess.getFileTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_FileTypeDefinition(context, (FileTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.FOR_GENERATION_SCHEME:
				if(context == grammarAccess.getForGenerationSchemeRule() ||
				   context == grammarAccess.getGenerationSchemeRule()) {
					sequence_ForGenerationScheme(context, (ForGenerationScheme) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.FOR_ITERATION_SCHEME:
				if(context == grammarAccess.getForIterationSchemeRule() ||
				   context == grammarAccess.getIterationSchemeRule()) {
					sequence_ForIterationScheme(context, (ForIterationScheme) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.FREE_QUANTITY_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getFreeQuantityDeclarationRule() ||
				   context == grammarAccess.getQuantityDeclarationRule()) {
					sequence_FreeQuantityDeclaration(context, (FreeQuantityDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.GENERATE_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getGenerateStatementRule()) {
					sequence_GenerateStatement(context, (GenerateStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.GENERIC_MAPS:
				if(context == grammarAccess.getGenericMapsRule()) {
					sequence_GenericMaps(context, (GenericMaps) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.GENERICS:
				if(context == grammarAccess.getGenericsRule()) {
					sequence_Generics(context, (Generics) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.GROUP_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getConfigurationDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getGroupDeclarationRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_GroupDeclaration(context, (GroupDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.GROUP_TEMPLATE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getGroupTemplateDeclarationRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_GroupTemplateDeclaration(context, (GroupTemplateDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.IDENTIFIER:
				if(context == grammarAccess.getEntityDesignatorRule()) {
					sequence_EntityDesignator_IdentifierName(context, (Identifier) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getAliasIndicationRule() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getEnumerationLiteralRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getIdentifierNameRule() ||
				   context == grammarAccess.getNameRule() ||
				   context == grammarAccess.getNamePrefixRule() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSelectedNameRule() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getSubnatureIndicationRule() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_1_1_0() ||
				   context == grammarAccess.getSubtypeIndicationRule() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_IdentifierName(context, (Identifier) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.IF_GENERATION_SCHEME:
				if(context == grammarAccess.getGenerationSchemeRule() ||
				   context == grammarAccess.getIfGenerationSchemeRule()) {
					sequence_IfGenerationScheme(context, (IfGenerationScheme) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.IF_STATEMENT:
				if(context == grammarAccess.getIfStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_IfStatement(context, (IfStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.IF_STATEMENT_TEST:
				if(context == grammarAccess.getIfStatementTestRule()) {
					sequence_IfStatementTest(context, (IfStatementTest) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.INDICATION:
				if(context == grammarAccess.getAliasIndicationRule()) {
					sequence_AliasIndication(context, (Indication) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSubnatureIndicationRule()) {
					sequence_SubnatureIndication(context, (Indication) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getSubtypeIndicationRule()) {
					sequence_SubtypeIndication(context, (Indication) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.LIMIT_SPECIFICATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getLimitSpecificationRule()) {
					sequence_LimitSpecification(context, (LimitSpecification) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.LOGICAL_EXPRESSION:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Expression(context, (LogicalExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.LOOP_STATEMENT:
				if(context == grammarAccess.getLoopStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_LoopStatement(context, (LoopStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.MULTIPLYING_EXPRESSION:
				if(context == grammarAccess.getSignTermExpressionRule() ||
				   context == grammarAccess.getSignTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_1_1_0()) {
					sequence_SignTermExpression(context, (MultiplyingExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_SignTermExpression_TermExpression(context, (MultiplyingExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0()) {
					sequence_TermExpression(context, (MultiplyingExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NAME:
				if(context == grammarAccess.getAliasIndicationRule() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getNameRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getSubnatureIndicationRule() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_1_1_0() ||
				   context == grammarAccess.getSubtypeIndicationRule() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Name(context, (Name) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NAME_LIST:
				if(context == grammarAccess.getEntityNameListRule()) {
					sequence_EntityNameList(context, (NameList) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getInstantiationListRule()) {
					sequence_InstantiationList(context, (NameList) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getNameListRule()) {
					sequence_NameList(context, (NameList) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NATURE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getNatureDeclarationRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule()) {
					sequence_NatureDeclaration(context, (NatureDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NEXT_STATEMENT:
				if(context == grammarAccess.getNextStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_NextStatement(context, (NextStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NOISE:
				if(context == grammarAccess.getNoiseRule() ||
				   context == grammarAccess.getSourceAspectRule()) {
					sequence_Noise(context, (Noise) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NULL:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getNullRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Null(context, (Null) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.NULL_STATEMENT:
				if(context == grammarAccess.getNullStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule() ||
				   context == grammarAccess.getSimultaneousStatementRule()) {
					sequence_NullStatement(context, (NullStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.OPEN:
				if(context == grammarAccess.getOpenRule() ||
				   context == grammarAccess.getParameterRule()) {
					sequence_Open(context, (Open) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.OTHERS:
				if(context == grammarAccess.getAliasIndicationRule() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getEntityDesignatorRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getNameRule() ||
				   context == grammarAccess.getNamePrefixRule() ||
				   context == grammarAccess.getOthersRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getSubnatureIndicationRule() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_1_1_0() ||
				   context == grammarAccess.getSubtypeIndicationRule() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Others(context, (Others) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PACKAGE_BODY:
				if(context == grammarAccess.getLibraryUnitRule() ||
				   context == grammarAccess.getPackageBodyRule()) {
					sequence_PackageBody(context, (PackageBody) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PACKAGE_DECLARATION:
				if(context == grammarAccess.getLibraryUnitRule() ||
				   context == grammarAccess.getPackageDeclarationRule()) {
					sequence_PackageDeclaration(context, (PackageDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION:
				if(context == grammarAccess.getPhysicalTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_PhysicalTypeDefinition(context, (PhysicalTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PHYSICAL_TYPE_DEFINITION_SECONDARY:
				if(context == grammarAccess.getPhysicalTypeDefinitionSecondaryRule()) {
					sequence_PhysicalTypeDefinitionSecondary(context, (PhysicalTypeDefinitionSecondary) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PORT_MAPS:
				if(context == grammarAccess.getPortMapsRule()) {
					sequence_PortMaps(context, (PortMaps) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PORTS:
				if(context == grammarAccess.getPortsRule()) {
					sequence_Ports(context, (Ports) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.POWER_EXPRESSION:
				if(context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0()) {
					sequence_Factor(context, (PowerExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Factor_SignFactor(context, (PowerExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSignFactorRule() ||
				   context == grammarAccess.getSignTermExpressionRule() ||
				   context == grammarAccess.getSignTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_1_1_0()) {
					sequence_SignFactor(context, (PowerExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PROCEDURE_CALL_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getConcurrentProcedureCallStatementRule() ||
				   context == grammarAccess.getEntityStatementRule()) {
					sequence_ConcurrentProcedureCallStatement(context, (ProcedureCallStatement) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getProcedureCallStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_ProcedureCallStatement(context, (ProcedureCallStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.PROCESS_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getEntityStatementRule() ||
				   context == grammarAccess.getProcessStatementRule()) {
					sequence_ProcessStatement(context, (ProcessStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.QUANTITY_ASPECT:
				if(context == grammarAccess.getQuantityAspectRule()) {
					sequence_QuantityAspect(context, (QuantityAspect) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RANGE:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRangeSpecificationRule() ||
				   context == grammarAccess.getSliceNameRule()) {
					sequence_RangeSpecification(context, (Range) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RANGE_TYPE_DEFINITION:
				if(context == grammarAccess.getRangeTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_RangeTypeDefinition(context, (RangeTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RECORD_NATURE_DEFINITION:
				if(context == grammarAccess.getCompositeNatureDefinitionRule() ||
				   context == grammarAccess.getNatureDefinitionRule() ||
				   context == grammarAccess.getRecordNatureDefinitionRule()) {
					sequence_RecordNatureDefinition(context, (RecordNatureDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RECORD_NATURE_ELEMENT:
				if(context == grammarAccess.getRecordNatureElementRule()) {
					sequence_RecordNatureElement(context, (RecordNatureElement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RECORD_TYPE_DEFINITION:
				if(context == grammarAccess.getCompositeTypeDefinitionRule() ||
				   context == grammarAccess.getRecordTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule()) {
					sequence_RecordTypeDefinition(context, (RecordTypeDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.REJECT_MECHANISM:
				if(context == grammarAccess.getDelayMechanismRule() ||
				   context == grammarAccess.getRejectMechanismRule()) {
					sequence_RejectMechanism(context, (RejectMechanism) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RELATIONAL_EXPRESSION:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_Relation(context, (RelationalExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.REPORT_STATEMENT:
				if(context == grammarAccess.getReportStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_ReportStatement(context, (ReportStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.RETURN_STATEMENT:
				if(context == grammarAccess.getReturnStatementRule() ||
				   context == grammarAccess.getSequentialStatementRule()) {
					sequence_ReturnStatement(context, (ReturnStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SCALAR_NATURE_DEFINITION:
				if(context == grammarAccess.getNatureDefinitionRule() ||
				   context == grammarAccess.getScalarNatureDefinitionRule()) {
					sequence_ScalarNatureDefinition(context, (ScalarNatureDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SELECTED_SIGNAL_ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSelectedSignalAssignmentStatementRule()) {
					sequence_SelectedSignalAssignmentStatement(context, (SelectedSignalAssignmentStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SEQUENTIAL_SIGNAL_ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSequentialSignalAssignmentStatementRule()) {
					sequence_SequentialSignalAssignmentStatement(context, (SequentialSignalAssignmentStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SHIFT_EXPRESSION:
				if(context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_ShiftExpression(context, (ShiftExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIGN_EXPRESSION:
				if(context == grammarAccess.getSignFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getSignPrimaryRule()) {
					sequence_SignPrimary(context, (SignExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSignFactorRule() ||
				   context == grammarAccess.getSignTermExpressionRule() ||
				   context == grammarAccess.getSignTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_1_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_SignPrimary_SignUnaryExpression(context, (SignExpression) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSignUnaryExpressionRule()) {
					sequence_SignUnaryExpression(context, (SignExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIGNAL_ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getSequentialStatementRule() ||
				   context == grammarAccess.getSignalAssignmentStatementRule()) {
					sequence_SignalAssignmentStatement(context, (SignalAssignmentStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIGNAL_DECLARATION:
				if(context == grammarAccess.getFunctionParameterDeclarationRule() ||
				   context == grammarAccess.getInterfaceSignalDeclarationRule() ||
				   context == grammarAccess.getPortDeclarationRule() ||
				   context == grammarAccess.getProcedureParameterDeclarationRule()) {
					sequence_InterfaceSignalDeclaration(context, (SignalDeclaration) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getSignalDeclarationRule()) {
					sequence_SignalDeclaration(context, (SignalDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIGNATURE:
				if(context == grammarAccess.getSignatureRule()) {
					sequence_Signature(context, (Signature) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSimpleSimultaneousStatementRule() ||
				   context == grammarAccess.getSimultaneousStatementRule()) {
					sequence_SimpleSimultaneousStatement(context, (SimpleSimultaneousStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMULTANEOUS_ALTERNATIVE:
				if(context == grammarAccess.getSimultaneousAlternativeRule()) {
					sequence_SimultaneousAlternative(context, (SimultaneousAlternative) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMULTANEOUS_CASE_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSimultaneousCaseStatementRule() ||
				   context == grammarAccess.getSimultaneousStatementRule()) {
					sequence_SimultaneousCaseStatement(context, (SimultaneousCaseStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSimultaneousIfStatementRule() ||
				   context == grammarAccess.getSimultaneousStatementRule()) {
					sequence_SimultaneousIfStatement(context, (SimultaneousIfStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMULTANEOUS_IF_STATEMENT_TEST:
				if(context == grammarAccess.getSimultaneousIfStatementTestRule()) {
					sequence_SimultaneousIfStatementTest(context, (SimultaneousIfStatementTest) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SIMULTANEOUS_PROCEDURAL_STATEMENT:
				if(context == grammarAccess.getArchitectureStatementRule() ||
				   context == grammarAccess.getSimultaneousProceduralStatementRule() ||
				   context == grammarAccess.getSimultaneousStatementRule()) {
					sequence_SimultaneousProceduralStatement(context, (SimultaneousProceduralStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SOURCE_QUANTITY_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getQuantityDeclarationRule() ||
				   context == grammarAccess.getSourceQuantityDeclarationRule()) {
					sequence_SourceQuantityDeclaration(context, (SourceQuantityDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SPECTRUM:
				if(context == grammarAccess.getSourceAspectRule() ||
				   context == grammarAccess.getSpectrumRule()) {
					sequence_Spectrum(context, (Spectrum) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.STRING:
				if(context == grammarAccess.getEntityDesignatorRule()) {
					sequence_EntityDesignator_StringName(context, (fr.labsticc.language.vhdl.model.vhdl.String) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getAliasIndicationRule() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getAliasIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getDiscreteRangeRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getGroupConstituentRule() ||
				   context == grammarAccess.getNameRule() ||
				   context == grammarAccess.getNamePrefixRule() ||
				   context == grammarAccess.getNameSuffixRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSelectedNameRule() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getStringNameRule() ||
				   context == grammarAccess.getSubnatureIndicationRule() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubnatureIndicationAccess().getIndicationMarkAction_1_1_0() ||
				   context == grammarAccess.getSubtypeIndicationRule() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_0_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_1_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_2_1() ||
				   context == grammarAccess.getSubtypeIndicationAccess().getIndicationMarkAction_3_1_0() ||
				   context == grammarAccess.getTargetRule() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_StringName(context, (fr.labsticc.language.vhdl.model.vhdl.String) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SUBNATURE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getSubnatureDeclarationRule()) {
					sequence_SubnatureDeclaration(context, (SubnatureDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SUBPROGRAM_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarationRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule()) {
					sequence_SubprogramDeclaration(context, (SubprogramDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SUBPROGRAM_SPECIFICATION:
				if(context == grammarAccess.getSubprogramSpecificationRule()) {
					sequence_SubprogramSpecification(context, (SubprogramSpecification) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.SUBTYPE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule() ||
				   context == grammarAccess.getSubtypeDeclarationRule()) {
					sequence_SubtypeDeclaration(context, (SubtypeDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.TERMINAL_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getTerminalDeclarationRule()) {
					sequence_TerminalDeclaration(context, (TerminalDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.TRANSPORT:
				if(context == grammarAccess.getDelayMechanismRule() ||
				   context == grammarAccess.getTransportRule()) {
					sequence_Transport(context, (Transport) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.TYPE_DECLARATION:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule() ||
				   context == grammarAccess.getTypeDeclarationRule()) {
					sequence_TypeDeclaration(context, (TypeDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.UNARY_EXPRESSION:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getUnaryExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_UnaryExpression(context, (UnaryExpression) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.UNCONSTRAINED_ARRAY_DEFINITION:
				if(context == grammarAccess.getArrayTypeDefinitionRule() ||
				   context == grammarAccess.getCompositeTypeDefinitionRule() ||
				   context == grammarAccess.getTypeDefinitionRule() ||
				   context == grammarAccess.getUnconstrainedArrayDefinitionRule()) {
					sequence_UnconstrainedArrayDefinition(context, (UnconstrainedArrayDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.UNCONSTRAINED_NATURE_DEFINITION:
				if(context == grammarAccess.getArrayNatureDefinitionRule() ||
				   context == grammarAccess.getCompositeNatureDefinitionRule() ||
				   context == grammarAccess.getNatureDefinitionRule() ||
				   context == grammarAccess.getUnconstrainedNatureDefinitionRule()) {
					sequence_UnconstrainedNatureDefinition(context, (UnconstrainedNatureDefinition) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.UNIT_VALUE:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getValueExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_ValueExpression(context, (UnitValue) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.USE_CLAUSE:
				if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getConfigurationDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule() ||
				   context == grammarAccess.getUseClauseRule()) {
					sequence_UseClause(context, (UseClause) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.VALUE:
				if(context == grammarAccess.getChoiceRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getExpressionAccess().getLogicalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getFactorRule() ||
				   context == grammarAccess.getFactorAccess().getPowerExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getParameterRule() ||
				   context == grammarAccess.getPrimaryRule() ||
				   context == grammarAccess.getRangeRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationalExpressionLeftAction_1_0() ||
				   context == grammarAccess.getShiftExpressionRule() ||
				   context == grammarAccess.getShiftExpressionAccess().getShiftExpressionLeftAction_1_0() ||
				   context == grammarAccess.getSimpleExpressionRule() ||
				   context == grammarAccess.getSimpleExpressionAccess().getAddingExpressionLeftAction_0_1_0() ||
				   context == grammarAccess.getTermExpressionRule() ||
				   context == grammarAccess.getTermExpressionAccess().getMultiplyingExpressionLeftAction_1_0() ||
				   context == grammarAccess.getValueExpressionRule() ||
				   context == grammarAccess.getWaveformRule() ||
				   context == grammarAccess.getWaveformAccess().getWaveformExpressionAction_1_0()) {
					sequence_ValueExpression(context, (Value) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.VARIABLE_ASSIGNMENT_STATEMENT:
				if(context == grammarAccess.getSequentialStatementRule() ||
				   context == grammarAccess.getVariableAssignmentStatementRule()) {
					sequence_VariableAssignmentStatement(context, (VariableAssignmentStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.VARIABLE_DECLARATION:
				if(context == grammarAccess.getFunctionParameterDeclarationRule() ||
				   context == grammarAccess.getInterfaceVariableDeclarationRule() ||
				   context == grammarAccess.getProcedureParameterDeclarationRule()) {
					sequence_InterfaceVariableDeclaration(context, (VariableDeclaration) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getBlockDeclarativeItemRule() ||
				   context == grammarAccess.getEntityDeclarativeItemRule() ||
				   context == grammarAccess.getPackageBodyDeclarativeItemRule() ||
				   context == grammarAccess.getPackageDeclarativeItemRule() ||
				   context == grammarAccess.getProceduralDeclarativeItemRule() ||
				   context == grammarAccess.getProcessDeclarativeItemRule() ||
				   context == grammarAccess.getSubprogramDeclarativeItemRule() ||
				   context == grammarAccess.getVariableDeclarationRule()) {
					sequence_VariableDeclaration(context, (VariableDeclaration) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.VHDL_FILE:
				if(context == grammarAccess.getVhdlFileRule()) {
					sequence_VhdlFile(context, (VhdlFile) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.WAIT_STATEMENT:
				if(context == grammarAccess.getSequentialStatementRule() ||
				   context == grammarAccess.getWaitStatementRule()) {
					sequence_WaitStatement(context, (WaitStatement) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.WAVEFORM:
				if(context == grammarAccess.getWaveformRule()) {
					sequence_Waveform(context, (Waveform) semanticObject); 
					return; 
				}
				else break;
			case VhdlPackage.WHILE_ITERATION_SCHEME:
				if(context == grammarAccess.getIterationSchemeRule() ||
				   context == grammarAccess.getWhileIterationSchemeRule()) {
					sequence_WhileIterationScheme(context, (WhileIterationScheme) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     type=SubtypeIndication
	 */
	protected void sequence_AccessTypeDefinition(EObject context, AccessTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((element+=Parameter element+=Parameter*)?)
	 */
	protected void sequence_Aggregate(EObject context, Aggregate semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Designator alias=AliasIndication? is=Name signature=Signature?)
	 */
	protected void sequence_AliasDeclaration(EObject context, AliasDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (mark+=AliasIndication_Indication_0_1 mark+=Name constraint=Constraint tolerance=Expression?) | 
	 *         (mark+=AliasIndication_Indication_1_1 mark+=Name tolerance=Expression?) | 
	 *         (mark+=AliasIndication_Indication_2_1 constraint=Constraint (tolerance=Expression across=Expression?)?) | 
	 *         (mark+=AliasIndication_Indication_3_1_0 tolerance=Expression across=Expression?)
	 *     )
	 */
	protected void sequence_AliasIndication(EObject context, Indication semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name='all'
	 */
	protected void sequence_All(EObject context, All semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     allocate=SubtypeIndication
	 */
	protected void sequence_Allocator(EObject context, Allocator semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier of=Name declaration+=BlockDeclarativeItem* statement+=ArchitectureStatement*)
	 */
	protected void sequence_Architecture(EObject context, Architecture semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? condition=Expression report=Expression? severity=Expression?)
	 */
	protected void sequence_AssertionStatement(EObject context, AssertionStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier type=Identifier)
	 */
	protected void sequence_AttributeDeclaration(EObject context, AttributeDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (signature=Signature? name=ATTRIBUTE)
	 */
	protected void sequence_AttributeName(EObject context, Attribute semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=AttributeDesignator entity=EntityNameList class=EntityClass is=Expression)
	 */
	protected void sequence_AttributeSpecification(EObject context, AttributeSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=BIT_STRING_LITERAL
	 */
	protected void sequence_BitStringName(EObject context, BitString semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Name use+=UseClause* item+=ConfigurationItem*)
	 */
	protected void sequence_BlockConfiguration(EObject context, BlockConfiguration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=Label 
	 *         guard=Expression? 
	 *         (generic=Generics genericMap=GenericMaps?)? 
	 *         (port=Ports portMap=PortMaps?)? 
	 *         declaration+=BlockDeclarativeItem* 
	 *         statement+=ArchitectureStatement*
	 *     )
	 */
	protected void sequence_BlockStatement(EObject context, BlockStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (across=QuantityAspect? through=QuantityAspect? left=Name right=Name?)
	 */
	protected void sequence_BranchQuantityDeclaration(EObject context, BranchQuantityDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Name? use=Name arrow=Expression)
	 */
	protected void sequence_BreakElement(EObject context, BreakElement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? (break+=BreakElement break+=BreakElement*)? when=Expression?)
	 */
	protected void sequence_BreakStatement(EObject context, BreakStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (choice+=Choice choice+=Choice* statement+=SequentialStatement*)
	 */
	protected void sequence_CaseAlternative(EObject context, CaseAlternative semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? case=Expression when+=CaseAlternative+)
	 */
	protected void sequence_CaseStatement(EObject context, CaseStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=CHAR
	 */
	protected void sequence_CharacterName(EObject context, fr.labsticc.language.vhdl.model.vhdl.Character semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CHAR signature=Signature?)
	 */
	protected void sequence_CharacterName_EntityDesignator(EObject context, fr.labsticc.language.vhdl.model.vhdl.Character semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         list=InstantiationList 
	 *         component=Name 
	 *         ((entity=Name | configuration=Name)? genericMap=GenericMaps? portMap=PortMaps?)? 
	 *         block=BlockConfiguration?
	 *     )
	 */
	protected void sequence_ComponentConfiguration(EObject context, ComponentConfiguration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier generic=Generics? port=Ports?)
	 */
	protected void sequence_ComponentDeclaration(EObject context, ComponentDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label component=Name genericMap=GenericMaps? portMap=PortMaps?)
	 */
	protected void sequence_ComponentInstantiationStatement(EObject context, ComponentInstantiationStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? postponed?='postponed'? condition=Expression report=Expression? severity=Expression?)
	 */
	protected void sequence_ConcurrentAssertionStatement(EObject context, AssertionStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? (break+=BreakElement break+=BreakElement*)? (sensitivity+=Name sensitivity+=Name*)? when=Expression?)
	 */
	protected void sequence_ConcurrentBreakStatement(EObject context, BreakStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? postponed?='postponed' procedure=Name)
	 */
	protected void sequence_ConcurrentProcedureCallStatement(EObject context, ProcedureCallStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=Label? 
	 *         postponed?='postponed'? 
	 *         target=Target 
	 *         guarded?='guarded'? 
	 *         delay=DelayMechanism? 
	 *         waveform+=ConditionalWaveform 
	 *         waveform+=ConditionalWaveform*
	 *     )
	 */
	protected void sequence_ConditionalSignalAssignmentStatement(EObject context, ConditionalSignalAssignmentStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((waveform+=Waveform waveform+=Waveform*)? choice+=Choice choice+=Choice*)
	 */
	protected void sequence_ConditionalWaveform(EObject context, ConditionalWaveform semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier of=Name declaration+=ConfigurationDeclarativeItem* block=BlockConfiguration)
	 */
	protected void sequence_ConfigurationDeclaration(EObject context, ConfigurationDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label configuration=Name genericMap=GenericMaps? portMap=PortMaps?)
	 */
	protected void sequence_ConfigurationInstantiationStatement(EObject context, ConfigurationInstantiationStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (list=InstantiationList component=Name (entity=Name | configuration=Name)? genericMap=GenericMaps? portMap=PortMaps?)
	 */
	protected void sequence_ConfigurationSpecification(EObject context, ConfigurationSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication initial=Expression?)
	 */
	protected void sequence_ConstantDeclaration(EObject context, ConstantDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (constraint=IndexConstraint type=SubtypeIndication)
	 */
	protected void sequence_ConstrainedArrayDefinition(EObject context, ConstrainedArrayDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (constraint=IndexConstraint nature=SubnatureIndication)
	 */
	protected void sequence_ConstrainedNatureDefinition(EObject context, ConstrainedNatureDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (range+=Range | (range+=DiscreteRange range+=DiscreteRange*))
	 */
	protected void sequence_Constraint_IndexConstraint_RangeConstraint(EObject context, Constraint semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (((library+=Identifier library+=Identifier*) | (use+=Name use+=Name*))* unit=LibraryUnit)
	 */
	protected void sequence_DesignUnit(EObject context, DesignUnit semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (disconnect=NameList type=Name after=Expression)
	 */
	protected void sequence_DisconnectionSpecification(EObject context, DisconnectionSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (identifier+=Identifier identifier+=Identifier* type=SubtypeIndication)
	 */
	protected void sequence_ElementDeclaration(EObject context, ElementDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier generic=Generics? port=Ports? declaration+=EntityDeclarativeItem* statement+=EntityStatement*)
	 */
	protected void sequence_EntityDeclaration(EObject context, EntityDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier signature=Signature?)
	 */
	protected void sequence_EntityDesignator_IdentifierName(EObject context, Identifier semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=STRING signature=Signature?)
	 */
	protected void sequence_EntityDesignator_StringName(EObject context, fr.labsticc.language.vhdl.model.vhdl.String semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label entity=Name genericMap=GenericMaps? portMap=PortMaps?)
	 */
	protected void sequence_EntityInstantiationStatement(EObject context, EntityInstantiationStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name+=EntityDesignator name+=EntityDesignator*)
	 */
	protected void sequence_EntityNameList(EObject context, NameList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (literal+=EnumerationLiteral literal+=EnumerationLiteral*)
	 */
	protected void sequence_EnumerationTypeDefinition(EObject context, EnumerationTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? exit=Identifier? when=Expression?)
	 */
	protected void sequence_ExitStatement(EObject context, ExitStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Expression_LogicalExpression_1_0 operator=LogicalOperator right=Relation)
	 */
	protected void sequence_Expression(EObject context, LogicalExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Factor_PowerExpression_0_1_0 right=Primary)
	 */
	protected void sequence_Factor(EObject context, PowerExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((left=Factor_PowerExpression_0_1_0 right=Primary) | (left=SignFactor_PowerExpression_0_1_0 right=Primary))
	 */
	protected void sequence_Factor_SignFactor(EObject context, PowerExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication (open=Expression? is=Expression)?)
	 */
	protected void sequence_FileDeclaration(EObject context, FileDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     file=Name
	 */
	protected void sequence_FileTypeDefinition(EObject context, FileTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (variable=Identifier in=DiscreteRange)
	 */
	protected void sequence_ForGenerationScheme(EObject context, ForGenerationScheme semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (variable=Identifier in=DiscreteRange)
	 */
	protected void sequence_ForIterationScheme(EObject context, ForIterationScheme semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication quantity=Expression?)
	 */
	protected void sequence_FreeQuantityDeclaration(EObject context, FreeQuantityDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label scheme=GenerationScheme declaration+=BlockDeclarativeItem* statement+=ArchitectureStatement*)
	 */
	protected void sequence_GenerateStatement(EObject context, GenerateStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (generic+=Parameter generic+=Parameter*)
	 */
	protected void sequence_GenericMaps(EObject context, GenericMaps semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (declaration+=GenericDeclaration declaration+=GenericDeclaration*)
	 */
	protected void sequence_Generics(EObject context, Generics semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier is=Name member+=GroupConstituent member+=GroupConstituent*)
	 */
	protected void sequence_GroupDeclaration(EObject context, GroupDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier entry+=EntityClass entry+=EntityClass*)
	 */
	protected void sequence_GroupTemplateDeclaration(EObject context, GroupTemplateDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=Identifier
	 */
	protected void sequence_IdentifierName(EObject context, Identifier semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     condition=Expression
	 */
	protected void sequence_IfGenerationScheme(EObject context, IfGenerationScheme semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (condition=Expression statement+=SequentialStatement*)
	 */
	protected void sequence_IfStatementTest(EObject context, IfStatementTest semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? test+=IfStatementTest test+=IfStatementTest* statement+=SequentialStatement*)
	 */
	protected void sequence_IfStatement(EObject context, IfStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (range+=DiscreteRange range+=DiscreteRange*)
	 */
	protected void sequence_IndexConstraint(EObject context, Constraint semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((name+=IdentifierName name+=IdentifierName*) | name+=Others | name+=All)
	 */
	protected void sequence_InstantiationList(EObject context, NameList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication initial=Expression?)
	 */
	protected void sequence_InterfaceConstantDeclaration(EObject context, ConstantDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication)
	 */
	protected void sequence_InterfaceFileDeclaration(EObject context, FileDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier mode=Mode? type=SubtypeIndication kind=SignalKind? initial=Expression?)
	 */
	protected void sequence_InterfaceSignalDeclaration(EObject context, SignalDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier mode=Mode? type=SubtypeIndication initial=Expression?)
	 */
	protected void sequence_InterfaceVariableDeclaration(EObject context, VariableDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (limit=NameList type=Name value=Expression)
	 */
	protected void sequence_LimitSpecification(EObject context, LimitSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? iteration=IterationScheme? statement+=SequentialStatement*)
	 */
	protected void sequence_LoopStatement(EObject context, LoopStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name+=Name name+=Name*)
	 */
	protected void sequence_NameList(EObject context, NameList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (element+=NamePrefix element+=NameSuffix element+=NameSuffix*)
	 */
	protected void sequence_Name(EObject context, Name semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier is=NatureDefinition)
	 */
	protected void sequence_NatureDeclaration(EObject context, NatureDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? next=Identifier? when=Expression?)
	 */
	protected void sequence_NextStatement(EObject context, NextStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     noise=SimpleExpression
	 */
	protected void sequence_Noise(EObject context, Noise semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label?)
	 */
	protected void sequence_NullStatement(EObject context, NullStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name='null'
	 */
	protected void sequence_Null(EObject context, Null semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name='open'
	 */
	protected void sequence_Open(EObject context, Open semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name='others'
	 */
	protected void sequence_Others(EObject context, Others semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier declaration+=PackageBodyDeclarativeItem*)
	 */
	protected void sequence_PackageBody(EObject context, PackageBody semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier declaration+=PackageDeclarativeItem*)
	 */
	protected void sequence_PackageDeclaration(EObject context, PackageDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (choice+=Choice choice+=Choice* expression=Expression?)
	 */
	protected void sequence_Parameter(EObject context, Association semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier number=ABSTRACT_LITERAL? of=Name)
	 */
	protected void sequence_PhysicalTypeDefinitionSecondary(EObject context, PhysicalTypeDefinitionSecondary semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (range=RangeConstraint primary=Identifier secondary+=PhysicalTypeDefinitionSecondary*)
	 */
	protected void sequence_PhysicalTypeDefinition(EObject context, PhysicalTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (port+=Parameter port+=Parameter*)
	 */
	protected void sequence_PortMaps(EObject context, PortMaps semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (declaration+=PortDeclaration declaration+=PortDeclaration*)
	 */
	protected void sequence_Ports(EObject context, Ports semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? procedure=Name)
	 */
	protected void sequence_ProcedureCallStatement(EObject context, ProcedureCallStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=Label? 
	 *         postponed?='postponed'? 
	 *         (sensitivity+=Identifier sensitivity+=Identifier*)? 
	 *         declaration+=ProcessDeclarativeItem* 
	 *         statement+=SequentialStatement*
	 *     )
	 */
	protected void sequence_ProcessStatement(EObject context, ProcessStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name+=Identifier name+=Identifier* tolerance=Expression? expression=Expression?)
	 */
	protected void sequence_QuantityAspect(EObject context, QuantityAspect semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     range+=Range
	 */
	protected void sequence_RangeConstraint(EObject context, Constraint semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SimpleExpression direction=RangeDirection right=SimpleExpression)
	 */
	protected void sequence_RangeSpecification(EObject context, Range semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SimpleExpression direction=RangeDirection right=SimpleExpression)
	 */
	protected void sequence_RangeTypeDefinition(EObject context, RangeTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     record+=RecordNatureElement+
	 */
	protected void sequence_RecordNatureDefinition(EObject context, RecordNatureDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name+=Identifier name+=Identifier* nature=SubnatureIndication)
	 */
	protected void sequence_RecordNatureElement(EObject context, RecordNatureElement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     declaration+=ElementDeclaration+
	 */
	protected void sequence_RecordTypeDefinition(EObject context, RecordTypeDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (reject=Expression?)
	 */
	protected void sequence_RejectMechanism(EObject context, RejectMechanism semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_RelationalExpression_1_0 operator=RelationalOperator right=ShiftExpression)
	 */
	protected void sequence_Relation(EObject context, RelationalExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? report=Expression severity=Expression?)
	 */
	protected void sequence_ReportStatement(EObject context, ReportStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? return=Expression?)
	 */
	protected void sequence_ReturnStatement(EObject context, ReturnStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Name across=Name through=Name)
	 */
	protected void sequence_ScalarNatureDefinition(EObject context, ScalarNatureDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=Label? 
	 *         postponed?='postponed'? 
	 *         expression=Expression 
	 *         target=Target 
	 *         guarded?='guarded'? 
	 *         delay=DelayMechanism? 
	 *         waveform+=ConditionalWaveform 
	 *         waveform+=ConditionalWaveform*
	 *     )
	 */
	protected void sequence_SelectedSignalAssignmentStatement(EObject context, SelectedSignalAssignmentStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=Label? 
	 *         postponed?='postponed'? 
	 *         target=Target 
	 *         guarded?='guarded'? 
	 *         delay=DelayMechanism? 
	 *         (waveform+=Waveform waveform+=Waveform*)?
	 *     )
	 */
	protected void sequence_SequentialSignalAssignmentStatement(EObject context, SequentialSignalAssignmentStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=ShiftExpression_ShiftExpression_1_0 operator=ShiftOperator right=SimpleExpression)
	 */
	protected void sequence_ShiftExpression(EObject context, ShiftExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SignFactor_PowerExpression_0_1_0 right=Primary)
	 */
	protected void sequence_SignFactor(EObject context, PowerExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (sign=Sign expression=Primary)
	 */
	protected void sequence_SignPrimary(EObject context, SignExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((sign=Sign expression=Primary) | (sign=Sign expression=UnaryExpression))
	 */
	protected void sequence_SignPrimary_SignUnaryExpression(EObject context, SignExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SignTermExpression_MultiplyingExpression_1_0 operator=MultiplyingOperator right=Factor)
	 */
	protected void sequence_SignTermExpression(EObject context, MultiplyingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (left=TermExpression_MultiplyingExpression_1_0 operator=MultiplyingOperator right=Factor) | 
	 *         (left=SignTermExpression_MultiplyingExpression_1_0 operator=MultiplyingOperator right=Factor)
	 *     )
	 */
	protected void sequence_SignTermExpression_TermExpression(EObject context, MultiplyingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (sign=Sign expression=UnaryExpression)
	 */
	protected void sequence_SignUnaryExpression(EObject context, SignExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? target=Target delay=DelayMechanism? (waveform+=Waveform waveform+=Waveform*)?)
	 */
	protected void sequence_SignalAssignmentStatement(EObject context, SignalAssignmentStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication kind=SignalKind? initial=Expression?)
	 */
	protected void sequence_SignalDeclaration(EObject context, SignalDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((name+=Name name+=Name*)? return=Name?)
	 */
	protected void sequence_Signature(EObject context, Signature semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (left=SimpleExpression_AddingExpression_0_1_0 operator=AddingOperator right=TermExpression) | 
	 *         (left=SimpleExpression_AddingExpression_1_1_0 operator=AddingOperator right=TermExpression)
	 *     )
	 */
	protected void sequence_SimpleExpression(EObject context, AddingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SimpleExpression_AddingExpression_0_1_0 operator=AddingOperator right=TermExpression)
	 */
	protected void sequence_SimpleExpression_AddingExpression_0_1_0(EObject context, AddingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SimpleExpression_AddingExpression_1_1_0 operator=AddingOperator right=TermExpression)
	 */
	protected void sequence_SimpleExpression_AddingExpression_1_1_0(EObject context, AddingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? left=SimpleExpression right=SimpleExpression tolerance=Expression?)
	 */
	protected void sequence_SimpleSimultaneousStatement(EObject context, SimpleSimultaneousStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (choice+=Choice choice+=Choice* statememt+=SimultaneousStatement*)
	 */
	protected void sequence_SimultaneousAlternative(EObject context, SimultaneousAlternative semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? case=Expression when+=SimultaneousAlternative+)
	 */
	protected void sequence_SimultaneousCaseStatement(EObject context, SimultaneousCaseStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (condition=Expression statememt+=SimultaneousStatement*)
	 */
	protected void sequence_SimultaneousIfStatementTest(EObject context, SimultaneousIfStatementTest semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? test+=SimultaneousIfStatementTest test+=SimultaneousIfStatementTest* statememt+=SimultaneousStatement*)
	 */
	protected void sequence_SimultaneousIfStatement(EObject context, SimultaneousIfStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? declaration+=ProceduralDeclarativeItem* statement+=SequentialStatement*)
	 */
	protected void sequence_SimultaneousProceduralStatement(EObject context, SimultaneousProceduralStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier type=SubtypeIndication source=SourceAspect)
	 */
	protected void sequence_SourceQuantityDeclaration(EObject context, SourceQuantityDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SimpleExpression right=SimpleExpression)
	 */
	protected void sequence_Spectrum(EObject context, Spectrum semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_StringName(EObject context, fr.labsticc.language.vhdl.model.vhdl.String semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier nature=SubnatureIndication)
	 */
	protected void sequence_SubnatureDeclaration(EObject context, SubnatureDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (mark+=SubnatureIndication_Indication_0_1 constraint=IndexConstraint (tolerance=Expression across=Expression)?) | 
	 *         (mark+=SubnatureIndication_Indication_1_1_0 tolerance=Expression across=Expression)
	 *     )
	 */
	protected void sequence_SubnatureIndication(EObject context, Indication semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (specification=SubprogramSpecification (declaration+=SubprogramDeclarativeItem* statement+=SequentialStatement*)?)
	 */
	protected void sequence_SubprogramDeclaration(EObject context, SubprogramDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (name=Designator (declaration+=ProcedureParameterDeclaration declaration+=ProcedureParameterDeclaration*)?) | 
	 *         (
	 *             (purity='pure' | purity='impure')? 
	 *             name=Designator 
	 *             (declaration+=FunctionParameterDeclaration declaration+=FunctionParameterDeclaration*)? 
	 *             return=Name
	 *         )
	 *     )
	 */
	protected void sequence_SubprogramSpecification(EObject context, SubprogramSpecification semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier type=SubtypeIndication)
	 */
	protected void sequence_SubtypeDeclaration(EObject context, SubtypeDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         (mark+=SubtypeIndication_Indication_0_1 mark+=Name constraint=Constraint tolerance=Expression?) | 
	 *         (mark+=SubtypeIndication_Indication_1_1 mark+=Name tolerance=Expression?) | 
	 *         (mark+=SubtypeIndication_Indication_2_1 constraint=Constraint tolerance=Expression?) | 
	 *         (mark+=SubtypeIndication_Indication_3_1_0 tolerance=Expression)
	 *     )
	 */
	protected void sequence_SubtypeIndication(EObject context, Indication semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=TermExpression_MultiplyingExpression_1_0 operator=MultiplyingOperator right=Factor)
	 */
	protected void sequence_TermExpression(EObject context, MultiplyingExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=CommaIdentifier nature=SubnatureIndication)
	 */
	protected void sequence_TerminalDeclaration(EObject context, TerminalDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     {Transport}
	 */
	protected void sequence_Transport(EObject context, Transport semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Identifier is=TypeDefinition?)
	 */
	protected void sequence_TypeDeclaration(EObject context, TypeDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (operator=UnaryOperator expression=Primary)
	 */
	protected void sequence_UnaryExpression(EObject context, UnaryExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (index+=Name index+=Name* type=SubtypeIndication)
	 */
	protected void sequence_UnconstrainedArrayDefinition(EObject context, UnconstrainedArrayDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (index+=Name index+=Name* nature=SubnatureIndication)
	 */
	protected void sequence_UnconstrainedNatureDefinition(EObject context, UnconstrainedNatureDefinition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (use+=Name use+=Name*)
	 */
	protected void sequence_UseClause(EObject context, UseClause semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (number=ABSTRACT_LITERAL unit=Name)
	 */
	protected void sequence_ValueExpression(EObject context, UnitValue semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     number=ABSTRACT_LITERAL
	 */
	protected void sequence_ValueExpression(EObject context, Value semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? target=Target initial=Expression)
	 */
	protected void sequence_VariableAssignmentStatement(EObject context, VariableAssignmentStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (shared?='shared'? name=CommaIdentifier type=SubtypeIndication initial=Expression?)
	 */
	protected void sequence_VariableDeclaration(EObject context, VariableDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     design+=DesignUnit*
	 */
	protected void sequence_VhdlFile(EObject context, VhdlFile semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=Label? (sensitivity+=Name sensitivity+=Name*)? until=Expression? time=Expression?)
	 */
	protected void sequence_WaitStatement(EObject context, WaitStatement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Waveform_Waveform_1_0 after=Expression)
	 */
	protected void sequence_Waveform(EObject context, Waveform semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     condition=Expression
	 */
	protected void sequence_WhileIterationScheme(EObject context, WhileIterationScheme semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
