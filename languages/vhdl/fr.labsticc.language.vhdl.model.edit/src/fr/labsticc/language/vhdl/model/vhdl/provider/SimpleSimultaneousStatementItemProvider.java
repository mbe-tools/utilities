/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.SimpleSimultaneousStatement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleSimultaneousStatementItemProvider extends StatementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleSimultaneousStatementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SimpleSimultaneousStatement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SimpleSimultaneousStatement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SimpleSimultaneousStatement)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_SimpleSimultaneousStatement_type") :
			getString("_UI_SimpleSimultaneousStatement_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SimpleSimultaneousStatement.class)) {
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT__LEFT:
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT__RIGHT:
			case VhdlPackage.SIMPLE_SIMULTANEOUS_STATEMENT__TOLERANCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance(),
				 VhdlFactory.eINSTANCE.createUnitValue()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Left() ||
			childFeature == VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Right() ||
			childFeature == VhdlPackage.eINSTANCE.getSimpleSimultaneousStatement_Tolerance();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
