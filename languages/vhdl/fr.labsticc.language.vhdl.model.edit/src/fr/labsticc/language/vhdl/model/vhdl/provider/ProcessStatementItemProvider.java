/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.ProcessStatement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.ProcessStatement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcessStatementItemProvider extends StatementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcessStatementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPostponedPropertyDescriptor(object);
			addSensitivityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Postponed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPostponedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ProcessStatement_postponed_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ProcessStatement_postponed_feature", "_UI_ProcessStatement_type"),
				 VhdlPackage.eINSTANCE.getProcessStatement_Postponed(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sensitivity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSensitivityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ProcessStatement_sensitivity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ProcessStatement_sensitivity_feature", "_UI_ProcessStatement_type"),
				 VhdlPackage.eINSTANCE.getProcessStatement_Sensitivity(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getProcessStatement_Declaration());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getProcessStatement_Statement());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ProcessStatement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ProcessStatement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ProcessStatement)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ProcessStatement_type") :
			getString("_UI_ProcessStatement_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ProcessStatement.class)) {
			case VhdlPackage.PROCESS_STATEMENT__POSTPONED:
			case VhdlPackage.PROCESS_STATEMENT__SENSITIVITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VhdlPackage.PROCESS_STATEMENT__DECLARATION:
			case VhdlPackage.PROCESS_STATEMENT__STATEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createUseClause()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createAttributeDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createAttributeSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createComponentDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createConfigurationSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createDisconnectionSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createFileDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createGroupDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createGroupTemplateDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createNatureDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createSignalDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createVariableDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createConstantDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createFreeQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createBranchQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createSourceQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createLimitSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createSubnatureDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createSubprogramDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createSubtypeDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createAliasDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createTerminalDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Declaration(),
				 VhdlFactory.eINSTANCE.createTypeDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createAssertionStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createBlockStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createBreakStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createCaseStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createComponentInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createEntityInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createConfigurationInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSequentialSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createConditionalSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSelectedSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimpleSimultaneousStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createExitStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createGenerateStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createIfStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createLoopStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createNextStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createProcedureCallStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createProcessStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createReportStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createReturnStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createNullStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createVariableAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousCaseStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousIfStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousProceduralStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getProcessStatement_Statement(),
				 VhdlFactory.eINSTANCE.createWaitStatement()));
	}

}
