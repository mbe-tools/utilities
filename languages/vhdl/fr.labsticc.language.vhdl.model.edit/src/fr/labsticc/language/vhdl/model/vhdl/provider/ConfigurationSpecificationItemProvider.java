/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.ConfigurationSpecification} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ConfigurationSpecificationItemProvider extends DeclarationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationSpecificationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_List());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_GenericMap());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getConfigurationSpecification_PortMap());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ConfigurationSpecification.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ConfigurationSpecification"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ConfigurationSpecification)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ConfigurationSpecification_type") :
			getString("_UI_ConfigurationSpecification_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ConfigurationSpecification.class)) {
			case VhdlPackage.CONFIGURATION_SPECIFICATION__LIST:
			case VhdlPackage.CONFIGURATION_SPECIFICATION__COMPONENT:
			case VhdlPackage.CONFIGURATION_SPECIFICATION__ENTITY:
			case VhdlPackage.CONFIGURATION_SPECIFICATION__CONFIGURATION:
			case VhdlPackage.CONFIGURATION_SPECIFICATION__GENERIC_MAP:
			case VhdlPackage.CONFIGURATION_SPECIFICATION__PORT_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_List(),
				 VhdlFactory.eINSTANCE.createNameList()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Component(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_GenericMap(),
				 VhdlFactory.eINSTANCE.createGenericMaps()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getConfigurationSpecification_PortMap(),
				 VhdlFactory.eINSTANCE.createPortMaps()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VhdlPackage.eINSTANCE.getConfigurationSpecification_Component() ||
			childFeature == VhdlPackage.eINSTANCE.getConfigurationSpecification_Entity() ||
			childFeature == VhdlPackage.eINSTANCE.getConfigurationSpecification_Configuration();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
