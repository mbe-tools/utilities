/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.LoopStatement;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.LoopStatement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LoopStatementItemProvider extends StatementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopStatementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getLoopStatement_Iteration());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getLoopStatement_Statement());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LoopStatement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LoopStatement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LoopStatement)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LoopStatement_type") :
			getString("_UI_LoopStatement_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LoopStatement.class)) {
			case VhdlPackage.LOOP_STATEMENT__ITERATION:
			case VhdlPackage.LOOP_STATEMENT__STATEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Iteration(),
				 VhdlFactory.eINSTANCE.createIterationScheme()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Iteration(),
				 VhdlFactory.eINSTANCE.createWhileIterationScheme()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Iteration(),
				 VhdlFactory.eINSTANCE.createForIterationScheme()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createAssertionStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createBlockStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createBreakStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createCaseStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createComponentInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createEntityInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createConfigurationInstantiationStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSequentialSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createConditionalSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSelectedSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimpleSimultaneousStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createExitStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createGenerateStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createIfStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createLoopStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createNextStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createProcedureCallStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createProcessStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createReportStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createReturnStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createNullStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSignalAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createVariableAssignmentStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousCaseStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousIfStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createSimultaneousProceduralStatement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLoopStatement_Statement(),
				 VhdlFactory.eINSTANCE.createWaitStatement()));
	}

}
