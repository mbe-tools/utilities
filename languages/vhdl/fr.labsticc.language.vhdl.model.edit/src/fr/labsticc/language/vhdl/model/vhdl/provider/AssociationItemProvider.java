/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.Association;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.Association} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AssociationItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getAssociation_Choice());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getAssociation_Expression());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Association.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Association"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Association)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Association_type") :
			getString("_UI_Association_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Association.class)) {
			case VhdlPackage.ASSOCIATION__CHOICE:
			case VhdlPackage.ASSOCIATION__EXPRESSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Choice(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getAssociation_Expression(),
				 VhdlFactory.eINSTANCE.createUnitValue()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VhdlPackage.eINSTANCE.getAssociation_Choice() ||
			childFeature == VhdlPackage.eINSTANCE.getAssociation_Expression();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
