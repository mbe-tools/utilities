/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.DisconnectionSpecification} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DisconnectionSpecificationItemProvider extends DeclarationItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisconnectionSpecificationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Disconnect());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type());
			childrenFeatures.add(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DisconnectionSpecification.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DisconnectionSpecification"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DisconnectionSpecification)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_DisconnectionSpecification_type") :
			getString("_UI_DisconnectionSpecification_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DisconnectionSpecification.class)) {
			case VhdlPackage.DISCONNECTION_SPECIFICATION__DISCONNECT:
			case VhdlPackage.DISCONNECTION_SPECIFICATION__TYPE:
			case VhdlPackage.DISCONNECTION_SPECIFICATION__AFTER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Disconnect(),
				 VhdlFactory.eINSTANCE.createNameList()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type(),
				 VhdlFactory.eINSTANCE.createUnitValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createNameElement()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createOpen()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAllocator()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createNull()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createValue()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAggregate()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAll()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createOthers()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createRelationalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createShiftExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createAddingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createMultiplyingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createPowerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createSignExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createRange()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createName()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createBitString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createString()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createCharacter()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createIndication()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createWaveform()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getDisconnectionSpecification_After(),
				 VhdlFactory.eINSTANCE.createUnitValue()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VhdlPackage.eINSTANCE.getDisconnectionSpecification_Type() ||
			childFeature == VhdlPackage.eINSTANCE.getDisconnectionSpecification_After();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
