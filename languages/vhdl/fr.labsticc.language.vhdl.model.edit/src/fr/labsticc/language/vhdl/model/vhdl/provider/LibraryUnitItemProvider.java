/**
 */
package fr.labsticc.language.vhdl.model.vhdl.provider;


import fr.labsticc.language.vhdl.model.vhdl.LibraryUnit;
import fr.labsticc.language.vhdl.model.vhdl.VhdlFactory;
import fr.labsticc.language.vhdl.model.vhdl.VhdlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.labsticc.language.vhdl.model.vhdl.LibraryUnit} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LibraryUnitItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryUnitItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LibraryUnit.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LibraryUnit"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LibraryUnit)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LibraryUnit_type") :
			getString("_UI_LibraryUnit_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LibraryUnit.class)) {
			case VhdlPackage.LIBRARY_UNIT__DECLARATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createUseClause()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createAttributeDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createAttributeSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createComponentDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createConfigurationSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createDisconnectionSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createFileDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createGroupDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createGroupTemplateDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createNatureDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createSignalDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createVariableDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createConstantDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createFreeQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createBranchQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createSourceQuantityDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createLimitSpecification()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createSubnatureDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createSubprogramDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createSubtypeDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createAliasDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createTerminalDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(VhdlPackage.eINSTANCE.getLibraryUnit_Declaration(),
				 VhdlFactory.eINSTANCE.createTypeDeclaration()));
	}

}
