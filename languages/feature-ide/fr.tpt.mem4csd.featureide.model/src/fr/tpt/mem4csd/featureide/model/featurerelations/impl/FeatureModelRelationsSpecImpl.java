/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations.impl;

import fr.tpt.mem4csd.featureide.model.featurerelations.Disabled;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage;
import fr.tpt.mem4csd.featureide.model.featurerelations.Individual;
import fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Model Relations Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl#getOwnedSubClassRelations <em>Owned Sub Class Relations</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl#getIndividuals <em>Individuals</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl#getDisabledFeatures <em>Disabled Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureModelRelationsSpecImpl extends MinimalEObjectImpl.Container implements FeatureModelRelationsSpec {
	/**
	 * The cached value of the '{@link #getOwnedSubClassRelations() <em>Owned Sub Class Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedSubClassRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<SubClassRelation> ownedSubClassRelations;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIndividuals() <em>Individuals</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndividuals()
	 * @generated
	 * @ordered
	 */
	protected Individual individuals;

	/**
	 * The cached value of the '{@link #getDisabledFeatures() <em>Disabled Features</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisabledFeatures()
	 * @generated
	 * @ordered
	 */
	protected Disabled disabledFeatures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModelRelationsSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeaturerelationsPackage.Literals.FEATURE_MODEL_RELATIONS_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubClassRelation> getOwnedSubClassRelations() {
		if (ownedSubClassRelations == null) {
			ownedSubClassRelations = new EObjectContainmentEList<SubClassRelation>(SubClassRelation.class, this, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS);
		}
		return ownedSubClassRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Individual getIndividuals() {
		return individuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndividuals(Individual newIndividuals, NotificationChain msgs) {
		Individual oldIndividuals = individuals;
		individuals = newIndividuals;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS, oldIndividuals, newIndividuals);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIndividuals(Individual newIndividuals) {
		if (newIndividuals != individuals) {
			NotificationChain msgs = null;
			if (individuals != null)
				msgs = ((InternalEObject)individuals).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS, null, msgs);
			if (newIndividuals != null)
				msgs = ((InternalEObject)newIndividuals).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS, null, msgs);
			msgs = basicSetIndividuals(newIndividuals, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS, newIndividuals, newIndividuals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Disabled getDisabledFeatures() {
		return disabledFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisabledFeatures(Disabled newDisabledFeatures, NotificationChain msgs) {
		Disabled oldDisabledFeatures = disabledFeatures;
		disabledFeatures = newDisabledFeatures;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES, oldDisabledFeatures, newDisabledFeatures);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDisabledFeatures(Disabled newDisabledFeatures) {
		if (newDisabledFeatures != disabledFeatures) {
			NotificationChain msgs = null;
			if (disabledFeatures != null)
				msgs = ((InternalEObject)disabledFeatures).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES, null, msgs);
			if (newDisabledFeatures != null)
				msgs = ((InternalEObject)newDisabledFeatures).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES, null, msgs);
			msgs = basicSetDisabledFeatures(newDisabledFeatures, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES, newDisabledFeatures, newDisabledFeatures));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS:
				return ((InternalEList<?>)getOwnedSubClassRelations()).basicRemove(otherEnd, msgs);
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS:
				return basicSetIndividuals(null, msgs);
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES:
				return basicSetDisabledFeatures(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS:
				return getOwnedSubClassRelations();
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__NAME:
				return getName();
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS:
				return getIndividuals();
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES:
				return getDisabledFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS:
				getOwnedSubClassRelations().clear();
				getOwnedSubClassRelations().addAll((Collection<? extends SubClassRelation>)newValue);
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__NAME:
				setName((String)newValue);
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS:
				setIndividuals((Individual)newValue);
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES:
				setDisabledFeatures((Disabled)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS:
				getOwnedSubClassRelations().clear();
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS:
				setIndividuals((Individual)null);
				return;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES:
				setDisabledFeatures((Disabled)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS:
				return ownedSubClassRelations != null && !ownedSubClassRelations.isEmpty();
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS:
				return individuals != null;
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES:
				return disabledFeatures != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //FeatureModelRelationsSpecImpl
