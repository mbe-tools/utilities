/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;

import fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage;
import fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Class Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl#getSuperBranch <em>Super Branch</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl#getSubBranch <em>Sub Branch</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl#getSubFeature <em>Sub Feature</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl#isHasIndividuals <em>Has Individuals</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubClassRelationImpl extends MinimalEObjectImpl.Container implements SubClassRelation {
	/**
	 * The cached value of the '{@link #getSuperBranch() <em>Super Branch</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperBranch()
	 * @generated
	 * @ordered
	 */
	protected BranchType superBranch;

	/**
	 * The cached value of the '{@link #getSubBranch() <em>Sub Branch</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubBranch()
	 * @generated
	 * @ordered
	 */
	protected EList<BranchType> subBranch;

	/**
	 * The cached value of the '{@link #getSubFeature() <em>Sub Feature</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<FeatureType> subFeature;

	/**
	 * The default value of the '{@link #isHasIndividuals() <em>Has Individuals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasIndividuals()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_INDIVIDUALS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasIndividuals() <em>Has Individuals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasIndividuals()
	 * @generated
	 * @ordered
	 */
	protected boolean hasIndividuals = HAS_INDIVIDUALS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubClassRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeaturerelationsPackage.Literals.SUB_CLASS_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BranchType getSuperBranch() {
		if (superBranch != null && superBranch.eIsProxy()) {
			InternalEObject oldSuperBranch = (InternalEObject)superBranch;
			superBranch = (BranchType)eResolveProxy(oldSuperBranch);
			if (superBranch != oldSuperBranch) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH, oldSuperBranch, superBranch));
			}
		}
		return superBranch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BranchType basicGetSuperBranch() {
		return superBranch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSuperBranch(BranchType newSuperBranch) {
		BranchType oldSuperBranch = superBranch;
		superBranch = newSuperBranch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH, oldSuperBranch, superBranch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BranchType> getSubBranch() {
		if (subBranch == null) {
			subBranch = new EObjectResolvingEList<BranchType>(BranchType.class, this, FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_BRANCH);
		}
		return subBranch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FeatureType> getSubFeature() {
		if (subFeature == null) {
			subFeature = new EObjectResolvingEList<FeatureType>(FeatureType.class, this, FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_FEATURE);
		}
		return subFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isHasIndividuals() {
		return hasIndividuals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHasIndividuals(boolean newHasIndividuals) {
		boolean oldHasIndividuals = hasIndividuals;
		hasIndividuals = newHasIndividuals;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeaturerelationsPackage.SUB_CLASS_RELATION__HAS_INDIVIDUALS, oldHasIndividuals, hasIndividuals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH:
				if (resolve) return getSuperBranch();
				return basicGetSuperBranch();
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_BRANCH:
				return getSubBranch();
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_FEATURE:
				return getSubFeature();
			case FeaturerelationsPackage.SUB_CLASS_RELATION__HAS_INDIVIDUALS:
				return isHasIndividuals();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH:
				setSuperBranch((BranchType)newValue);
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_BRANCH:
				getSubBranch().clear();
				getSubBranch().addAll((Collection<? extends BranchType>)newValue);
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_FEATURE:
				getSubFeature().clear();
				getSubFeature().addAll((Collection<? extends FeatureType>)newValue);
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__HAS_INDIVIDUALS:
				setHasIndividuals((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH:
				setSuperBranch((BranchType)null);
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_BRANCH:
				getSubBranch().clear();
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_FEATURE:
				getSubFeature().clear();
				return;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__HAS_INDIVIDUALS:
				setHasIndividuals(HAS_INDIVIDUALS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUPER_BRANCH:
				return superBranch != null;
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_BRANCH:
				return subBranch != null && !subBranch.isEmpty();
			case FeaturerelationsPackage.SUB_CLASS_RELATION__SUB_FEATURE:
				return subFeature != null && !subFeature.isEmpty();
			case FeaturerelationsPackage.SUB_CLASS_RELATION__HAS_INDIVIDUALS:
				return hasIndividuals != HAS_INDIVIDUALS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (hasIndividuals: ");
		result.append(hasIndividuals);
		result.append(')');
		return result.toString();
	}

} //SubClassRelationImpl
