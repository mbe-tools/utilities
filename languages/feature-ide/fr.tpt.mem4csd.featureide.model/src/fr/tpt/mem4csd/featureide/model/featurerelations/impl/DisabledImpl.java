/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;

import fr.tpt.mem4csd.featureide.model.featurerelations.Disabled;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Disabled</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl#getBranches <em>Branches</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DisabledImpl extends MinimalEObjectImpl.Container implements Disabled {
	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<FeatureType> features;

	/**
	 * The cached value of the '{@link #getBranches() <em>Branches</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBranches()
	 * @generated
	 * @ordered
	 */
	protected EList<BranchType> branches;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DisabledImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeaturerelationsPackage.Literals.DISABLED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FeatureType> getFeatures() {
		if (features == null) {
			features = new EObjectResolvingEList<FeatureType>(FeatureType.class, this, FeaturerelationsPackage.DISABLED__FEATURES);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BranchType> getBranches() {
		if (branches == null) {
			branches = new EObjectResolvingEList<BranchType>(BranchType.class, this, FeaturerelationsPackage.DISABLED__BRANCHES);
		}
		return branches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeaturerelationsPackage.DISABLED__FEATURES:
				return getFeatures();
			case FeaturerelationsPackage.DISABLED__BRANCHES:
				return getBranches();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeaturerelationsPackage.DISABLED__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends FeatureType>)newValue);
				return;
			case FeaturerelationsPackage.DISABLED__BRANCHES:
				getBranches().clear();
				getBranches().addAll((Collection<? extends BranchType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.DISABLED__FEATURES:
				getFeatures().clear();
				return;
			case FeaturerelationsPackage.DISABLED__BRANCHES:
				getBranches().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeaturerelationsPackage.DISABLED__FEATURES:
				return features != null && !features.isEmpty();
			case FeaturerelationsPackage.DISABLED__BRANCHES:
				return branches != null && !branches.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DisabledImpl
