/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations;

import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Disabled</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getFeatures <em>Features</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getBranches <em>Branches</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getDisabled()
 * @model
 * @generated
 */
public interface Disabled extends EObject {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getDisabled_Features()
	 * @model
	 * @generated
	 */
	EList<FeatureType> getFeatures();

	/**
	 * Returns the value of the '<em><b>Branches</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Branches</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getDisabled_Branches()
	 * @model
	 * @generated
	 */
	EList<BranchType> getBranches();

} // Disabled
