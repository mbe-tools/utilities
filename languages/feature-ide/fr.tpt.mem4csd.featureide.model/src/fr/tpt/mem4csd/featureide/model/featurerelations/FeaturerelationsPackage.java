/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsFactory
 * @model kind="package"
 * @generated
 */
public interface FeaturerelationsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "featurerelations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/fr.tpt.mem4csd.featureide.model/model/featurerelations.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "featurerelations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeaturerelationsPackage eINSTANCE = fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl <em>Sub Class Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getSubClassRelation()
	 * @generated
	 */
	int SUB_CLASS_RELATION = 0;

	/**
	 * The feature id for the '<em><b>Super Branch</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION__SUPER_BRANCH = 0;

	/**
	 * The feature id for the '<em><b>Sub Branch</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION__SUB_BRANCH = 1;

	/**
	 * The feature id for the '<em><b>Sub Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION__SUB_FEATURE = 2;

	/**
	 * The feature id for the '<em><b>Has Individuals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION__HAS_INDIVIDUALS = 3;

	/**
	 * The number of structural features of the '<em>Sub Class Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Sub Class Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_CLASS_RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl <em>Feature Model Relations Spec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getFeatureModelRelationsSpec()
	 * @generated
	 */
	int FEATURE_MODEL_RELATIONS_SPEC = 1;

	/**
	 * The feature id for the '<em><b>Owned Sub Class Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC__NAME = 1;

	/**
	 * The feature id for the '<em><b>Individuals</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS = 2;

	/**
	 * The feature id for the '<em><b>Disabled Features</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES = 3;

	/**
	 * The number of structural features of the '<em>Feature Model Relations Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Feature Model Relations Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_RELATIONS_SPEC_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.IndividualImpl <em>Individual</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.IndividualImpl
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getIndividual()
	 * @generated
	 */
	int INDIVIDUAL = 2;

	/**
	 * The feature id for the '<em><b>Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL__FEATURES = 0;

	/**
	 * The number of structural features of the '<em>Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Individual</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDIVIDUAL_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl <em>Disabled</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getDisabled()
	 * @generated
	 */
	int DISABLED = 3;

	/**
	 * The feature id for the '<em><b>Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISABLED__FEATURES = 0;

	/**
	 * The feature id for the '<em><b>Branches</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISABLED__BRANCHES = 1;

	/**
	 * The number of structural features of the '<em>Disabled</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISABLED_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Disabled</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISABLED_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation <em>Sub Class Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Class Relation</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation
	 * @generated
	 */
	EClass getSubClassRelation();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSuperBranch <em>Super Branch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Branch</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSuperBranch()
	 * @see #getSubClassRelation()
	 * @generated
	 */
	EReference getSubClassRelation_SuperBranch();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubBranch <em>Sub Branch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Branch</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubBranch()
	 * @see #getSubClassRelation()
	 * @generated
	 */
	EReference getSubClassRelation_SubBranch();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubFeature <em>Sub Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Feature</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubFeature()
	 * @see #getSubClassRelation()
	 * @generated
	 */
	EReference getSubClassRelation_SubFeature();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#isHasIndividuals <em>Has Individuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Individuals</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#isHasIndividuals()
	 * @see #getSubClassRelation()
	 * @generated
	 */
	EAttribute getSubClassRelation_HasIndividuals();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec <em>Feature Model Relations Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model Relations Spec</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec
	 * @generated
	 */
	EClass getFeatureModelRelationsSpec();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getOwnedSubClassRelations <em>Owned Sub Class Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Sub Class Relations</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getOwnedSubClassRelations()
	 * @see #getFeatureModelRelationsSpec()
	 * @generated
	 */
	EReference getFeatureModelRelationsSpec_OwnedSubClassRelations();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getName()
	 * @see #getFeatureModelRelationsSpec()
	 * @generated
	 */
	EAttribute getFeatureModelRelationsSpec_Name();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getIndividuals <em>Individuals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Individuals</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getIndividuals()
	 * @see #getFeatureModelRelationsSpec()
	 * @generated
	 */
	EReference getFeatureModelRelationsSpec_Individuals();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getDisabledFeatures <em>Disabled Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disabled Features</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getDisabledFeatures()
	 * @see #getFeatureModelRelationsSpec()
	 * @generated
	 */
	EReference getFeatureModelRelationsSpec_DisabledFeatures();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.featurerelations.Individual <em>Individual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Individual</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.Individual
	 * @generated
	 */
	EClass getIndividual();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.Individual#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Features</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.Individual#getFeatures()
	 * @see #getIndividual()
	 * @generated
	 */
	EReference getIndividual_Features();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.featurerelations.Disabled <em>Disabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disabled</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.Disabled
	 * @generated
	 */
	EClass getDisabled();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Features</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getFeatures()
	 * @see #getDisabled()
	 * @generated
	 */
	EReference getDisabled_Features();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getBranches <em>Branches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Branches</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.Disabled#getBranches()
	 * @see #getDisabled()
	 * @generated
	 */
	EReference getDisabled_Branches();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeaturerelationsFactory getFeaturerelationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl <em>Sub Class Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.SubClassRelationImpl
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getSubClassRelation()
		 * @generated
		 */
		EClass SUB_CLASS_RELATION = eINSTANCE.getSubClassRelation();

		/**
		 * The meta object literal for the '<em><b>Super Branch</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_CLASS_RELATION__SUPER_BRANCH = eINSTANCE.getSubClassRelation_SuperBranch();

		/**
		 * The meta object literal for the '<em><b>Sub Branch</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_CLASS_RELATION__SUB_BRANCH = eINSTANCE.getSubClassRelation_SubBranch();

		/**
		 * The meta object literal for the '<em><b>Sub Feature</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_CLASS_RELATION__SUB_FEATURE = eINSTANCE.getSubClassRelation_SubFeature();

		/**
		 * The meta object literal for the '<em><b>Has Individuals</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_CLASS_RELATION__HAS_INDIVIDUALS = eINSTANCE.getSubClassRelation_HasIndividuals();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl <em>Feature Model Relations Spec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeatureModelRelationsSpecImpl
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getFeatureModelRelationsSpec()
		 * @generated
		 */
		EClass FEATURE_MODEL_RELATIONS_SPEC = eINSTANCE.getFeatureModelRelationsSpec();

		/**
		 * The meta object literal for the '<em><b>Owned Sub Class Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS = eINSTANCE.getFeatureModelRelationsSpec_OwnedSubClassRelations();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_MODEL_RELATIONS_SPEC__NAME = eINSTANCE.getFeatureModelRelationsSpec_Name();

		/**
		 * The meta object literal for the '<em><b>Individuals</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS = eINSTANCE.getFeatureModelRelationsSpec_Individuals();

		/**
		 * The meta object literal for the '<em><b>Disabled Features</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES = eINSTANCE.getFeatureModelRelationsSpec_DisabledFeatures();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.IndividualImpl <em>Individual</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.IndividualImpl
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getIndividual()
		 * @generated
		 */
		EClass INDIVIDUAL = eINSTANCE.getIndividual();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INDIVIDUAL__FEATURES = eINSTANCE.getIndividual_Features();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl <em>Disabled</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.DisabledImpl
		 * @see fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsPackageImpl#getDisabled()
		 * @generated
		 */
		EClass DISABLED = eINSTANCE.getDisabled();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISABLED__FEATURES = eINSTANCE.getDisabled_Features();

		/**
		 * The meta object literal for the '<em><b>Branches</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISABLED__BRANCHES = eINSTANCE.getDisabled_Branches();

	}

} //FeaturerelationsPackage
