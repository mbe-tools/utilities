/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations;

import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Class Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSuperBranch <em>Super Branch</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubBranch <em>Sub Branch</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSubFeature <em>Sub Feature</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#isHasIndividuals <em>Has Individuals</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getSubClassRelation()
 * @model
 * @generated
 */
public interface SubClassRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Super Branch</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Branch</em>' reference.
	 * @see #setSuperBranch(BranchType)
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getSubClassRelation_SuperBranch()
	 * @model
	 * @generated
	 */
	BranchType getSuperBranch();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#getSuperBranch <em>Super Branch</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Branch</em>' reference.
	 * @see #getSuperBranch()
	 * @generated
	 */
	void setSuperBranch(BranchType value);

	/**
	 * Returns the value of the '<em><b>Sub Branch</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Branch</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getSubClassRelation_SubBranch()
	 * @model
	 * @generated
	 */
	EList<BranchType> getSubBranch();

	/**
	 * Returns the value of the '<em><b>Sub Feature</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Feature</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getSubClassRelation_SubFeature()
	 * @model
	 * @generated
	 */
	EList<FeatureType> getSubFeature();

	/**
	 * Returns the value of the '<em><b>Has Individuals</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Individuals</em>' attribute.
	 * @see #setHasIndividuals(boolean)
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getSubClassRelation_HasIndividuals()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isHasIndividuals();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation#isHasIndividuals <em>Has Individuals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Individuals</em>' attribute.
	 * @see #isHasIndividuals()
	 * @generated
	 */
	void setHasIndividuals(boolean value);

} // SubClassRelation
