/**
 */
package fr.tpt.mem4csd.featureide.model.Configuration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationType#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getConfigurationType()
 * @model extendedMetaData="name='configuration_._type' kind='elementOnly'"
 * @generated
 */
public interface ConfigurationType extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getConfigurationType_Feature()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='feature' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FeatureType> getFeature();

} // ConfigurationType
