/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphics Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getKey <em>Key</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getGraphicsType()
 * @model extendedMetaData="name='GraphicsType' kind='elementOnly'"
 * @generated
 */
public interface GraphicsType extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getGraphicsType_Key()
	 * @model required="true"
	 * @generated
	 */
	String getKey();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getGraphicsType_Value()
	 * @model required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // GraphicsType
