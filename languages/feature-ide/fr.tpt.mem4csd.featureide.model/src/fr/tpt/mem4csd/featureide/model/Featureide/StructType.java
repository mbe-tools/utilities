/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getFeature <em>Feature</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAnd <em>And</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getOr <em>Or</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAlt <em>Alt</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getStructType()
 * @model extendedMetaData="name='struct_._type' kind='elementOnly'"
 * @generated
 */
public interface StructType extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference.
	 * @see #setFeature(FeatureType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getStructType_Feature()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='feature' namespace='##targetNamespace'"
	 * @generated
	 */
	FeatureType getFeature();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getFeature <em>Feature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' containment reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(FeatureType value);

	/**
	 * Returns the value of the '<em><b>And</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And</em>' containment reference.
	 * @see #setAnd(BranchType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getStructType_And()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='and' namespace='##targetNamespace'"
	 * @generated
	 */
	BranchType getAnd();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAnd <em>And</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And</em>' containment reference.
	 * @see #getAnd()
	 * @generated
	 */
	void setAnd(BranchType value);

	/**
	 * Returns the value of the '<em><b>Or</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or</em>' containment reference.
	 * @see #setOr(BranchType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getStructType_Or()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or' namespace='##targetNamespace'"
	 * @generated
	 */
	BranchType getOr();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getOr <em>Or</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or</em>' containment reference.
	 * @see #getOr()
	 * @generated
	 */
	void setOr(BranchType value);

	/**
	 * Returns the value of the '<em><b>Alt</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alt</em>' containment reference.
	 * @see #setAlt(BranchType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getStructType_Alt()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='alt' namespace='##targetNamespace'"
	 * @generated
	 */
	BranchType getAlt();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAlt <em>Alt</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alt</em>' containment reference.
	 * @see #getAlt()
	 * @generated
	 */
	void setAlt(BranchType value);

} // StructType
