/**
 */
package fr.tpt.mem4csd.featureide.model.Configuration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue <em>Value</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic <em>Automatic</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual <em>Manual</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue1 <em>Value1</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType()
 * @model extendedMetaData="name='featureType' kind='simple'"
 * @generated
 */
public interface FeatureType extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Automatic</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Automatic</em>' attribute.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see #isSetAutomatic()
	 * @see #unsetAutomatic()
	 * @see #setAutomatic(SelectionType)
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType_Automatic()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='automatic' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectionType getAutomatic();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic <em>Automatic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Automatic</em>' attribute.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see #isSetAutomatic()
	 * @see #unsetAutomatic()
	 * @see #getAutomatic()
	 * @generated
	 */
	void setAutomatic(SelectionType value);

	/**
	 * Unsets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic <em>Automatic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAutomatic()
	 * @see #getAutomatic()
	 * @see #setAutomatic(SelectionType)
	 * @generated
	 */
	void unsetAutomatic();

	/**
	 * Returns whether the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic <em>Automatic</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Automatic</em>' attribute is set.
	 * @see #unsetAutomatic()
	 * @see #getAutomatic()
	 * @see #setAutomatic(SelectionType)
	 * @generated
	 */
	boolean isSetAutomatic();

	/**
	 * Returns the value of the '<em><b>Manual</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manual</em>' attribute.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see #isSetManual()
	 * @see #unsetManual()
	 * @see #setManual(SelectionType)
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType_Manual()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='manual' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectionType getManual();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual <em>Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manual</em>' attribute.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see #isSetManual()
	 * @see #unsetManual()
	 * @see #getManual()
	 * @generated
	 */
	void setManual(SelectionType value);

	/**
	 * Unsets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual <em>Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetManual()
	 * @see #getManual()
	 * @see #setManual(SelectionType)
	 * @generated
	 */
	void unsetManual();

	/**
	 * Returns whether the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual <em>Manual</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Manual</em>' attribute is set.
	 * @see #unsetManual()
	 * @see #getManual()
	 * @see #setManual(SelectionType)
	 * @generated
	 */
	boolean isSetManual();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value1</em>' attribute.
	 * @see #setValue1(String)
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage#getFeatureType_Value1()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='value' namespace='##targetNamespace'"
	 * @generated
	 */
	String getValue1();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue1 <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value1</em>' attribute.
	 * @see #getValue1()
	 * @generated
	 */
	void setValue1(String value);

} // FeatureType
