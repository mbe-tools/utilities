/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureideFactoryImpl extends EFactoryImpl implements FeatureideFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureideFactory init() {
		try {
			FeatureideFactory theFeatureideFactory = (FeatureideFactory)EPackage.Registry.INSTANCE.getEFactory(FeatureidePackage.eNS_URI);
			if (theFeatureideFactory != null) {
				return theFeatureideFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FeatureideFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureideFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FeatureidePackage.BINARY_FORMULA_TYPE: return createBinaryFormulaType();
			case FeatureidePackage.BRANCH_TYPE: return createBranchType();
			case FeatureidePackage.CONSTRAINTS_TYPE: return createConstraintsType();
			case FeatureidePackage.DOCUMENT_ROOT: return createDocumentRoot();
			case FeatureidePackage.FEATURE_MODEL_TYPE: return createFeatureModelType();
			case FeatureidePackage.FEATURE_TYPE: return createFeatureType();
			case FeatureidePackage.RULE_TYPE: return createRuleType();
			case FeatureidePackage.STRUCT_TYPE: return createStructType();
			case FeatureidePackage.UNARY_FORMULA_TYPE: return createUnaryFormulaType();
			case FeatureidePackage.PROPERTIES_TYPE: return createPropertiesType();
			case FeatureidePackage.GRAPHICS_TYPE: return createGraphicsType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryFormulaType createBinaryFormulaType() {
		BinaryFormulaTypeImpl binaryFormulaType = new BinaryFormulaTypeImpl();
		return binaryFormulaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BranchType createBranchType() {
		BranchTypeImpl branchType = new BranchTypeImpl();
		return branchType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstraintsType createConstraintsType() {
		ConstraintsTypeImpl constraintsType = new ConstraintsTypeImpl();
		return constraintsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureModelType createFeatureModelType() {
		FeatureModelTypeImpl featureModelType = new FeatureModelTypeImpl();
		return featureModelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureType createFeatureType() {
		FeatureTypeImpl featureType = new FeatureTypeImpl();
		return featureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RuleType createRuleType() {
		RuleTypeImpl ruleType = new RuleTypeImpl();
		return ruleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StructType createStructType() {
		StructTypeImpl structType = new StructTypeImpl();
		return structType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnaryFormulaType createUnaryFormulaType() {
		UnaryFormulaTypeImpl unaryFormulaType = new UnaryFormulaTypeImpl();
		return unaryFormulaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PropertiesType createPropertiesType() {
		PropertiesTypeImpl propertiesType = new PropertiesTypeImpl();
		return propertiesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GraphicsType createGraphicsType() {
		GraphicsTypeImpl graphicsType = new GraphicsTypeImpl();
		return graphicsType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureidePackage getFeatureidePackage() {
		return (FeatureidePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FeatureidePackage getPackage() {
		return FeatureidePackage.eINSTANCE;
	}

} //FeatureideFactoryImpl
