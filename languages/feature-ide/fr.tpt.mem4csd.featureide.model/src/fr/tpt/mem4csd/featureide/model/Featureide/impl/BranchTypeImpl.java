/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Branch Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getNode <em>Node</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getAnd <em>And</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getOr <em>Or</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getAlt <em>Alt</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#getGraphics <em>Graphics</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl#isHidden <em>Hidden</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BranchTypeImpl extends MinimalEObjectImpl.Container implements BranchType {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap node;

	/**
	 * The default value of the '{@link #getAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final String ABSTRACT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstract()
	 * @generated
	 * @ordered
	 */
	protected String abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final String MANDATORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected String mandatory = MANDATORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGraphics() <em>Graphics</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphics()
	 * @generated
	 * @ordered
	 */
	protected EList<GraphicsType> graphics;

	/**
	 * The default value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
	protected boolean hidden = HIDDEN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BranchTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeatureidePackage.Literals.BRANCH_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.BRANCH_TYPE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getNode() {
		if (node == null) {
			node = new BasicFeatureMap(this, FeatureidePackage.BRANCH_TYPE__NODE);
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FeatureType> getFeature() {
		return getNode().list(FeatureidePackage.Literals.BRANCH_TYPE__FEATURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BranchType> getAnd() {
		return getNode().list(FeatureidePackage.Literals.BRANCH_TYPE__AND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BranchType> getOr() {
		return getNode().list(FeatureidePackage.Literals.BRANCH_TYPE__OR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BranchType> getAlt() {
		return getNode().list(FeatureidePackage.Literals.BRANCH_TYPE__ALT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAbstract(String newAbstract) {
		String oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.BRANCH_TYPE__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMandatory() {
		return mandatory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMandatory(String newMandatory) {
		String oldMandatory = mandatory;
		mandatory = newMandatory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.BRANCH_TYPE__MANDATORY, oldMandatory, mandatory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.BRANCH_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GraphicsType> getGraphics() {
		if (graphics == null) {
			graphics = new EObjectResolvingEList<GraphicsType>(GraphicsType.class, this, FeatureidePackage.BRANCH_TYPE__GRAPHICS);
		}
		return graphics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHidden(boolean newHidden) {
		boolean oldHidden = hidden;
		hidden = newHidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.BRANCH_TYPE__HIDDEN, oldHidden, hidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeatureidePackage.BRANCH_TYPE__NODE:
				return ((InternalEList<?>)getNode()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BRANCH_TYPE__FEATURE:
				return ((InternalEList<?>)getFeature()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BRANCH_TYPE__AND:
				return ((InternalEList<?>)getAnd()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BRANCH_TYPE__OR:
				return ((InternalEList<?>)getOr()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BRANCH_TYPE__ALT:
				return ((InternalEList<?>)getAlt()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeatureidePackage.BRANCH_TYPE__DESCRIPTION:
				return getDescription();
			case FeatureidePackage.BRANCH_TYPE__NODE:
				if (coreType) return getNode();
				return ((FeatureMap.Internal)getNode()).getWrapper();
			case FeatureidePackage.BRANCH_TYPE__FEATURE:
				return getFeature();
			case FeatureidePackage.BRANCH_TYPE__AND:
				return getAnd();
			case FeatureidePackage.BRANCH_TYPE__OR:
				return getOr();
			case FeatureidePackage.BRANCH_TYPE__ALT:
				return getAlt();
			case FeatureidePackage.BRANCH_TYPE__ABSTRACT:
				return getAbstract();
			case FeatureidePackage.BRANCH_TYPE__MANDATORY:
				return getMandatory();
			case FeatureidePackage.BRANCH_TYPE__NAME:
				return getName();
			case FeatureidePackage.BRANCH_TYPE__GRAPHICS:
				return getGraphics();
			case FeatureidePackage.BRANCH_TYPE__HIDDEN:
				return isHidden();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeatureidePackage.BRANCH_TYPE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__NODE:
				((FeatureMap.Internal)getNode()).set(newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__FEATURE:
				getFeature().clear();
				getFeature().addAll((Collection<? extends FeatureType>)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__AND:
				getAnd().clear();
				getAnd().addAll((Collection<? extends BranchType>)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__OR:
				getOr().clear();
				getOr().addAll((Collection<? extends BranchType>)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__ALT:
				getAlt().clear();
				getAlt().addAll((Collection<? extends BranchType>)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__ABSTRACT:
				setAbstract((String)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__MANDATORY:
				setMandatory((String)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__NAME:
				setName((String)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__GRAPHICS:
				getGraphics().clear();
				getGraphics().addAll((Collection<? extends GraphicsType>)newValue);
				return;
			case FeatureidePackage.BRANCH_TYPE__HIDDEN:
				setHidden((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeatureidePackage.BRANCH_TYPE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case FeatureidePackage.BRANCH_TYPE__NODE:
				getNode().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__FEATURE:
				getFeature().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__AND:
				getAnd().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__OR:
				getOr().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__ALT:
				getAlt().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case FeatureidePackage.BRANCH_TYPE__MANDATORY:
				setMandatory(MANDATORY_EDEFAULT);
				return;
			case FeatureidePackage.BRANCH_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FeatureidePackage.BRANCH_TYPE__GRAPHICS:
				getGraphics().clear();
				return;
			case FeatureidePackage.BRANCH_TYPE__HIDDEN:
				setHidden(HIDDEN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeatureidePackage.BRANCH_TYPE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case FeatureidePackage.BRANCH_TYPE__NODE:
				return node != null && !node.isEmpty();
			case FeatureidePackage.BRANCH_TYPE__FEATURE:
				return !getFeature().isEmpty();
			case FeatureidePackage.BRANCH_TYPE__AND:
				return !getAnd().isEmpty();
			case FeatureidePackage.BRANCH_TYPE__OR:
				return !getOr().isEmpty();
			case FeatureidePackage.BRANCH_TYPE__ALT:
				return !getAlt().isEmpty();
			case FeatureidePackage.BRANCH_TYPE__ABSTRACT:
				return ABSTRACT_EDEFAULT == null ? abstract_ != null : !ABSTRACT_EDEFAULT.equals(abstract_);
			case FeatureidePackage.BRANCH_TYPE__MANDATORY:
				return MANDATORY_EDEFAULT == null ? mandatory != null : !MANDATORY_EDEFAULT.equals(mandatory);
			case FeatureidePackage.BRANCH_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FeatureidePackage.BRANCH_TYPE__GRAPHICS:
				return graphics != null && !graphics.isEmpty();
			case FeatureidePackage.BRANCH_TYPE__HIDDEN:
				return hidden != HIDDEN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", node: ");
		result.append(node);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(", mandatory: ");
		result.append(mandatory);
		result.append(", name: ");
		result.append(name);
		result.append(", hidden: ");
		result.append(hidden);
		result.append(')');
		return result.toString();
	}

} //BranchTypeImpl
