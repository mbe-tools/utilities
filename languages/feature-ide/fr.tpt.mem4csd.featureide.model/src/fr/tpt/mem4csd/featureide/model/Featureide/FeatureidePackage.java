/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureideFactory
 * @model kind="package"
 *        extendedMetaData="qualified='false'"
 * @generated
 */
public interface FeatureidePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Featureide";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/fr.tpt.mem4csd.featureide.model/model/featureide.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Featureide";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeatureidePackage eINSTANCE = fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl <em>Binary Formula Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getBinaryFormulaType()
	 * @generated
	 */
	int BINARY_FORMULA_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__FORMULA = 0;

	/**
	 * The feature id for the '<em><b>Conj</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__CONJ = 1;

	/**
	 * The feature id for the '<em><b>Disj</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__DISJ = 2;

	/**
	 * The feature id for the '<em><b>Not</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__NOT = 3;

	/**
	 * The feature id for the '<em><b>Imp</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__IMP = 4;

	/**
	 * The feature id for the '<em><b>Eq</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__EQ = 5;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE__VAR = 6;

	/**
	 * The number of structural features of the '<em>Binary Formula Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Binary Formula Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FORMULA_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl <em>Branch Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getBranchType()
	 * @generated
	 */
	int BRANCH_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Node</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__NODE = 1;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__FEATURE = 2;

	/**
	 * The feature id for the '<em><b>And</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__AND = 3;

	/**
	 * The feature id for the '<em><b>Or</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__OR = 4;

	/**
	 * The feature id for the '<em><b>Alt</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__ALT = 5;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__ABSTRACT = 6;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__MANDATORY = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__NAME = 8;

	/**
	 * The feature id for the '<em><b>Graphics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__GRAPHICS = 9;

	/**
	 * The feature id for the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE__HIDDEN = 10;

	/**
	 * The number of structural features of the '<em>Branch Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Branch Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRANCH_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.ConstraintsTypeImpl <em>Constraints Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.ConstraintsTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getConstraintsType()
	 * @generated
	 */
	int CONSTRAINTS_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_TYPE__RULE = 0;

	/**
	 * The number of structural features of the '<em>Constraints Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Constraints Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.DocumentRootImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 3;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Feature Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__FEATURE_MODEL = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureModelTypeImpl <em>Feature Model Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureModelTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getFeatureModelType()
	 * @generated
	 */
	int FEATURE_MODEL_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__PROPERTIES = 0;

	/**
	 * The feature id for the '<em><b>Struct</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__STRUCT = 1;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__CONSTRAINTS = 2;

	/**
	 * The feature id for the '<em><b>Calculations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__CALCULATIONS = 3;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__COMMENTS = 4;

	/**
	 * The feature id for the '<em><b>Feature Order</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__FEATURE_ORDER = 5;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE__ANY_ATTRIBUTE = 6;

	/**
	 * The number of structural features of the '<em>Feature Model Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Feature Model Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MODEL_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureTypeImpl <em>Feature Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getFeatureType()
	 * @generated
	 */
	int FEATURE_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__ABSTRACT = 1;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__MANDATORY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__NAME = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__VALUE = 4;

	/**
	 * The feature id for the '<em><b>Graphics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__GRAPHICS = 5;

	/**
	 * The feature id for the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__HIDDEN = 6;

	/**
	 * The number of structural features of the '<em>Feature Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Feature Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl <em>Rule Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getRuleType()
	 * @generated
	 */
	int RULE_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Conj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__CONJ = 0;

	/**
	 * The feature id for the '<em><b>Disj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__DISJ = 1;

	/**
	 * The feature id for the '<em><b>Not</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__NOT = 2;

	/**
	 * The feature id for the '<em><b>Imp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__IMP = 3;

	/**
	 * The feature id for the '<em><b>Eq</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__EQ = 4;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE__VAR = 5;

	/**
	 * The number of structural features of the '<em>Rule Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Rule Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.StructTypeImpl <em>Struct Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.StructTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getStructType()
	 * @generated
	 */
	int STRUCT_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE__FEATURE = 0;

	/**
	 * The feature id for the '<em><b>And</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE__AND = 1;

	/**
	 * The feature id for the '<em><b>Or</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE__OR = 2;

	/**
	 * The feature id for the '<em><b>Alt</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE__ALT = 3;

	/**
	 * The number of structural features of the '<em>Struct Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Struct Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.UnaryFormulaTypeImpl <em>Unary Formula Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.UnaryFormulaTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getUnaryFormulaType()
	 * @generated
	 */
	int UNARY_FORMULA_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Conj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__CONJ = 0;

	/**
	 * The feature id for the '<em><b>Disj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__DISJ = 1;

	/**
	 * The feature id for the '<em><b>Not</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__NOT = 2;

	/**
	 * The feature id for the '<em><b>Imp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__IMP = 3;

	/**
	 * The feature id for the '<em><b>Eq</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__EQ = 4;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE__VAR = 5;

	/**
	 * The number of structural features of the '<em>Unary Formula Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Unary Formula Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FORMULA_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.PropertiesTypeImpl <em>Properties Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.PropertiesTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getPropertiesType()
	 * @generated
	 */
	int PROPERTIES_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Graphics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_TYPE__GRAPHICS = 0;

	/**
	 * The number of structural features of the '<em>Properties Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Properties Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTIES_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.GraphicsTypeImpl <em>Graphics Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.GraphicsTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getGraphicsType()
	 * @generated
	 */
	int GRAPHICS_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICS_TYPE__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICS_TYPE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Graphics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICS_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Graphics Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHICS_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType <em>Binary Formula Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Formula Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType
	 * @generated
	 */
	EClass getBinaryFormulaType();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Formula</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getFormula()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EAttribute getBinaryFormulaType_Formula();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getConj <em>Conj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Conj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getConj()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EReference getBinaryFormulaType_Conj();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getDisj <em>Disj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Disj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getDisj()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EReference getBinaryFormulaType_Disj();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getNot <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Not</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getNot()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EReference getBinaryFormulaType_Not();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getImp <em>Imp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imp</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getImp()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EReference getBinaryFormulaType_Imp();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getEq <em>Eq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Eq</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getEq()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EReference getBinaryFormulaType_Eq();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Var</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getVar()
	 * @see #getBinaryFormulaType()
	 * @generated
	 */
	EAttribute getBinaryFormulaType_Var();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType <em>Branch Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Branch Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType
	 * @generated
	 */
	EClass getBranchType();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getDescription()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Description();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Node</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getNode()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Node();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getFeature()
	 * @see #getBranchType()
	 * @generated
	 */
	EReference getBranchType_Feature();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAnd <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>And</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAnd()
	 * @see #getBranchType()
	 * @generated
	 */
	EReference getBranchType_And();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getOr <em>Or</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Or</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getOr()
	 * @see #getBranchType()
	 * @generated
	 */
	EReference getBranchType_Or();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAlt <em>Alt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alt</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAlt()
	 * @see #getBranchType()
	 * @generated
	 */
	EReference getBranchType_Alt();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAbstract()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getMandatory <em>Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mandatory</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getMandatory()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Mandatory();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getName()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Name();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getGraphics <em>Graphics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graphics</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getGraphics()
	 * @see #getBranchType()
	 * @generated
	 */
	EReference getBranchType_Graphics();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#isHidden <em>Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hidden</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.BranchType#isHidden()
	 * @see #getBranchType()
	 * @generated
	 */
	EAttribute getBranchType_Hidden();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType <em>Constraints Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraints Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType
	 * @generated
	 */
	EClass getConstraintsType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType#getRule()
	 * @see #getConstraintsType()
	 * @generated
	 */
	EReference getConstraintsType_Rule();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getFeatureModel <em>Feature Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Model</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot#getFeatureModel()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_FeatureModel();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType <em>Feature Model Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Model Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType
	 * @generated
	 */
	EClass getFeatureModelType();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Properties</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getProperties()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_Properties();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getStruct <em>Struct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Struct</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getStruct()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_Struct();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constraints</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getConstraints()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_Constraints();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getCalculations <em>Calculations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Calculations</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getCalculations()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_Calculations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Comments</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getComments()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_Comments();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getFeatureOrder <em>Feature Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Order</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getFeatureOrder()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EReference getFeatureModelType_FeatureOrder();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getAnyAttribute()
	 * @see #getFeatureModelType()
	 * @generated
	 */
	EAttribute getFeatureModelType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType
	 * @generated
	 */
	EClass getFeatureType();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getDescription()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Description();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isAbstract()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isMandatory <em>Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mandatory</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isMandatory()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Mandatory();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getName()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getValue()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Value();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getGraphics <em>Graphics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graphics</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#getGraphics()
	 * @see #getFeatureType()
	 * @generated
	 */
	EReference getFeatureType_Graphics();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isHidden <em>Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hidden</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureType#isHidden()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Hidden();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType <em>Rule Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType
	 * @generated
	 */
	EClass getRuleType();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getConj <em>Conj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getConj()
	 * @see #getRuleType()
	 * @generated
	 */
	EReference getRuleType_Conj();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getDisj <em>Disj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getDisj()
	 * @see #getRuleType()
	 * @generated
	 */
	EReference getRuleType_Disj();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getNot <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Not</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getNot()
	 * @see #getRuleType()
	 * @generated
	 */
	EReference getRuleType_Not();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getImp <em>Imp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getImp()
	 * @see #getRuleType()
	 * @generated
	 */
	EReference getRuleType_Imp();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getEq <em>Eq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Eq</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getEq()
	 * @see #getRuleType()
	 * @generated
	 */
	EReference getRuleType_Eq();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getVar()
	 * @see #getRuleType()
	 * @generated
	 */
	EAttribute getRuleType_Var();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType <em>Struct Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Struct Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.StructType
	 * @generated
	 */
	EClass getStructType();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.StructType#getFeature()
	 * @see #getStructType()
	 * @generated
	 */
	EReference getStructType_Feature();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAnd <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>And</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAnd()
	 * @see #getStructType()
	 * @generated
	 */
	EReference getStructType_And();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getOr <em>Or</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Or</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.StructType#getOr()
	 * @see #getStructType()
	 * @generated
	 */
	EReference getStructType_Or();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAlt <em>Alt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alt</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.StructType#getAlt()
	 * @see #getStructType()
	 * @generated
	 */
	EReference getStructType_Alt();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType <em>Unary Formula Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Formula Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType
	 * @generated
	 */
	EClass getUnaryFormulaType();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getConj <em>Conj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getConj()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EReference getUnaryFormulaType_Conj();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getDisj <em>Disj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Disj</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getDisj()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EReference getUnaryFormulaType_Disj();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getNot <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Not</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getNot()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EReference getUnaryFormulaType_Not();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getImp <em>Imp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Imp</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getImp()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EReference getUnaryFormulaType_Imp();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getEq <em>Eq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Eq</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getEq()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EReference getUnaryFormulaType_Eq();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType#getVar()
	 * @see #getUnaryFormulaType()
	 * @generated
	 */
	EAttribute getUnaryFormulaType_Var();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType <em>Properties Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Properties Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType
	 * @generated
	 */
	EClass getPropertiesType();

	/**
	 * Returns the meta object for the reference list '{@link fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType#getGraphics <em>Graphics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Graphics</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType#getGraphics()
	 * @see #getPropertiesType()
	 * @generated
	 */
	EReference getPropertiesType_Graphics();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType <em>Graphics Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graphics Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType
	 * @generated
	 */
	EClass getGraphicsType();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getKey()
	 * @see #getGraphicsType()
	 * @generated
	 */
	EAttribute getGraphicsType_Key();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType#getValue()
	 * @see #getGraphicsType()
	 * @generated
	 */
	EAttribute getGraphicsType_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeatureideFactory getFeatureideFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl <em>Binary Formula Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getBinaryFormulaType()
		 * @generated
		 */
		EClass BINARY_FORMULA_TYPE = eINSTANCE.getBinaryFormulaType();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_FORMULA_TYPE__FORMULA = eINSTANCE.getBinaryFormulaType_Formula();

		/**
		 * The meta object literal for the '<em><b>Conj</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_FORMULA_TYPE__CONJ = eINSTANCE.getBinaryFormulaType_Conj();

		/**
		 * The meta object literal for the '<em><b>Disj</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_FORMULA_TYPE__DISJ = eINSTANCE.getBinaryFormulaType_Disj();

		/**
		 * The meta object literal for the '<em><b>Not</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_FORMULA_TYPE__NOT = eINSTANCE.getBinaryFormulaType_Not();

		/**
		 * The meta object literal for the '<em><b>Imp</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_FORMULA_TYPE__IMP = eINSTANCE.getBinaryFormulaType_Imp();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_FORMULA_TYPE__EQ = eINSTANCE.getBinaryFormulaType_Eq();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_FORMULA_TYPE__VAR = eINSTANCE.getBinaryFormulaType_Var();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl <em>Branch Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.BranchTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getBranchType()
		 * @generated
		 */
		EClass BRANCH_TYPE = eINSTANCE.getBranchType();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__DESCRIPTION = eINSTANCE.getBranchType_Description();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__NODE = eINSTANCE.getBranchType_Node();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRANCH_TYPE__FEATURE = eINSTANCE.getBranchType_Feature();

		/**
		 * The meta object literal for the '<em><b>And</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRANCH_TYPE__AND = eINSTANCE.getBranchType_And();

		/**
		 * The meta object literal for the '<em><b>Or</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRANCH_TYPE__OR = eINSTANCE.getBranchType_Or();

		/**
		 * The meta object literal for the '<em><b>Alt</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRANCH_TYPE__ALT = eINSTANCE.getBranchType_Alt();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__ABSTRACT = eINSTANCE.getBranchType_Abstract();

		/**
		 * The meta object literal for the '<em><b>Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__MANDATORY = eINSTANCE.getBranchType_Mandatory();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__NAME = eINSTANCE.getBranchType_Name();

		/**
		 * The meta object literal for the '<em><b>Graphics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRANCH_TYPE__GRAPHICS = eINSTANCE.getBranchType_Graphics();

		/**
		 * The meta object literal for the '<em><b>Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRANCH_TYPE__HIDDEN = eINSTANCE.getBranchType_Hidden();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.ConstraintsTypeImpl <em>Constraints Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.ConstraintsTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getConstraintsType()
		 * @generated
		 */
		EClass CONSTRAINTS_TYPE = eINSTANCE.getConstraintsType();

		/**
		 * The meta object literal for the '<em><b>Rule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINTS_TYPE__RULE = eINSTANCE.getConstraintsType_Rule();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.DocumentRootImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Feature Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__FEATURE_MODEL = eINSTANCE.getDocumentRoot_FeatureModel();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureModelTypeImpl <em>Feature Model Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureModelTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getFeatureModelType()
		 * @generated
		 */
		EClass FEATURE_MODEL_TYPE = eINSTANCE.getFeatureModelType();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__PROPERTIES = eINSTANCE.getFeatureModelType_Properties();

		/**
		 * The meta object literal for the '<em><b>Struct</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__STRUCT = eINSTANCE.getFeatureModelType_Struct();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__CONSTRAINTS = eINSTANCE.getFeatureModelType_Constraints();

		/**
		 * The meta object literal for the '<em><b>Calculations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__CALCULATIONS = eINSTANCE.getFeatureModelType_Calculations();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__COMMENTS = eINSTANCE.getFeatureModelType_Comments();

		/**
		 * The meta object literal for the '<em><b>Feature Order</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MODEL_TYPE__FEATURE_ORDER = eINSTANCE.getFeatureModelType_FeatureOrder();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_MODEL_TYPE__ANY_ATTRIBUTE = eINSTANCE.getFeatureModelType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureTypeImpl <em>Feature Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getFeatureType()
		 * @generated
		 */
		EClass FEATURE_TYPE = eINSTANCE.getFeatureType();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__DESCRIPTION = eINSTANCE.getFeatureType_Description();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__ABSTRACT = eINSTANCE.getFeatureType_Abstract();

		/**
		 * The meta object literal for the '<em><b>Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__MANDATORY = eINSTANCE.getFeatureType_Mandatory();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__NAME = eINSTANCE.getFeatureType_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__VALUE = eINSTANCE.getFeatureType_Value();

		/**
		 * The meta object literal for the '<em><b>Graphics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TYPE__GRAPHICS = eINSTANCE.getFeatureType_Graphics();

		/**
		 * The meta object literal for the '<em><b>Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__HIDDEN = eINSTANCE.getFeatureType_Hidden();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl <em>Rule Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getRuleType()
		 * @generated
		 */
		EClass RULE_TYPE = eINSTANCE.getRuleType();

		/**
		 * The meta object literal for the '<em><b>Conj</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TYPE__CONJ = eINSTANCE.getRuleType_Conj();

		/**
		 * The meta object literal for the '<em><b>Disj</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TYPE__DISJ = eINSTANCE.getRuleType_Disj();

		/**
		 * The meta object literal for the '<em><b>Not</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TYPE__NOT = eINSTANCE.getRuleType_Not();

		/**
		 * The meta object literal for the '<em><b>Imp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TYPE__IMP = eINSTANCE.getRuleType_Imp();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TYPE__EQ = eINSTANCE.getRuleType_Eq();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_TYPE__VAR = eINSTANCE.getRuleType_Var();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.StructTypeImpl <em>Struct Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.StructTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getStructType()
		 * @generated
		 */
		EClass STRUCT_TYPE = eINSTANCE.getStructType();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_TYPE__FEATURE = eINSTANCE.getStructType_Feature();

		/**
		 * The meta object literal for the '<em><b>And</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_TYPE__AND = eINSTANCE.getStructType_And();

		/**
		 * The meta object literal for the '<em><b>Or</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_TYPE__OR = eINSTANCE.getStructType_Or();

		/**
		 * The meta object literal for the '<em><b>Alt</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_TYPE__ALT = eINSTANCE.getStructType_Alt();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.UnaryFormulaTypeImpl <em>Unary Formula Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.UnaryFormulaTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getUnaryFormulaType()
		 * @generated
		 */
		EClass UNARY_FORMULA_TYPE = eINSTANCE.getUnaryFormulaType();

		/**
		 * The meta object literal for the '<em><b>Conj</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_FORMULA_TYPE__CONJ = eINSTANCE.getUnaryFormulaType_Conj();

		/**
		 * The meta object literal for the '<em><b>Disj</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_FORMULA_TYPE__DISJ = eINSTANCE.getUnaryFormulaType_Disj();

		/**
		 * The meta object literal for the '<em><b>Not</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_FORMULA_TYPE__NOT = eINSTANCE.getUnaryFormulaType_Not();

		/**
		 * The meta object literal for the '<em><b>Imp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_FORMULA_TYPE__IMP = eINSTANCE.getUnaryFormulaType_Imp();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_FORMULA_TYPE__EQ = eINSTANCE.getUnaryFormulaType_Eq();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_FORMULA_TYPE__VAR = eINSTANCE.getUnaryFormulaType_Var();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.PropertiesTypeImpl <em>Properties Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.PropertiesTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getPropertiesType()
		 * @generated
		 */
		EClass PROPERTIES_TYPE = eINSTANCE.getPropertiesType();

		/**
		 * The meta object literal for the '<em><b>Graphics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTIES_TYPE__GRAPHICS = eINSTANCE.getPropertiesType_Graphics();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.GraphicsTypeImpl <em>Graphics Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.GraphicsTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Featureide.impl.FeatureidePackageImpl#getGraphicsType()
		 * @generated
		 */
		EClass GRAPHICS_TYPE = eINSTANCE.getGraphicsType();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPHICS_TYPE__KEY = eINSTANCE.getGraphicsType_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPHICS_TYPE__VALUE = eINSTANCE.getGraphicsType_Value();

	}

} //FeatureidePackage
