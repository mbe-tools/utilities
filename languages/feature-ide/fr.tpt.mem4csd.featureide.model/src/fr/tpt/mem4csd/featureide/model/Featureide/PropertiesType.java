/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Properties Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType#getGraphics <em>Graphics</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getPropertiesType()
 * @model extendedMetaData="name='propertiesType' kind='elementOnly'"
 * @generated
 */
public interface PropertiesType extends EObject {
	/**
	 * Returns the value of the '<em><b>Graphics</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphics</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getPropertiesType_Graphics()
	 * @model
	 * @generated
	 */
	EList<GraphicsType> getGraphics();

} // PropertiesType
