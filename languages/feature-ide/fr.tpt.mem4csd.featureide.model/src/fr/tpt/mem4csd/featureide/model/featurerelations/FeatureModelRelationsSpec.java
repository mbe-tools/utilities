/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Model Relations Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getOwnedSubClassRelations <em>Owned Sub Class Relations</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getIndividuals <em>Individuals</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getDisabledFeatures <em>Disabled Features</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getFeatureModelRelationsSpec()
 * @model
 * @generated
 */
public interface FeatureModelRelationsSpec extends EObject {
	/**
	 * Returns the value of the '<em><b>Owned Sub Class Relations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Sub Class Relations</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getFeatureModelRelationsSpec_OwnedSubClassRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<SubClassRelation> getOwnedSubClassRelations();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getFeatureModelRelationsSpec_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Individuals</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Individuals</em>' containment reference.
	 * @see #setIndividuals(Individual)
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getFeatureModelRelationsSpec_Individuals()
	 * @model containment="true"
	 * @generated
	 */
	Individual getIndividuals();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getIndividuals <em>Individuals</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Individuals</em>' containment reference.
	 * @see #getIndividuals()
	 * @generated
	 */
	void setIndividuals(Individual value);

	/**
	 * Returns the value of the '<em><b>Disabled Features</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disabled Features</em>' containment reference.
	 * @see #setDisabledFeatures(Disabled)
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#getFeatureModelRelationsSpec_DisabledFeatures()
	 * @model containment="true"
	 * @generated
	 */
	Disabled getDisabledFeatures();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec#getDisabledFeatures <em>Disabled Features</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disabled Features</em>' containment reference.
	 * @see #getDisabledFeatures()
	 * @generated
	 */
	void setDisabledFeatures(Disabled value);

} // FeatureModelRelationsSpec
