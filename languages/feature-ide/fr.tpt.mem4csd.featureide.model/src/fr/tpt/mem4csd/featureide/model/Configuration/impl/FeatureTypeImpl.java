/**
 */
package fr.tpt.mem4csd.featureide.model.Configuration.impl;

import fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationPackage;
import fr.tpt.mem4csd.featureide.model.Configuration.FeatureType;
import fr.tpt.mem4csd.featureide.model.Configuration.SelectionType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl#getValue <em>Value</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl#getAutomatic <em>Automatic</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl#getManual <em>Manual</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl#getValue1 <em>Value1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureTypeImpl extends MinimalEObjectImpl.Container implements FeatureType {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAutomatic() <em>Automatic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutomatic()
	 * @generated
	 * @ordered
	 */
	protected static final SelectionType AUTOMATIC_EDEFAULT = SelectionType.SELECTED;

	/**
	 * The cached value of the '{@link #getAutomatic() <em>Automatic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAutomatic()
	 * @generated
	 * @ordered
	 */
	protected SelectionType automatic = AUTOMATIC_EDEFAULT;

	/**
	 * This is true if the Automatic attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean automaticESet;

	/**
	 * The default value of the '{@link #getManual() <em>Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManual()
	 * @generated
	 * @ordered
	 */
	protected static final SelectionType MANUAL_EDEFAULT = SelectionType.SELECTED;

	/**
	 * The cached value of the '{@link #getManual() <em>Manual</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getManual()
	 * @generated
	 * @ordered
	 */
	protected SelectionType manual = MANUAL_EDEFAULT;

	/**
	 * This is true if the Manual attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean manualESet;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue1() <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue1()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue1() <em>Value1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue1()
	 * @generated
	 * @ordered
	 */
	protected String value1 = VALUE1_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.FEATURE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.FEATURE_TYPE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SelectionType getAutomatic() {
		return automatic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAutomatic(SelectionType newAutomatic) {
		SelectionType oldAutomatic = automatic;
		automatic = newAutomatic == null ? AUTOMATIC_EDEFAULT : newAutomatic;
		boolean oldAutomaticESet = automaticESet;
		automaticESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.FEATURE_TYPE__AUTOMATIC, oldAutomatic, automatic, !oldAutomaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetAutomatic() {
		SelectionType oldAutomatic = automatic;
		boolean oldAutomaticESet = automaticESet;
		automatic = AUTOMATIC_EDEFAULT;
		automaticESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConfigurationPackage.FEATURE_TYPE__AUTOMATIC, oldAutomatic, AUTOMATIC_EDEFAULT, oldAutomaticESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetAutomatic() {
		return automaticESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SelectionType getManual() {
		return manual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setManual(SelectionType newManual) {
		SelectionType oldManual = manual;
		manual = newManual == null ? MANUAL_EDEFAULT : newManual;
		boolean oldManualESet = manualESet;
		manualESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.FEATURE_TYPE__MANUAL, oldManual, manual, !oldManualESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetManual() {
		SelectionType oldManual = manual;
		boolean oldManualESet = manualESet;
		manual = MANUAL_EDEFAULT;
		manualESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConfigurationPackage.FEATURE_TYPE__MANUAL, oldManual, MANUAL_EDEFAULT, oldManualESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetManual() {
		return manualESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.FEATURE_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue1() {
		return value1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue1(String newValue1) {
		String oldValue1 = value1;
		value1 = newValue1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.FEATURE_TYPE__VALUE1, oldValue1, value1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.FEATURE_TYPE__VALUE:
				return getValue();
			case ConfigurationPackage.FEATURE_TYPE__AUTOMATIC:
				return getAutomatic();
			case ConfigurationPackage.FEATURE_TYPE__MANUAL:
				return getManual();
			case ConfigurationPackage.FEATURE_TYPE__NAME:
				return getName();
			case ConfigurationPackage.FEATURE_TYPE__VALUE1:
				return getValue1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.FEATURE_TYPE__VALUE:
				setValue((String)newValue);
				return;
			case ConfigurationPackage.FEATURE_TYPE__AUTOMATIC:
				setAutomatic((SelectionType)newValue);
				return;
			case ConfigurationPackage.FEATURE_TYPE__MANUAL:
				setManual((SelectionType)newValue);
				return;
			case ConfigurationPackage.FEATURE_TYPE__NAME:
				setName((String)newValue);
				return;
			case ConfigurationPackage.FEATURE_TYPE__VALUE1:
				setValue1((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.FEATURE_TYPE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case ConfigurationPackage.FEATURE_TYPE__AUTOMATIC:
				unsetAutomatic();
				return;
			case ConfigurationPackage.FEATURE_TYPE__MANUAL:
				unsetManual();
				return;
			case ConfigurationPackage.FEATURE_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ConfigurationPackage.FEATURE_TYPE__VALUE1:
				setValue1(VALUE1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.FEATURE_TYPE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case ConfigurationPackage.FEATURE_TYPE__AUTOMATIC:
				return isSetAutomatic();
			case ConfigurationPackage.FEATURE_TYPE__MANUAL:
				return isSetManual();
			case ConfigurationPackage.FEATURE_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ConfigurationPackage.FEATURE_TYPE__VALUE1:
				return VALUE1_EDEFAULT == null ? value1 != null : !VALUE1_EDEFAULT.equals(value1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", automatic: ");
		if (automaticESet) result.append(automatic); else result.append("<unset>");
		result.append(", manual: ");
		if (manualESet) result.append(manual); else result.append("<unset>");
		result.append(", name: ");
		result.append(name);
		result.append(", value1: ");
		result.append(value1);
		result.append(')');
		return result.toString();
	}

} //FeatureTypeImpl
