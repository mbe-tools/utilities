/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;

import fr.tpt.mem4csd.featureide.model.featurerelations.Disabled;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeatureModelRelationsSpec;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsFactory;
import fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage;
import fr.tpt.mem4csd.featureide.model.featurerelations.Individual;
import fr.tpt.mem4csd.featureide.model.featurerelations.SubClassRelation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeaturerelationsPackageImpl extends EPackageImpl implements FeaturerelationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subClassRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureModelRelationsSpecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass individualEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass disabledEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FeaturerelationsPackageImpl() {
		super(eNS_URI, FeaturerelationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FeaturerelationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FeaturerelationsPackage init() {
		if (isInited) return (FeaturerelationsPackage)EPackage.Registry.INSTANCE.getEPackage(FeaturerelationsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFeaturerelationsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FeaturerelationsPackageImpl theFeaturerelationsPackage = registeredFeaturerelationsPackage instanceof FeaturerelationsPackageImpl ? (FeaturerelationsPackageImpl)registeredFeaturerelationsPackage : new FeaturerelationsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		FeatureidePackage.eINSTANCE.eClass();
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theFeaturerelationsPackage.createPackageContents();

		// Initialize created meta-data
		theFeaturerelationsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFeaturerelationsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FeaturerelationsPackage.eNS_URI, theFeaturerelationsPackage);
		return theFeaturerelationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubClassRelation() {
		return subClassRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubClassRelation_SuperBranch() {
		return (EReference)subClassRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubClassRelation_SubBranch() {
		return (EReference)subClassRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubClassRelation_SubFeature() {
		return (EReference)subClassRelationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubClassRelation_HasIndividuals() {
		return (EAttribute)subClassRelationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureModelRelationsSpec() {
		return featureModelRelationsSpecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelRelationsSpec_OwnedSubClassRelations() {
		return (EReference)featureModelRelationsSpecEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureModelRelationsSpec_Name() {
		return (EAttribute)featureModelRelationsSpecEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelRelationsSpec_Individuals() {
		return (EReference)featureModelRelationsSpecEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelRelationsSpec_DisabledFeatures() {
		return (EReference)featureModelRelationsSpecEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIndividual() {
		return individualEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIndividual_Features() {
		return (EReference)individualEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDisabled() {
		return disabledEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDisabled_Features() {
		return (EReference)disabledEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDisabled_Branches() {
		return (EReference)disabledEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeaturerelationsFactory getFeaturerelationsFactory() {
		return (FeaturerelationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		subClassRelationEClass = createEClass(SUB_CLASS_RELATION);
		createEReference(subClassRelationEClass, SUB_CLASS_RELATION__SUPER_BRANCH);
		createEReference(subClassRelationEClass, SUB_CLASS_RELATION__SUB_BRANCH);
		createEReference(subClassRelationEClass, SUB_CLASS_RELATION__SUB_FEATURE);
		createEAttribute(subClassRelationEClass, SUB_CLASS_RELATION__HAS_INDIVIDUALS);

		featureModelRelationsSpecEClass = createEClass(FEATURE_MODEL_RELATIONS_SPEC);
		createEReference(featureModelRelationsSpecEClass, FEATURE_MODEL_RELATIONS_SPEC__OWNED_SUB_CLASS_RELATIONS);
		createEAttribute(featureModelRelationsSpecEClass, FEATURE_MODEL_RELATIONS_SPEC__NAME);
		createEReference(featureModelRelationsSpecEClass, FEATURE_MODEL_RELATIONS_SPEC__INDIVIDUALS);
		createEReference(featureModelRelationsSpecEClass, FEATURE_MODEL_RELATIONS_SPEC__DISABLED_FEATURES);

		individualEClass = createEClass(INDIVIDUAL);
		createEReference(individualEClass, INDIVIDUAL__FEATURES);

		disabledEClass = createEClass(DISABLED);
		createEReference(disabledEClass, DISABLED__FEATURES);
		createEReference(disabledEClass, DISABLED__BRANCHES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FeatureidePackage theFeatureidePackage = (FeatureidePackage)EPackage.Registry.INSTANCE.getEPackage(FeatureidePackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(subClassRelationEClass, SubClassRelation.class, "SubClassRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSubClassRelation_SuperBranch(), theFeatureidePackage.getBranchType(), null, "superBranch", null, 0, 1, SubClassRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubClassRelation_SubBranch(), theFeatureidePackage.getBranchType(), null, "subBranch", null, 0, -1, SubClassRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSubClassRelation_SubFeature(), theFeatureidePackage.getFeatureType(), null, "subFeature", null, 0, -1, SubClassRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSubClassRelation_HasIndividuals(), theXMLTypePackage.getBoolean(), "hasIndividuals", "false", 0, 1, SubClassRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureModelRelationsSpecEClass, FeatureModelRelationsSpec.class, "FeatureModelRelationsSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureModelRelationsSpec_OwnedSubClassRelations(), this.getSubClassRelation(), null, "ownedSubClassRelations", null, 0, -1, FeatureModelRelationsSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureModelRelationsSpec_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, FeatureModelRelationsSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelRelationsSpec_Individuals(), this.getIndividual(), null, "individuals", null, 0, 1, FeatureModelRelationsSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelRelationsSpec_DisabledFeatures(), this.getDisabled(), null, "disabledFeatures", null, 0, 1, FeatureModelRelationsSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(individualEClass, Individual.class, "Individual", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIndividual_Features(), theFeatureidePackage.getFeatureType(), null, "features", null, 0, -1, Individual.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(disabledEClass, Disabled.class, "Disabled", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDisabled_Features(), theFeatureidePackage.getFeatureType(), null, "features", null, 0, -1, Disabled.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDisabled_Branches(), theFeatureidePackage.getBranchType(), null, "branches", null, 0, -1, Disabled.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //FeaturerelationsPackageImpl
