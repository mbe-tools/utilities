/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Formula Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getFormula <em>Formula</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getConj <em>Conj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getDisj <em>Disj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getNot <em>Not</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getImp <em>Imp</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getEq <em>Eq</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType#getVar <em>Var</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType()
 * @model extendedMetaData="name='binaryFormulaType' kind='elementOnly'"
 * @generated
 */
public interface BinaryFormulaType extends EObject {
	/**
	 * Returns the value of the '<em><b>Formula</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' attribute list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Formula()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='Formula:0'"
	 * @generated
	 */
	FeatureMap getFormula();

	/**
	 * Returns the value of the '<em><b>Conj</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conj</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Conj()
	 * @model containment="true" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='conj' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<BinaryFormulaType> getConj();

	/**
	 * Returns the value of the '<em><b>Disj</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disj</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Disj()
	 * @model containment="true" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='disj' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<BinaryFormulaType> getDisj();

	/**
	 * Returns the value of the '<em><b>Not</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Not()
	 * @model containment="true" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<UnaryFormulaType> getNot();

	/**
	 * Returns the value of the '<em><b>Imp</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Imp()
	 * @model containment="true" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='imp' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<BinaryFormulaType> getImp();

	/**
	 * Returns the value of the '<em><b>Eq</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eq</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Eq()
	 * @model containment="true" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='eq' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<BinaryFormulaType> getEq();

	/**
	 * Returns the value of the '<em><b>Var</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' attribute list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBinaryFormulaType_Var()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" upper="2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='var' namespace='##targetNamespace' group='Formula:0'"
	 * @generated
	 */
	EList<String> getVar();

} // BinaryFormulaType
