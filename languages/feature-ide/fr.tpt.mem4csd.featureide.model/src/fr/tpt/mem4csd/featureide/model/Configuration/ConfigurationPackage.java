/**
 */
package fr.tpt.mem4csd.featureide.model.Configuration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationFactory
 * @model kind="package"
 *        extendedMetaData="qualified='false'"
 * @generated
 */
public interface ConfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Configuration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/fr.tpt.mem4csd.featureide.model/model/configuration.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Configuration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigurationPackage eINSTANCE = fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getConfigurationType()
	 * @generated
	 */
	int CONFIGURATION_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_TYPE__FEATURE = 0;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.DocumentRootImpl
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 1;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CONFIGURATION = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl <em>Feature Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getFeatureType()
	 * @generated
	 */
	int FEATURE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Automatic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__AUTOMATIC = 1;

	/**
	 * The feature id for the '<em><b>Manual</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__MANUAL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__NAME = 3;

	/**
	 * The feature id for the '<em><b>Value1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE__VALUE1 = 4;

	/**
	 * The number of structural features of the '<em>Feature Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Feature Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType <em>Selection Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getSelectionType()
	 * @generated
	 */
	int SELECTION_TYPE = 3;

	/**
	 * The meta object id for the '<em>Selection Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getSelectionTypeObject()
	 * @generated
	 */
	int SELECTION_TYPE_OBJECT = 4;


	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationType
	 * @generated
	 */
	EClass getConfigurationType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationType#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.ConfigurationType#getFeature()
	 * @see #getConfigurationType()
	 * @generated
	 */
	EReference getConfigurationType_Feature();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.DocumentRoot#getConfiguration()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Configuration();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType
	 * @generated
	 */
	EClass getFeatureType();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Value();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic <em>Automatic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Automatic</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getAutomatic()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Automatic();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual <em>Manual</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Manual</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getManual()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Manual();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getName()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue1 <em>Value1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value1</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.FeatureType#getValue1()
	 * @see #getFeatureType()
	 * @generated
	 */
	EAttribute getFeatureType_Value1();

	/**
	 * Returns the meta object for enum '{@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType <em>Selection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Selection Type</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @generated
	 */
	EEnum getSelectionType();

	/**
	 * Returns the meta object for data type '{@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType <em>Selection Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Selection Type Object</em>'.
	 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
	 * @model instanceClass="fr.tpt.mem4csd.featureide.model.Configuration.SelectionType"
	 *        extendedMetaData="name='selectionType:Object' baseType='selectionType'"
	 * @generated
	 */
	EDataType getSelectionTypeObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigurationFactory getConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getConfigurationType()
		 * @generated
		 */
		EClass CONFIGURATION_TYPE = eINSTANCE.getConfigurationType();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION_TYPE__FEATURE = eINSTANCE.getConfigurationType_Feature();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.DocumentRootImpl
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__CONFIGURATION = eINSTANCE.getDocumentRoot_Configuration();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl <em>Feature Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.FeatureTypeImpl
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getFeatureType()
		 * @generated
		 */
		EClass FEATURE_TYPE = eINSTANCE.getFeatureType();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__VALUE = eINSTANCE.getFeatureType_Value();

		/**
		 * The meta object literal for the '<em><b>Automatic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__AUTOMATIC = eINSTANCE.getFeatureType_Automatic();

		/**
		 * The meta object literal for the '<em><b>Manual</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__MANUAL = eINSTANCE.getFeatureType_Manual();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__NAME = eINSTANCE.getFeatureType_Name();

		/**
		 * The meta object literal for the '<em><b>Value1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_TYPE__VALUE1 = eINSTANCE.getFeatureType_Value1();

		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.featureide.model.Configuration.SelectionType <em>Selection Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getSelectionType()
		 * @generated
		 */
		EEnum SELECTION_TYPE = eINSTANCE.getSelectionType();

		/**
		 * The meta object literal for the '<em>Selection Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.SelectionType
		 * @see fr.tpt.mem4csd.featureide.model.Configuration.impl.ConfigurationPackageImpl#getSelectionTypeObject()
		 * @generated
		 */
		EDataType SELECTION_TYPE_OBJECT = eINSTANCE.getSelectionTypeObject();

	}

} //ConfigurationPackage
