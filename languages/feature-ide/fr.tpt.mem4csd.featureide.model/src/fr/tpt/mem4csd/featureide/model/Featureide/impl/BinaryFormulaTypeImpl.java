/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Formula Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getFormula <em>Formula</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getConj <em>Conj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getDisj <em>Disj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getNot <em>Not</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getImp <em>Imp</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getEq <em>Eq</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.BinaryFormulaTypeImpl#getVar <em>Var</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryFormulaTypeImpl extends MinimalEObjectImpl.Container implements BinaryFormulaType {
	/**
	 * The cached value of the '{@link #getFormula() <em>Formula</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormula()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap formula;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryFormulaTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeatureidePackage.Literals.BINARY_FORMULA_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getFormula() {
		if (formula == null) {
			formula = new BasicFeatureMap(this, FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA);
		}
		return formula;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BinaryFormulaType> getConj() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__CONJ);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BinaryFormulaType> getDisj() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__DISJ);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UnaryFormulaType> getNot() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__NOT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BinaryFormulaType> getImp() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__IMP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BinaryFormulaType> getEq() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__EQ);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getVar() {
		return getFormula().list(FeatureidePackage.Literals.BINARY_FORMULA_TYPE__VAR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA:
				return ((InternalEList<?>)getFormula()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BINARY_FORMULA_TYPE__CONJ:
				return ((InternalEList<?>)getConj()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BINARY_FORMULA_TYPE__DISJ:
				return ((InternalEList<?>)getDisj()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BINARY_FORMULA_TYPE__NOT:
				return ((InternalEList<?>)getNot()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BINARY_FORMULA_TYPE__IMP:
				return ((InternalEList<?>)getImp()).basicRemove(otherEnd, msgs);
			case FeatureidePackage.BINARY_FORMULA_TYPE__EQ:
				return ((InternalEList<?>)getEq()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA:
				if (coreType) return getFormula();
				return ((FeatureMap.Internal)getFormula()).getWrapper();
			case FeatureidePackage.BINARY_FORMULA_TYPE__CONJ:
				return getConj();
			case FeatureidePackage.BINARY_FORMULA_TYPE__DISJ:
				return getDisj();
			case FeatureidePackage.BINARY_FORMULA_TYPE__NOT:
				return getNot();
			case FeatureidePackage.BINARY_FORMULA_TYPE__IMP:
				return getImp();
			case FeatureidePackage.BINARY_FORMULA_TYPE__EQ:
				return getEq();
			case FeatureidePackage.BINARY_FORMULA_TYPE__VAR:
				return getVar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA:
				((FeatureMap.Internal)getFormula()).set(newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__CONJ:
				getConj().clear();
				getConj().addAll((Collection<? extends BinaryFormulaType>)newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__DISJ:
				getDisj().clear();
				getDisj().addAll((Collection<? extends BinaryFormulaType>)newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__NOT:
				getNot().clear();
				getNot().addAll((Collection<? extends UnaryFormulaType>)newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__IMP:
				getImp().clear();
				getImp().addAll((Collection<? extends BinaryFormulaType>)newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__EQ:
				getEq().clear();
				getEq().addAll((Collection<? extends BinaryFormulaType>)newValue);
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__VAR:
				getVar().clear();
				getVar().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA:
				getFormula().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__CONJ:
				getConj().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__DISJ:
				getDisj().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__NOT:
				getNot().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__IMP:
				getImp().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__EQ:
				getEq().clear();
				return;
			case FeatureidePackage.BINARY_FORMULA_TYPE__VAR:
				getVar().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeatureidePackage.BINARY_FORMULA_TYPE__FORMULA:
				return formula != null && !formula.isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__CONJ:
				return !getConj().isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__DISJ:
				return !getDisj().isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__NOT:
				return !getNot().isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__IMP:
				return !getImp().isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__EQ:
				return !getEq().isEmpty();
			case FeatureidePackage.BINARY_FORMULA_TYPE__VAR:
				return !getVar().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (formula: ");
		result.append(formula);
		result.append(')');
		return result.toString();
	}

} //BinaryFormulaTypeImpl
