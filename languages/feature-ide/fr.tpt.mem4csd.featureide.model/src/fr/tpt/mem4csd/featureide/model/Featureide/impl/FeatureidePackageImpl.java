/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType;
import fr.tpt.mem4csd.featureide.model.Featureide.BranchType;
import fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType;
import fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureideFactory;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType;
import fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType;
import fr.tpt.mem4csd.featureide.model.Featureide.RuleType;
import fr.tpt.mem4csd.featureide.model.Featureide.StructType;
import fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureidePackageImpl extends EPackageImpl implements FeatureidePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryFormulaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass branchTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintsTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureModelTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryFormulaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertiesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphicsTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FeatureidePackageImpl() {
		super(eNS_URI, FeatureideFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FeatureidePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FeatureidePackage init() {
		if (isInited) return (FeatureidePackage)EPackage.Registry.INSTANCE.getEPackage(FeatureidePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFeatureidePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FeatureidePackageImpl theFeatureidePackage = registeredFeatureidePackage instanceof FeatureidePackageImpl ? (FeatureidePackageImpl)registeredFeatureidePackage : new FeatureidePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theFeatureidePackage.createPackageContents();

		// Initialize created meta-data
		theFeatureidePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFeatureidePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FeatureidePackage.eNS_URI, theFeatureidePackage);
		return theFeatureidePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBinaryFormulaType() {
		return binaryFormulaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBinaryFormulaType_Formula() {
		return (EAttribute)binaryFormulaTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryFormulaType_Conj() {
		return (EReference)binaryFormulaTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryFormulaType_Disj() {
		return (EReference)binaryFormulaTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryFormulaType_Not() {
		return (EReference)binaryFormulaTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryFormulaType_Imp() {
		return (EReference)binaryFormulaTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBinaryFormulaType_Eq() {
		return (EReference)binaryFormulaTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBinaryFormulaType_Var() {
		return (EAttribute)binaryFormulaTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBranchType() {
		return branchTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Description() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Node() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBranchType_Feature() {
		return (EReference)branchTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBranchType_And() {
		return (EReference)branchTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBranchType_Or() {
		return (EReference)branchTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBranchType_Alt() {
		return (EReference)branchTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Abstract() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Mandatory() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Name() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBranchType_Graphics() {
		return (EReference)branchTypeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBranchType_Hidden() {
		return (EAttribute)branchTypeEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstraintsType() {
		return constraintsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConstraintsType_Rule() {
		return (EReference)constraintsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDocumentRoot_Mixed() {
		return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_FeatureModel() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureModelType() {
		return featureModelTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_Properties() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_Struct() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_Constraints() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_Calculations() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_Comments() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureModelType_FeatureOrder() {
		return (EReference)featureModelTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureModelType_AnyAttribute() {
		return (EAttribute)featureModelTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFeatureType() {
		return featureTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Description() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Abstract() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Mandatory() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Name() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Value() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFeatureType_Graphics() {
		return (EReference)featureTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFeatureType_Hidden() {
		return (EAttribute)featureTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRuleType() {
		return ruleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleType_Conj() {
		return (EReference)ruleTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleType_Disj() {
		return (EReference)ruleTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleType_Not() {
		return (EReference)ruleTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleType_Imp() {
		return (EReference)ruleTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRuleType_Eq() {
		return (EReference)ruleTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRuleType_Var() {
		return (EAttribute)ruleTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStructType() {
		return structTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStructType_Feature() {
		return (EReference)structTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStructType_And() {
		return (EReference)structTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStructType_Or() {
		return (EReference)structTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStructType_Alt() {
		return (EReference)structTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUnaryFormulaType() {
		return unaryFormulaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryFormulaType_Conj() {
		return (EReference)unaryFormulaTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryFormulaType_Disj() {
		return (EReference)unaryFormulaTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryFormulaType_Not() {
		return (EReference)unaryFormulaTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryFormulaType_Imp() {
		return (EReference)unaryFormulaTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUnaryFormulaType_Eq() {
		return (EReference)unaryFormulaTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUnaryFormulaType_Var() {
		return (EAttribute)unaryFormulaTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropertiesType() {
		return propertiesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPropertiesType_Graphics() {
		return (EReference)propertiesTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGraphicsType() {
		return graphicsTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGraphicsType_Key() {
		return (EAttribute)graphicsTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGraphicsType_Value() {
		return (EAttribute)graphicsTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureideFactory getFeatureideFactory() {
		return (FeatureideFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		binaryFormulaTypeEClass = createEClass(BINARY_FORMULA_TYPE);
		createEAttribute(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__FORMULA);
		createEReference(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__CONJ);
		createEReference(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__DISJ);
		createEReference(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__NOT);
		createEReference(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__IMP);
		createEReference(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__EQ);
		createEAttribute(binaryFormulaTypeEClass, BINARY_FORMULA_TYPE__VAR);

		branchTypeEClass = createEClass(BRANCH_TYPE);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__DESCRIPTION);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__NODE);
		createEReference(branchTypeEClass, BRANCH_TYPE__FEATURE);
		createEReference(branchTypeEClass, BRANCH_TYPE__AND);
		createEReference(branchTypeEClass, BRANCH_TYPE__OR);
		createEReference(branchTypeEClass, BRANCH_TYPE__ALT);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__ABSTRACT);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__MANDATORY);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__NAME);
		createEReference(branchTypeEClass, BRANCH_TYPE__GRAPHICS);
		createEAttribute(branchTypeEClass, BRANCH_TYPE__HIDDEN);

		constraintsTypeEClass = createEClass(CONSTRAINTS_TYPE);
		createEReference(constraintsTypeEClass, CONSTRAINTS_TYPE__RULE);

		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__FEATURE_MODEL);

		featureModelTypeEClass = createEClass(FEATURE_MODEL_TYPE);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__PROPERTIES);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__STRUCT);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__CONSTRAINTS);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__CALCULATIONS);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__COMMENTS);
		createEReference(featureModelTypeEClass, FEATURE_MODEL_TYPE__FEATURE_ORDER);
		createEAttribute(featureModelTypeEClass, FEATURE_MODEL_TYPE__ANY_ATTRIBUTE);

		featureTypeEClass = createEClass(FEATURE_TYPE);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__DESCRIPTION);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__ABSTRACT);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__MANDATORY);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__NAME);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__VALUE);
		createEReference(featureTypeEClass, FEATURE_TYPE__GRAPHICS);
		createEAttribute(featureTypeEClass, FEATURE_TYPE__HIDDEN);

		ruleTypeEClass = createEClass(RULE_TYPE);
		createEReference(ruleTypeEClass, RULE_TYPE__CONJ);
		createEReference(ruleTypeEClass, RULE_TYPE__DISJ);
		createEReference(ruleTypeEClass, RULE_TYPE__NOT);
		createEReference(ruleTypeEClass, RULE_TYPE__IMP);
		createEReference(ruleTypeEClass, RULE_TYPE__EQ);
		createEAttribute(ruleTypeEClass, RULE_TYPE__VAR);

		structTypeEClass = createEClass(STRUCT_TYPE);
		createEReference(structTypeEClass, STRUCT_TYPE__FEATURE);
		createEReference(structTypeEClass, STRUCT_TYPE__AND);
		createEReference(structTypeEClass, STRUCT_TYPE__OR);
		createEReference(structTypeEClass, STRUCT_TYPE__ALT);

		unaryFormulaTypeEClass = createEClass(UNARY_FORMULA_TYPE);
		createEReference(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__CONJ);
		createEReference(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__DISJ);
		createEReference(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__NOT);
		createEReference(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__IMP);
		createEReference(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__EQ);
		createEAttribute(unaryFormulaTypeEClass, UNARY_FORMULA_TYPE__VAR);

		propertiesTypeEClass = createEClass(PROPERTIES_TYPE);
		createEReference(propertiesTypeEClass, PROPERTIES_TYPE__GRAPHICS);

		graphicsTypeEClass = createEClass(GRAPHICS_TYPE);
		createEAttribute(graphicsTypeEClass, GRAPHICS_TYPE__KEY);
		createEAttribute(graphicsTypeEClass, GRAPHICS_TYPE__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(binaryFormulaTypeEClass, BinaryFormulaType.class, "BinaryFormulaType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBinaryFormulaType_Formula(), ecorePackage.getEFeatureMapEntry(), "formula", null, 0, -1, BinaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryFormulaType_Conj(), this.getBinaryFormulaType(), null, "conj", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryFormulaType_Disj(), this.getBinaryFormulaType(), null, "disj", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryFormulaType_Not(), this.getUnaryFormulaType(), null, "not", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryFormulaType_Imp(), this.getBinaryFormulaType(), null, "imp", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryFormulaType_Eq(), this.getBinaryFormulaType(), null, "eq", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getBinaryFormulaType_Var(), theXMLTypePackage.getString(), "var", null, 0, 2, BinaryFormulaType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(branchTypeEClass, BranchType.class, "BranchType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBranchType_Description(), theXMLTypePackage.getString(), "description", null, 0, 1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBranchType_Node(), ecorePackage.getEFeatureMapEntry(), "node", null, 0, -1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBranchType_Feature(), this.getFeatureType(), null, "feature", null, 0, -1, BranchType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBranchType_And(), this.getBranchType(), null, "and", null, 0, -1, BranchType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBranchType_Or(), this.getBranchType(), null, "or", null, 0, -1, BranchType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getBranchType_Alt(), this.getBranchType(), null, "alt", null, 0, -1, BranchType.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getBranchType_Abstract(), theXMLTypePackage.getString(), "abstract", null, 0, 1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBranchType_Mandatory(), theXMLTypePackage.getString(), "mandatory", null, 0, 1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBranchType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBranchType_Graphics(), this.getGraphicsType(), null, "graphics", null, 0, -1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBranchType_Hidden(), ecorePackage.getEBoolean(), "hidden", null, 0, 1, BranchType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constraintsTypeEClass, ConstraintsType.class, "ConstraintsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstraintsType_Rule(), this.getRuleType(), null, "rule", null, 0, -1, ConstraintsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_FeatureModel(), this.getFeatureModelType(), null, "featureModel", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(featureModelTypeEClass, FeatureModelType.class, "FeatureModelType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureModelType_Properties(), ecorePackage.getEObject(), null, "properties", null, 0, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelType_Struct(), this.getStructType(), null, "struct", null, 1, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelType_Constraints(), this.getConstraintsType(), null, "constraints", null, 1, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelType_Calculations(), ecorePackage.getEObject(), null, "calculations", null, 0, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelType_Comments(), ecorePackage.getEObject(), null, "comments", null, 0, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureModelType_FeatureOrder(), ecorePackage.getEObject(), null, "featureOrder", null, 0, 1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureModelType_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, FeatureModelType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureTypeEClass, FeatureType.class, "FeatureType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFeatureType_Description(), theXMLTypePackage.getString(), "description", null, 0, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureType_Abstract(), theXMLTypePackage.getBoolean(), "abstract", null, 0, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureType_Mandatory(), theXMLTypePackage.getBoolean(), "mandatory", null, 0, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureType_Name(), theXMLTypePackage.getString(), "name", null, 1, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureType_Value(), theXMLTypePackage.getString(), "value", null, 0, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureType_Graphics(), this.getGraphicsType(), null, "graphics", null, 0, -1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureType_Hidden(), ecorePackage.getEBoolean(), "hidden", null, 0, 1, FeatureType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleTypeEClass, RuleType.class, "RuleType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleType_Conj(), this.getBinaryFormulaType(), null, "conj", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleType_Disj(), this.getBinaryFormulaType(), null, "disj", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleType_Not(), this.getUnaryFormulaType(), null, "not", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleType_Imp(), this.getBinaryFormulaType(), null, "imp", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuleType_Eq(), this.getBinaryFormulaType(), null, "eq", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleType_Var(), theXMLTypePackage.getString(), "var", null, 0, 1, RuleType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(structTypeEClass, StructType.class, "StructType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStructType_Feature(), this.getFeatureType(), null, "feature", null, 0, 1, StructType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStructType_And(), this.getBranchType(), null, "and", null, 0, 1, StructType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStructType_Or(), this.getBranchType(), null, "or", null, 0, 1, StructType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStructType_Alt(), this.getBranchType(), null, "alt", null, 0, 1, StructType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryFormulaTypeEClass, UnaryFormulaType.class, "UnaryFormulaType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryFormulaType_Conj(), this.getBinaryFormulaType(), null, "conj", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryFormulaType_Disj(), this.getBinaryFormulaType(), null, "disj", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryFormulaType_Not(), this.getUnaryFormulaType(), null, "not", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryFormulaType_Imp(), this.getBinaryFormulaType(), null, "imp", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUnaryFormulaType_Eq(), this.getBinaryFormulaType(), null, "eq", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnaryFormulaType_Var(), theXMLTypePackage.getString(), "var", null, 0, 1, UnaryFormulaType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertiesTypeEClass, PropertiesType.class, "PropertiesType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertiesType_Graphics(), this.getGraphicsType(), null, "graphics", null, 0, -1, PropertiesType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(graphicsTypeEClass, GraphicsType.class, "GraphicsType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGraphicsType_Key(), ecorePackage.getEString(), "key", null, 1, 1, GraphicsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphicsType_Value(), ecorePackage.getEString(), "value", null, 1, 1, GraphicsType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "qualified", "false"
		   });
		addAnnotation
		  (binaryFormulaTypeEClass,
		   source,
		   new String[] {
			   "name", "binaryFormulaType",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getBinaryFormulaType_Formula(),
		   source,
		   new String[] {
			   "kind", "group",
			   "name", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Conj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conj",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Disj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "disj",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Not(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "not",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Imp(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "imp",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Eq(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "eq",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (getBinaryFormulaType_Var(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "var",
			   "namespace", "##targetNamespace",
			   "group", "Formula:0"
		   });
		addAnnotation
		  (branchTypeEClass,
		   source,
		   new String[] {
			   "name", "branchType",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getBranchType_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBranchType_Node(),
		   source,
		   new String[] {
			   "kind", "group",
			   "name", "Node:1"
		   });
		addAnnotation
		  (getBranchType_Feature(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "feature",
			   "namespace", "##targetNamespace",
			   "group", "Node:1"
		   });
		addAnnotation
		  (getBranchType_And(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "and",
			   "namespace", "##targetNamespace",
			   "group", "Node:1"
		   });
		addAnnotation
		  (getBranchType_Or(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "or",
			   "namespace", "##targetNamespace",
			   "group", "Node:1"
		   });
		addAnnotation
		  (getBranchType_Alt(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "alt",
			   "namespace", "##targetNamespace",
			   "group", "Node:1"
		   });
		addAnnotation
		  (getBranchType_Abstract(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "abstract",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBranchType_Mandatory(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "mandatory",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getBranchType_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (constraintsTypeEClass,
		   source,
		   new String[] {
			   "name", "constraints_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getConstraintsType_Rule(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "rule",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_Mixed(),
		   source,
		   new String[] {
			   "kind", "elementWildcard",
			   "name", ":mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_FeatureModel(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "featureModel",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (featureModelTypeEClass,
		   source,
		   new String[] {
			   "name", "featureModel_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getFeatureModelType_Properties(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "properties",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_Struct(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "struct",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_Constraints(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "constraints",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_Calculations(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "calculations",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_Comments(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "comments",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_FeatureOrder(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "featureOrder",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureModelType_AnyAttribute(),
		   source,
		   new String[] {
			   "kind", "attributeWildcard",
			   "wildcards", "##any",
			   "name", ":6",
			   "processing", "skip"
		   });
		addAnnotation
		  (featureTypeEClass,
		   source,
		   new String[] {
			   "name", "featureType",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getFeatureType_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureType_Abstract(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "abstract",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureType_Mandatory(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "mandatory",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureType_Name(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "name",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getFeatureType_Value(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "value",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (ruleTypeEClass,
		   source,
		   new String[] {
			   "name", "rule_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getRuleType_Conj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conj",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRuleType_Disj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "disj",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRuleType_Not(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "not",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRuleType_Imp(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "imp",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRuleType_Eq(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "eq",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getRuleType_Var(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "var",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (structTypeEClass,
		   source,
		   new String[] {
			   "name", "struct_._type",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getStructType_Feature(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "feature",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getStructType_And(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "and",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getStructType_Or(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "or",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getStructType_Alt(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "alt",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (unaryFormulaTypeEClass,
		   source,
		   new String[] {
			   "name", "unaryFormulaType",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (getUnaryFormulaType_Conj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "conj",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnaryFormulaType_Disj(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "disj",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnaryFormulaType_Not(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "not",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnaryFormulaType_Imp(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "imp",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnaryFormulaType_Eq(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "eq",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getUnaryFormulaType_Var(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "var",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (propertiesTypeEClass,
		   source,
		   new String[] {
			   "name", "propertiesType",
			   "kind", "elementOnly"
		   });
		addAnnotation
		  (graphicsTypeEClass,
		   source,
		   new String[] {
			   "name", "GraphicsType",
			   "kind", "elementOnly"
		   });
	}

} //FeatureidePackageImpl
