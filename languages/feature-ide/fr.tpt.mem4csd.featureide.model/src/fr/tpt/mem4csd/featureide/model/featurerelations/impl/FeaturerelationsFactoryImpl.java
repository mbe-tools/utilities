/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations.impl;

import fr.tpt.mem4csd.featureide.model.featurerelations.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeaturerelationsFactoryImpl extends EFactoryImpl implements FeaturerelationsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeaturerelationsFactory init() {
		try {
			FeaturerelationsFactory theFeaturerelationsFactory = (FeaturerelationsFactory)EPackage.Registry.INSTANCE.getEFactory(FeaturerelationsPackage.eNS_URI);
			if (theFeaturerelationsFactory != null) {
				return theFeaturerelationsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FeaturerelationsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeaturerelationsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FeaturerelationsPackage.SUB_CLASS_RELATION: return createSubClassRelation();
			case FeaturerelationsPackage.FEATURE_MODEL_RELATIONS_SPEC: return createFeatureModelRelationsSpec();
			case FeaturerelationsPackage.INDIVIDUAL: return createIndividual();
			case FeaturerelationsPackage.DISABLED: return createDisabled();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubClassRelation createSubClassRelation() {
		SubClassRelationImpl subClassRelation = new SubClassRelationImpl();
		return subClassRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureModelRelationsSpec createFeatureModelRelationsSpec() {
		FeatureModelRelationsSpecImpl featureModelRelationsSpec = new FeatureModelRelationsSpecImpl();
		return featureModelRelationsSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Individual createIndividual() {
		IndividualImpl individual = new IndividualImpl();
		return individual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Disabled createDisabled() {
		DisabledImpl disabled = new DisabledImpl();
		return disabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeaturerelationsPackage getFeaturerelationsPackage() {
		return (FeaturerelationsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FeaturerelationsPackage getPackage() {
		return FeaturerelationsPackage.eINSTANCE;
	}

} //FeaturerelationsFactoryImpl
