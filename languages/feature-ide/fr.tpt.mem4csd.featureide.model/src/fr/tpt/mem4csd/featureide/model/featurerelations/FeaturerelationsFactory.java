/**
 */
package fr.tpt.mem4csd.featureide.model.featurerelations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.featureide.model.featurerelations.FeaturerelationsPackage
 * @generated
 */
public interface FeaturerelationsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeaturerelationsFactory eINSTANCE = fr.tpt.mem4csd.featureide.model.featurerelations.impl.FeaturerelationsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Sub Class Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Class Relation</em>'.
	 * @generated
	 */
	SubClassRelation createSubClassRelation();

	/**
	 * Returns a new object of class '<em>Feature Model Relations Spec</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Model Relations Spec</em>'.
	 * @generated
	 */
	FeatureModelRelationsSpec createFeatureModelRelationsSpec();

	/**
	 * Returns a new object of class '<em>Individual</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Individual</em>'.
	 * @generated
	 */
	Individual createIndividual();

	/**
	 * Returns a new object of class '<em>Disabled</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Disabled</em>'.
	 * @generated
	 */
	Disabled createDisabled();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FeaturerelationsPackage getFeaturerelationsPackage();

} //FeaturerelationsFactory
