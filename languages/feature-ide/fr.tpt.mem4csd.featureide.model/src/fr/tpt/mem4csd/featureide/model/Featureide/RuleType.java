/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getConj <em>Conj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getDisj <em>Disj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getNot <em>Not</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getImp <em>Imp</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getEq <em>Eq</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getVar <em>Var</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType()
 * @model extendedMetaData="name='rule_._type' kind='elementOnly'"
 * @generated
 */
public interface RuleType extends EObject {
	/**
	 * Returns the value of the '<em><b>Conj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conj</em>' containment reference.
	 * @see #setConj(BinaryFormulaType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Conj()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conj' namespace='##targetNamespace'"
	 * @generated
	 */
	BinaryFormulaType getConj();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getConj <em>Conj</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conj</em>' containment reference.
	 * @see #getConj()
	 * @generated
	 */
	void setConj(BinaryFormulaType value);

	/**
	 * Returns the value of the '<em><b>Disj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disj</em>' containment reference.
	 * @see #setDisj(BinaryFormulaType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Disj()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='disj' namespace='##targetNamespace'"
	 * @generated
	 */
	BinaryFormulaType getDisj();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getDisj <em>Disj</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disj</em>' containment reference.
	 * @see #getDisj()
	 * @generated
	 */
	void setDisj(BinaryFormulaType value);

	/**
	 * Returns the value of the '<em><b>Not</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not</em>' containment reference.
	 * @see #setNot(UnaryFormulaType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Not()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryFormulaType getNot();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getNot <em>Not</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not</em>' containment reference.
	 * @see #getNot()
	 * @generated
	 */
	void setNot(UnaryFormulaType value);

	/**
	 * Returns the value of the '<em><b>Imp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imp</em>' containment reference.
	 * @see #setImp(BinaryFormulaType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Imp()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='imp' namespace='##targetNamespace'"
	 * @generated
	 */
	BinaryFormulaType getImp();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getImp <em>Imp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imp</em>' containment reference.
	 * @see #getImp()
	 * @generated
	 */
	void setImp(BinaryFormulaType value);

	/**
	 * Returns the value of the '<em><b>Eq</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Eq</em>' containment reference.
	 * @see #setEq(BinaryFormulaType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Eq()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='eq' namespace='##targetNamespace'"
	 * @generated
	 */
	BinaryFormulaType getEq();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getEq <em>Eq</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Eq</em>' containment reference.
	 * @see #getEq()
	 * @generated
	 */
	void setEq(BinaryFormulaType value);

	/**
	 * Returns the value of the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' attribute.
	 * @see #setVar(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getRuleType_Var()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='var' namespace='##targetNamespace'"
	 * @generated
	 */
	String getVar();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType#getVar <em>Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var</em>' attribute.
	 * @see #getVar()
	 * @generated
	 */
	void setVar(String value);

} // RuleType
