/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Model Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getProperties <em>Properties</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getStruct <em>Struct</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getCalculations <em>Calculations</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getComments <em>Comments</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getFeatureOrder <em>Feature Order</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getAnyAttribute <em>Any Attribute</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType()
 * @model extendedMetaData="name='featureModel_._type' kind='elementOnly'"
 * @generated
 */
public interface FeatureModelType extends EObject {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference.
	 * @see #setProperties(EObject)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_Properties()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='properties' namespace='##targetNamespace'"
	 * @generated
	 */
	EObject getProperties();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getProperties <em>Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Properties</em>' containment reference.
	 * @see #getProperties()
	 * @generated
	 */
	void setProperties(EObject value);

	/**
	 * Returns the value of the '<em><b>Struct</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Struct</em>' containment reference.
	 * @see #setStruct(StructType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_Struct()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='struct' namespace='##targetNamespace'"
	 * @generated
	 */
	StructType getStruct();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getStruct <em>Struct</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Struct</em>' containment reference.
	 * @see #getStruct()
	 * @generated
	 */
	void setStruct(StructType value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference.
	 * @see #setConstraints(ConstraintsType)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_Constraints()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='constraints' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstraintsType getConstraints();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getConstraints <em>Constraints</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints</em>' containment reference.
	 * @see #getConstraints()
	 * @generated
	 */
	void setConstraints(ConstraintsType value);

	/**
	 * Returns the value of the '<em><b>Calculations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculations</em>' containment reference.
	 * @see #setCalculations(EObject)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_Calculations()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='calculations' namespace='##targetNamespace'"
	 * @generated
	 */
	EObject getCalculations();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getCalculations <em>Calculations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Calculations</em>' containment reference.
	 * @see #getCalculations()
	 * @generated
	 */
	void setCalculations(EObject value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference.
	 * @see #setComments(EObject)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_Comments()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comments' namespace='##targetNamespace'"
	 * @generated
	 */
	EObject getComments();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getComments <em>Comments</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comments</em>' containment reference.
	 * @see #getComments()
	 * @generated
	 */
	void setComments(EObject value);

	/**
	 * Returns the value of the '<em><b>Feature Order</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Order</em>' containment reference.
	 * @see #setFeatureOrder(EObject)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_FeatureOrder()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='featureOrder' namespace='##targetNamespace'"
	 * @generated
	 */
	EObject getFeatureOrder();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType#getFeatureOrder <em>Feature Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Order</em>' containment reference.
	 * @see #getFeatureOrder()
	 * @generated
	 */
	void setFeatureOrder(EObject value);

	/**
	 * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Any Attribute</em>' attribute list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getFeatureModelType_AnyAttribute()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':6' processing='skip'"
	 * @generated
	 */
	FeatureMap getAnyAttribute();

} // FeatureModelType
