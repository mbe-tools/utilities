/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.impl;

import fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.RuleType;
import fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getConj <em>Conj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getDisj <em>Disj</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getNot <em>Not</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getImp <em>Imp</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getEq <em>Eq</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.impl.RuleTypeImpl#getVar <em>Var</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuleTypeImpl extends MinimalEObjectImpl.Container implements RuleType {
	/**
	 * The cached value of the '{@link #getConj() <em>Conj</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConj()
	 * @generated
	 * @ordered
	 */
	protected BinaryFormulaType conj;

	/**
	 * The cached value of the '{@link #getDisj() <em>Disj</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisj()
	 * @generated
	 * @ordered
	 */
	protected BinaryFormulaType disj;

	/**
	 * The cached value of the '{@link #getNot() <em>Not</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNot()
	 * @generated
	 * @ordered
	 */
	protected UnaryFormulaType not;

	/**
	 * The cached value of the '{@link #getImp() <em>Imp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImp()
	 * @generated
	 * @ordered
	 */
	protected BinaryFormulaType imp;

	/**
	 * The cached value of the '{@link #getEq() <em>Eq</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEq()
	 * @generated
	 * @ordered
	 */
	protected BinaryFormulaType eq;

	/**
	 * The default value of the '{@link #getVar() <em>Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVar()
	 * @generated
	 * @ordered
	 */
	protected static final String VAR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVar() <em>Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVar()
	 * @generated
	 * @ordered
	 */
	protected String var = VAR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FeatureidePackage.Literals.RULE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryFormulaType getConj() {
		return conj;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConj(BinaryFormulaType newConj, NotificationChain msgs) {
		BinaryFormulaType oldConj = conj;
		conj = newConj;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__CONJ, oldConj, newConj);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConj(BinaryFormulaType newConj) {
		if (newConj != conj) {
			NotificationChain msgs = null;
			if (conj != null)
				msgs = ((InternalEObject)conj).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__CONJ, null, msgs);
			if (newConj != null)
				msgs = ((InternalEObject)newConj).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__CONJ, null, msgs);
			msgs = basicSetConj(newConj, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__CONJ, newConj, newConj));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryFormulaType getDisj() {
		return disj;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDisj(BinaryFormulaType newDisj, NotificationChain msgs) {
		BinaryFormulaType oldDisj = disj;
		disj = newDisj;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__DISJ, oldDisj, newDisj);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDisj(BinaryFormulaType newDisj) {
		if (newDisj != disj) {
			NotificationChain msgs = null;
			if (disj != null)
				msgs = ((InternalEObject)disj).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__DISJ, null, msgs);
			if (newDisj != null)
				msgs = ((InternalEObject)newDisj).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__DISJ, null, msgs);
			msgs = basicSetDisj(newDisj, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__DISJ, newDisj, newDisj));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnaryFormulaType getNot() {
		return not;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNot(UnaryFormulaType newNot, NotificationChain msgs) {
		UnaryFormulaType oldNot = not;
		not = newNot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__NOT, oldNot, newNot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNot(UnaryFormulaType newNot) {
		if (newNot != not) {
			NotificationChain msgs = null;
			if (not != null)
				msgs = ((InternalEObject)not).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__NOT, null, msgs);
			if (newNot != null)
				msgs = ((InternalEObject)newNot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__NOT, null, msgs);
			msgs = basicSetNot(newNot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__NOT, newNot, newNot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryFormulaType getImp() {
		return imp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImp(BinaryFormulaType newImp, NotificationChain msgs) {
		BinaryFormulaType oldImp = imp;
		imp = newImp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__IMP, oldImp, newImp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setImp(BinaryFormulaType newImp) {
		if (newImp != imp) {
			NotificationChain msgs = null;
			if (imp != null)
				msgs = ((InternalEObject)imp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__IMP, null, msgs);
			if (newImp != null)
				msgs = ((InternalEObject)newImp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__IMP, null, msgs);
			msgs = basicSetImp(newImp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__IMP, newImp, newImp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryFormulaType getEq() {
		return eq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEq(BinaryFormulaType newEq, NotificationChain msgs) {
		BinaryFormulaType oldEq = eq;
		eq = newEq;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__EQ, oldEq, newEq);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEq(BinaryFormulaType newEq) {
		if (newEq != eq) {
			NotificationChain msgs = null;
			if (eq != null)
				msgs = ((InternalEObject)eq).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__EQ, null, msgs);
			if (newEq != null)
				msgs = ((InternalEObject)newEq).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FeatureidePackage.RULE_TYPE__EQ, null, msgs);
			msgs = basicSetEq(newEq, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__EQ, newEq, newEq));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVar() {
		return var;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVar(String newVar) {
		String oldVar = var;
		var = newVar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FeatureidePackage.RULE_TYPE__VAR, oldVar, var));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeatureidePackage.RULE_TYPE__CONJ:
				return basicSetConj(null, msgs);
			case FeatureidePackage.RULE_TYPE__DISJ:
				return basicSetDisj(null, msgs);
			case FeatureidePackage.RULE_TYPE__NOT:
				return basicSetNot(null, msgs);
			case FeatureidePackage.RULE_TYPE__IMP:
				return basicSetImp(null, msgs);
			case FeatureidePackage.RULE_TYPE__EQ:
				return basicSetEq(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeatureidePackage.RULE_TYPE__CONJ:
				return getConj();
			case FeatureidePackage.RULE_TYPE__DISJ:
				return getDisj();
			case FeatureidePackage.RULE_TYPE__NOT:
				return getNot();
			case FeatureidePackage.RULE_TYPE__IMP:
				return getImp();
			case FeatureidePackage.RULE_TYPE__EQ:
				return getEq();
			case FeatureidePackage.RULE_TYPE__VAR:
				return getVar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeatureidePackage.RULE_TYPE__CONJ:
				setConj((BinaryFormulaType)newValue);
				return;
			case FeatureidePackage.RULE_TYPE__DISJ:
				setDisj((BinaryFormulaType)newValue);
				return;
			case FeatureidePackage.RULE_TYPE__NOT:
				setNot((UnaryFormulaType)newValue);
				return;
			case FeatureidePackage.RULE_TYPE__IMP:
				setImp((BinaryFormulaType)newValue);
				return;
			case FeatureidePackage.RULE_TYPE__EQ:
				setEq((BinaryFormulaType)newValue);
				return;
			case FeatureidePackage.RULE_TYPE__VAR:
				setVar((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeatureidePackage.RULE_TYPE__CONJ:
				setConj((BinaryFormulaType)null);
				return;
			case FeatureidePackage.RULE_TYPE__DISJ:
				setDisj((BinaryFormulaType)null);
				return;
			case FeatureidePackage.RULE_TYPE__NOT:
				setNot((UnaryFormulaType)null);
				return;
			case FeatureidePackage.RULE_TYPE__IMP:
				setImp((BinaryFormulaType)null);
				return;
			case FeatureidePackage.RULE_TYPE__EQ:
				setEq((BinaryFormulaType)null);
				return;
			case FeatureidePackage.RULE_TYPE__VAR:
				setVar(VAR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeatureidePackage.RULE_TYPE__CONJ:
				return conj != null;
			case FeatureidePackage.RULE_TYPE__DISJ:
				return disj != null;
			case FeatureidePackage.RULE_TYPE__NOT:
				return not != null;
			case FeatureidePackage.RULE_TYPE__IMP:
				return imp != null;
			case FeatureidePackage.RULE_TYPE__EQ:
				return eq != null;
			case FeatureidePackage.RULE_TYPE__VAR:
				return VAR_EDEFAULT == null ? var != null : !VAR_EDEFAULT.equals(var);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (var: ");
		result.append(var);
		result.append(')');
		return result.toString();
	}

} //RuleTypeImpl
