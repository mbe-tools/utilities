/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Branch Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getNode <em>Node</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getFeature <em>Feature</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAnd <em>And</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getOr <em>Or</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAlt <em>Alt</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getName <em>Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getGraphics <em>Graphics</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#isHidden <em>Hidden</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType()
 * @model extendedMetaData="name='branchType' kind='elementOnly'"
 * @generated
 */
public interface BranchType extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Description()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='description' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Node</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' attribute list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Node()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='Node:1'"
	 * @generated
	 */
	FeatureMap getNode();

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Feature()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='feature' namespace='##targetNamespace' group='Node:1'"
	 * @generated
	 */
	EList<FeatureType> getFeature();

	/**
	 * Returns the value of the '<em><b>And</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_And()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='and' namespace='##targetNamespace' group='Node:1'"
	 * @generated
	 */
	EList<BranchType> getAnd();

	/**
	 * Returns the value of the '<em><b>Or</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Or()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or' namespace='##targetNamespace' group='Node:1'"
	 * @generated
	 */
	EList<BranchType> getOr();

	/**
	 * Returns the value of the '<em><b>Alt</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alt</em>' containment reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Alt()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='alt' namespace='##targetNamespace' group='Node:1'"
	 * @generated
	 */
	EList<BranchType> getAlt();

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Abstract()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='abstract' namespace='##targetNamespace'"
	 * @generated
	 */
	String getAbstract();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #getAbstract()
	 * @generated
	 */
	void setAbstract(String value);

	/**
	 * Returns the value of the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory</em>' attribute.
	 * @see #setMandatory(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Mandatory()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='mandatory' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMandatory();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory</em>' attribute.
	 * @see #getMandatory()
	 * @generated
	 */
	void setMandatory(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Graphics</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphics</em>' reference list.
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Graphics()
	 * @model
	 * @generated
	 */
	EList<GraphicsType> getGraphics();

	/**
	 * Returns the value of the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hidden</em>' attribute.
	 * @see #setHidden(boolean)
	 * @see fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage#getBranchType_Hidden()
	 * @model
	 * @generated
	 */
	boolean isHidden();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType#isHidden <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hidden</em>' attribute.
	 * @see #isHidden()
	 * @generated
	 */
	void setHidden(boolean value);

} // BranchType
