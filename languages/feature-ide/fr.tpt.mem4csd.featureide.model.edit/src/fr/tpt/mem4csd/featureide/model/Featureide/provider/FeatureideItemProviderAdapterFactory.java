/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.provider;

import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.util.FeatureideAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import java.util.List;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ChildCreationExtenderManager;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureideItemProviderAdapterFactory extends FeatureideAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable, IChildCreationExtender {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This helps manage the child creation extenders.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChildCreationExtenderManager childCreationExtenderManager = new ChildCreationExtenderManager(FeatureideEditPlugin.INSTANCE, FeatureidePackage.eNS_URI);

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureideItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryFormulaTypeItemProvider binaryFormulaTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.BinaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBinaryFormulaTypeAdapter() {
		if (binaryFormulaTypeItemProvider == null) {
			binaryFormulaTypeItemProvider = new BinaryFormulaTypeItemProvider(this);
		}

		return binaryFormulaTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BranchTypeItemProvider branchTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.BranchType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBranchTypeAdapter() {
		if (branchTypeItemProvider == null) {
			branchTypeItemProvider = new BranchTypeItemProvider(this);
		}

		return branchTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintsTypeItemProvider constraintsTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.ConstraintsType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createConstraintsTypeAdapter() {
		if (constraintsTypeItemProvider == null) {
			constraintsTypeItemProvider = new ConstraintsTypeItemProvider(this);
		}

		return constraintsTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootItemProvider documentRootItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.DocumentRoot}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createDocumentRootAdapter() {
		if (documentRootItemProvider == null) {
			documentRootItemProvider = new DocumentRootItemProvider(this);
		}

		return documentRootItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModelTypeItemProvider featureModelTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createFeatureModelTypeAdapter() {
		if (featureModelTypeItemProvider == null) {
			featureModelTypeItemProvider = new FeatureModelTypeItemProvider(this);
		}

		return featureModelTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureTypeItemProvider featureTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createFeatureTypeAdapter() {
		if (featureTypeItemProvider == null) {
			featureTypeItemProvider = new FeatureTypeItemProvider(this);
		}

		return featureTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleTypeItemProvider ruleTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.RuleType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRuleTypeAdapter() {
		if (ruleTypeItemProvider == null) {
			ruleTypeItemProvider = new RuleTypeItemProvider(this);
		}

		return ruleTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.StructType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StructTypeItemProvider structTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.StructType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createStructTypeAdapter() {
		if (structTypeItemProvider == null) {
			structTypeItemProvider = new StructTypeItemProvider(this);
		}

		return structTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryFormulaTypeItemProvider unaryFormulaTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUnaryFormulaTypeAdapter() {
		if (unaryFormulaTypeItemProvider == null) {
			unaryFormulaTypeItemProvider = new UnaryFormulaTypeItemProvider(this);
		}

		return unaryFormulaTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertiesTypeItemProvider propertiesTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.PropertiesType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPropertiesTypeAdapter() {
		if (propertiesTypeItemProvider == null) {
			propertiesTypeItemProvider = new PropertiesTypeItemProvider(this);
		}

		return propertiesTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphicsTypeItemProvider graphicsTypeItemProvider;

	/**
	 * This creates an adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.GraphicsType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGraphicsTypeAdapter() {
		if (graphicsTypeItemProvider == null) {
			graphicsTypeItemProvider = new GraphicsTypeItemProvider(this);
		}

		return graphicsTypeItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<IChildCreationExtender> getChildCreationExtenders() {
		return childCreationExtenderManager.getChildCreationExtenders();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collection<?> getNewChildDescriptors(Object object, EditingDomain editingDomain) {
		return childCreationExtenderManager.getNewChildDescriptors(object, editingDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceLocator getResourceLocator() {
		return childCreationExtenderManager;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void dispose() {
		if (binaryFormulaTypeItemProvider != null) binaryFormulaTypeItemProvider.dispose();
		if (branchTypeItemProvider != null) branchTypeItemProvider.dispose();
		if (constraintsTypeItemProvider != null) constraintsTypeItemProvider.dispose();
		if (documentRootItemProvider != null) documentRootItemProvider.dispose();
		if (featureModelTypeItemProvider != null) featureModelTypeItemProvider.dispose();
		if (featureTypeItemProvider != null) featureTypeItemProvider.dispose();
		if (ruleTypeItemProvider != null) ruleTypeItemProvider.dispose();
		if (structTypeItemProvider != null) structTypeItemProvider.dispose();
		if (unaryFormulaTypeItemProvider != null) unaryFormulaTypeItemProvider.dispose();
		if (propertiesTypeItemProvider != null) propertiesTypeItemProvider.dispose();
		if (graphicsTypeItemProvider != null) graphicsTypeItemProvider.dispose();
	}

}
