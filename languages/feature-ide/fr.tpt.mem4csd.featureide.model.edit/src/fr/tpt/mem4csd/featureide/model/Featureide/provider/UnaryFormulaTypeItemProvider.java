/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.provider;


import fr.tpt.mem4csd.featureide.model.Featureide.FeatureideFactory;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;
import fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.UnaryFormulaType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UnaryFormulaTypeItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryFormulaTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVarPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Var feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVarPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UnaryFormulaType_var_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UnaryFormulaType_var_feature", "_UI_UnaryFormulaType_type"),
				 FeatureidePackage.Literals.UNARY_FORMULA_TYPE__VAR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__CONJ);
			childrenFeatures.add(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__DISJ);
			childrenFeatures.add(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__NOT);
			childrenFeatures.add(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__IMP);
			childrenFeatures.add(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__EQ);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns UnaryFormulaType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UnaryFormulaType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UnaryFormulaType)object).getVar();
		return label == null || label.length() == 0 ?
			getString("_UI_UnaryFormulaType_type") :
			getString("_UI_UnaryFormulaType_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UnaryFormulaType.class)) {
			case FeatureidePackage.UNARY_FORMULA_TYPE__VAR:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case FeatureidePackage.UNARY_FORMULA_TYPE__CONJ:
			case FeatureidePackage.UNARY_FORMULA_TYPE__DISJ:
			case FeatureidePackage.UNARY_FORMULA_TYPE__NOT:
			case FeatureidePackage.UNARY_FORMULA_TYPE__IMP:
			case FeatureidePackage.UNARY_FORMULA_TYPE__EQ:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__CONJ,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__DISJ,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__NOT,
				 FeatureideFactory.eINSTANCE.createUnaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__IMP,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.UNARY_FORMULA_TYPE__EQ,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == FeatureidePackage.Literals.UNARY_FORMULA_TYPE__CONJ ||
			childFeature == FeatureidePackage.Literals.UNARY_FORMULA_TYPE__DISJ ||
			childFeature == FeatureidePackage.Literals.UNARY_FORMULA_TYPE__IMP ||
			childFeature == FeatureidePackage.Literals.UNARY_FORMULA_TYPE__EQ;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
