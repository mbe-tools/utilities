/**
 */
package fr.tpt.mem4csd.featureide.model.Featureide.provider;


import fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureideFactory;
import fr.tpt.mem4csd.featureide.model.Featureide.FeatureidePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;

import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.tpt.mem4csd.featureide.model.Featureide.FeatureModelType} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureModelTypeItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModelTypeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__STRUCT);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CONSTRAINTS);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER);
			childrenFeatures.add(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__ANY_ATTRIBUTE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FeatureModelType.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FeatureModelType"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_FeatureModelType_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FeatureModelType.class)) {
			case FeatureidePackage.FEATURE_MODEL_TYPE__PROPERTIES:
			case FeatureidePackage.FEATURE_MODEL_TYPE__STRUCT:
			case FeatureidePackage.FEATURE_MODEL_TYPE__CONSTRAINTS:
			case FeatureidePackage.FEATURE_MODEL_TYPE__CALCULATIONS:
			case FeatureidePackage.FEATURE_MODEL_TYPE__COMMENTS:
			case FeatureidePackage.FEATURE_MODEL_TYPE__FEATURE_ORDER:
			case FeatureidePackage.FEATURE_MODEL_TYPE__ANY_ATTRIBUTE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createBranchType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createFeatureType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createUnaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createPropertiesType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 FeatureideFactory.eINSTANCE.createGraphicsType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES,
				 XMLTypeFactory.eINSTANCE.createAnyType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__STRUCT,
				 FeatureideFactory.eINSTANCE.createStructType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CONSTRAINTS,
				 FeatureideFactory.eINSTANCE.createConstraintsType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createBranchType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createFeatureType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createUnaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createPropertiesType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 FeatureideFactory.eINSTANCE.createGraphicsType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS,
				 XMLTypeFactory.eINSTANCE.createAnyType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createBranchType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createFeatureType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createUnaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createPropertiesType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 FeatureideFactory.eINSTANCE.createGraphicsType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS,
				 XMLTypeFactory.eINSTANCE.createAnyType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createBinaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createBranchType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createFeatureType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createUnaryFormulaType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createPropertiesType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 FeatureideFactory.eINSTANCE.createGraphicsType()));

		newChildDescriptors.add
			(createChildParameter
				(FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER,
				 XMLTypeFactory.eINSTANCE.createAnyType()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		if (childFeature instanceof EStructuralFeature && FeatureMapUtil.isFeatureMap((EStructuralFeature)childFeature)) {
			FeatureMap.Entry entry = (FeatureMap.Entry)childObject;
			childFeature = entry.getEStructuralFeature();
			childObject = entry.getValue();
		}

		boolean qualify =
			childFeature == FeatureidePackage.Literals.FEATURE_MODEL_TYPE__PROPERTIES ||
			childFeature == FeatureidePackage.Literals.FEATURE_MODEL_TYPE__CALCULATIONS ||
			childFeature == FeatureidePackage.Literals.FEATURE_MODEL_TYPE__COMMENTS ||
			childFeature == FeatureidePackage.Literals.FEATURE_MODEL_TYPE__FEATURE_ORDER;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
