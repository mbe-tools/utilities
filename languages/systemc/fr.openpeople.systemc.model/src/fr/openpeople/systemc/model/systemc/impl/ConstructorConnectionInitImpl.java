/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.ClassMember;
import fr.openpeople.systemc.model.systemc.ConnectionId;
import fr.openpeople.systemc.model.systemc.ConstructorConnectionInit;
import fr.openpeople.systemc.model.systemc.SystemcPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constructor Connection Init</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl#getClassMember <em>Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl#getInputConnection <em>Input Connection</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl#getOutputConnection <em>Output Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConstructorConnectionInitImpl extends NameImpl implements ConstructorConnectionInit {
	/**
	 * The cached value of the '{@link #getClassMember() <em>Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassMember()
	 * @generated
	 * @ordered
	 */
	protected ClassMember classMember;

	/**
	 * The cached value of the '{@link #getInputConnection() <em>Input Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputConnection()
	 * @generated
	 * @ordered
	 */
	protected ConnectionId inputConnection;

	/**
	 * The cached value of the '{@link #getOutputConnection() <em>Output Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputConnection()
	 * @generated
	 * @ordered
	 */
	protected ConnectionId outputConnection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstructorConnectionInitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemcPackage.Literals.CONSTRUCTOR_CONNECTION_INIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember getClassMember() {
		if (classMember != null && classMember.eIsProxy()) {
			InternalEObject oldClassMember = (InternalEObject)classMember;
			classMember = (ClassMember)eResolveProxy(oldClassMember);
			if (classMember != oldClassMember) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, oldClassMember, classMember));
			}
		}
		return classMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember basicGetClassMember() {
		return classMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassMember(ClassMember newClassMember, NotificationChain msgs) {
		ClassMember oldClassMember = classMember;
		classMember = newClassMember;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, oldClassMember, newClassMember);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassMember(ClassMember newClassMember) {
		if (newClassMember != classMember) {
			NotificationChain msgs = null;
			if (classMember != null)
				msgs = ((InternalEObject)classMember).eInverseRemove(this, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, ClassMember.class, msgs);
			if (newClassMember != null)
				msgs = ((InternalEObject)newClassMember).eInverseAdd(this, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, ClassMember.class, msgs);
			msgs = basicSetClassMember(newClassMember, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, newClassMember, newClassMember));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionId getInputConnection() {
		if (inputConnection != null && inputConnection.eIsProxy()) {
			InternalEObject oldInputConnection = (InternalEObject)inputConnection;
			inputConnection = (ConnectionId)eResolveProxy(oldInputConnection);
			if (inputConnection != oldInputConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, oldInputConnection, inputConnection));
			}
		}
		return inputConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionId basicGetInputConnection() {
		return inputConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputConnection(ConnectionId newInputConnection, NotificationChain msgs) {
		ConnectionId oldInputConnection = inputConnection;
		inputConnection = newInputConnection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, oldInputConnection, newInputConnection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputConnection(ConnectionId newInputConnection) {
		if (newInputConnection != inputConnection) {
			NotificationChain msgs = null;
			if (inputConnection != null)
				msgs = ((InternalEObject)inputConnection).eInverseRemove(this, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
			if (newInputConnection != null)
				msgs = ((InternalEObject)newInputConnection).eInverseAdd(this, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
			msgs = basicSetInputConnection(newInputConnection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, newInputConnection, newInputConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionId getOutputConnection() {
		if (outputConnection != null && outputConnection.eIsProxy()) {
			InternalEObject oldOutputConnection = (InternalEObject)outputConnection;
			outputConnection = (ConnectionId)eResolveProxy(oldOutputConnection);
			if (outputConnection != oldOutputConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, oldOutputConnection, outputConnection));
			}
		}
		return outputConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionId basicGetOutputConnection() {
		return outputConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputConnection(ConnectionId newOutputConnection, NotificationChain msgs) {
		ConnectionId oldOutputConnection = outputConnection;
		outputConnection = newOutputConnection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, oldOutputConnection, newOutputConnection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputConnection(ConnectionId newOutputConnection) {
		if (newOutputConnection != outputConnection) {
			NotificationChain msgs = null;
			if (outputConnection != null)
				msgs = ((InternalEObject)outputConnection).eInverseRemove(this, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
			if (newOutputConnection != null)
				msgs = ((InternalEObject)newOutputConnection).eInverseAdd(this, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
			msgs = basicSetOutputConnection(newOutputConnection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, newOutputConnection, newOutputConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				if (classMember != null)
					msgs = ((InternalEObject)classMember).eInverseRemove(this, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, ClassMember.class, msgs);
				return basicSetClassMember((ClassMember)otherEnd, msgs);
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				if (inputConnection != null)
					msgs = ((InternalEObject)inputConnection).eInverseRemove(this, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
				return basicSetInputConnection((ConnectionId)otherEnd, msgs);
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				if (outputConnection != null)
					msgs = ((InternalEObject)outputConnection).eInverseRemove(this, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, ConnectionId.class, msgs);
				return basicSetOutputConnection((ConnectionId)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				return basicSetClassMember(null, msgs);
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				return basicSetInputConnection(null, msgs);
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				return basicSetOutputConnection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				if (resolve) return getClassMember();
				return basicGetClassMember();
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				if (resolve) return getInputConnection();
				return basicGetInputConnection();
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				if (resolve) return getOutputConnection();
				return basicGetOutputConnection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				setClassMember((ClassMember)newValue);
				return;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				setInputConnection((ConnectionId)newValue);
				return;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				setOutputConnection((ConnectionId)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				setClassMember((ClassMember)null);
				return;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				setInputConnection((ConnectionId)null);
				return;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				setOutputConnection((ConnectionId)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER:
				return classMember != null;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION:
				return inputConnection != null;
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION:
				return outputConnection != null;
		}
		return super.eIsSet(featureID);
	}

} //ConstructorConnectionInitImpl
