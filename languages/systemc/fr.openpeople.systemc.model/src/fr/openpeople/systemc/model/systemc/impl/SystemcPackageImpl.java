/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.Binding;
import fr.openpeople.systemc.model.systemc.ClassList;
import fr.openpeople.systemc.model.systemc.ClassMember;
import fr.openpeople.systemc.model.systemc.ClassSection;
import fr.openpeople.systemc.model.systemc.ConnectionId;
import fr.openpeople.systemc.model.systemc.ConstructorConnectionInit;
import fr.openpeople.systemc.model.systemc.Name;
import fr.openpeople.systemc.model.systemc.NameSpace;
import fr.openpeople.systemc.model.systemc.SystemcFactory;
import fr.openpeople.systemc.model.systemc.SystemcPackage;
import fr.openpeople.systemc.model.systemc.TopLevel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemcPackageImpl extends EPackageImpl implements SystemcPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionIdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constructorConnectionInitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classMemberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classSectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nameSpaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass topLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bindingEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SystemcPackageImpl() {
		super(eNS_URI, SystemcFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SystemcPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SystemcPackage init() {
		if (isInited) return (SystemcPackage)EPackage.Registry.INSTANCE.getEPackage(SystemcPackage.eNS_URI);

		// Obtain or create and register package
		SystemcPackageImpl theSystemcPackage = (SystemcPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SystemcPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SystemcPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSystemcPackage.createPackageContents();

		// Initialize created meta-data
		theSystemcPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSystemcPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SystemcPackage.eNS_URI, theSystemcPackage);
		return theSystemcPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getName_() {
		return nameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getName_Name() {
		return (EAttribute)nameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionId() {
		return connectionIdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConnectionId_ComponentName() {
		return (EAttribute)connectionIdEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionId_ComponentClass() {
		return (EReference)connectionIdEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionId_ComponentClassMember() {
		return (EReference)connectionIdEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConnectionId_PortName() {
		return (EAttribute)connectionIdEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionId_PortClassMember() {
		return (EReference)connectionIdEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionId_InputConstructorConnectionInit() {
		return (EReference)connectionIdEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionId_OutputConstructorConnectionInit() {
		return (EReference)connectionIdEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstructorConnectionInit() {
		return constructorConnectionInitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstructorConnectionInit_ClassMember() {
		return (EReference)constructorConnectionInitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstructorConnectionInit_InputConnection() {
		return (EReference)constructorConnectionInitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConstructorConnectionInit_OutputConnection() {
		return (EReference)constructorConnectionInitEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassMember() {
		return classMemberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassMember_ClassSection() {
		return (EReference)classMemberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassMember_InstanceOfName() {
		return (EAttribute)classMemberEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassMember_InstanceOfClass() {
		return (EReference)classMemberEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassMember_TemplateName() {
		return (EAttribute)classMemberEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassMember_ConstructorConnectionInit() {
		return (EReference)classMemberEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassMember_ProcessorBinding() {
		return (EAttribute)classMemberEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassSection() {
		return classSectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassSection_Public() {
		return (EAttribute)classSectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassSection_Members() {
		return (EReference)classSectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassSection_Class() {
		return (EReference)classSectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClass_() {
		return classEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_ClassList() {
		return (EReference)classEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClass_TypeInterface() {
		return (EAttribute)classEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClass_ScmoduleInterface() {
		return (EAttribute)classEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClass_RuntimeExtend() {
		return (EAttribute)classEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Extend() {
		return (EReference)classEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Sections() {
		return (EReference)classEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClass_Bindings() {
		return (EReference)classEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassList() {
		return classListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassList_Classes() {
		return (EReference)classListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassList_NameSpace() {
		return (EReference)classListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNameSpace() {
		return nameSpaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_ClassLists() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_NameSpace() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_SubNameSpaces() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNameSpace_TopLevel() {
		return (EReference)nameSpaceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTopLevel() {
		return topLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTopLevel_Include() {
		return (EAttribute)topLevelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTopLevel_FileName() {
		return (EAttribute)topLevelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopLevel_NameSpace() {
		return (EReference)topLevelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTopLevel_OrderedClasses() {
		return (EReference)topLevelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinding() {
		return bindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_Thread() {
		return (EReference)bindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinding_Processor() {
		return (EReference)bindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemcFactory getSystemcFactory() {
		return (SystemcFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		nameEClass = createEClass(NAME);
		createEAttribute(nameEClass, NAME__NAME);

		connectionIdEClass = createEClass(CONNECTION_ID);
		createEAttribute(connectionIdEClass, CONNECTION_ID__COMPONENT_NAME);
		createEReference(connectionIdEClass, CONNECTION_ID__COMPONENT_CLASS);
		createEReference(connectionIdEClass, CONNECTION_ID__COMPONENT_CLASS_MEMBER);
		createEAttribute(connectionIdEClass, CONNECTION_ID__PORT_NAME);
		createEReference(connectionIdEClass, CONNECTION_ID__PORT_CLASS_MEMBER);
		createEReference(connectionIdEClass, CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT);
		createEReference(connectionIdEClass, CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT);

		constructorConnectionInitEClass = createEClass(CONSTRUCTOR_CONNECTION_INIT);
		createEReference(constructorConnectionInitEClass, CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER);
		createEReference(constructorConnectionInitEClass, CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION);
		createEReference(constructorConnectionInitEClass, CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION);

		classMemberEClass = createEClass(CLASS_MEMBER);
		createEReference(classMemberEClass, CLASS_MEMBER__CLASS_SECTION);
		createEAttribute(classMemberEClass, CLASS_MEMBER__INSTANCE_OF_NAME);
		createEReference(classMemberEClass, CLASS_MEMBER__INSTANCE_OF_CLASS);
		createEAttribute(classMemberEClass, CLASS_MEMBER__TEMPLATE_NAME);
		createEReference(classMemberEClass, CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT);
		createEAttribute(classMemberEClass, CLASS_MEMBER__PROCESSOR_BINDING);

		classSectionEClass = createEClass(CLASS_SECTION);
		createEAttribute(classSectionEClass, CLASS_SECTION__PUBLIC);
		createEReference(classSectionEClass, CLASS_SECTION__MEMBERS);
		createEReference(classSectionEClass, CLASS_SECTION__CLASS);

		classEClass = createEClass(CLASS);
		createEReference(classEClass, CLASS__CLASS_LIST);
		createEAttribute(classEClass, CLASS__TYPE_INTERFACE);
		createEAttribute(classEClass, CLASS__SCMODULE_INTERFACE);
		createEAttribute(classEClass, CLASS__RUNTIME_EXTEND);
		createEReference(classEClass, CLASS__EXTEND);
		createEReference(classEClass, CLASS__SECTIONS);
		createEReference(classEClass, CLASS__BINDINGS);

		classListEClass = createEClass(CLASS_LIST);
		createEReference(classListEClass, CLASS_LIST__CLASSES);
		createEReference(classListEClass, CLASS_LIST__NAME_SPACE);

		nameSpaceEClass = createEClass(NAME_SPACE);
		createEReference(nameSpaceEClass, NAME_SPACE__CLASS_LISTS);
		createEReference(nameSpaceEClass, NAME_SPACE__NAME_SPACE);
		createEReference(nameSpaceEClass, NAME_SPACE__SUB_NAME_SPACES);
		createEReference(nameSpaceEClass, NAME_SPACE__TOP_LEVEL);

		topLevelEClass = createEClass(TOP_LEVEL);
		createEAttribute(topLevelEClass, TOP_LEVEL__INCLUDE);
		createEAttribute(topLevelEClass, TOP_LEVEL__FILE_NAME);
		createEReference(topLevelEClass, TOP_LEVEL__NAME_SPACE);
		createEReference(topLevelEClass, TOP_LEVEL__ORDERED_CLASSES);

		bindingEClass = createEClass(BINDING);
		createEReference(bindingEClass, BINDING__THREAD);
		createEReference(bindingEClass, BINDING__PROCESSOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		connectionIdEClass.getESuperTypes().add(this.getName_());
		constructorConnectionInitEClass.getESuperTypes().add(this.getName_());
		classMemberEClass.getESuperTypes().add(this.getName_());
		classSectionEClass.getESuperTypes().add(this.getName_());
		classEClass.getESuperTypes().add(this.getName_());
		classListEClass.getESuperTypes().add(this.getName_());
		nameSpaceEClass.getESuperTypes().add(this.getName_());
		topLevelEClass.getESuperTypes().add(this.getName_());

		// Initialize classes and features; add operations and parameters
		initEClass(nameEClass, Name.class, "Name", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getName_Name(), ecorePackage.getEString(), "name", null, 1, 1, Name.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionIdEClass, ConnectionId.class, "ConnectionId", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConnectionId_ComponentName(), ecorePackage.getEString(), "componentName", null, 1, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionId_ComponentClass(), this.getClass_(), null, "componentClass", null, 0, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionId_ComponentClassMember(), this.getClassMember(), null, "componentClassMember", null, 0, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConnectionId_PortName(), ecorePackage.getEString(), "portName", null, 1, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionId_PortClassMember(), this.getClassMember(), null, "portClassMember", null, 0, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionId_InputConstructorConnectionInit(), this.getConstructorConnectionInit(), this.getConstructorConnectionInit_InputConnection(), "inputConstructorConnectionInit", null, 0, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionId_OutputConstructorConnectionInit(), this.getConstructorConnectionInit(), this.getConstructorConnectionInit_OutputConnection(), "outputConstructorConnectionInit", null, 0, 1, ConnectionId.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constructorConnectionInitEClass, ConstructorConnectionInit.class, "ConstructorConnectionInit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConstructorConnectionInit_ClassMember(), this.getClassMember(), this.getClassMember_ConstructorConnectionInit(), "classMember", null, 1, 1, ConstructorConnectionInit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConstructorConnectionInit_InputConnection(), this.getConnectionId(), this.getConnectionId_InputConstructorConnectionInit(), "inputConnection", null, 1, 1, ConstructorConnectionInit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConstructorConnectionInit_OutputConnection(), this.getConnectionId(), this.getConnectionId_OutputConstructorConnectionInit(), "outputConnection", null, 1, 1, ConstructorConnectionInit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classMemberEClass, ClassMember.class, "ClassMember", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClassMember_ClassSection(), this.getClassSection(), this.getClassSection_Members(), "classSection", null, 1, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassMember_InstanceOfName(), ecorePackage.getEString(), "instanceOfName", null, 1, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassMember_InstanceOfClass(), this.getClass_(), null, "instanceOfClass", null, 0, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassMember_TemplateName(), ecorePackage.getEString(), "templateName", null, 0, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassMember_ConstructorConnectionInit(), this.getConstructorConnectionInit(), this.getConstructorConnectionInit_ClassMember(), "constructorConnectionInit", null, 0, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassMember_ProcessorBinding(), ecorePackage.getEString(), "processorBinding", null, 0, 1, ClassMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classSectionEClass, ClassSection.class, "ClassSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClassSection_Public(), ecorePackage.getEBoolean(), "public", "true", 1, 1, ClassSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassSection_Members(), this.getClassMember(), this.getClassMember_ClassSection(), "members", null, 0, -1, ClassSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassSection_Class(), this.getClass_(), this.getClass_Sections(), "class", null, 1, 1, ClassSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classEClass, fr.openpeople.systemc.model.systemc.Class.class, "Class", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClass_ClassList(), this.getClassList(), this.getClassList_Classes(), "classList", null, 1, 1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClass_TypeInterface(), ecorePackage.getEString(), "typeInterface", null, 0, 1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClass_ScmoduleInterface(), ecorePackage.getEString(), "scmoduleInterface", null, 0, 1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClass_RuntimeExtend(), ecorePackage.getEString(), "runtimeExtend", null, 0, 1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_Extend(), this.getClass_(), null, "extend", null, 0, -1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_Sections(), this.getClassSection(), this.getClassSection_Class(), "sections", null, 0, -1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClass_Bindings(), this.getBinding(), null, "bindings", null, 0, -1, fr.openpeople.systemc.model.systemc.Class.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classListEClass, ClassList.class, "ClassList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClassList_Classes(), this.getClass_(), this.getClass_ClassList(), "classes", null, 0, -1, ClassList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassList_NameSpace(), this.getNameSpace(), this.getNameSpace_ClassLists(), "nameSpace", null, 1, 1, ClassList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nameSpaceEClass, NameSpace.class, "NameSpace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNameSpace_ClassLists(), this.getClassList(), this.getClassList_NameSpace(), "classLists", null, 0, -1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNameSpace_NameSpace(), this.getNameSpace(), this.getNameSpace_SubNameSpaces(), "nameSpace", null, 0, 1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNameSpace_SubNameSpaces(), this.getNameSpace(), this.getNameSpace_NameSpace(), "subNameSpaces", null, 0, -1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNameSpace_TopLevel(), this.getTopLevel(), this.getTopLevel_NameSpace(), "topLevel", null, 0, 1, NameSpace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(topLevelEClass, TopLevel.class, "TopLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTopLevel_Include(), ecorePackage.getEString(), "include", null, 1, 1, TopLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTopLevel_FileName(), ecorePackage.getEString(), "fileName", null, 1, 1, TopLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTopLevel_NameSpace(), this.getNameSpace(), this.getNameSpace_TopLevel(), "nameSpace", null, 1, 1, TopLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTopLevel_OrderedClasses(), this.getClass_(), null, "orderedClasses", null, 0, -1, TopLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bindingEClass, Binding.class, "Binding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinding_Thread(), this.getClass_(), null, "Thread", null, 1, 1, Binding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinding_Processor(), this.getClass_(), null, "Processor", null, 1, 1, Binding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SystemcPackageImpl
