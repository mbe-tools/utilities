/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.ClassMember;
import fr.openpeople.systemc.model.systemc.ClassSection;
import fr.openpeople.systemc.model.systemc.ConstructorConnectionInit;
import fr.openpeople.systemc.model.systemc.SystemcPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Member</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getClassSection <em>Class Section</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getInstanceOfName <em>Instance Of Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getInstanceOfClass <em>Instance Of Class</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getTemplateName <em>Template Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getConstructorConnectionInit <em>Constructor Connection Init</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl#getProcessorBinding <em>Processor Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassMemberImpl extends NameImpl implements ClassMember {
	/**
	 * The cached value of the '{@link #getClassSection() <em>Class Section</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSection()
	 * @generated
	 * @ordered
	 */
	protected ClassSection classSection;

	/**
	 * The default value of the '{@link #getInstanceOfName() <em>Instance Of Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceOfName()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_OF_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstanceOfName() <em>Instance Of Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceOfName()
	 * @generated
	 * @ordered
	 */
	protected String instanceOfName = INSTANCE_OF_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstanceOfClass() <em>Instance Of Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceOfClass()
	 * @generated
	 * @ordered
	 */
	protected fr.openpeople.systemc.model.systemc.Class instanceOfClass;

	/**
	 * The default value of the '{@link #getTemplateName() <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateName()
	 * @generated
	 * @ordered
	 */
	protected static final String TEMPLATE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTemplateName() <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateName()
	 * @generated
	 * @ordered
	 */
	protected String templateName = TEMPLATE_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstructorConnectionInit() <em>Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructorConnectionInit()
	 * @generated
	 * @ordered
	 */
	protected ConstructorConnectionInit constructorConnectionInit;

	/**
	 * The default value of the '{@link #getProcessorBinding() <em>Processor Binding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorBinding()
	 * @generated
	 * @ordered
	 */
	protected static final String PROCESSOR_BINDING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProcessorBinding() <em>Processor Binding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorBinding()
	 * @generated
	 * @ordered
	 */
	protected String processorBinding = PROCESSOR_BINDING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassMemberImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemcPackage.Literals.CLASS_MEMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSection getClassSection() {
		if (classSection != null && classSection.eIsProxy()) {
			InternalEObject oldClassSection = (InternalEObject)classSection;
			classSection = (ClassSection)eResolveProxy(oldClassSection);
			if (classSection != oldClassSection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CLASS_MEMBER__CLASS_SECTION, oldClassSection, classSection));
			}
		}
		return classSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSection basicGetClassSection() {
		return classSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassSection(ClassSection newClassSection, NotificationChain msgs) {
		ClassSection oldClassSection = classSection;
		classSection = newClassSection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__CLASS_SECTION, oldClassSection, newClassSection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassSection(ClassSection newClassSection) {
		if (newClassSection != classSection) {
			NotificationChain msgs = null;
			if (classSection != null)
				msgs = ((InternalEObject)classSection).eInverseRemove(this, SystemcPackage.CLASS_SECTION__MEMBERS, ClassSection.class, msgs);
			if (newClassSection != null)
				msgs = ((InternalEObject)newClassSection).eInverseAdd(this, SystemcPackage.CLASS_SECTION__MEMBERS, ClassSection.class, msgs);
			msgs = basicSetClassSection(newClassSection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__CLASS_SECTION, newClassSection, newClassSection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInstanceOfName() {
		return instanceOfName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceOfName(String newInstanceOfName) {
		String oldInstanceOfName = instanceOfName;
		instanceOfName = newInstanceOfName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__INSTANCE_OF_NAME, oldInstanceOfName, instanceOfName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.openpeople.systemc.model.systemc.Class getInstanceOfClass() {
		if (instanceOfClass != null && instanceOfClass.eIsProxy()) {
			InternalEObject oldInstanceOfClass = (InternalEObject)instanceOfClass;
			instanceOfClass = (fr.openpeople.systemc.model.systemc.Class)eResolveProxy(oldInstanceOfClass);
			if (instanceOfClass != oldInstanceOfClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS, oldInstanceOfClass, instanceOfClass));
			}
		}
		return instanceOfClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.openpeople.systemc.model.systemc.Class basicGetInstanceOfClass() {
		return instanceOfClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceOfClass(fr.openpeople.systemc.model.systemc.Class newInstanceOfClass) {
		fr.openpeople.systemc.model.systemc.Class oldInstanceOfClass = instanceOfClass;
		instanceOfClass = newInstanceOfClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS, oldInstanceOfClass, instanceOfClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplateName(String newTemplateName) {
		String oldTemplateName = templateName;
		templateName = newTemplateName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__TEMPLATE_NAME, oldTemplateName, templateName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit getConstructorConnectionInit() {
		if (constructorConnectionInit != null && constructorConnectionInit.eIsProxy()) {
			InternalEObject oldConstructorConnectionInit = (InternalEObject)constructorConnectionInit;
			constructorConnectionInit = (ConstructorConnectionInit)eResolveProxy(oldConstructorConnectionInit);
			if (constructorConnectionInit != oldConstructorConnectionInit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, oldConstructorConnectionInit, constructorConnectionInit));
			}
		}
		return constructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit basicGetConstructorConnectionInit() {
		return constructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstructorConnectionInit(ConstructorConnectionInit newConstructorConnectionInit, NotificationChain msgs) {
		ConstructorConnectionInit oldConstructorConnectionInit = constructorConnectionInit;
		constructorConnectionInit = newConstructorConnectionInit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, oldConstructorConnectionInit, newConstructorConnectionInit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstructorConnectionInit(ConstructorConnectionInit newConstructorConnectionInit) {
		if (newConstructorConnectionInit != constructorConnectionInit) {
			NotificationChain msgs = null;
			if (constructorConnectionInit != null)
				msgs = ((InternalEObject)constructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, ConstructorConnectionInit.class, msgs);
			if (newConstructorConnectionInit != null)
				msgs = ((InternalEObject)newConstructorConnectionInit).eInverseAdd(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, ConstructorConnectionInit.class, msgs);
			msgs = basicSetConstructorConnectionInit(newConstructorConnectionInit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT, newConstructorConnectionInit, newConstructorConnectionInit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProcessorBinding() {
		return processorBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessorBinding(String newProcessorBinding) {
		String oldProcessorBinding = processorBinding;
		processorBinding = newProcessorBinding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS_MEMBER__PROCESSOR_BINDING, oldProcessorBinding, processorBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				if (classSection != null)
					msgs = ((InternalEObject)classSection).eInverseRemove(this, SystemcPackage.CLASS_SECTION__MEMBERS, ClassSection.class, msgs);
				return basicSetClassSection((ClassSection)otherEnd, msgs);
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				if (constructorConnectionInit != null)
					msgs = ((InternalEObject)constructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER, ConstructorConnectionInit.class, msgs);
				return basicSetConstructorConnectionInit((ConstructorConnectionInit)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				return basicSetClassSection(null, msgs);
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				return basicSetConstructorConnectionInit(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				if (resolve) return getClassSection();
				return basicGetClassSection();
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_NAME:
				return getInstanceOfName();
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS:
				if (resolve) return getInstanceOfClass();
				return basicGetInstanceOfClass();
			case SystemcPackage.CLASS_MEMBER__TEMPLATE_NAME:
				return getTemplateName();
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				if (resolve) return getConstructorConnectionInit();
				return basicGetConstructorConnectionInit();
			case SystemcPackage.CLASS_MEMBER__PROCESSOR_BINDING:
				return getProcessorBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				setClassSection((ClassSection)newValue);
				return;
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_NAME:
				setInstanceOfName((String)newValue);
				return;
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS:
				setInstanceOfClass((fr.openpeople.systemc.model.systemc.Class)newValue);
				return;
			case SystemcPackage.CLASS_MEMBER__TEMPLATE_NAME:
				setTemplateName((String)newValue);
				return;
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				setConstructorConnectionInit((ConstructorConnectionInit)newValue);
				return;
			case SystemcPackage.CLASS_MEMBER__PROCESSOR_BINDING:
				setProcessorBinding((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				setClassSection((ClassSection)null);
				return;
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_NAME:
				setInstanceOfName(INSTANCE_OF_NAME_EDEFAULT);
				return;
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS:
				setInstanceOfClass((fr.openpeople.systemc.model.systemc.Class)null);
				return;
			case SystemcPackage.CLASS_MEMBER__TEMPLATE_NAME:
				setTemplateName(TEMPLATE_NAME_EDEFAULT);
				return;
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				setConstructorConnectionInit((ConstructorConnectionInit)null);
				return;
			case SystemcPackage.CLASS_MEMBER__PROCESSOR_BINDING:
				setProcessorBinding(PROCESSOR_BINDING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemcPackage.CLASS_MEMBER__CLASS_SECTION:
				return classSection != null;
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_NAME:
				return INSTANCE_OF_NAME_EDEFAULT == null ? instanceOfName != null : !INSTANCE_OF_NAME_EDEFAULT.equals(instanceOfName);
			case SystemcPackage.CLASS_MEMBER__INSTANCE_OF_CLASS:
				return instanceOfClass != null;
			case SystemcPackage.CLASS_MEMBER__TEMPLATE_NAME:
				return TEMPLATE_NAME_EDEFAULT == null ? templateName != null : !TEMPLATE_NAME_EDEFAULT.equals(templateName);
			case SystemcPackage.CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT:
				return constructorConnectionInit != null;
			case SystemcPackage.CLASS_MEMBER__PROCESSOR_BINDING:
				return PROCESSOR_BINDING_EDEFAULT == null ? processorBinding != null : !PROCESSOR_BINDING_EDEFAULT.equals(processorBinding);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (instanceOfName: ");
		result.append(instanceOfName);
		result.append(", templateName: ");
		result.append(templateName);
		result.append(", processorBinding: ");
		result.append(processorBinding);
		result.append(')');
		return result.toString();
	}

} //ClassMemberImpl
