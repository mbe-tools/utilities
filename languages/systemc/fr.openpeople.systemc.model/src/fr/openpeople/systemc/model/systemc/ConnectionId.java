/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection Id</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentName <em>Component Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClass <em>Component Class</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClassMember <em>Component Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortName <em>Port Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortClassMember <em>Port Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit <em>Input Constructor Connection Init</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit <em>Output Constructor Connection Init</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId()
 * @model
 * @generated
 */
public interface ConnectionId extends Name {
	/**
	 * Returns the value of the '<em><b>Component Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Name</em>' attribute.
	 * @see #setComponentName(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_ComponentName()
	 * @model required="true"
	 * @generated
	 */
	String getComponentName();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentName <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Name</em>' attribute.
	 * @see #getComponentName()
	 * @generated
	 */
	void setComponentName(String value);

	/**
	 * Returns the value of the '<em><b>Component Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Class</em>' reference.
	 * @see #setComponentClass(fr.openpeople.systemc.model.systemc.Class)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_ComponentClass()
	 * @model
	 * @generated
	 */
	fr.openpeople.systemc.model.systemc.Class getComponentClass();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClass <em>Component Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Class</em>' reference.
	 * @see #getComponentClass()
	 * @generated
	 */
	void setComponentClass(fr.openpeople.systemc.model.systemc.Class value);

	/**
	 * Returns the value of the '<em><b>Component Class Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Class Member</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Class Member</em>' reference.
	 * @see #setComponentClassMember(ClassMember)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_ComponentClassMember()
	 * @model
	 * @generated
	 */
	ClassMember getComponentClassMember();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClassMember <em>Component Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Class Member</em>' reference.
	 * @see #getComponentClassMember()
	 * @generated
	 */
	void setComponentClassMember(ClassMember value);

	/**
	 * Returns the value of the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Name</em>' attribute.
	 * @see #setPortName(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_PortName()
	 * @model required="true"
	 * @generated
	 */
	String getPortName();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortName <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Name</em>' attribute.
	 * @see #getPortName()
	 * @generated
	 */
	void setPortName(String value);

	/**
	 * Returns the value of the '<em><b>Port Class Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Class Member</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Class Member</em>' reference.
	 * @see #setPortClassMember(ClassMember)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_PortClassMember()
	 * @model
	 * @generated
	 */
	ClassMember getPortClassMember();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortClassMember <em>Port Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Class Member</em>' reference.
	 * @see #getPortClassMember()
	 * @generated
	 */
	void setPortClassMember(ClassMember value);

	/**
	 * Returns the value of the '<em><b>Input Constructor Connection Init</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection <em>Input Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Constructor Connection Init</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Constructor Connection Init</em>' reference.
	 * @see #setInputConstructorConnectionInit(ConstructorConnectionInit)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_InputConstructorConnectionInit()
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection
	 * @model opposite="inputConnection"
	 * @generated
	 */
	ConstructorConnectionInit getInputConstructorConnectionInit();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit <em>Input Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Constructor Connection Init</em>' reference.
	 * @see #getInputConstructorConnectionInit()
	 * @generated
	 */
	void setInputConstructorConnectionInit(ConstructorConnectionInit value);

	/**
	 * Returns the value of the '<em><b>Output Constructor Connection Init</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection <em>Output Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Constructor Connection Init</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Constructor Connection Init</em>' reference.
	 * @see #setOutputConstructorConnectionInit(ConstructorConnectionInit)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConnectionId_OutputConstructorConnectionInit()
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection
	 * @model opposite="outputConnection"
	 * @generated
	 */
	ConstructorConnectionInit getOutputConstructorConnectionInit();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit <em>Output Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Constructor Connection Init</em>' reference.
	 * @see #getOutputConstructorConnectionInit()
	 * @generated
	 */
	void setOutputConstructorConnectionInit(ConstructorConnectionInit value);

} // ConnectionId
