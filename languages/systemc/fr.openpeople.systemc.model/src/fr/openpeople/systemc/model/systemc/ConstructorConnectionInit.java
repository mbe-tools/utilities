/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constructor Connection Init</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember <em>Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection <em>Input Connection</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection <em>Output Connection</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConstructorConnectionInit()
 * @model
 * @generated
 */
public interface ConstructorConnectionInit extends Name {
	/**
	 * Returns the value of the '<em><b>Class Member</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit <em>Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Member</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Member</em>' reference.
	 * @see #setClassMember(ClassMember)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConstructorConnectionInit_ClassMember()
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit
	 * @model opposite="constructorConnectionInit" required="true"
	 * @generated
	 */
	ClassMember getClassMember();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember <em>Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Member</em>' reference.
	 * @see #getClassMember()
	 * @generated
	 */
	void setClassMember(ClassMember value);

	/**
	 * Returns the value of the '<em><b>Input Connection</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit <em>Input Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Connection</em>' reference.
	 * @see #setInputConnection(ConnectionId)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConstructorConnectionInit_InputConnection()
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit
	 * @model opposite="inputConstructorConnectionInit" required="true"
	 * @generated
	 */
	ConnectionId getInputConnection();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection <em>Input Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Connection</em>' reference.
	 * @see #getInputConnection()
	 * @generated
	 */
	void setInputConnection(ConnectionId value);

	/**
	 * Returns the value of the '<em><b>Output Connection</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit <em>Output Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Connection</em>' reference.
	 * @see #setOutputConnection(ConnectionId)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getConstructorConnectionInit_OutputConnection()
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit
	 * @model opposite="outputConstructorConnectionInit" required="true"
	 * @generated
	 */
	ConnectionId getOutputConnection();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection <em>Output Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Connection</em>' reference.
	 * @see #getOutputConnection()
	 * @generated
	 */
	void setOutputConnection(ConnectionId value);

} // ConstructorConnectionInit
