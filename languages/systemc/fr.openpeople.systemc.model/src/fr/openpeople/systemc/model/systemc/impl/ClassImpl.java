/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.Binding;
import fr.openpeople.systemc.model.systemc.ClassList;
import fr.openpeople.systemc.model.systemc.ClassSection;
import fr.openpeople.systemc.model.systemc.SystemcPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getClassList <em>Class List</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getTypeInterface <em>Type Interface</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getScmoduleInterface <em>Scmodule Interface</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getRuntimeExtend <em>Runtime Extend</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getExtend <em>Extend</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getSections <em>Sections</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl#getBindings <em>Bindings</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassImpl extends NameImpl implements fr.openpeople.systemc.model.systemc.Class {
	/**
	 * The cached value of the '{@link #getClassList() <em>Class List</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassList()
	 * @generated
	 * @ordered
	 */
	protected ClassList classList;

	/**
	 * The default value of the '{@link #getTypeInterface() <em>Type Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeInterface()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_INTERFACE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeInterface() <em>Type Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeInterface()
	 * @generated
	 * @ordered
	 */
	protected String typeInterface = TYPE_INTERFACE_EDEFAULT;

	/**
	 * The default value of the '{@link #getScmoduleInterface() <em>Scmodule Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScmoduleInterface()
	 * @generated
	 * @ordered
	 */
	protected static final String SCMODULE_INTERFACE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScmoduleInterface() <em>Scmodule Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScmoduleInterface()
	 * @generated
	 * @ordered
	 */
	protected String scmoduleInterface = SCMODULE_INTERFACE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRuntimeExtend() <em>Runtime Extend</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeExtend()
	 * @generated
	 * @ordered
	 */
	protected static final String RUNTIME_EXTEND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRuntimeExtend() <em>Runtime Extend</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeExtend()
	 * @generated
	 * @ordered
	 */
	protected String runtimeExtend = RUNTIME_EXTEND_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExtend() <em>Extend</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtend()
	 * @generated
	 * @ordered
	 */
	protected EList<fr.openpeople.systemc.model.systemc.Class> extend;

	/**
	 * The cached value of the '{@link #getSections() <em>Sections</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSections()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassSection> sections;

	/**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<Binding> bindings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemcPackage.Literals.CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassList getClassList() {
		if (classList != null && classList.eIsProxy()) {
			InternalEObject oldClassList = (InternalEObject)classList;
			classList = (ClassList)eResolveProxy(oldClassList);
			if (classList != oldClassList) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CLASS__CLASS_LIST, oldClassList, classList));
			}
		}
		return classList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassList basicGetClassList() {
		return classList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassList(ClassList newClassList, NotificationChain msgs) {
		ClassList oldClassList = classList;
		classList = newClassList;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS__CLASS_LIST, oldClassList, newClassList);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassList(ClassList newClassList) {
		if (newClassList != classList) {
			NotificationChain msgs = null;
			if (classList != null)
				msgs = ((InternalEObject)classList).eInverseRemove(this, SystemcPackage.CLASS_LIST__CLASSES, ClassList.class, msgs);
			if (newClassList != null)
				msgs = ((InternalEObject)newClassList).eInverseAdd(this, SystemcPackage.CLASS_LIST__CLASSES, ClassList.class, msgs);
			msgs = basicSetClassList(newClassList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS__CLASS_LIST, newClassList, newClassList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeInterface() {
		return typeInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeInterface(String newTypeInterface) {
		String oldTypeInterface = typeInterface;
		typeInterface = newTypeInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS__TYPE_INTERFACE, oldTypeInterface, typeInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScmoduleInterface() {
		return scmoduleInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScmoduleInterface(String newScmoduleInterface) {
		String oldScmoduleInterface = scmoduleInterface;
		scmoduleInterface = newScmoduleInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS__SCMODULE_INTERFACE, oldScmoduleInterface, scmoduleInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRuntimeExtend() {
		return runtimeExtend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRuntimeExtend(String newRuntimeExtend) {
		String oldRuntimeExtend = runtimeExtend;
		runtimeExtend = newRuntimeExtend;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CLASS__RUNTIME_EXTEND, oldRuntimeExtend, runtimeExtend));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<fr.openpeople.systemc.model.systemc.Class> getExtend() {
		if (extend == null) {
			extend = new EObjectResolvingEList<fr.openpeople.systemc.model.systemc.Class>(fr.openpeople.systemc.model.systemc.Class.class, this, SystemcPackage.CLASS__EXTEND);
		}
		return extend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassSection> getSections() {
		if (sections == null) {
			sections = new EObjectWithInverseResolvingEList<ClassSection>(ClassSection.class, this, SystemcPackage.CLASS__SECTIONS, SystemcPackage.CLASS_SECTION__CLASS);
		}
		return sections;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Binding> getBindings() {
		if (bindings == null) {
			bindings = new EObjectResolvingEList<Binding>(Binding.class, this, SystemcPackage.CLASS__BINDINGS);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				if (classList != null)
					msgs = ((InternalEObject)classList).eInverseRemove(this, SystemcPackage.CLASS_LIST__CLASSES, ClassList.class, msgs);
				return basicSetClassList((ClassList)otherEnd, msgs);
			case SystemcPackage.CLASS__SECTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSections()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				return basicSetClassList(null, msgs);
			case SystemcPackage.CLASS__SECTIONS:
				return ((InternalEList<?>)getSections()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				if (resolve) return getClassList();
				return basicGetClassList();
			case SystemcPackage.CLASS__TYPE_INTERFACE:
				return getTypeInterface();
			case SystemcPackage.CLASS__SCMODULE_INTERFACE:
				return getScmoduleInterface();
			case SystemcPackage.CLASS__RUNTIME_EXTEND:
				return getRuntimeExtend();
			case SystemcPackage.CLASS__EXTEND:
				return getExtend();
			case SystemcPackage.CLASS__SECTIONS:
				return getSections();
			case SystemcPackage.CLASS__BINDINGS:
				return getBindings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				setClassList((ClassList)newValue);
				return;
			case SystemcPackage.CLASS__TYPE_INTERFACE:
				setTypeInterface((String)newValue);
				return;
			case SystemcPackage.CLASS__SCMODULE_INTERFACE:
				setScmoduleInterface((String)newValue);
				return;
			case SystemcPackage.CLASS__RUNTIME_EXTEND:
				setRuntimeExtend((String)newValue);
				return;
			case SystemcPackage.CLASS__EXTEND:
				getExtend().clear();
				getExtend().addAll((Collection<? extends fr.openpeople.systemc.model.systemc.Class>)newValue);
				return;
			case SystemcPackage.CLASS__SECTIONS:
				getSections().clear();
				getSections().addAll((Collection<? extends ClassSection>)newValue);
				return;
			case SystemcPackage.CLASS__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends Binding>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				setClassList((ClassList)null);
				return;
			case SystemcPackage.CLASS__TYPE_INTERFACE:
				setTypeInterface(TYPE_INTERFACE_EDEFAULT);
				return;
			case SystemcPackage.CLASS__SCMODULE_INTERFACE:
				setScmoduleInterface(SCMODULE_INTERFACE_EDEFAULT);
				return;
			case SystemcPackage.CLASS__RUNTIME_EXTEND:
				setRuntimeExtend(RUNTIME_EXTEND_EDEFAULT);
				return;
			case SystemcPackage.CLASS__EXTEND:
				getExtend().clear();
				return;
			case SystemcPackage.CLASS__SECTIONS:
				getSections().clear();
				return;
			case SystemcPackage.CLASS__BINDINGS:
				getBindings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemcPackage.CLASS__CLASS_LIST:
				return classList != null;
			case SystemcPackage.CLASS__TYPE_INTERFACE:
				return TYPE_INTERFACE_EDEFAULT == null ? typeInterface != null : !TYPE_INTERFACE_EDEFAULT.equals(typeInterface);
			case SystemcPackage.CLASS__SCMODULE_INTERFACE:
				return SCMODULE_INTERFACE_EDEFAULT == null ? scmoduleInterface != null : !SCMODULE_INTERFACE_EDEFAULT.equals(scmoduleInterface);
			case SystemcPackage.CLASS__RUNTIME_EXTEND:
				return RUNTIME_EXTEND_EDEFAULT == null ? runtimeExtend != null : !RUNTIME_EXTEND_EDEFAULT.equals(runtimeExtend);
			case SystemcPackage.CLASS__EXTEND:
				return extend != null && !extend.isEmpty();
			case SystemcPackage.CLASS__SECTIONS:
				return sections != null && !sections.isEmpty();
			case SystemcPackage.CLASS__BINDINGS:
				return bindings != null && !bindings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typeInterface: ");
		result.append(typeInterface);
		result.append(", scmoduleInterface: ");
		result.append(scmoduleInterface);
		result.append(", runtimeExtend: ");
		result.append(runtimeExtend);
		result.append(')');
		return result.toString();
	}

} //ClassImpl
