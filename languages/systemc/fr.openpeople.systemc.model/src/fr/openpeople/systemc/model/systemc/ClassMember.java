/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getClassSection <em>Class Section</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfName <em>Instance Of Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfClass <em>Instance Of Class</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getTemplateName <em>Template Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit <em>Constructor Connection Init</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassMember#getProcessorBinding <em>Processor Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember()
 * @model
 * @generated
 */
public interface ClassMember extends Name {
	/**
	 * Returns the value of the '<em><b>Class Section</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassSection#getMembers <em>Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Section</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Section</em>' reference.
	 * @see #setClassSection(ClassSection)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_ClassSection()
	 * @see fr.openpeople.systemc.model.systemc.ClassSection#getMembers
	 * @model opposite="members" required="true"
	 * @generated
	 */
	ClassSection getClassSection();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getClassSection <em>Class Section</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Section</em>' reference.
	 * @see #getClassSection()
	 * @generated
	 */
	void setClassSection(ClassSection value);

	/**
	 * Returns the value of the '<em><b>Instance Of Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Of Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Of Name</em>' attribute.
	 * @see #setInstanceOfName(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_InstanceOfName()
	 * @model required="true"
	 * @generated
	 */
	String getInstanceOfName();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfName <em>Instance Of Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Of Name</em>' attribute.
	 * @see #getInstanceOfName()
	 * @generated
	 */
	void setInstanceOfName(String value);

	/**
	 * Returns the value of the '<em><b>Instance Of Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Of Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Of Class</em>' reference.
	 * @see #setInstanceOfClass(fr.openpeople.systemc.model.systemc.Class)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_InstanceOfClass()
	 * @model
	 * @generated
	 */
	fr.openpeople.systemc.model.systemc.Class getInstanceOfClass();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfClass <em>Instance Of Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Of Class</em>' reference.
	 * @see #getInstanceOfClass()
	 * @generated
	 */
	void setInstanceOfClass(fr.openpeople.systemc.model.systemc.Class value);

	/**
	 * Returns the value of the '<em><b>Template Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template Name</em>' attribute.
	 * @see #setTemplateName(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_TemplateName()
	 * @model
	 * @generated
	 */
	String getTemplateName();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getTemplateName <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template Name</em>' attribute.
	 * @see #getTemplateName()
	 * @generated
	 */
	void setTemplateName(String value);

	/**
	 * Returns the value of the '<em><b>Constructor Connection Init</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember <em>Class Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constructor Connection Init</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constructor Connection Init</em>' reference.
	 * @see #setConstructorConnectionInit(ConstructorConnectionInit)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_ConstructorConnectionInit()
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember
	 * @model opposite="classMember"
	 * @generated
	 */
	ConstructorConnectionInit getConstructorConnectionInit();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit <em>Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constructor Connection Init</em>' reference.
	 * @see #getConstructorConnectionInit()
	 * @generated
	 */
	void setConstructorConnectionInit(ConstructorConnectionInit value);

	/**
	 * Returns the value of the '<em><b>Processor Binding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor Binding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor Binding</em>' attribute.
	 * @see #setProcessorBinding(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassMember_ProcessorBinding()
	 * @model
	 * @generated
	 */
	String getProcessorBinding();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassMember#getProcessorBinding <em>Processor Binding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor Binding</em>' attribute.
	 * @see #getProcessorBinding()
	 * @generated
	 */
	void setProcessorBinding(String value);

} // ClassMember
