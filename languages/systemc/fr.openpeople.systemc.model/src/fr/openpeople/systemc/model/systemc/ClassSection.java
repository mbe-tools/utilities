/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassSection#isPublic <em>Public</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassSection#getMembers <em>Members</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassSection#getClass_ <em>Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassSection()
 * @model
 * @generated
 */
public interface ClassSection extends Name {
	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Public</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassSection_Public()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassSection#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Members</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.ClassMember}.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassMember#getClassSection <em>Class Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Members</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Members</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassSection_Members()
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getClassSection
	 * @model opposite="classSection"
	 * @generated
	 */
	EList<ClassMember> getMembers();

	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.Class#getSections <em>Sections</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #setClass(fr.openpeople.systemc.model.systemc.Class)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassSection_Class()
	 * @see fr.openpeople.systemc.model.systemc.Class#getSections
	 * @model opposite="sections" required="true"
	 * @generated
	 */
	fr.openpeople.systemc.model.systemc.Class getClass_();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassSection#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(fr.openpeople.systemc.model.systemc.Class value);

} // ClassSection
