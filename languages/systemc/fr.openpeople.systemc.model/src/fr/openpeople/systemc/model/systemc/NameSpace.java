/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name Space</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.NameSpace#getClassLists <em>Class Lists</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.NameSpace#getSubNameSpaces <em>Sub Name Spaces</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel <em>Top Level</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getNameSpace()
 * @model
 * @generated
 */
public interface NameSpace extends Name {
	/**
	 * Returns the value of the '<em><b>Class Lists</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.ClassList}.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassList#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Lists</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Lists</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getNameSpace_ClassLists()
	 * @see fr.openpeople.systemc.model.systemc.ClassList#getNameSpace
	 * @model opposite="nameSpace"
	 * @generated
	 */
	EList<ClassList> getClassLists();

	/**
	 * Returns the value of the '<em><b>Name Space</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.NameSpace#getSubNameSpaces <em>Sub Name Spaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Space</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Space</em>' reference.
	 * @see #setNameSpace(NameSpace)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getNameSpace_NameSpace()
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getSubNameSpaces
	 * @model opposite="subNameSpaces"
	 * @generated
	 */
	NameSpace getNameSpace();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace <em>Name Space</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Space</em>' reference.
	 * @see #getNameSpace()
	 * @generated
	 */
	void setNameSpace(NameSpace value);

	/**
	 * Returns the value of the '<em><b>Sub Name Spaces</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.NameSpace}.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Name Spaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Name Spaces</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getNameSpace_SubNameSpaces()
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace
	 * @model opposite="nameSpace"
	 * @generated
	 */
	EList<NameSpace> getSubNameSpaces();

	/**
	 * Returns the value of the '<em><b>Top Level</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Top Level</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Top Level</em>' reference.
	 * @see #setTopLevel(TopLevel)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getNameSpace_TopLevel()
	 * @see fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace
	 * @model opposite="nameSpace"
	 * @generated
	 */
	TopLevel getTopLevel();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel <em>Top Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Top Level</em>' reference.
	 * @see #getTopLevel()
	 * @generated
	 */
	void setTopLevel(TopLevel value);

} // NameSpace
