/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getClassList <em>Class List</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getTypeInterface <em>Type Interface</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getScmoduleInterface <em>Scmodule Interface</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getRuntimeExtend <em>Runtime Extend</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getExtend <em>Extend</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getSections <em>Sections</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Class#getBindings <em>Bindings</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends Name {
	/**
	 * Returns the value of the '<em><b>Class List</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassList#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class List</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class List</em>' reference.
	 * @see #setClassList(ClassList)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_ClassList()
	 * @see fr.openpeople.systemc.model.systemc.ClassList#getClasses
	 * @model opposite="classes" required="true"
	 * @generated
	 */
	ClassList getClassList();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Class#getClassList <em>Class List</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class List</em>' reference.
	 * @see #getClassList()
	 * @generated
	 */
	void setClassList(ClassList value);

	/**
	 * Returns the value of the '<em><b>Type Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Interface</em>' attribute.
	 * @see #setTypeInterface(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_TypeInterface()
	 * @model
	 * @generated
	 */
	String getTypeInterface();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Class#getTypeInterface <em>Type Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Interface</em>' attribute.
	 * @see #getTypeInterface()
	 * @generated
	 */
	void setTypeInterface(String value);

	/**
	 * Returns the value of the '<em><b>Scmodule Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scmodule Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scmodule Interface</em>' attribute.
	 * @see #setScmoduleInterface(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_ScmoduleInterface()
	 * @model
	 * @generated
	 */
	String getScmoduleInterface();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Class#getScmoduleInterface <em>Scmodule Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scmodule Interface</em>' attribute.
	 * @see #getScmoduleInterface()
	 * @generated
	 */
	void setScmoduleInterface(String value);

	/**
	 * Returns the value of the '<em><b>Runtime Extend</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Extend</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Extend</em>' attribute.
	 * @see #setRuntimeExtend(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_RuntimeExtend()
	 * @model
	 * @generated
	 */
	String getRuntimeExtend();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Class#getRuntimeExtend <em>Runtime Extend</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runtime Extend</em>' attribute.
	 * @see #getRuntimeExtend()
	 * @generated
	 */
	void setRuntimeExtend(String value);

	/**
	 * Returns the value of the '<em><b>Extend</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extend</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extend</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_Extend()
	 * @model
	 * @generated
	 */
	EList<Class> getExtend();

	/**
	 * Returns the value of the '<em><b>Sections</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.ClassSection}.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.ClassSection#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sections</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sections</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_Sections()
	 * @see fr.openpeople.systemc.model.systemc.ClassSection#getClass_
	 * @model opposite="class"
	 * @generated
	 */
	EList<ClassSection> getSections();

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.Binding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClass_Bindings()
	 * @model
	 * @generated
	 */
	EList<Binding> getBindings();

} // Class
