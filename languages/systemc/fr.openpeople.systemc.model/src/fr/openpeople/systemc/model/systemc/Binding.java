/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Binding#getThread <em>Thread</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.Binding#getProcessor <em>Processor</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getBinding()
 * @model
 * @generated
 */
public interface Binding extends EObject {
	/**
	 * Returns the value of the '<em><b>Thread</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Thread</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Thread</em>' reference.
	 * @see #setThread(fr.openpeople.systemc.model.systemc.Class)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getBinding_Thread()
	 * @model required="true"
	 * @generated
	 */
	fr.openpeople.systemc.model.systemc.Class getThread();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Binding#getThread <em>Thread</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Thread</em>' reference.
	 * @see #getThread()
	 * @generated
	 */
	void setThread(fr.openpeople.systemc.model.systemc.Class value);

	/**
	 * Returns the value of the '<em><b>Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor</em>' reference.
	 * @see #setProcessor(fr.openpeople.systemc.model.systemc.Class)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getBinding_Processor()
	 * @model required="true"
	 * @generated
	 */
	fr.openpeople.systemc.model.systemc.Class getProcessor();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.Binding#getProcessor <em>Processor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor</em>' reference.
	 * @see #getProcessor()
	 * @generated
	 */
	void setProcessor(fr.openpeople.systemc.model.systemc.Class value);

} // Binding
