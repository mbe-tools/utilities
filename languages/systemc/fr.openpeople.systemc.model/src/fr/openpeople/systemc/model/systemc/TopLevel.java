/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Top Level</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.TopLevel#getInclude <em>Include</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.TopLevel#getFileName <em>File Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.TopLevel#getOrderedClasses <em>Ordered Classes</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getTopLevel()
 * @model
 * @generated
 */
public interface TopLevel extends Name {
	/**
	 * Returns the value of the '<em><b>Include</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Include</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include</em>' attribute.
	 * @see #setInclude(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getTopLevel_Include()
	 * @model required="true"
	 * @generated
	 */
	String getInclude();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.TopLevel#getInclude <em>Include</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include</em>' attribute.
	 * @see #getInclude()
	 * @generated
	 */
	void setInclude(String value);

	/**
	 * Returns the value of the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Name</em>' attribute.
	 * @see #setFileName(String)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getTopLevel_FileName()
	 * @model required="true"
	 * @generated
	 */
	String getFileName();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.TopLevel#getFileName <em>File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File Name</em>' attribute.
	 * @see #getFileName()
	 * @generated
	 */
	void setFileName(String value);

	/**
	 * Returns the value of the '<em><b>Name Space</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel <em>Top Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Space</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Space</em>' reference.
	 * @see #setNameSpace(NameSpace)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getTopLevel_NameSpace()
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel
	 * @model opposite="topLevel" required="true"
	 * @generated
	 */
	NameSpace getNameSpace();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace <em>Name Space</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Space</em>' reference.
	 * @see #getNameSpace()
	 * @generated
	 */
	void setNameSpace(NameSpace value);

	/**
	 * Returns the value of the '<em><b>Ordered Classes</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordered Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordered Classes</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getTopLevel_OrderedClasses()
	 * @model
	 * @generated
	 */
	EList<fr.openpeople.systemc.model.systemc.Class> getOrderedClasses();

} // TopLevel
