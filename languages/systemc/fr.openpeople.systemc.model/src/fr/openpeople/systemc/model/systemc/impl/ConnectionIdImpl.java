/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.ClassMember;
import fr.openpeople.systemc.model.systemc.ConnectionId;
import fr.openpeople.systemc.model.systemc.ConstructorConnectionInit;
import fr.openpeople.systemc.model.systemc.SystemcPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection Id</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getComponentName <em>Component Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getComponentClass <em>Component Class</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getComponentClassMember <em>Component Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getPortName <em>Port Name</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getPortClassMember <em>Port Class Member</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getInputConstructorConnectionInit <em>Input Constructor Connection Init</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl#getOutputConstructorConnectionInit <em>Output Constructor Connection Init</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConnectionIdImpl extends NameImpl implements ConnectionId {
	/**
	 * The default value of the '{@link #getComponentName() <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentName()
	 * @generated
	 * @ordered
	 */
	protected static final String COMPONENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComponentName() <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentName()
	 * @generated
	 * @ordered
	 */
	protected String componentName = COMPONENT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComponentClass() <em>Component Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentClass()
	 * @generated
	 * @ordered
	 */
	protected fr.openpeople.systemc.model.systemc.Class componentClass;

	/**
	 * The cached value of the '{@link #getComponentClassMember() <em>Component Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentClassMember()
	 * @generated
	 * @ordered
	 */
	protected ClassMember componentClassMember;

	/**
	 * The default value of the '{@link #getPortName() <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortName()
	 * @generated
	 * @ordered
	 */
	protected static final String PORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPortName() <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortName()
	 * @generated
	 * @ordered
	 */
	protected String portName = PORT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortClassMember() <em>Port Class Member</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortClassMember()
	 * @generated
	 * @ordered
	 */
	protected ClassMember portClassMember;

	/**
	 * The cached value of the '{@link #getInputConstructorConnectionInit() <em>Input Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputConstructorConnectionInit()
	 * @generated
	 * @ordered
	 */
	protected ConstructorConnectionInit inputConstructorConnectionInit;

	/**
	 * The cached value of the '{@link #getOutputConstructorConnectionInit() <em>Output Constructor Connection Init</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputConstructorConnectionInit()
	 * @generated
	 * @ordered
	 */
	protected ConstructorConnectionInit outputConstructorConnectionInit;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionIdImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemcPackage.Literals.CONNECTION_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentName(String newComponentName) {
		String oldComponentName = componentName;
		componentName = newComponentName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__COMPONENT_NAME, oldComponentName, componentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.openpeople.systemc.model.systemc.Class getComponentClass() {
		if (componentClass != null && componentClass.eIsProxy()) {
			InternalEObject oldComponentClass = (InternalEObject)componentClass;
			componentClass = (fr.openpeople.systemc.model.systemc.Class)eResolveProxy(oldComponentClass);
			if (componentClass != oldComponentClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONNECTION_ID__COMPONENT_CLASS, oldComponentClass, componentClass));
			}
		}
		return componentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.openpeople.systemc.model.systemc.Class basicGetComponentClass() {
		return componentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentClass(fr.openpeople.systemc.model.systemc.Class newComponentClass) {
		fr.openpeople.systemc.model.systemc.Class oldComponentClass = componentClass;
		componentClass = newComponentClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__COMPONENT_CLASS, oldComponentClass, componentClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember getComponentClassMember() {
		if (componentClassMember != null && componentClassMember.eIsProxy()) {
			InternalEObject oldComponentClassMember = (InternalEObject)componentClassMember;
			componentClassMember = (ClassMember)eResolveProxy(oldComponentClassMember);
			if (componentClassMember != oldComponentClassMember) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER, oldComponentClassMember, componentClassMember));
			}
		}
		return componentClassMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember basicGetComponentClassMember() {
		return componentClassMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentClassMember(ClassMember newComponentClassMember) {
		ClassMember oldComponentClassMember = componentClassMember;
		componentClassMember = newComponentClassMember;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER, oldComponentClassMember, componentClassMember));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPortName() {
		return portName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortName(String newPortName) {
		String oldPortName = portName;
		portName = newPortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__PORT_NAME, oldPortName, portName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember getPortClassMember() {
		if (portClassMember != null && portClassMember.eIsProxy()) {
			InternalEObject oldPortClassMember = (InternalEObject)portClassMember;
			portClassMember = (ClassMember)eResolveProxy(oldPortClassMember);
			if (portClassMember != oldPortClassMember) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER, oldPortClassMember, portClassMember));
			}
		}
		return portClassMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember basicGetPortClassMember() {
		return portClassMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortClassMember(ClassMember newPortClassMember) {
		ClassMember oldPortClassMember = portClassMember;
		portClassMember = newPortClassMember;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER, oldPortClassMember, portClassMember));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit getInputConstructorConnectionInit() {
		if (inputConstructorConnectionInit != null && inputConstructorConnectionInit.eIsProxy()) {
			InternalEObject oldInputConstructorConnectionInit = (InternalEObject)inputConstructorConnectionInit;
			inputConstructorConnectionInit = (ConstructorConnectionInit)eResolveProxy(oldInputConstructorConnectionInit);
			if (inputConstructorConnectionInit != oldInputConstructorConnectionInit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, oldInputConstructorConnectionInit, inputConstructorConnectionInit));
			}
		}
		return inputConstructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit basicGetInputConstructorConnectionInit() {
		return inputConstructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputConstructorConnectionInit(ConstructorConnectionInit newInputConstructorConnectionInit, NotificationChain msgs) {
		ConstructorConnectionInit oldInputConstructorConnectionInit = inputConstructorConnectionInit;
		inputConstructorConnectionInit = newInputConstructorConnectionInit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, oldInputConstructorConnectionInit, newInputConstructorConnectionInit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputConstructorConnectionInit(ConstructorConnectionInit newInputConstructorConnectionInit) {
		if (newInputConstructorConnectionInit != inputConstructorConnectionInit) {
			NotificationChain msgs = null;
			if (inputConstructorConnectionInit != null)
				msgs = ((InternalEObject)inputConstructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
			if (newInputConstructorConnectionInit != null)
				msgs = ((InternalEObject)newInputConstructorConnectionInit).eInverseAdd(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
			msgs = basicSetInputConstructorConnectionInit(newInputConstructorConnectionInit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT, newInputConstructorConnectionInit, newInputConstructorConnectionInit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit getOutputConstructorConnectionInit() {
		if (outputConstructorConnectionInit != null && outputConstructorConnectionInit.eIsProxy()) {
			InternalEObject oldOutputConstructorConnectionInit = (InternalEObject)outputConstructorConnectionInit;
			outputConstructorConnectionInit = (ConstructorConnectionInit)eResolveProxy(oldOutputConstructorConnectionInit);
			if (outputConstructorConnectionInit != oldOutputConstructorConnectionInit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, oldOutputConstructorConnectionInit, outputConstructorConnectionInit));
			}
		}
		return outputConstructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit basicGetOutputConstructorConnectionInit() {
		return outputConstructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputConstructorConnectionInit(ConstructorConnectionInit newOutputConstructorConnectionInit, NotificationChain msgs) {
		ConstructorConnectionInit oldOutputConstructorConnectionInit = outputConstructorConnectionInit;
		outputConstructorConnectionInit = newOutputConstructorConnectionInit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, oldOutputConstructorConnectionInit, newOutputConstructorConnectionInit);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputConstructorConnectionInit(ConstructorConnectionInit newOutputConstructorConnectionInit) {
		if (newOutputConstructorConnectionInit != outputConstructorConnectionInit) {
			NotificationChain msgs = null;
			if (outputConstructorConnectionInit != null)
				msgs = ((InternalEObject)outputConstructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
			if (newOutputConstructorConnectionInit != null)
				msgs = ((InternalEObject)newOutputConstructorConnectionInit).eInverseAdd(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
			msgs = basicSetOutputConstructorConnectionInit(newOutputConstructorConnectionInit, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT, newOutputConstructorConnectionInit, newOutputConstructorConnectionInit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				if (inputConstructorConnectionInit != null)
					msgs = ((InternalEObject)inputConstructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
				return basicSetInputConstructorConnectionInit((ConstructorConnectionInit)otherEnd, msgs);
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				if (outputConstructorConnectionInit != null)
					msgs = ((InternalEObject)outputConstructorConnectionInit).eInverseRemove(this, SystemcPackage.CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION, ConstructorConnectionInit.class, msgs);
				return basicSetOutputConstructorConnectionInit((ConstructorConnectionInit)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				return basicSetInputConstructorConnectionInit(null, msgs);
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				return basicSetOutputConstructorConnectionInit(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__COMPONENT_NAME:
				return getComponentName();
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS:
				if (resolve) return getComponentClass();
				return basicGetComponentClass();
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER:
				if (resolve) return getComponentClassMember();
				return basicGetComponentClassMember();
			case SystemcPackage.CONNECTION_ID__PORT_NAME:
				return getPortName();
			case SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER:
				if (resolve) return getPortClassMember();
				return basicGetPortClassMember();
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				if (resolve) return getInputConstructorConnectionInit();
				return basicGetInputConstructorConnectionInit();
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				if (resolve) return getOutputConstructorConnectionInit();
				return basicGetOutputConstructorConnectionInit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__COMPONENT_NAME:
				setComponentName((String)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS:
				setComponentClass((fr.openpeople.systemc.model.systemc.Class)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER:
				setComponentClassMember((ClassMember)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__PORT_NAME:
				setPortName((String)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER:
				setPortClassMember((ClassMember)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				setInputConstructorConnectionInit((ConstructorConnectionInit)newValue);
				return;
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				setOutputConstructorConnectionInit((ConstructorConnectionInit)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__COMPONENT_NAME:
				setComponentName(COMPONENT_NAME_EDEFAULT);
				return;
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS:
				setComponentClass((fr.openpeople.systemc.model.systemc.Class)null);
				return;
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER:
				setComponentClassMember((ClassMember)null);
				return;
			case SystemcPackage.CONNECTION_ID__PORT_NAME:
				setPortName(PORT_NAME_EDEFAULT);
				return;
			case SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER:
				setPortClassMember((ClassMember)null);
				return;
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				setInputConstructorConnectionInit((ConstructorConnectionInit)null);
				return;
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				setOutputConstructorConnectionInit((ConstructorConnectionInit)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemcPackage.CONNECTION_ID__COMPONENT_NAME:
				return COMPONENT_NAME_EDEFAULT == null ? componentName != null : !COMPONENT_NAME_EDEFAULT.equals(componentName);
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS:
				return componentClass != null;
			case SystemcPackage.CONNECTION_ID__COMPONENT_CLASS_MEMBER:
				return componentClassMember != null;
			case SystemcPackage.CONNECTION_ID__PORT_NAME:
				return PORT_NAME_EDEFAULT == null ? portName != null : !PORT_NAME_EDEFAULT.equals(portName);
			case SystemcPackage.CONNECTION_ID__PORT_CLASS_MEMBER:
				return portClassMember != null;
			case SystemcPackage.CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT:
				return inputConstructorConnectionInit != null;
			case SystemcPackage.CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT:
				return outputConstructorConnectionInit != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (componentName: ");
		result.append(componentName);
		result.append(", portName: ");
		result.append(portName);
		result.append(')');
		return result.toString();
	}

} //ConnectionIdImpl
