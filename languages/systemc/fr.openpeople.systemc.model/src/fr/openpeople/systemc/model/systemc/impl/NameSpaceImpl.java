/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.ClassList;
import fr.openpeople.systemc.model.systemc.NameSpace;
import fr.openpeople.systemc.model.systemc.SystemcPackage;
import fr.openpeople.systemc.model.systemc.TopLevel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Name Space</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl#getClassLists <em>Class Lists</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl#getNameSpace <em>Name Space</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl#getSubNameSpaces <em>Sub Name Spaces</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl#getTopLevel <em>Top Level</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NameSpaceImpl extends NameImpl implements NameSpace {
	/**
	 * The cached value of the '{@link #getClassLists() <em>Class Lists</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassLists()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassList> classLists;

	/**
	 * The cached value of the '{@link #getNameSpace() <em>Name Space</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNameSpace()
	 * @generated
	 * @ordered
	 */
	protected NameSpace nameSpace;

	/**
	 * The cached value of the '{@link #getSubNameSpaces() <em>Sub Name Spaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubNameSpaces()
	 * @generated
	 * @ordered
	 */
	protected EList<NameSpace> subNameSpaces;

	/**
	 * The cached value of the '{@link #getTopLevel() <em>Top Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopLevel()
	 * @generated
	 * @ordered
	 */
	protected TopLevel topLevel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NameSpaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemcPackage.Literals.NAME_SPACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassList> getClassLists() {
		if (classLists == null) {
			classLists = new EObjectWithInverseResolvingEList<ClassList>(ClassList.class, this, SystemcPackage.NAME_SPACE__CLASS_LISTS, SystemcPackage.CLASS_LIST__NAME_SPACE);
		}
		return classLists;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace getNameSpace() {
		if (nameSpace != null && nameSpace.eIsProxy()) {
			InternalEObject oldNameSpace = (InternalEObject)nameSpace;
			nameSpace = (NameSpace)eResolveProxy(oldNameSpace);
			if (nameSpace != oldNameSpace) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.NAME_SPACE__NAME_SPACE, oldNameSpace, nameSpace));
			}
		}
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace basicGetNameSpace() {
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNameSpace(NameSpace newNameSpace, NotificationChain msgs) {
		NameSpace oldNameSpace = nameSpace;
		nameSpace = newNameSpace;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.NAME_SPACE__NAME_SPACE, oldNameSpace, newNameSpace);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameSpace(NameSpace newNameSpace) {
		if (newNameSpace != nameSpace) {
			NotificationChain msgs = null;
			if (nameSpace != null)
				msgs = ((InternalEObject)nameSpace).eInverseRemove(this, SystemcPackage.NAME_SPACE__SUB_NAME_SPACES, NameSpace.class, msgs);
			if (newNameSpace != null)
				msgs = ((InternalEObject)newNameSpace).eInverseAdd(this, SystemcPackage.NAME_SPACE__SUB_NAME_SPACES, NameSpace.class, msgs);
			msgs = basicSetNameSpace(newNameSpace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.NAME_SPACE__NAME_SPACE, newNameSpace, newNameSpace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NameSpace> getSubNameSpaces() {
		if (subNameSpaces == null) {
			subNameSpaces = new EObjectWithInverseResolvingEList<NameSpace>(NameSpace.class, this, SystemcPackage.NAME_SPACE__SUB_NAME_SPACES, SystemcPackage.NAME_SPACE__NAME_SPACE);
		}
		return subNameSpaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TopLevel getTopLevel() {
		if (topLevel != null && topLevel.eIsProxy()) {
			InternalEObject oldTopLevel = (InternalEObject)topLevel;
			topLevel = (TopLevel)eResolveProxy(oldTopLevel);
			if (topLevel != oldTopLevel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemcPackage.NAME_SPACE__TOP_LEVEL, oldTopLevel, topLevel));
			}
		}
		return topLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TopLevel basicGetTopLevel() {
		return topLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTopLevel(TopLevel newTopLevel, NotificationChain msgs) {
		TopLevel oldTopLevel = topLevel;
		topLevel = newTopLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SystemcPackage.NAME_SPACE__TOP_LEVEL, oldTopLevel, newTopLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopLevel(TopLevel newTopLevel) {
		if (newTopLevel != topLevel) {
			NotificationChain msgs = null;
			if (topLevel != null)
				msgs = ((InternalEObject)topLevel).eInverseRemove(this, SystemcPackage.TOP_LEVEL__NAME_SPACE, TopLevel.class, msgs);
			if (newTopLevel != null)
				msgs = ((InternalEObject)newTopLevel).eInverseAdd(this, SystemcPackage.TOP_LEVEL__NAME_SPACE, TopLevel.class, msgs);
			msgs = basicSetTopLevel(newTopLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemcPackage.NAME_SPACE__TOP_LEVEL, newTopLevel, newTopLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassLists()).basicAdd(otherEnd, msgs);
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				if (nameSpace != null)
					msgs = ((InternalEObject)nameSpace).eInverseRemove(this, SystemcPackage.NAME_SPACE__SUB_NAME_SPACES, NameSpace.class, msgs);
				return basicSetNameSpace((NameSpace)otherEnd, msgs);
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubNameSpaces()).basicAdd(otherEnd, msgs);
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				if (topLevel != null)
					msgs = ((InternalEObject)topLevel).eInverseRemove(this, SystemcPackage.TOP_LEVEL__NAME_SPACE, TopLevel.class, msgs);
				return basicSetTopLevel((TopLevel)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				return ((InternalEList<?>)getClassLists()).basicRemove(otherEnd, msgs);
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				return basicSetNameSpace(null, msgs);
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				return ((InternalEList<?>)getSubNameSpaces()).basicRemove(otherEnd, msgs);
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				return basicSetTopLevel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				return getClassLists();
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				if (resolve) return getNameSpace();
				return basicGetNameSpace();
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				return getSubNameSpaces();
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				if (resolve) return getTopLevel();
				return basicGetTopLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				getClassLists().clear();
				getClassLists().addAll((Collection<? extends ClassList>)newValue);
				return;
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				setNameSpace((NameSpace)newValue);
				return;
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				getSubNameSpaces().clear();
				getSubNameSpaces().addAll((Collection<? extends NameSpace>)newValue);
				return;
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				setTopLevel((TopLevel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				getClassLists().clear();
				return;
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				setNameSpace((NameSpace)null);
				return;
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				getSubNameSpaces().clear();
				return;
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				setTopLevel((TopLevel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemcPackage.NAME_SPACE__CLASS_LISTS:
				return classLists != null && !classLists.isEmpty();
			case SystemcPackage.NAME_SPACE__NAME_SPACE:
				return nameSpace != null;
			case SystemcPackage.NAME_SPACE__SUB_NAME_SPACES:
				return subNameSpaces != null && !subNameSpaces.isEmpty();
			case SystemcPackage.NAME_SPACE__TOP_LEVEL:
				return topLevel != null;
		}
		return super.eIsSet(featureID);
	}

} //NameSpaceImpl
