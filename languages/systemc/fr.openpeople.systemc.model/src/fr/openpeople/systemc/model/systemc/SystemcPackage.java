/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.openpeople.systemc.model.systemc.SystemcFactory
 * @model kind="package"
 * @generated
 */
public interface SystemcPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "systemc";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.open-people.fr/systemc";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "systemc";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SystemcPackage eINSTANCE = fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.NameImpl <em>Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.NameImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getName_()
	 * @generated
	 */
	int NAME = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__NAME = 0;

	/**
	 * The number of structural features of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl <em>Connection Id</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getConnectionId()
	 * @generated
	 */
	int CONNECTION_ID = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__COMPONENT_NAME = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__COMPONENT_CLASS = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Component Class Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__COMPONENT_CLASS_MEMBER = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__PORT_NAME = NAME_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Port Class Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__PORT_CLASS_MEMBER = NAME_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Input Constructor Connection Init</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT = NAME_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Output Constructor Connection Init</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT = NAME_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Connection Id</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_ID_FEATURE_COUNT = NAME_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl <em>Constructor Connection Init</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getConstructorConnectionInit()
	 * @generated
	 */
	int CONSTRUCTOR_CONNECTION_INIT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTOR_CONNECTION_INIT__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Class Member</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Output Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION = NAME_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Constructor Connection Init</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTOR_CONNECTION_INIT_FEATURE_COUNT = NAME_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl <em>Class Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassMember()
	 * @generated
	 */
	int CLASS_MEMBER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Class Section</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__CLASS_SECTION = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Instance Of Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__INSTANCE_OF_NAME = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Instance Of Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__INSTANCE_OF_CLASS = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Template Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__TEMPLATE_NAME = NAME_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Constructor Connection Init</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT = NAME_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Processor Binding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER__PROCESSOR_BINDING = NAME_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Class Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_MEMBER_FEATURE_COUNT = NAME_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassSectionImpl <em>Class Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ClassSectionImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassSection()
	 * @generated
	 */
	int CLASS_SECTION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SECTION__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SECTION__PUBLIC = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Members</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SECTION__MEMBERS = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SECTION__CLASS = NAME_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Class Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SECTION_FEATURE_COUNT = NAME_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl <em>Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ClassImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClass_()
	 * @generated
	 */
	int CLASS = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Class List</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__CLASS_LIST = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__TYPE_INTERFACE = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scmodule Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__SCMODULE_INTERFACE = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Runtime Extend</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__RUNTIME_EXTEND = NAME_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Extend</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__EXTEND = NAME_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Sections</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__SECTIONS = NAME_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS__BINDINGS = NAME_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FEATURE_COUNT = NAME_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassListImpl <em>Class List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.ClassListImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassList()
	 * @generated
	 */
	int CLASS_LIST = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_LIST__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_LIST__CLASSES = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_LIST__NAME_SPACE = NAME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_LIST_FEATURE_COUNT = NAME_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl <em>Name Space</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getNameSpace()
	 * @generated
	 */
	int NAME_SPACE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Class Lists</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__CLASS_LISTS = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__NAME_SPACE = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub Name Spaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__SUB_NAME_SPACES = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Top Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE__TOP_LEVEL = NAME_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Name Space</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_SPACE_FEATURE_COUNT = NAME_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.TopLevelImpl <em>Top Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.TopLevelImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getTopLevel()
	 * @generated
	 */
	int TOP_LEVEL = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL__NAME = NAME__NAME;

	/**
	 * The feature id for the '<em><b>Include</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL__INCLUDE = NAME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL__FILE_NAME = NAME_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name Space</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL__NAME_SPACE = NAME_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ordered Classes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL__ORDERED_CLASSES = NAME_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Top Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_FEATURE_COUNT = NAME_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.openpeople.systemc.model.systemc.impl.BindingImpl <em>Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.openpeople.systemc.model.systemc.impl.BindingImpl
	 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getBinding()
	 * @generated
	 */
	int BINDING = 9;

	/**
	 * The feature id for the '<em><b>Thread</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__THREAD = 0;

	/**
	 * The feature id for the '<em><b>Processor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING__PROCESSOR = 1;

	/**
	 * The number of structural features of the '<em>Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINDING_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Name
	 * @generated
	 */
	EClass getName_();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.Name#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Name#getName()
	 * @see #getName_()
	 * @generated
	 */
	EAttribute getName_Name();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.ConnectionId <em>Connection Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Id</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId
	 * @generated
	 */
	EClass getConnectionId();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentName <em>Component Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Component Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getComponentName()
	 * @see #getConnectionId()
	 * @generated
	 */
	EAttribute getConnectionId_ComponentName();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClass <em>Component Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Class</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClass()
	 * @see #getConnectionId()
	 * @generated
	 */
	EReference getConnectionId_ComponentClass();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClassMember <em>Component Class Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Class Member</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getComponentClassMember()
	 * @see #getConnectionId()
	 * @generated
	 */
	EReference getConnectionId_ComponentClassMember();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortName <em>Port Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getPortName()
	 * @see #getConnectionId()
	 * @generated
	 */
	EAttribute getConnectionId_PortName();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getPortClassMember <em>Port Class Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Class Member</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getPortClassMember()
	 * @see #getConnectionId()
	 * @generated
	 */
	EReference getConnectionId_PortClassMember();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit <em>Input Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Constructor Connection Init</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getInputConstructorConnectionInit()
	 * @see #getConnectionId()
	 * @generated
	 */
	EReference getConnectionId_InputConstructorConnectionInit();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit <em>Output Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output Constructor Connection Init</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConnectionId#getOutputConstructorConnectionInit()
	 * @see #getConnectionId()
	 * @generated
	 */
	EReference getConnectionId_OutputConstructorConnectionInit();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit <em>Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constructor Connection Init</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit
	 * @generated
	 */
	EClass getConstructorConnectionInit();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember <em>Class Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class Member</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getClassMember()
	 * @see #getConstructorConnectionInit()
	 * @generated
	 */
	EReference getConstructorConnectionInit_ClassMember();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection <em>Input Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Connection</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getInputConnection()
	 * @see #getConstructorConnectionInit()
	 * @generated
	 */
	EReference getConstructorConnectionInit_InputConnection();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection <em>Output Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output Connection</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ConstructorConnectionInit#getOutputConnection()
	 * @see #getConstructorConnectionInit()
	 * @generated
	 */
	EReference getConstructorConnectionInit_OutputConnection();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.ClassMember <em>Class Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Member</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember
	 * @generated
	 */
	EClass getClassMember();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ClassMember#getClassSection <em>Class Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class Section</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getClassSection()
	 * @see #getClassMember()
	 * @generated
	 */
	EReference getClassMember_ClassSection();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfName <em>Instance Of Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance Of Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfName()
	 * @see #getClassMember()
	 * @generated
	 */
	EAttribute getClassMember_InstanceOfName();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfClass <em>Instance Of Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instance Of Class</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getInstanceOfClass()
	 * @see #getClassMember()
	 * @generated
	 */
	EReference getClassMember_InstanceOfClass();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ClassMember#getTemplateName <em>Template Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Template Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getTemplateName()
	 * @see #getClassMember()
	 * @generated
	 */
	EAttribute getClassMember_TemplateName();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit <em>Constructor Connection Init</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constructor Connection Init</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getConstructorConnectionInit()
	 * @see #getClassMember()
	 * @generated
	 */
	EReference getClassMember_ConstructorConnectionInit();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ClassMember#getProcessorBinding <em>Processor Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Processor Binding</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassMember#getProcessorBinding()
	 * @see #getClassMember()
	 * @generated
	 */
	EAttribute getClassMember_ProcessorBinding();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.ClassSection <em>Class Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Section</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassSection
	 * @generated
	 */
	EClass getClassSection();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.ClassSection#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassSection#isPublic()
	 * @see #getClassSection()
	 * @generated
	 */
	EAttribute getClassSection_Public();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.ClassSection#getMembers <em>Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Members</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassSection#getMembers()
	 * @see #getClassSection()
	 * @generated
	 */
	EReference getClassSection_Members();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ClassSection#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassSection#getClass_()
	 * @see #getClassSection()
	 * @generated
	 */
	EReference getClassSection_Class();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.Class <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class
	 * @generated
	 */
	EClass getClass_();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.Class#getClassList <em>Class List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class List</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getClassList()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_ClassList();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.Class#getTypeInterface <em>Type Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Interface</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getTypeInterface()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_TypeInterface();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.Class#getScmoduleInterface <em>Scmodule Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scmodule Interface</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getScmoduleInterface()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_ScmoduleInterface();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.Class#getRuntimeExtend <em>Runtime Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Runtime Extend</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getRuntimeExtend()
	 * @see #getClass_()
	 * @generated
	 */
	EAttribute getClass_RuntimeExtend();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.Class#getExtend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extend</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getExtend()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Extend();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.Class#getSections <em>Sections</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sections</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getSections()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Sections();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.Class#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bindings</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Class#getBindings()
	 * @see #getClass_()
	 * @generated
	 */
	EReference getClass_Bindings();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.ClassList <em>Class List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class List</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassList
	 * @generated
	 */
	EClass getClassList();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.ClassList#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Classes</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassList#getClasses()
	 * @see #getClassList()
	 * @generated
	 */
	EReference getClassList_Classes();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.ClassList#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name Space</em>'.
	 * @see fr.openpeople.systemc.model.systemc.ClassList#getNameSpace()
	 * @see #getClassList()
	 * @generated
	 */
	EReference getClassList_NameSpace();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.NameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Space</em>'.
	 * @see fr.openpeople.systemc.model.systemc.NameSpace
	 * @generated
	 */
	EClass getNameSpace();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.NameSpace#getClassLists <em>Class Lists</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Class Lists</em>'.
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getClassLists()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_ClassLists();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name Space</em>'.
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getNameSpace()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_NameSpace();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.NameSpace#getSubNameSpaces <em>Sub Name Spaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Name Spaces</em>'.
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getSubNameSpaces()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_SubNameSpaces();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel <em>Top Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Top Level</em>'.
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getTopLevel()
	 * @see #getNameSpace()
	 * @generated
	 */
	EReference getNameSpace_TopLevel();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.TopLevel <em>Top Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Top Level</em>'.
	 * @see fr.openpeople.systemc.model.systemc.TopLevel
	 * @generated
	 */
	EClass getTopLevel();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.TopLevel#getInclude <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include</em>'.
	 * @see fr.openpeople.systemc.model.systemc.TopLevel#getInclude()
	 * @see #getTopLevel()
	 * @generated
	 */
	EAttribute getTopLevel_Include();

	/**
	 * Returns the meta object for the attribute '{@link fr.openpeople.systemc.model.systemc.TopLevel#getFileName <em>File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Name</em>'.
	 * @see fr.openpeople.systemc.model.systemc.TopLevel#getFileName()
	 * @see #getTopLevel()
	 * @generated
	 */
	EAttribute getTopLevel_FileName();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace <em>Name Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name Space</em>'.
	 * @see fr.openpeople.systemc.model.systemc.TopLevel#getNameSpace()
	 * @see #getTopLevel()
	 * @generated
	 */
	EReference getTopLevel_NameSpace();

	/**
	 * Returns the meta object for the reference list '{@link fr.openpeople.systemc.model.systemc.TopLevel#getOrderedClasses <em>Ordered Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ordered Classes</em>'.
	 * @see fr.openpeople.systemc.model.systemc.TopLevel#getOrderedClasses()
	 * @see #getTopLevel()
	 * @generated
	 */
	EReference getTopLevel_OrderedClasses();

	/**
	 * Returns the meta object for class '{@link fr.openpeople.systemc.model.systemc.Binding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binding</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Binding
	 * @generated
	 */
	EClass getBinding();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.Binding#getThread <em>Thread</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Thread</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Binding#getThread()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_Thread();

	/**
	 * Returns the meta object for the reference '{@link fr.openpeople.systemc.model.systemc.Binding#getProcessor <em>Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Processor</em>'.
	 * @see fr.openpeople.systemc.model.systemc.Binding#getProcessor()
	 * @see #getBinding()
	 * @generated
	 */
	EReference getBinding_Processor();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SystemcFactory getSystemcFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.NameImpl <em>Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.NameImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getName_()
		 * @generated
		 */
		EClass NAME = eINSTANCE.getName_();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME__NAME = eINSTANCE.getName_Name();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl <em>Connection Id</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ConnectionIdImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getConnectionId()
		 * @generated
		 */
		EClass CONNECTION_ID = eINSTANCE.getConnectionId();

		/**
		 * The meta object literal for the '<em><b>Component Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTION_ID__COMPONENT_NAME = eINSTANCE.getConnectionId_ComponentName();

		/**
		 * The meta object literal for the '<em><b>Component Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ID__COMPONENT_CLASS = eINSTANCE.getConnectionId_ComponentClass();

		/**
		 * The meta object literal for the '<em><b>Component Class Member</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ID__COMPONENT_CLASS_MEMBER = eINSTANCE.getConnectionId_ComponentClassMember();

		/**
		 * The meta object literal for the '<em><b>Port Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTION_ID__PORT_NAME = eINSTANCE.getConnectionId_PortName();

		/**
		 * The meta object literal for the '<em><b>Port Class Member</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ID__PORT_CLASS_MEMBER = eINSTANCE.getConnectionId_PortClassMember();

		/**
		 * The meta object literal for the '<em><b>Input Constructor Connection Init</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ID__INPUT_CONSTRUCTOR_CONNECTION_INIT = eINSTANCE.getConnectionId_InputConstructorConnectionInit();

		/**
		 * The meta object literal for the '<em><b>Output Constructor Connection Init</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_ID__OUTPUT_CONSTRUCTOR_CONNECTION_INIT = eINSTANCE.getConnectionId_OutputConstructorConnectionInit();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl <em>Constructor Connection Init</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ConstructorConnectionInitImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getConstructorConnectionInit()
		 * @generated
		 */
		EClass CONSTRUCTOR_CONNECTION_INIT = eINSTANCE.getConstructorConnectionInit();

		/**
		 * The meta object literal for the '<em><b>Class Member</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRUCTOR_CONNECTION_INIT__CLASS_MEMBER = eINSTANCE.getConstructorConnectionInit_ClassMember();

		/**
		 * The meta object literal for the '<em><b>Input Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRUCTOR_CONNECTION_INIT__INPUT_CONNECTION = eINSTANCE.getConstructorConnectionInit_InputConnection();

		/**
		 * The meta object literal for the '<em><b>Output Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRUCTOR_CONNECTION_INIT__OUTPUT_CONNECTION = eINSTANCE.getConstructorConnectionInit_OutputConnection();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl <em>Class Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ClassMemberImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassMember()
		 * @generated
		 */
		EClass CLASS_MEMBER = eINSTANCE.getClassMember();

		/**
		 * The meta object literal for the '<em><b>Class Section</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__CLASS_SECTION = eINSTANCE.getClassMember_ClassSection();

		/**
		 * The meta object literal for the '<em><b>Instance Of Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_MEMBER__INSTANCE_OF_NAME = eINSTANCE.getClassMember_InstanceOfName();

		/**
		 * The meta object literal for the '<em><b>Instance Of Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__INSTANCE_OF_CLASS = eINSTANCE.getClassMember_InstanceOfClass();

		/**
		 * The meta object literal for the '<em><b>Template Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_MEMBER__TEMPLATE_NAME = eINSTANCE.getClassMember_TemplateName();

		/**
		 * The meta object literal for the '<em><b>Constructor Connection Init</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_MEMBER__CONSTRUCTOR_CONNECTION_INIT = eINSTANCE.getClassMember_ConstructorConnectionInit();

		/**
		 * The meta object literal for the '<em><b>Processor Binding</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_MEMBER__PROCESSOR_BINDING = eINSTANCE.getClassMember_ProcessorBinding();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassSectionImpl <em>Class Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ClassSectionImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassSection()
		 * @generated
		 */
		EClass CLASS_SECTION = eINSTANCE.getClassSection();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_SECTION__PUBLIC = eINSTANCE.getClassSection_Public();

		/**
		 * The meta object literal for the '<em><b>Members</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_SECTION__MEMBERS = eINSTANCE.getClassSection_Members();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_SECTION__CLASS = eINSTANCE.getClassSection_Class();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassImpl <em>Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ClassImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClass_()
		 * @generated
		 */
		EClass CLASS = eINSTANCE.getClass_();

		/**
		 * The meta object literal for the '<em><b>Class List</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__CLASS_LIST = eINSTANCE.getClass_ClassList();

		/**
		 * The meta object literal for the '<em><b>Type Interface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__TYPE_INTERFACE = eINSTANCE.getClass_TypeInterface();

		/**
		 * The meta object literal for the '<em><b>Scmodule Interface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__SCMODULE_INTERFACE = eINSTANCE.getClass_ScmoduleInterface();

		/**
		 * The meta object literal for the '<em><b>Runtime Extend</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS__RUNTIME_EXTEND = eINSTANCE.getClass_RuntimeExtend();

		/**
		 * The meta object literal for the '<em><b>Extend</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__EXTEND = eINSTANCE.getClass_Extend();

		/**
		 * The meta object literal for the '<em><b>Sections</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__SECTIONS = eINSTANCE.getClass_Sections();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS__BINDINGS = eINSTANCE.getClass_Bindings();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.ClassListImpl <em>Class List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.ClassListImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getClassList()
		 * @generated
		 */
		EClass CLASS_LIST = eINSTANCE.getClassList();

		/**
		 * The meta object literal for the '<em><b>Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_LIST__CLASSES = eINSTANCE.getClassList_Classes();

		/**
		 * The meta object literal for the '<em><b>Name Space</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_LIST__NAME_SPACE = eINSTANCE.getClassList_NameSpace();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl <em>Name Space</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.NameSpaceImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getNameSpace()
		 * @generated
		 */
		EClass NAME_SPACE = eINSTANCE.getNameSpace();

		/**
		 * The meta object literal for the '<em><b>Class Lists</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__CLASS_LISTS = eINSTANCE.getNameSpace_ClassLists();

		/**
		 * The meta object literal for the '<em><b>Name Space</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__NAME_SPACE = eINSTANCE.getNameSpace_NameSpace();

		/**
		 * The meta object literal for the '<em><b>Sub Name Spaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__SUB_NAME_SPACES = eINSTANCE.getNameSpace_SubNameSpaces();

		/**
		 * The meta object literal for the '<em><b>Top Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAME_SPACE__TOP_LEVEL = eINSTANCE.getNameSpace_TopLevel();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.TopLevelImpl <em>Top Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.TopLevelImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getTopLevel()
		 * @generated
		 */
		EClass TOP_LEVEL = eINSTANCE.getTopLevel();

		/**
		 * The meta object literal for the '<em><b>Include</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOP_LEVEL__INCLUDE = eINSTANCE.getTopLevel_Include();

		/**
		 * The meta object literal for the '<em><b>File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOP_LEVEL__FILE_NAME = eINSTANCE.getTopLevel_FileName();

		/**
		 * The meta object literal for the '<em><b>Name Space</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOP_LEVEL__NAME_SPACE = eINSTANCE.getTopLevel_NameSpace();

		/**
		 * The meta object literal for the '<em><b>Ordered Classes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOP_LEVEL__ORDERED_CLASSES = eINSTANCE.getTopLevel_OrderedClasses();

		/**
		 * The meta object literal for the '{@link fr.openpeople.systemc.model.systemc.impl.BindingImpl <em>Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.openpeople.systemc.model.systemc.impl.BindingImpl
		 * @see fr.openpeople.systemc.model.systemc.impl.SystemcPackageImpl#getBinding()
		 * @generated
		 */
		EClass BINDING = eINSTANCE.getBinding();

		/**
		 * The meta object literal for the '<em><b>Thread</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__THREAD = eINSTANCE.getBinding_Thread();

		/**
		 * The meta object literal for the '<em><b>Processor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINDING__PROCESSOR = eINSTANCE.getBinding_Processor();

	}

} //SystemcPackage
