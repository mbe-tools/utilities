/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassList#getClasses <em>Classes</em>}</li>
 *   <li>{@link fr.openpeople.systemc.model.systemc.ClassList#getNameSpace <em>Name Space</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassList()
 * @model
 * @generated
 */
public interface ClassList extends Name {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' reference list.
	 * The list contents are of type {@link fr.openpeople.systemc.model.systemc.Class}.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.Class#getClassList <em>Class List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' reference list.
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassList_Classes()
	 * @see fr.openpeople.systemc.model.systemc.Class#getClassList
	 * @model opposite="classList"
	 * @generated
	 */
	EList<fr.openpeople.systemc.model.systemc.Class> getClasses();

	/**
	 * Returns the value of the '<em><b>Name Space</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.openpeople.systemc.model.systemc.NameSpace#getClassLists <em>Class Lists</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Space</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Space</em>' reference.
	 * @see #setNameSpace(NameSpace)
	 * @see fr.openpeople.systemc.model.systemc.SystemcPackage#getClassList_NameSpace()
	 * @see fr.openpeople.systemc.model.systemc.NameSpace#getClassLists
	 * @model opposite="classLists" required="true"
	 * @generated
	 */
	NameSpace getNameSpace();

	/**
	 * Sets the value of the '{@link fr.openpeople.systemc.model.systemc.ClassList#getNameSpace <em>Name Space</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Space</em>' reference.
	 * @see #getNameSpace()
	 * @generated
	 */
	void setNameSpace(NameSpace value);

} // ClassList
