/*******************************************************************************
 * Copyright (c) 2012 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Pierre BOMEL  (Lab-STICC UBS), pierre.bomel@univ-ubs.fr
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.openpeople.systemc.model.systemc.impl;

import fr.openpeople.systemc.model.systemc.Binding;
import fr.openpeople.systemc.model.systemc.ClassList;
import fr.openpeople.systemc.model.systemc.ClassMember;
import fr.openpeople.systemc.model.systemc.ClassSection;
import fr.openpeople.systemc.model.systemc.ConnectionId;
import fr.openpeople.systemc.model.systemc.ConstructorConnectionInit;
import fr.openpeople.systemc.model.systemc.NameSpace;
import fr.openpeople.systemc.model.systemc.SystemcFactory;
import fr.openpeople.systemc.model.systemc.SystemcPackage;
import fr.openpeople.systemc.model.systemc.TopLevel;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemcFactoryImpl extends EFactoryImpl implements SystemcFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SystemcFactory init() {
		try {
			SystemcFactory theSystemcFactory = (SystemcFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.open-people.fr/systemc"); 
			if (theSystemcFactory != null) {
				return theSystemcFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SystemcFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemcFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SystemcPackage.CONNECTION_ID: return createConnectionId();
			case SystemcPackage.CONSTRUCTOR_CONNECTION_INIT: return createConstructorConnectionInit();
			case SystemcPackage.CLASS_MEMBER: return createClassMember();
			case SystemcPackage.CLASS_SECTION: return createClassSection();
			case SystemcPackage.CLASS: return createClass();
			case SystemcPackage.CLASS_LIST: return createClassList();
			case SystemcPackage.NAME_SPACE: return createNameSpace();
			case SystemcPackage.TOP_LEVEL: return createTopLevel();
			case SystemcPackage.BINDING: return createBinding();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionId createConnectionId() {
		ConnectionIdImpl connectionId = new ConnectionIdImpl();
		return connectionId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructorConnectionInit createConstructorConnectionInit() {
		ConstructorConnectionInitImpl constructorConnectionInit = new ConstructorConnectionInitImpl();
		return constructorConnectionInit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassMember createClassMember() {
		ClassMemberImpl classMember = new ClassMemberImpl();
		return classMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSection createClassSection() {
		ClassSectionImpl classSection = new ClassSectionImpl();
		return classSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.openpeople.systemc.model.systemc.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassList createClassList() {
		ClassListImpl classList = new ClassListImpl();
		return classList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameSpace createNameSpace() {
		NameSpaceImpl nameSpace = new NameSpaceImpl();
		return nameSpace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TopLevel createTopLevel() {
		TopLevelImpl topLevel = new TopLevelImpl();
		return topLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Binding createBinding() {
		BindingImpl binding = new BindingImpl();
		return binding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemcPackage getSystemcPackage() {
		return (SystemcPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SystemcPackage getPackage() {
		return SystemcPackage.eINSTANCE;
	}

} //SystemcFactoryImpl
