package fr.tpt.mem4csd.prism.model.prism.util;
/*******************************************************************************
 * Copyright (c) 2017 Roberto Medina
 * Written by Roberto Medina (rmedina@telecom-paristech.fr)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PrismUnparser {

	private final String inputFile;
	private final String outPrismFile;
	
	// Only references do not have to be instantiated
	//private Automata auto;
	
	private int hPeriod;
	private int nbLevels;
	
	public PrismUnparser(	final String inputFileName,
						final String outPrismFileName ) {
		this.inputFile = inputFileName;
		this.outPrismFile = outPrismFileName;

	}

	
	/**
	 * Writes the properties to check by PRISM
	 * @throws IOException
	 */
	public void writePCTL () throws IOException {
		BufferedWriter out = null;
		
		try {
			String fileName = getOutPrismFile();
			int pos = fileName.lastIndexOf(".");
			if (pos > 0) {
			    fileName = fileName.substring(0, pos);
			}
			File f = new File(fileName+".pctl");
			f.createNewFile();
			FileWriter fstream = new FileWriter(f);
			out = new BufferedWriter(fstream);
			
			// Write total cycles
			out.write("// Total Cycles\n");
			out.write("R{\"total_cycles\"}=? [ C <= D ]\n\n");
			
			// Write properties for all LO outputs
//			for ( final DagSpecification dagSpec : mcDagsSpec.getOwnedDagSpecs() ) {
//				if ( dagSpec instanceof ModalDagSpec ) {
//					final ModalDagSpec modDag = (ModalDagSpec) dagSpec;
//					
//					for ( final DagNode aout : modDag.sinkTasks( modDag.mode( CriticalityLevel.LOW ) ) ) {
//						out.write("(R{\""+aout.getName()+"_cycles\"}=? [ C <= D ])/(R{\"total_cycles\"}=? [ C <= D ])\n\n");
//					}
//				}
//			}
			
			out.write("E[F \"deadlock\"]\n");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if ( out != null ) {
				out.close();
			}
		}		
	}
	
	/**
	 * Writes a model for the PRISM model checker
	 */
//	public void writePRISM( final List<FaultToleranceMechanism> faultTolMechanisms,
//							final PrismSpec prismSpec )
//	throws IOException {
//		BufferedWriter out = null;
//		
//		try {
//			File f = new File(getOutPrismFile());
//			f.createNewFile();
//			FileWriter fstream = new FileWriter(f);
//			out = new BufferedWriter(fstream);
//			
//			out.write("dtmc\n\n");
//			out.write("const int D;\n\n");
//			
//			// Write FTMs
//			int countFtm = 0;
//
//			for ( final FaultToleranceMechanism faultTolMech : faultTolMechanisms ) {
//				// TODO: Check
//				if ( faultTolMech instanceof VoterFaultToleranceMechanism ) {
//				//if (ftm.getType() == ActorAvail.VOTER) {
//					
//					// TODO 
//					out.write("module "+ prismSpec.getName() +"\n");
//					out.write("\tv"+countFtm+": [0..20] init 0;\n");
//					//Iterator<Transition> it = prismSpec.getOwnedTransitions().iterator();
//					//int i = 0;
//					
//					for ( final Transition transition : prismSpec.getOwnedTransitions() ) {
//						//Transition t = it.next();
//						
//						if ( transition.isToSelf() ) {
//							out.write( "\t[" + transition.getName() + "] v" + countFtm + " = " + transition.getSourceState().getId() + " -> (v" + countFtm + "' = " + transition.getDestinationState().getId() + ");\n" );
//						}
//						else {
//							// TODO: Change the way names are generated to incorporate the _ok and _fail suffixes
//							out.write( "\t[" + transition.getDestinationState().getName() + "_ok] v" + countFtm + " = " + transition.getSourceState().getId() + " -> (v" + countFtm + "' = " + transition.getDestinationState().getId() + ");\n" );
//	//						out.write("\t["+t.getDestOk().getTask()+"_ok] v"+countFtm+" = "+t.getSrc().getId()+" -> (v"+countFtm+"' = "+t.getDestOk().getId()+");\n");
//	//						out.write("\t["+t.getDestOk().getTask()+"_fail] v"+countFtm+" = "+t.getSrc().getId()+" -> (v"+countFtm+"' = "+t.getDestFail().getId()+");\n");
//							out.write("\n");
//						}
//					}
//				
//					//it = ftm.getFinTrans().iterator();
//	//					while (it.hasNext()) {
//	//						Transition t = it.next();
//	//						out.write("\t["+t.getName()+"] v"+countFtm+" = "+t.getSrc().getId()+" -> (v"+countFtm+"' = "+t.getDestOk().getId()+");\n");
//	//					}
//					
//					// Write reinitialization of Voter due to HI transitions -> avoids deadlock
//					// TODO: Ask Roberto
//					final Transition transition = auto.getH_transitions().get(auto.getH_transitions().size() - 1);
//					
//					for ( int index = 0; index < prismSpec.getOwnedStates().size(); index++ ) {	
//						out.write( "\t[" + transition.getSourceState().getName() + "] v" + countFtm + " = " + index + " -> (v" + countFtm + "' = 0);\n" );
//					}
//					
//					out.write( "endmodule\n" );
//					out.write( "\n" );
//				
//					final VoterFaultToleranceMechanism voterFaultTolMech = (VoterFaultToleranceMechanism) faultTolMech;
//					final String voterTaskName  = voterFaultTolMech.getVoterTask().getName();
//					final float voterTaskFailProb = voterFaultTolMech.getVoterTask().getFailureProbability();
//					final String sourceTaskName = transition.getSourceState().getName();
//					
//					// Create replicas if it is a voter
//					for ( int index = 0; index < voterFaultTolMech.getNumberVoters(); index++ ) {
//						out.write( "module " + voterTaskName + index + "\n" );
//						out.write( "\tr_" + countFtm + "_" + index + ": [0..2] init 0;\n" );
//						out.write( "\t[" + voterTaskName + "0_run] r_" + countFtm + "_" + index + " = 0 ->  1 - " + voterTaskFailProb + " : (r_" + countFtm + "_" + index + "' = 1) + " + voterTaskFailProb + " : (r_" + countFtm +"_" + index + "' = 2);\n" );
//						out.write( "\t[" + voterTaskName + index + "_ok] r_" + countFtm + "_" + index + " = 1 -> (r_" + countFtm +"_" + index + "' = 0);\n" );
//						out.write( "\t[" + voterTaskName + index + "_fail] r_" + countFtm + "_" + index + " = 2 -> (r_" + countFtm +"_" + index + "' = 0);\n" );
//						out.write( "\t[" + sourceTaskName + "] r_" + countFtm + "_" + index + " = 0 -> (r_" + countFtm + "_" + index + "' = 0);\n" );
//						out.write( "\t[" + sourceTaskName + "] r_" + countFtm + "_" + index + " = 1 -> (r_" + countFtm + "_" + index + "' = 0);\n" );
//						out.write( "\t[" + sourceTaskName + "] r_" + countFtm + "_" + index + " = 2 -> (r_" + countFtm + "_" + index + "' = 0);\n" );
//	
//						out.write( "endmodule\n" );
//						out.write("\n");
//					}
//				}
//				else if ( faultTolMech instanceof MkFaultToleranceMechanism ) {
//					final MkFaultToleranceMechanism mkFaultTolMech = (MkFaultToleranceMechanism) faultTolMech;
//
//					for (int index = 0; index < mkFaultTolMech.getKValue(); index++ ) {
//						out.write( "\t" + mkFaultTolMech.getName() + "_v" + index + ": [0..1] init 1;\n" );
//					}
//				}
//				else {
//					throw new IllegalArgumentException( "Uknown voting mechanism: '" + faultTolMech + "'." );
//				}
//			}		
//						
//			// Write formulas
////			Iterator<Formula> iab = auto.getL_outs_b().iterator();
//			for ( final Formula formula : prismSpec.getOwnedFormulas() ) {
////				Formula form = iab.next();
//				out.write( "formula " + formula.getName() + " = " );
//				
//				// TODO: Compose expressions
//				for ( final Expression expression : formula.getOwnedExpressions() ) {
//					out.write( expression.getText() );
//				}
////				Iterator<AutoBoolean> ia = form.getLab().iterator();
////				while (ia.hasNext()) {
////					out.write(ia.next().getTask()+"bool");
////					if (ia.hasNext())
////						out.write(" & ");
////				}
//				out.write(";\n");
//			}
//			
//			out.write("\n");
//			
//			// Write Processor Module
//			out.write( "module proc\n" );
//			
//			// TODO: Ask Roberto
//			out.write("\ts : [0.." + prismSpec.getOwnedStates().size() + "] init " + auto.getLo_sched().get(0).getId()+";\n");
//			
//			// TODO: Create variables appropriately
//			for ( final Variable var : prismSpec.getOwnedVariables() ) {
//				out.write( "\t" + var.getName() + " : " + var.getType().getLiteral() + " init " + var.getInitialization().getText() + ";\n" );
//			}
//			// Create all necessary booleans
////			for (Actor a : dag.getNodes()) {
////				if (a.getCI(1) == 0) // It is a LO task
////					out.write("\t"+a.getName()+"bool: bool init false;\n");
////			}
//			
//			out.write( "\n" );
//			
//			// Create MK firms if they exist
////			iftm = auto.getFtms().iterator();
////			while (iftm.hasNext()) {
////				FTM ftm = iftm.next();
////				if (ftm.getType() == ActorAvail.MKFIRM) {
////					for (int i = 0; i < ftm.getK(); i ++) {
////						out.write("\t"+ftm.getName()+"_v"+i+": [0..1] init 1;\n");
////					}
////				}
////			}
//			
////			out.write("\n");
//			
//			// Create the LO scheduling zone
//			// TODO: What is the difference between FTM transitions and automata transitions?
//			
//			Iterator<State> is = null;
//			Iterator<Transition> it = auto.getL_transitions().iterator();
//			while (it.hasNext()) {
//				Transition t = it.next();
//				if (t.getSrc().getMode() == ActorSched.HI) {
//					if (! t.getSrc().isfMechanism())
//						out.write("\t["+t.getSrc().getTask()+"_lo] s = " + t.getSrc().getId()
//								+ " -> 1 - "+ t.getP() +" : (s' = " + t.getDestOk().getId() + ") +"
//								+ t.getP() + ": (s' =" + t.getDestFail().getId() +");\n");
//					else {
//						out.write("\t["+t.getSrc().getTask()+"_ok] s = " + t.getSrc().getId()
//								+ " -> (s' = " + t.getDestOk().getId() + ");\n");
//						out.write("\t["+t.getSrc().getTask()+"_fail] s = " + t.getSrc().getId()
//								+ " -> (s' = " + t.getDestFail().getId() + ");\n");
//					}
//				} else { // If it's a LO task we need to update the boolean
//					if (t.getSrc().getId() == 0) { // Initial state resets booleans
//						out.write("\t["+t.getSrc().getTask()+"_lo] s = " + t.getSrc().getId()
//								+ " -> (s' = " + t.getDestOk().getId()+")");
//						is = auto.getLo_sched().iterator();
//						while (is.hasNext()) {
//							State s = is.next();
//							if (s.getMode() == 0 && !s.getTask().contains("Final")
//									&& !s.getTask().contains("Init") && !s.isExit() && !s.isSynched()) // It is a LO task
//								out.write(" & ("+s.getTask()+"bool' = false)");
//						}
//						out.write(";\n");
//					} else if (t.getSrc().isVoted()){
//						out.write("\t["+t.getSrc().getTask()+"0_run] s = " + t.getSrc().getId()
//								+" -> 1 - "+t.getP()+": (s' = " + t.getDestOk().getId() +") & ");
//						FTM ftm = auto.getFTMbyName(t.getSrc().getTask());
//						for (int i = ftm.getK() - 1 ; i > 0; i--) {
//							out.write("("+ftm.getName()+"_v"+i+"' = "+ftm.getName()+"_v"+(i-1)+") &");
//						}
//						out.write(" ("+ftm.getName()+"_v0' = 1) + "+t.getP()+": (s' = " + t.getDestOk().getId() +") & ");
//						for (int i = ftm.getK() - 1 ; i > 0; i--) {
//							out.write("("+ftm.getName()+"_v"+i+"' = "+ftm.getName()+"_v"+(i-1)+") &");
//						}
//						out.write("("+ftm.getName()+"_v0' =0);\n");
//					} else if (t.getSrc().isSynched()) {
//						// OK transition
//						out.write("\t["+t.getSrc().getTask()+"_ok] s = " + t.getSrc().getId()+" & ");
//						FTM ftm = auto.getFTMbyName(t.getSrc().getTask());
//						for (int i = 0; i < ftm.getK(); i++) {
//							if (i < ftm.getK() - 1)
//								out.write(ftm.getName()+"_v"+i+" + ");
//							else
//								out.write(ftm.getName()+"_v"+i+" >= "+ftm.getM());
//						}
//						out.write(" -> (s' = " + t.getDestOk().getId() +") & ("+t.getSrc().getTask()+"bool' = true);\n" );
//						
//						// Fail transition
//						out.write("\t["+t.getSrc().getTask()+"_fail] s = " + t.getSrc().getId()+" & ");
//						for (int i = 0; i < ftm.getK(); i++) {
//							if (i < ftm.getK() - 1)
//								out.write(ftm.getName()+"_v"+i+" + ");
//							else
//								out.write(ftm.getName()+"_v"+i+" < "+ftm.getM());
//						}
//						out.write(" -> (s' = " + t.getDestOk().getId() +");\n");
//					} else if (t.getSrc().isExit()){
//						out.write("\t["+t.getSrc().getTask()+"_ok] s = " + t.getSrc().getId()+ " & "
//								+t.getSrc().getTask()+" -> (s' = " + t.getDestOk().getId() +");\n" );
//						out.write("\t["+t.getSrc().getTask()+"_fail] s = " + t.getSrc().getId()+ " & "
//								+t.getSrc().getTask()+" = false -> (s' = " + t.getDestOk().getId() +");\n" );
//					} else { 
//						out.write("\t["+t.getSrc().getTask()+"_lo] s = " + t.getSrc().getId()
//								+ " -> 1 - "+ t.getP() +" : (s' = " + t.getDestOk().getId() +") & ("+t.getSrc().getTask()+"bool' = true) + "
//								+ t.getP() + ": (s' =" + t.getDestFail().getId() + ");\n" );
//					}
//				}
//			}
//			
//			// Create the 2^n transitions for the end of LO
//			Iterator<Transition> itf = auto.getF_transitions().iterator();
//			int curr = 0;
//			while (itf.hasNext()) {
//				Transition t = itf.next();
//				out.write("\t["+t.getSrc().getTask()+curr+"] s = " + t.getSrc().getId());
//				Iterator<Formula> ib = t.getbSet().iterator();
//				while(ib.hasNext()) {
//					Formula ab = ib.next();
//					out.write(" & " + ab.getName()+" = true");
//				}
//				Iterator<Formula> iff = t.getfSet().iterator();
//				while(iff.hasNext()) {
//					Formula ab = iff.next();
//					out.write(" & " + ab.getName()+" = false");
//				}
//				out.write(" -> (s' = "+t.getDestOk().getId()+");\n");
//				curr++;
//			}
//			
//			// Create the HI scheduling zone
//			// Need to iterate through transitions
//			out.write("\n");
//			it = auto.getH_transitions().iterator();
//			while (it.hasNext()) {
//				Transition t = it.next();
//				out.write("\t["+t.getSrc().getTask()+"_hi] s = " + t.getSrc().getId() + " -> (s' =" + t.getDestOk().getId() +");\n");
//			}
//			
//			out.write("endmodule\n");
//					
//			// Create the rewards
//			out.write("\n");
//			Iterator<Actor> in = dag.getLoOuts().iterator();
//			while (in.hasNext()) {
//				Actor n = in.next();
//				out.write("rewards \""+n.getName()+"_cycles\"\n");
//				out.write("\t["+n.getName()+"_ok] true : 1;\n");
//				out.write("endrewards\n");
//				out.write("\n");
//			}
//					
//			// Total cycles reward
//			out.write("rewards \"total_cycles\"\n");
//			it = auto.getF_transitions().iterator();
//			int c = 0;
//			while (it.hasNext()) {
//				Transition t = it.next();
//				out.write("\t["+t.getSrc().getTask()+c+"] true : 1;\n");
//				c++;
//			}
//			out.write("\t["+auto.getH_transitions().get(auto.getH_transitions().size() - 1).getSrc().getTask()+"_hi] true : 1;\n");
//			out.write("endrewards\n");
//			out.write("\n");
//			
//			writePCTL();
//		} catch (IOException ie){
//			System.out.println(ie.getMessage());
//		} finally {
//			if (out != null)
//				out.close();
//		}
//	}
	
	/* Getters and setters */
	
	public String getInputFile() {
		return inputFile;
	}

	public String getOutPrismFile() {
		return outPrismFile;
	}

	public int gethPeriod() {
		return hPeriod;
	}

	public void sethPeriod(int hPeriod) {
		this.hPeriod = hPeriod;
	}

	public int getNbLevels() {
		return nbLevels;
	}

	public void setNbLevels(int nbLevels) {
		this.nbLevels = nbLevels;
	}
}
