/**
 */
package fr.tpt.mem4csd.prism.model.prism.impl;

import fr.tpt.mem4csd.prism.model.prism.PrismPackage;
import fr.tpt.mem4csd.prism.model.prism.PrismSpec;
import fr.tpt.mem4csd.prism.model.prism.Probability;
import fr.tpt.mem4csd.prism.model.prism.State;
import fr.tpt.mem4csd.prism.model.prism.Transition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.TransitionImpl#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.TransitionImpl#getOwnedProbability <em>Owned Probability</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.TransitionImpl#getSourceState <em>Source State</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.TransitionImpl#getDestinationState <em>Destination State</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.TransitionImpl#isToSelf <em>To Self</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends NamedElementImpl implements Transition {
	/**
	 * The cached value of the '{@link #getOwnedProbability() <em>Owned Probability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedProbability()
	 * @generated
	 * @ordered
	 */
	protected Probability ownedProbability;

	/**
	 * The cached value of the '{@link #getSourceState() <em>Source State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceState()
	 * @generated
	 * @ordered
	 */
	protected State sourceState;

	/**
	 * The cached value of the '{@link #getDestinationState() <em>Destination State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationState()
	 * @generated
	 * @ordered
	 */
	protected State destinationState;

	/**
	 * The cached setting delegate for the '{@link #isToSelf() <em>To Self</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isToSelf()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate TO_SELF__ESETTING_DELEGATE = ((EStructuralFeature.Internal) PrismPackage.Literals.TRANSITION__TO_SELF)
			.getSettingDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrismPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrismSpec getPrismSpec() {
		if (eContainerFeatureID() != PrismPackage.TRANSITION__PRISM_SPEC)
			return null;
		return (PrismSpec) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrismSpec(PrismSpec newPrismSpec, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newPrismSpec, PrismPackage.TRANSITION__PRISM_SPEC, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrismSpec(PrismSpec newPrismSpec) {
		if (newPrismSpec != eInternalContainer()
				|| (eContainerFeatureID() != PrismPackage.TRANSITION__PRISM_SPEC && newPrismSpec != null)) {
			if (EcoreUtil.isAncestor(this, newPrismSpec))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPrismSpec != null)
				msgs = ((InternalEObject) newPrismSpec).eInverseAdd(this, PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS,
						PrismSpec.class, msgs);
			msgs = basicSetPrismSpec(newPrismSpec, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.TRANSITION__PRISM_SPEC, newPrismSpec,
					newPrismSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Probability getOwnedProbability() {
		if (ownedProbability != null && ownedProbability.eIsProxy()) {
			InternalEObject oldOwnedProbability = (InternalEObject) ownedProbability;
			ownedProbability = (Probability) eResolveProxy(oldOwnedProbability);
			if (ownedProbability != oldOwnedProbability) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							PrismPackage.TRANSITION__OWNED_PROBABILITY, oldOwnedProbability, ownedProbability));
			}
		}
		return ownedProbability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Probability basicGetOwnedProbability() {
		return ownedProbability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedProbability(Probability newOwnedProbability) {
		Probability oldOwnedProbability = ownedProbability;
		ownedProbability = newOwnedProbability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.TRANSITION__OWNED_PROBABILITY,
					oldOwnedProbability, ownedProbability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getSourceState() {
		if (sourceState != null && sourceState.eIsProxy()) {
			InternalEObject oldSourceState = (InternalEObject) sourceState;
			sourceState = (State) eResolveProxy(oldSourceState);
			if (sourceState != oldSourceState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrismPackage.TRANSITION__SOURCE_STATE,
							oldSourceState, sourceState));
			}
		}
		return sourceState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetSourceState() {
		return sourceState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceState(State newSourceState, NotificationChain msgs) {
		State oldSourceState = sourceState;
		sourceState = newSourceState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					PrismPackage.TRANSITION__SOURCE_STATE, oldSourceState, newSourceState);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceState(State newSourceState) {
		if (newSourceState != sourceState) {
			NotificationChain msgs = null;
			if (sourceState != null)
				msgs = ((InternalEObject) sourceState).eInverseRemove(this, PrismPackage.STATE__SOURCE_TRANSITIONS,
						State.class, msgs);
			if (newSourceState != null)
				msgs = ((InternalEObject) newSourceState).eInverseAdd(this, PrismPackage.STATE__SOURCE_TRANSITIONS,
						State.class, msgs);
			msgs = basicSetSourceState(newSourceState, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.TRANSITION__SOURCE_STATE, newSourceState,
					newSourceState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getDestinationState() {
		if (destinationState != null && destinationState.eIsProxy()) {
			InternalEObject oldDestinationState = (InternalEObject) destinationState;
			destinationState = (State) eResolveProxy(oldDestinationState);
			if (destinationState != oldDestinationState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							PrismPackage.TRANSITION__DESTINATION_STATE, oldDestinationState, destinationState));
			}
		}
		return destinationState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetDestinationState() {
		return destinationState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDestinationState(State newDestinationState, NotificationChain msgs) {
		State oldDestinationState = destinationState;
		destinationState = newDestinationState;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					PrismPackage.TRANSITION__DESTINATION_STATE, oldDestinationState, newDestinationState);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDestinationState(State newDestinationState) {
		if (newDestinationState != destinationState) {
			NotificationChain msgs = null;
			if (destinationState != null)
				msgs = ((InternalEObject) destinationState).eInverseRemove(this, PrismPackage.STATE__DEST_TRANSITIONS,
						State.class, msgs);
			if (newDestinationState != null)
				msgs = ((InternalEObject) newDestinationState).eInverseAdd(this, PrismPackage.STATE__DEST_TRANSITIONS,
						State.class, msgs);
			msgs = basicSetDestinationState(newDestinationState, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.TRANSITION__DESTINATION_STATE,
					newDestinationState, newDestinationState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isToSelf() {
		return (Boolean) TO_SELF__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPrismSpec((PrismSpec) otherEnd, msgs);
		case PrismPackage.TRANSITION__SOURCE_STATE:
			if (sourceState != null)
				msgs = ((InternalEObject) sourceState).eInverseRemove(this, PrismPackage.STATE__SOURCE_TRANSITIONS,
						State.class, msgs);
			return basicSetSourceState((State) otherEnd, msgs);
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			if (destinationState != null)
				msgs = ((InternalEObject) destinationState).eInverseRemove(this, PrismPackage.STATE__DEST_TRANSITIONS,
						State.class, msgs);
			return basicSetDestinationState((State) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			return basicSetPrismSpec(null, msgs);
		case PrismPackage.TRANSITION__SOURCE_STATE:
			return basicSetSourceState(null, msgs);
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			return basicSetDestinationState(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			return eInternalContainer().eInverseRemove(this, PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS,
					PrismSpec.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			return getPrismSpec();
		case PrismPackage.TRANSITION__OWNED_PROBABILITY:
			if (resolve)
				return getOwnedProbability();
			return basicGetOwnedProbability();
		case PrismPackage.TRANSITION__SOURCE_STATE:
			if (resolve)
				return getSourceState();
			return basicGetSourceState();
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			if (resolve)
				return getDestinationState();
			return basicGetDestinationState();
		case PrismPackage.TRANSITION__TO_SELF:
			return isToSelf();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			setPrismSpec((PrismSpec) newValue);
			return;
		case PrismPackage.TRANSITION__OWNED_PROBABILITY:
			setOwnedProbability((Probability) newValue);
			return;
		case PrismPackage.TRANSITION__SOURCE_STATE:
			setSourceState((State) newValue);
			return;
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			setDestinationState((State) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			setPrismSpec((PrismSpec) null);
			return;
		case PrismPackage.TRANSITION__OWNED_PROBABILITY:
			setOwnedProbability((Probability) null);
			return;
		case PrismPackage.TRANSITION__SOURCE_STATE:
			setSourceState((State) null);
			return;
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			setDestinationState((State) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case PrismPackage.TRANSITION__PRISM_SPEC:
			return getPrismSpec() != null;
		case PrismPackage.TRANSITION__OWNED_PROBABILITY:
			return ownedProbability != null;
		case PrismPackage.TRANSITION__SOURCE_STATE:
			return sourceState != null;
		case PrismPackage.TRANSITION__DESTINATION_STATE:
			return destinationState != null;
		case PrismPackage.TRANSITION__TO_SELF:
			return TO_SELF__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		}
		return super.eIsSet(featureID);
	}

} //TransitionImpl
