/**
 */
package fr.tpt.mem4csd.prism.model.prism;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Transition#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Transition#getOwnedProbability <em>Owned Probability</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Transition#getSourceState <em>Source State</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Transition#getDestinationState <em>Destination State</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Transition#isToSelf <em>To Self</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Prism Spec</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedTransitions <em>Owned Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prism Spec</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prism Spec</em>' container reference.
	 * @see #setPrismSpec(PrismSpec)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition_PrismSpec()
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedTransitions
	 * @model opposite="ownedTransitions" required="true" transient="false"
	 * @generated
	 */
	PrismSpec getPrismSpec();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getPrismSpec <em>Prism Spec</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prism Spec</em>' container reference.
	 * @see #getPrismSpec()
	 * @generated
	 */
	void setPrismSpec(PrismSpec value);

	/**
	 * Returns the value of the '<em><b>Owned Probability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Probability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Probability</em>' reference.
	 * @see #setOwnedProbability(Probability)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition_OwnedProbability()
	 * @model
	 * @generated
	 */
	Probability getOwnedProbability();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getOwnedProbability <em>Owned Probability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned Probability</em>' reference.
	 * @see #getOwnedProbability()
	 * @generated
	 */
	void setOwnedProbability(Probability value);

	/**
	 * Returns the value of the '<em><b>Source State</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.State#getSourceTransitions <em>Source Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source State</em>' reference.
	 * @see #setSourceState(State)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition_SourceState()
	 * @see fr.tpt.mem4csd.prism.model.prism.State#getSourceTransitions
	 * @model opposite="sourceTransitions" required="true"
	 * @generated
	 */
	State getSourceState();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getSourceState <em>Source State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source State</em>' reference.
	 * @see #getSourceState()
	 * @generated
	 */
	void setSourceState(State value);

	/**
	 * Returns the value of the '<em><b>Destination State</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.State#getDestTransitions <em>Dest Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination State</em>' reference.
	 * @see #setDestinationState(State)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition_DestinationState()
	 * @see fr.tpt.mem4csd.prism.model.prism.State#getDestTransitions
	 * @model opposite="destTransitions" required="true"
	 * @generated
	 */
	State getDestinationState();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getDestinationState <em>Destination State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination State</em>' reference.
	 * @see #getDestinationState()
	 * @generated
	 */
	void setDestinationState(State value);

	/**
	 * Returns the value of the '<em><b>To Self</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Self</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Self</em>' attribute.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getTransition_ToSelf()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='self.sourceState = self.destinationState'"
	 * @generated
	 */
	boolean isToSelf();

} // Transition
