/**
 */
package fr.tpt.mem4csd.prism.model.prism;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Probability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Probability#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getProbability()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validValue'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot validValue='self.value = null or  ( self.value &gt;= 0.0 and self.value &lt;= 1.0 )'"
 * @generated
 */
public interface Probability extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Float)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getProbability_Value()
	 * @model
	 * @generated
	 */
	Float getValue();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Probability#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Float value);

} // Probability
