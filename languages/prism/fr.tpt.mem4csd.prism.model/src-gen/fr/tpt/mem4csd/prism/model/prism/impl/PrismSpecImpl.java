/**
 */
package fr.tpt.mem4csd.prism.model.prism.impl;

import fr.tpt.mem4csd.prism.model.prism.Formula;
import fr.tpt.mem4csd.prism.model.prism.PrismPackage;
import fr.tpt.mem4csd.prism.model.prism.PrismSpec;
import fr.tpt.mem4csd.prism.model.prism.State;
import fr.tpt.mem4csd.prism.model.prism.Transition;

import fr.tpt.mem4csd.prism.model.prism.Variable;
import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.PrismSpecImpl#getOwnedStates <em>Owned States</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.PrismSpecImpl#getOwnedTransitions <em>Owned Transitions</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.PrismSpecImpl#getOwnedFormulas <em>Owned Formulas</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.PrismSpecImpl#getOwnedVariables <em>Owned Variables</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrismSpecImpl extends NamedElementImpl implements PrismSpec {
	/**
	 * The cached value of the '{@link #getOwnedStates() <em>Owned States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> ownedStates;

	/**
	 * The cached value of the '{@link #getOwnedTransitions() <em>Owned Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> ownedTransitions;

	/**
	 * The cached value of the '{@link #getOwnedFormulas() <em>Owned Formulas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedFormulas()
	 * @generated
	 * @ordered
	 */
	protected EList<Formula> ownedFormulas;

	/**
	 * The cached value of the '{@link #getOwnedVariables() <em>Owned Variables</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable> ownedVariables;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrismSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrismPackage.Literals.PRISM_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getOwnedStates() {
		if (ownedStates == null) {
			ownedStates = new EObjectContainmentWithInverseEList<State>(State.class, this,
					PrismPackage.PRISM_SPEC__OWNED_STATES, PrismPackage.STATE__PRISM_SPEC);
		}
		return ownedStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOwnedTransitions() {
		if (ownedTransitions == null) {
			ownedTransitions = new EObjectContainmentWithInverseEList<Transition>(Transition.class, this,
					PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS, PrismPackage.TRANSITION__PRISM_SPEC);
		}
		return ownedTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Formula> getOwnedFormulas() {
		if (ownedFormulas == null) {
			ownedFormulas = new EObjectContainmentWithInverseEList<Formula>(Formula.class, this,
					PrismPackage.PRISM_SPEC__OWNED_FORMULAS, PrismPackage.FORMULA__PRISM_SPEC);
		}
		return ownedFormulas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable> getOwnedVariables() {
		if (ownedVariables == null) {
			ownedVariables = new EObjectWithInverseResolvingEList<Variable>(Variable.class, this,
					PrismPackage.PRISM_SPEC__OWNED_VARIABLES, PrismPackage.VARIABLE__PRISM_SPEC);
		}
		return ownedVariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOwnedStates()).basicAdd(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOwnedTransitions()).basicAdd(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOwnedFormulas()).basicAdd(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOwnedVariables()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			return ((InternalEList<?>) getOwnedStates()).basicRemove(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			return ((InternalEList<?>) getOwnedTransitions()).basicRemove(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			return ((InternalEList<?>) getOwnedFormulas()).basicRemove(otherEnd, msgs);
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			return ((InternalEList<?>) getOwnedVariables()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			return getOwnedStates();
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			return getOwnedTransitions();
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			return getOwnedFormulas();
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			return getOwnedVariables();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			getOwnedStates().clear();
			getOwnedStates().addAll((Collection<? extends State>) newValue);
			return;
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			getOwnedTransitions().clear();
			getOwnedTransitions().addAll((Collection<? extends Transition>) newValue);
			return;
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			getOwnedFormulas().clear();
			getOwnedFormulas().addAll((Collection<? extends Formula>) newValue);
			return;
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			getOwnedVariables().clear();
			getOwnedVariables().addAll((Collection<? extends Variable>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			getOwnedStates().clear();
			return;
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			getOwnedTransitions().clear();
			return;
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			getOwnedFormulas().clear();
			return;
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			getOwnedVariables().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case PrismPackage.PRISM_SPEC__OWNED_STATES:
			return ownedStates != null && !ownedStates.isEmpty();
		case PrismPackage.PRISM_SPEC__OWNED_TRANSITIONS:
			return ownedTransitions != null && !ownedTransitions.isEmpty();
		case PrismPackage.PRISM_SPEC__OWNED_FORMULAS:
			return ownedFormulas != null && !ownedFormulas.isEmpty();
		case PrismPackage.PRISM_SPEC__OWNED_VARIABLES:
			return ownedVariables != null && !ownedVariables.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PrismSpecImpl
