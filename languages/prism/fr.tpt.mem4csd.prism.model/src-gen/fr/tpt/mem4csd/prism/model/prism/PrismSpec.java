/**
 */
package fr.tpt.mem4csd.prism.model.prism;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedStates <em>Owned States</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedTransitions <em>Owned Transitions</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedFormulas <em>Owned Formulas</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedVariables <em>Owned Variables</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getPrismSpec()
 * @model
 * @generated
 */
public interface PrismSpec extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Owned States</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.State}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.State#getPrismSpec <em>Prism Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned States</em>' containment reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getPrismSpec_OwnedStates()
	 * @see fr.tpt.mem4csd.prism.model.prism.State#getPrismSpec
	 * @model opposite="prismSpec" containment="true"
	 * @generated
	 */
	EList<State> getOwnedStates();

	/**
	 * Returns the value of the '<em><b>Owned Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.Transition}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getPrismSpec <em>Prism Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Transitions</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Transitions</em>' containment reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getPrismSpec_OwnedTransitions()
	 * @see fr.tpt.mem4csd.prism.model.prism.Transition#getPrismSpec
	 * @model opposite="prismSpec" containment="true"
	 * @generated
	 */
	EList<Transition> getOwnedTransitions();

	/**
	 * Returns the value of the '<em><b>Owned Formulas</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.Formula}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Formula#getPrismSpec <em>Prism Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Formulas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Formulas</em>' containment reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getPrismSpec_OwnedFormulas()
	 * @see fr.tpt.mem4csd.prism.model.prism.Formula#getPrismSpec
	 * @model opposite="prismSpec" containment="true"
	 * @generated
	 */
	EList<Formula> getOwnedFormulas();

	/**
	 * Returns the value of the '<em><b>Owned Variables</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.Variable}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Variable#getPrismSpec <em>Prism Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Variables</em>' reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getPrismSpec_OwnedVariables()
	 * @see fr.tpt.mem4csd.prism.model.prism.Variable#getPrismSpec
	 * @model opposite="prismSpec"
	 * @generated
	 */
	EList<Variable> getOwnedVariables();

} // PrismSpec
