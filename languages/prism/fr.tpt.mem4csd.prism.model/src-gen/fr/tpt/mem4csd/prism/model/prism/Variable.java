/**
 */
package fr.tpt.mem4csd.prism.model.prism;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Variable#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Variable#getType <em>Type</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Variable#getInitialization <em>Initialization</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Prism Spec</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedVariables <em>Owned Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prism Spec</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prism Spec</em>' reference.
	 * @see #setPrismSpec(PrismSpec)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getVariable_PrismSpec()
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedVariables
	 * @model opposite="ownedVariables" required="true"
	 * @generated
	 */
	PrismSpec getPrismSpec();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Variable#getPrismSpec <em>Prism Spec</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prism Spec</em>' reference.
	 * @see #getPrismSpec()
	 * @generated
	 */
	void setPrismSpec(PrismSpec value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.tpt.mem4csd.prism.model.prism.VariableType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see fr.tpt.mem4csd.prism.model.prism.VariableType
	 * @see #setType(VariableType)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getVariable_Type()
	 * @model required="true"
	 * @generated
	 */
	VariableType getType();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Variable#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see fr.tpt.mem4csd.prism.model.prism.VariableType
	 * @see #getType()
	 * @generated
	 */
	void setType(VariableType value);

	/**
	 * Returns the value of the '<em><b>Initialization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialization</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialization</em>' reference.
	 * @see #setInitialization(Expression)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getVariable_Initialization()
	 * @model required="true"
	 * @generated
	 */
	Expression getInitialization();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Variable#getInitialization <em>Initialization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialization</em>' reference.
	 * @see #getInitialization()
	 * @generated
	 */
	void setInitialization(Expression value);

} // Variable
