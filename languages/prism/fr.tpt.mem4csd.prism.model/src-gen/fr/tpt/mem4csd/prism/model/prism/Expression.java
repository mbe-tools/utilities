/**
 */
package fr.tpt.mem4csd.prism.model.prism;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Expression#getFormula <em>Formula</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Expression#getText <em>Text</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Formula</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Formula#getOwnedExpressions <em>Owned Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' container reference.
	 * @see #setFormula(Formula)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getExpression_Formula()
	 * @see fr.tpt.mem4csd.prism.model.prism.Formula#getOwnedExpressions
	 * @model opposite="ownedExpressions" required="true" transient="false"
	 * @generated
	 */
	Formula getFormula();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Expression#getFormula <em>Formula</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' container reference.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(Formula value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getExpression_Text()
	 * @model required="true"
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Expression#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

} // Expression
