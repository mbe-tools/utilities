/**
 */
package fr.tpt.mem4csd.prism.model.prism;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.State#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.State#getSourceTransitions <em>Source Transitions</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.State#getDestTransitions <em>Dest Transitions</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getState()
 * @model
 * @generated
 */
public interface State extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Prism Spec</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedStates <em>Owned States</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prism Spec</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prism Spec</em>' container reference.
	 * @see #setPrismSpec(PrismSpec)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getState_PrismSpec()
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedStates
	 * @model opposite="ownedStates" required="true" transient="false"
	 * @generated
	 */
	PrismSpec getPrismSpec();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.State#getPrismSpec <em>Prism Spec</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prism Spec</em>' container reference.
	 * @see #getPrismSpec()
	 * @generated
	 */
	void setPrismSpec(PrismSpec value);

	/**
	 * Returns the value of the '<em><b>Source Transitions</b></em>' reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.Transition}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Transitions</em>' reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getState_SourceTransitions()
	 * @see fr.tpt.mem4csd.prism.model.prism.Transition#getSourceState
	 * @model opposite="sourceState"
	 * @generated
	 */
	EList<Transition> getSourceTransitions();

	/**
	 * Returns the value of the '<em><b>Dest Transitions</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Transition#getDestinationState <em>Destination State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dest Transitions</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest Transitions</em>' reference.
	 * @see #setDestTransitions(Transition)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getState_DestTransitions()
	 * @see fr.tpt.mem4csd.prism.model.prism.Transition#getDestinationState
	 * @model opposite="destinationState"
	 * @generated
	 */
	Transition getDestTransitions();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.State#getDestTransitions <em>Dest Transitions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest Transitions</em>' reference.
	 * @see #getDestTransitions()
	 * @generated
	 */
	void setDestTransitions(Transition value);

} // State
