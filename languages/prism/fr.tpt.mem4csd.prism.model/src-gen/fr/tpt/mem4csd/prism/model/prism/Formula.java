/**
 */
package fr.tpt.mem4csd.prism.model.prism;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Formula#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.Formula#getOwnedExpressions <em>Owned Expressions</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getFormula()
 * @model
 * @generated
 */
public interface Formula extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Prism Spec</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedFormulas <em>Owned Formulas</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prism Spec</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prism Spec</em>' container reference.
	 * @see #setPrismSpec(PrismSpec)
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getFormula_PrismSpec()
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismSpec#getOwnedFormulas
	 * @model opposite="ownedFormulas" required="true" transient="false"
	 * @generated
	 */
	PrismSpec getPrismSpec();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.prism.model.prism.Formula#getPrismSpec <em>Prism Spec</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prism Spec</em>' container reference.
	 * @see #getPrismSpec()
	 * @generated
	 */
	void setPrismSpec(PrismSpec value);

	/**
	 * Returns the value of the '<em><b>Owned Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.tpt.mem4csd.prism.model.prism.Expression}.
	 * It is bidirectional and its opposite is '{@link fr.tpt.mem4csd.prism.model.prism.Expression#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Expressions</em>' containment reference list.
	 * @see fr.tpt.mem4csd.prism.model.prism.PrismPackage#getFormula_OwnedExpressions()
	 * @see fr.tpt.mem4csd.prism.model.prism.Expression#getFormula
	 * @model opposite="formula" containment="true" required="true"
	 * @generated
	 */
	EList<Expression> getOwnedExpressions();

} // Formula
