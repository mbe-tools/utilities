/**
 */
package fr.tpt.mem4csd.prism.model.prism.impl;

import fr.tpt.mem4csd.prism.model.prism.PrismPackage;
import fr.tpt.mem4csd.prism.model.prism.PrismSpec;
import fr.tpt.mem4csd.prism.model.prism.State;

import fr.tpt.mem4csd.prism.model.prism.Transition;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.StateImpl#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.StateImpl#getSourceTransitions <em>Source Transitions</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.StateImpl#getDestTransitions <em>Dest Transitions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateImpl extends NamedElementImpl implements State {
	/**
	 * The cached value of the '{@link #getSourceTransitions() <em>Source Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> sourceTransitions;
	/**
	 * The cached value of the '{@link #getDestTransitions() <em>Dest Transitions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestTransitions()
	 * @generated
	 * @ordered
	 */
	protected Transition destTransitions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrismPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrismSpec getPrismSpec() {
		if (eContainerFeatureID() != PrismPackage.STATE__PRISM_SPEC)
			return null;
		return (PrismSpec) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrismSpec(PrismSpec newPrismSpec, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newPrismSpec, PrismPackage.STATE__PRISM_SPEC, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrismSpec(PrismSpec newPrismSpec) {
		if (newPrismSpec != eInternalContainer()
				|| (eContainerFeatureID() != PrismPackage.STATE__PRISM_SPEC && newPrismSpec != null)) {
			if (EcoreUtil.isAncestor(this, newPrismSpec))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPrismSpec != null)
				msgs = ((InternalEObject) newPrismSpec).eInverseAdd(this, PrismPackage.PRISM_SPEC__OWNED_STATES,
						PrismSpec.class, msgs);
			msgs = basicSetPrismSpec(newPrismSpec, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.STATE__PRISM_SPEC, newPrismSpec,
					newPrismSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getSourceTransitions() {
		if (sourceTransitions == null) {
			sourceTransitions = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this,
					PrismPackage.STATE__SOURCE_TRANSITIONS, PrismPackage.TRANSITION__SOURCE_STATE);
		}
		return sourceTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getDestTransitions() {
		if (destTransitions != null && destTransitions.eIsProxy()) {
			InternalEObject oldDestTransitions = (InternalEObject) destTransitions;
			destTransitions = (Transition) eResolveProxy(oldDestTransitions);
			if (destTransitions != oldDestTransitions) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrismPackage.STATE__DEST_TRANSITIONS,
							oldDestTransitions, destTransitions));
			}
		}
		return destTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetDestTransitions() {
		return destTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDestTransitions(Transition newDestTransitions, NotificationChain msgs) {
		Transition oldDestTransitions = destTransitions;
		destTransitions = newDestTransitions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					PrismPackage.STATE__DEST_TRANSITIONS, oldDestTransitions, newDestTransitions);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDestTransitions(Transition newDestTransitions) {
		if (newDestTransitions != destTransitions) {
			NotificationChain msgs = null;
			if (destTransitions != null)
				msgs = ((InternalEObject) destTransitions).eInverseRemove(this,
						PrismPackage.TRANSITION__DESTINATION_STATE, Transition.class, msgs);
			if (newDestTransitions != null)
				msgs = ((InternalEObject) newDestTransitions).eInverseAdd(this,
						PrismPackage.TRANSITION__DESTINATION_STATE, Transition.class, msgs);
			msgs = basicSetDestTransitions(newDestTransitions, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.STATE__DEST_TRANSITIONS,
					newDestTransitions, newDestTransitions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPrismSpec((PrismSpec) otherEnd, msgs);
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSourceTransitions()).basicAdd(otherEnd,
					msgs);
		case PrismPackage.STATE__DEST_TRANSITIONS:
			if (destTransitions != null)
				msgs = ((InternalEObject) destTransitions).eInverseRemove(this,
						PrismPackage.TRANSITION__DESTINATION_STATE, Transition.class, msgs);
			return basicSetDestTransitions((Transition) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			return basicSetPrismSpec(null, msgs);
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			return ((InternalEList<?>) getSourceTransitions()).basicRemove(otherEnd, msgs);
		case PrismPackage.STATE__DEST_TRANSITIONS:
			return basicSetDestTransitions(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case PrismPackage.STATE__PRISM_SPEC:
			return eInternalContainer().eInverseRemove(this, PrismPackage.PRISM_SPEC__OWNED_STATES, PrismSpec.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			return getPrismSpec();
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			return getSourceTransitions();
		case PrismPackage.STATE__DEST_TRANSITIONS:
			if (resolve)
				return getDestTransitions();
			return basicGetDestTransitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			setPrismSpec((PrismSpec) newValue);
			return;
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			getSourceTransitions().clear();
			getSourceTransitions().addAll((Collection<? extends Transition>) newValue);
			return;
		case PrismPackage.STATE__DEST_TRANSITIONS:
			setDestTransitions((Transition) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			setPrismSpec((PrismSpec) null);
			return;
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			getSourceTransitions().clear();
			return;
		case PrismPackage.STATE__DEST_TRANSITIONS:
			setDestTransitions((Transition) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case PrismPackage.STATE__PRISM_SPEC:
			return getPrismSpec() != null;
		case PrismPackage.STATE__SOURCE_TRANSITIONS:
			return sourceTransitions != null && !sourceTransitions.isEmpty();
		case PrismPackage.STATE__DEST_TRANSITIONS:
			return destTransitions != null;
		}
		return super.eIsSet(featureID);
	}

} //StateImpl
