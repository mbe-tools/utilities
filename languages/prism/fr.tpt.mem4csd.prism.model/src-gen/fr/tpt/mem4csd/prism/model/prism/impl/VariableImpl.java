/**
 */
package fr.tpt.mem4csd.prism.model.prism.impl;

import fr.tpt.mem4csd.prism.model.prism.Expression;
import fr.tpt.mem4csd.prism.model.prism.PrismPackage;
import fr.tpt.mem4csd.prism.model.prism.PrismSpec;
import fr.tpt.mem4csd.prism.model.prism.Variable;
import fr.tpt.mem4csd.prism.model.prism.VariableType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.VariableImpl#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.VariableImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.VariableImpl#getInitialization <em>Initialization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableImpl extends NamedElementImpl implements Variable {
	/**
	 * The cached value of the '{@link #getPrismSpec() <em>Prism Spec</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrismSpec()
	 * @generated
	 * @ordered
	 */
	protected PrismSpec prismSpec;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final VariableType TYPE_EDEFAULT = VariableType.BOOL;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected VariableType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInitialization() <em>Initialization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialization()
	 * @generated
	 * @ordered
	 */
	protected Expression initialization;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrismPackage.Literals.VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrismSpec getPrismSpec() {
		if (prismSpec != null && prismSpec.eIsProxy()) {
			InternalEObject oldPrismSpec = (InternalEObject) prismSpec;
			prismSpec = (PrismSpec) eResolveProxy(oldPrismSpec);
			if (prismSpec != oldPrismSpec) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrismPackage.VARIABLE__PRISM_SPEC,
							oldPrismSpec, prismSpec));
			}
		}
		return prismSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrismSpec basicGetPrismSpec() {
		return prismSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrismSpec(PrismSpec newPrismSpec, NotificationChain msgs) {
		PrismSpec oldPrismSpec = prismSpec;
		prismSpec = newPrismSpec;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					PrismPackage.VARIABLE__PRISM_SPEC, oldPrismSpec, newPrismSpec);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrismSpec(PrismSpec newPrismSpec) {
		if (newPrismSpec != prismSpec) {
			NotificationChain msgs = null;
			if (prismSpec != null)
				msgs = ((InternalEObject) prismSpec).eInverseRemove(this, PrismPackage.PRISM_SPEC__OWNED_VARIABLES,
						PrismSpec.class, msgs);
			if (newPrismSpec != null)
				msgs = ((InternalEObject) newPrismSpec).eInverseAdd(this, PrismPackage.PRISM_SPEC__OWNED_VARIABLES,
						PrismSpec.class, msgs);
			msgs = basicSetPrismSpec(newPrismSpec, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.VARIABLE__PRISM_SPEC, newPrismSpec,
					newPrismSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(VariableType newType) {
		VariableType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.VARIABLE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialization() {
		if (initialization != null && initialization.eIsProxy()) {
			InternalEObject oldInitialization = (InternalEObject) initialization;
			initialization = (Expression) eResolveProxy(oldInitialization);
			if (initialization != oldInitialization) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PrismPackage.VARIABLE__INITIALIZATION,
							oldInitialization, initialization));
			}
		}
		return initialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression basicGetInitialization() {
		return initialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialization(Expression newInitialization) {
		Expression oldInitialization = initialization;
		initialization = newInitialization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.VARIABLE__INITIALIZATION,
					oldInitialization, initialization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			if (prismSpec != null)
				msgs = ((InternalEObject) prismSpec).eInverseRemove(this, PrismPackage.PRISM_SPEC__OWNED_VARIABLES,
						PrismSpec.class, msgs);
			return basicSetPrismSpec((PrismSpec) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			return basicSetPrismSpec(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			if (resolve)
				return getPrismSpec();
			return basicGetPrismSpec();
		case PrismPackage.VARIABLE__TYPE:
			return getType();
		case PrismPackage.VARIABLE__INITIALIZATION:
			if (resolve)
				return getInitialization();
			return basicGetInitialization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			setPrismSpec((PrismSpec) newValue);
			return;
		case PrismPackage.VARIABLE__TYPE:
			setType((VariableType) newValue);
			return;
		case PrismPackage.VARIABLE__INITIALIZATION:
			setInitialization((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			setPrismSpec((PrismSpec) null);
			return;
		case PrismPackage.VARIABLE__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case PrismPackage.VARIABLE__INITIALIZATION:
			setInitialization((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case PrismPackage.VARIABLE__PRISM_SPEC:
			return prismSpec != null;
		case PrismPackage.VARIABLE__TYPE:
			return type != TYPE_EDEFAULT;
		case PrismPackage.VARIABLE__INITIALIZATION:
			return initialization != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //VariableImpl
