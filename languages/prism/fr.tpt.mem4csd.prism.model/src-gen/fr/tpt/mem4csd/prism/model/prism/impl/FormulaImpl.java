/**
 */
package fr.tpt.mem4csd.prism.model.prism.impl;

import fr.tpt.mem4csd.prism.model.prism.Expression;
import fr.tpt.mem4csd.prism.model.prism.Formula;
import fr.tpt.mem4csd.prism.model.prism.PrismPackage;
import fr.tpt.mem4csd.prism.model.prism.PrismSpec;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.FormulaImpl#getPrismSpec <em>Prism Spec</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.prism.model.prism.impl.FormulaImpl#getOwnedExpressions <em>Owned Expressions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormulaImpl extends NamedElementImpl implements Formula {
	/**
	 * The cached value of the '{@link #getOwnedExpressions() <em>Owned Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> ownedExpressions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PrismPackage.Literals.FORMULA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrismSpec getPrismSpec() {
		if (eContainerFeatureID() != PrismPackage.FORMULA__PRISM_SPEC)
			return null;
		return (PrismSpec) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrismSpec(PrismSpec newPrismSpec, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newPrismSpec, PrismPackage.FORMULA__PRISM_SPEC, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrismSpec(PrismSpec newPrismSpec) {
		if (newPrismSpec != eInternalContainer()
				|| (eContainerFeatureID() != PrismPackage.FORMULA__PRISM_SPEC && newPrismSpec != null)) {
			if (EcoreUtil.isAncestor(this, newPrismSpec))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPrismSpec != null)
				msgs = ((InternalEObject) newPrismSpec).eInverseAdd(this, PrismPackage.PRISM_SPEC__OWNED_FORMULAS,
						PrismSpec.class, msgs);
			msgs = basicSetPrismSpec(newPrismSpec, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PrismPackage.FORMULA__PRISM_SPEC, newPrismSpec,
					newPrismSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getOwnedExpressions() {
		if (ownedExpressions == null) {
			ownedExpressions = new EObjectContainmentWithInverseEList<Expression>(Expression.class, this,
					PrismPackage.FORMULA__OWNED_EXPRESSIONS, PrismPackage.EXPRESSION__FORMULA);
		}
		return ownedExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPrismSpec((PrismSpec) otherEnd, msgs);
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOwnedExpressions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			return basicSetPrismSpec(null, msgs);
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			return ((InternalEList<?>) getOwnedExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			return eInternalContainer().eInverseRemove(this, PrismPackage.PRISM_SPEC__OWNED_FORMULAS, PrismSpec.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			return getPrismSpec();
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			return getOwnedExpressions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			setPrismSpec((PrismSpec) newValue);
			return;
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			getOwnedExpressions().clear();
			getOwnedExpressions().addAll((Collection<? extends Expression>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			setPrismSpec((PrismSpec) null);
			return;
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			getOwnedExpressions().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case PrismPackage.FORMULA__PRISM_SPEC:
			return getPrismSpec() != null;
		case PrismPackage.FORMULA__OWNED_EXPRESSIONS:
			return ownedExpressions != null && !ownedExpressions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FormulaImpl
