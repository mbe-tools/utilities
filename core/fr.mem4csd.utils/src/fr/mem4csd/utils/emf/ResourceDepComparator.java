package fr.mem4csd.utils.emf;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

public class ResourceDepComparator implements Comparator<Resource> {
	
	@Override
    public int compare(	final Resource res1, 
    					final Resource res2 ) {
		if( res1 == null ) {
            return res2 == null ? 0 : 1;
        }

        // At this point, we know that c1 and c2 are not null
        if ( res1.equals( res2 ) ) {
            return 0;
        }

        boolean res1DepOnRes2 = dependsOn( res1, res2 );
        boolean res2DepOnRes1 = dependsOn( res2, res1 );
        
        if ( res1DepOnRes2 && !res2DepOnRes1 ) {
            return 1;
        } 

        if ( res2DepOnRes1 && !res1DepOnRes2 ) {
            return - 1;
        } 

        return 0;
    }
	
	public static boolean dependsOn( 	final Resource resource1,
										final Resource resource2 ) {
		return dependsOn( resource1, resource2, new HashSet<Resource>() );
	}
	
	private static boolean dependsOn( 	final Resource resource1,
										final Resource resource2,
										final Set<Resource> traversedRes ) {
		if ( resource1 == resource2 ) {
			return true;
		}
		
		final Iterator<EObject> content = resource1.getAllContents();

		while ( content.hasNext() ) {
			final EObject elem = content.next();

			for ( final EObject crossRef : elem.eCrossReferences() ) {
				final Resource crossRes = crossRef.eResource();
				
				if ( crossRes != null && crossRes != resource1 && !traversedRes.contains( crossRes ) ) {
					traversedRes.add( crossRes );
					
					if ( dependsOn( crossRes, resource2, traversedRes ) ) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
}
