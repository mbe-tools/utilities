package fr.mem4csd.utils.emf;

import org.eclipse.emf.ecore.EObject;

public interface IValidator {

	void validate( EObject... objects ) 
	throws InterruptedException;

	void validate( 	int featureId,
					boolean validateFeatureValue,
					EObject... bjects ) 
	throws InterruptedException;
}
