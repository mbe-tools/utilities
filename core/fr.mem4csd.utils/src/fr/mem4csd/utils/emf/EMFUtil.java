package fr.mem4csd.utils.emf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.internal.boot.PlatformURLConnection;
import org.eclipse.core.internal.boot.PlatformURLHandler;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.PackageNotFoundException;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.xml.sax.SAXParseException;
import fr.mem4csd.utils.comparators.ClassHierarchyComparator;
import fr.mem4csd.utils.eclipse.BundleUtil;

@SuppressWarnings("restriction")
public class EMFUtil {

	private static final ClassHierarchyComparator classHierarchyComp = new ClassHierarchyComparator();

	public static EFactory getEFactory( final Class<?> dataType ) {
		final EPackage emfPackage = getEPackage( dataType );
		
		return emfPackage.getEFactoryInstance();
	}

	public static EPackage getEPackage( final Class<?> dataType ) {
		final EClassifier eClassifier = getEClassifier( dataType );
		
		if ( eClassifier == null ) {
			return null;
		}
		
		return eClassifier.getEPackage();
	}
	
	public static EClassifier getEClassifier( final Class<?> dataType ) {
		final Class<?> interfaceClass = findInterface( dataType );
		final Registry packageRegistry = EPackage.Registry.INSTANCE;

		// Do not use the key set directly. getEPackage adds packages created from the
		// descriptors so would get concurrent modification.
		final Collection<String> uris = new ArrayList<String>(); 
		uris.addAll( packageRegistry.keySet() );
		
		for ( final String packURI : uris ) {
			final EPackage ePackage = (EPackage) packageRegistry.getEPackage( packURI );
			
			for ( final EClassifier eClassifier : ePackage.getEClassifiers() ) {
				if ( interfaceClass == eClassifier.getInstanceClass() ) {
					return eClassifier;
				}
			}
		}
		
		return null;
	}

	public static Class<?> findInterface( final Class<?> dataType ) {
		if ( dataType.isInterface() ) {
			return dataType;
		}
		
		for ( Class<?> upInterface : dataType.getInterfaces() ) {
			if ( EObject.class.isAssignableFrom( upInterface ) ) {
				return upInterface;
			}
		}
		
		return null;
	}
	
	public static Class<?> getFeatureType( 	final EClassifier featureContainerClass,
											final EStructuralFeature feature ) {
        EClassifier classifier = getFeatureClassifier( featureContainerClass, feature);
        
       	return classifier.getInstanceClass();
	}

	public static EClassifier getFeatureClassifier( final EClassifier featureContainerClass,
													final EStructuralFeature feature ) {
        EClassifier classifier = feature.getEType();
        final EGenericType genType = feature.getEGenericType();

        if ( genType != null && genType.getETypeParameter() != null ) {
        	classifier = getFeatureClassifier( featureContainerClass, feature, genType );
        }
        
       	return classifier;
	}

	public static EClassifier getFeatureClassifier( final EClassifier featureContainerClass,
													final EStructuralFeature feature,
													final EGenericType genType ) {
		for ( EGenericType superType : ( (EClass)featureContainerClass ).getEGenericSuperTypes() ) {
			final EClassifier classifier = superType.getEClassifier();
			final List<ETypeParameter> itClassTypeParms = classifier.getETypeParameters();
			
			for ( int index = 0; index < itClassTypeParms.size(); index++ ) {
				ETypeParameter typParm = itClassTypeParms.get( index );
	
				if ( typParm.getName().equals( genType.getETypeParameter().getName() ) ) {
					return superType.getETypeArguments().get( index ).getEClassifier();
				}
			}
			
			final EClassifier foundClass = getFeatureClassifier( classifier, feature );
			
			if ( foundClass != null ) {
				return foundClass;
			}
		}
       	
       	return null;
	}
	
	public static boolean isEMFResource( final IResource resource ) {
		if ( isEMFResource( resource.getFileExtension() ) ) {
			return true;
		}
		
		try {
			return convertToEMFResource( resource ) != null;
		}
		catch ( final WrappedException ex ) {
			return false;
		}
	}
	
	public static boolean isEMFResource( final String fileExtension ) {
		return Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().get( fileExtension ) != null;
	}

	public static EObject getRootObject( 	final EObject object,
											final EClass objectClass ) {
		EObject container = object;
		EObject root = null;
		
		while ( container != null ) {
			if ( objectClass == null || container.eClass().equals( objectClass ) ) {
				root = container;
			}
			
			container = container.eContainer();
		}
		
		return root;
	}
	
	public static <T extends EObject> void fillContentOfTypes( 	final Resource resource,
																final Collection<? extends EClassifier> supportedTypes,
																final Collection<T> elements ) {
		fillContentOfTypes( resource.getAllContents(), supportedTypes, elements );
	}

	public static <T extends EObject> void fillContentOfTypes( 	final EObject object,
																final boolean pb_resolve,
																final Collection<? extends EClassifier> supportedTypes,
																final Collection<T> elements,
																final boolean includeRoot ) {
		final Iterator<EObject> iterator = EcoreUtil.getAllContents( object, pb_resolve );
		fillContentOfTypes( iterator, supportedTypes, elements );
		
		if ( includeRoot ) {
			fillContentOfTypes( Collections.singletonList( object ).iterator(), supportedTypes, elements );
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> void fillContentOfTypes( 	final Iterator<EObject> content,
																final Collection<? extends EClassifier> supportedTypes,
																final Collection<T> elements ) {
		while ( content.hasNext() ) {
			final EObject eObject = content.next();
			
			if ( supportedTypes == null ) {
				elements.add( (T) eObject );
			}
			else {
				for ( final EClassifier classifier : supportedTypes ) {
					if ( classifier.isInstance( eObject ) ) {
						elements.add( (T) eObject );
						
						break;
					}
				}
			}
		}
	}
	
	public static EReference getEReference( final EClass eClass, 
											final String refName ) {
		for ( EReference reference : eClass.getEAllReferences() ) {
			if ( reference.getName().equals( refName ) ) {
				return reference;
			}
		}
		
		return null;
	}
	
	public static Map<?,?> getDefaultOptions() {
		final Map<Object, Object> options = new HashMap<Object, Object>();
		options.put( XMLResource.OPTION_ENCODING, "ISO-8859-1" );
		
		return options;
	}
	
	public static IResource convertToIDEResource( final Resource emfResource ) {
		return emfResource == null ? null : convertToIDEResource( emfResource.getURI() );
	}
	
	public static IResource convertToIDEResource( final URI resUri ) {
		assert resUri != null : "EMF resource URI cannot be null.";

		final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		
		if ( resUri.isPlatform() ) {
			return workspaceRoot.getFile( new Path( resUri.toPlatformString( true ) ) );
		}

		if ( resUri.isFile() ) {
			return workspaceRoot.getFileForLocation( new Path( resUri.toFileString() ) );
		}
		
		throw new IllegalArgumentException( "Resource scheme not managed: " + resUri.scheme() );
	}

	/**
	 * Tries to obtain the EMF resource from the eclipse IDE resource.
	 * 
	 * @param resource The eclipse IDE resource.
	 * @return The corresponding EMF resource if the specified resource is in xmi format.
	 */
	public static Resource convertToEMFResource( final IResource file ) {
		return convertToEMFResource( file, null );
	}

	public static Resource convertToEMFResource( 	final IResource file,
													ResourceSet resourceSet ) {
		assert file != null : "IDE file cannot be null.";
		final IPath path = file.getFullPath();
		final URI uri = URI.createPlatformResourceURI( path.toString(), false );
		resourceSet =  resourceSet == null ? new ResourceSetImpl() : resourceSet;

		try {
			if ( resourceSet.getURIConverter().exists( uri, null ) ) {
				return resourceSet.getResource( uri, true );
			}
			
			return null;
		}
		catch ( final RuntimeException ex ) {
			if ( ex.getCause() instanceof SAXParseException || ex.getCause() instanceof PackageNotFoundException ) {
				return null;
			}
				
			throw ex;
		}
	}
	
	public static boolean equalsByURI( 	final EObject object1,
										final EObject object2 ) {
		if ( object1 == null ) {
			return object2 == null;
		}

		if ( object2 == null ) {
			return object1 == null;
		}

		return EcoreUtil.getURI( object1 ).equals( EcoreUtil.getURI( object2 ) );
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T selectedObject(	final ISelection selection,
										final Class<T> objClass ) {
		T newObject = null;

		if ( selection instanceof IStructuredSelection ) {
			final IStructuredSelection structSel = (IStructuredSelection) selection;
			
			if ( structSel.size() == 1 ) {
				final Object element = AdapterFactoryEditingDomain.unwrap( ( (IStructuredSelection) selection ).getFirstElement() );

				if ( objClass.isInstance( element ) ) {
					newObject = (T) element;
				}
				else if ( element instanceof IAdaptable ) {
					newObject = (T) ( (IAdaptable) element ).getAdapter( objClass );
				}
			}
		}
		
		return newObject;
	}
	
    @SuppressWarnings("unchecked")
	public static <T> Collection<T> selectedObjects(	final ISelection selection,
														final Class<T> objClass ) {
		Collection<T> objects = new LinkedHashSet<T>();

		if ( selection instanceof IStructuredSelection ) {
			for ( final Object selElement : ( (IStructuredSelection) selection ).toList() ) {
				final Object element = AdapterFactoryEditingDomain.unwrap( selElement );

				if ( objClass.isInstance( element ) ) {
					objects.add( (T) element );
				}
				else if ( element instanceof IAdaptable ) {
					final T adaptedElement = (T) ( (IAdaptable) element ).getAdapter( objClass );
					
					if ( adaptedElement != null ) {
						objects.add( (T) adaptedElement );
					}
				}
			}
		}
		
		return objects;
	}

    public static Set<EObject> getAllRootReferredObjects( final EObject object ) {
    	final Set<EObject> objects = new HashSet<EObject>();
		final EObject rootElement = EcoreUtil.getRootContainer( object );
		
		if ( rootElement.eResource() != null ) {
			for ( final Resource resource : allReferencedExtents( rootElement.eResource() ) ) {
				objects.addAll( resource.getContents() );
			}
		}
		
		return objects;
    }
	
	public static Set<Resource> allReferencedExtents( final Resource resourceExtent ) {
		final Map<EObject, Collection<EStructuralFeature.Setting>> externalCrossReferences = new EcoreUtil.ExternalCrossReferencer( resourceExtent ) {
	          private static final long serialVersionUID = 1L;
	          @Override
	          public Map<EObject, Collection<EStructuralFeature.Setting>> findExternalCrossReferences()
	          {
	            return super.findExternalCrossReferences();
	          }
	        }.findExternalCrossReferences();
	        
		final Set<Resource> referencedExtents = new HashSet<Resource>();

		for ( final EObject object : externalCrossReferences.keySet() ) {
	    	final Resource resource = object.eResource();
	    	
	    	if ( resource != null ) {
	    		referencedExtents.add( resource );
	    	}
	    }

//		final Set<Resource> referencedExtents = new HashSet<Resource>();
//		final Iterator<EObject> contents = resourceExtent.getAllContents();
//		
//		while ( contents.hasNext() ) {
//			referencedExtents.addAll( allReferencedExtents( contents.next(), new HashSet<EObject>() ) );
//		}

		return referencedExtents;
	}
//
//	private static Set<Resource> allReferencedExtents( 	final EObject object,
//														final Set<EObject> traversedObjects ) {
//		final Set<Resource> referencedExtents = new HashSet<Resource>();
//
//		if ( object != null && !traversedObjects.contains( object ) ) {
//			traversedObjects.add( object );
//			
//			if ( object.eResource() != null ) {
//				referencedExtents.add( object.eResource() );
//			}
//
//			for ( final EObject refObject : object.eCrossReferences() ) {
//				referencedExtents.addAll( allReferencedExtents( refObject, traversedObjects ) );
//			}
//		}
//		
//		return referencedExtents;
//	}
	
	/**
	 * Check that the object is an instance of any of the specified classes. Uses the EMF API.
	 * @param object
	 * @param classes
	 * @return
	 */
	public static boolean isInstance( 	final EObject object,
										final Collection<EClass> classes ) {
		for ( final EClass eClass : classes ) {
			if ( eClass.isInstance( object ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public static <T extends EObject> T findEObjectById( 	final Object id,
															final EAttribute attribute,
															final EList<T> extendingList ) {
		for ( final T object : extendingList ) {
			if ( id.equals( object.eGet( attribute ) ) ) {
				return object;
			}
		}
		
		return null;
	}
	
	public static boolean isReadOnly( final URI uri ) {
		return isReadOnly( uri, URIConverter.INSTANCE/*new ExtensibleURIConverterImpl()*/ );
	}
	
	public static boolean isReadOnly( 	final URI uri,
										final URIConverter uriConverter ) {
		final Map<String, ? extends Object> resAtt = uriConverter.getAttributes( uri, null );
		
		return Boolean.TRUE.equals( resAtt.get( URIConverter.ATTRIBUTE_READ_ONLY ) );
	}
	
	public static boolean isReadOnly( final Resource resource ) {
		final ResourceSet resSet = resource.getResourceSet();
		final URIConverter converter = resSet == null ? URIConverter.INSTANCE/*new ExtensibleURIConverterImpl()*/ : resSet.getURIConverter();
		
		return isReadOnly( resource.getURI(), converter );
	}
	
	public static void setReadOnly( final Resource resource ) 
	throws IOException {
		final ResourceSet resSet = resource.getResourceSet();
		final URIConverter converter = resSet.getURIConverter();
		@SuppressWarnings("unchecked")
		final Map<String, Object> resAtt = (Map<String, Object>) converter.getAttributes( resource.getURI(), null );
		
		resAtt.put( URIConverter.ATTRIBUTE_READ_ONLY, Boolean.TRUE );
		converter.setAttributes( resource.getURI(), resAtt, null );
	}
	
	public static void copyResourceContent( final Resource sourceResource,
											final Resource targetResource,
											final Map<?, ?> options ) {
		if ( sourceResource.getContents().isEmpty() ) {
			targetResource.getContents().clear();
		}
		else {
			final ByteArrayOutputStream outStr = new ByteArrayOutputStream();
			
			try {
				sourceResource.save( outStr, options );
				final ByteArrayInputStream inputStr = new ByteArrayInputStream( outStr.toByteArray() );
				
				// If the resource was already loaded, it will not be loaded with the new content so
				// unload it first.
				targetResource.unload();
				targetResource.load( inputStr, targetResource.getResourceSet().getLoadOptions() );
			}
			catch( final IOException ex ) {
				throw new RuntimeException( ex );
			}
		}
	}
	
	public static final boolean setIntoParent( 	final EObject parent,
												final Object child,
												final EStructuralFeature abstractFeature ) {
		final EStructuralFeature featureToSet = getConcreteFeature( parent, abstractFeature, child );
		
		if ( featureToSet == null ) {
			return false;
		}

		
		if ( featureToSet.isMany() ) {
			@SuppressWarnings("unchecked")
			final EList<Object> targetList = (EList<Object>) parent.eGet( featureToSet );
			targetList.add( child ); 
		}
		else {
			parent.eSet( featureToSet, child );
		}
		
		return true;
	}
	
	public static EStructuralFeature getConcreteFeature( 	final Object sourceInstanceObject,
															final EStructuralFeature feature,
															Object targetObject ) {
		EStructuralFeature closestFeature = feature;
		Class<?> closestFeatureClass = closestFeature.getEType().getInstanceClass();

		if ( closestFeature.isDerived() ) {
			if ( targetObject == null ) {
				if ( feature.isMany() ) {
					throw new IllegalStateException( "Features with multiplicity greater than 1 not handled." );
				}
				
				final Object actualObject = ( (EObject) sourceInstanceObject ).eGet( feature );
				
				if ( actualObject == targetObject ) {
					// Return a null feature meaning that nothing needs to be set.
					return null;
				}
				
				targetObject = actualObject;
			}

			final EClass sourceObjectClass = ( (EObject) sourceInstanceObject ).eClass();
			
			for ( final EStructuralFeature sourceFeature : sourceObjectClass.getEAllStructuralFeatures() ) {
				if ( sourceFeature.isChangeable() && !sourceFeature.isDerived() && sourceFeature.getEType().isInstance( targetObject ) ) {
					final Class<?> currentFeatureClass = sourceFeature.getEType().getInstanceClass();
					final int comparison = classHierarchyComp.compare( closestFeatureClass, currentFeatureClass );
					
					// If the classes are not related (equals), use the current class because it ensure the starting feature is replaced with
					// one that is changeable.
					if ( comparison >= 0 ) {
						closestFeature = sourceFeature;
						closestFeatureClass = closestFeature.getEType().getInstanceClass();
					}
				}
			}
		}

		return closestFeature;
	}
	
	public static boolean dependsOn( 	final Resource resource1,
										final Resource resource2 ) {
		return ResourceDepComparator.dependsOn( resource1, resource2 );
	}

	/**
	 * Return a collection of objects not contained in the specified resource and referred by the
	 * specified object, either directly or through references to other objects contained in the
	 * specified resource.
	 * @param containingRes
	 * @param object
	 * @return
	 * @throws IllegalStateException
	 */
	public static Collection<EObject> externalReferences(	final Resource containingRes,
															final EObject object ) {
		return externalReferences( containingRes, object, new HashSet<EObject>() );
	}

	/**
	 * Return a collection of objects not contained in the specified resource and referred by the
	 * specified object, either directly or through references to other objects contained in the
	 * specified resource.
	 * @param parentRefRes
	 * @param object
	 * @param travObjects
	 * @return
	 * @throws IllegalStateException
	 */
	private static Collection<EObject> externalReferences(	final Resource parentResource,
															final EObject object,
															final Set<EObject> travObjects ) {
		final Collection<EObject> refs = new HashSet<EObject>();
		
		if ( !travObjects.contains( object ) ) {
			travObjects.add( object );
			final Resource crossRes = object.eResource();
			
			if ( crossRes == null ) {
				throw new IllegalStateException( "Object " + EcoreUtil.getURI( object ) + " is not contained in a resource!" );
			}
	
			// The object is outside so we need it
			if ( crossRes != parentResource ) {
				refs.add( object );
			}
			else {
				// The object is inside so we need to see if any of its cross references has a reference to the outside
				final EObject container = object.eContainer();
				
				for ( final EObject refObject : object.eCrossReferences() ) {
					if ( refObject != container ) {
						refs.addAll( externalReferences( parentResource, refObject, travObjects ) );
					}
				}
			}
		}
		
		return refs;
	}

	public static void unloadResource( 	final Resource resource,
										final boolean pb_unloadDepRes ) {
		if ( pb_unloadDepRes ) {
			// Remove all depending resources
			for ( final Resource cachedRes : new ArrayList<Resource>( resource.getResourceSet().getResources() ) ) {
				if ( cachedRes != resource && dependsOn( cachedRes, resource ) ) {
					unloadResource( cachedRes );
				}
			}
		}
		
		unloadResource( resource );
	}

	public static void unloadResource( final Resource resource ) {
		if ( resource.isLoaded() ) {
			resource.unload();
		}
		
		resource.getResourceSet().getResources().remove( resource );
	}

	@SuppressWarnings("unchecked")
	public static <T> T getFirstObjectOfType(	final Resource resource,
												final EClassifier type ) {
		final Iterator<EObject> contentIt = resource.getAllContents();
		
		while ( contentIt.hasNext() ) {
			final EObject element = contentIt.next();

			if ( type.isInstance( element ) ) {
		        return (T) element;
		     }
		}
		
		return null;
	}

	public static String getFullPath( final String objectId ) 
	throws IOException {
		return getFullPath( null, objectId, URI.createURI( objectId ) );
	}
	
	public static String getFullPath( 	final EObject context,
										final String objectId,
										final URI resUri ) 
	throws IOException {
		if ( resUri == null ) {
			return null;
		}
		
		if ( resUri.isPlatform() ) {
			if ( resUri.isPlatformResource() ) { 
				final String projectName = getProjectName( resUri );
				final IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject( projectName );
	
				if ( project == null || project.getLocation() == null ) {
					throw new IllegalArgumentException( "Resource project \"" + projectName + "\" not found in resource \"" + resUri + "\". Please check resource name." );
				}
				
				return project.getLocation() + resUri.toPlatformString( true ).replaceFirst( projectName, "" ); 
			}
			
			if ( resUri.isPlatformPlugin() ) {
				final String bundleId = resUri.segment( 1 );
				final StringBuffer endSegments = new StringBuffer();
				
				for ( int index = 2; index < resUri.segmentCount(); index++ ) {
					endSegments.append( resUri.segment( index ) );
					
					if ( index != resUri.segmentCount() - 1 ) {
						endSegments.append( File.separator );
					}
				}
				
				return BundleUtil.getBundleCanonicalEntryPath( bundleId, endSegments.toString() );
			}
			
			try {
				final PlatformURLHandler urlHandler = new PlatformURLHandler();
				final PlatformURLConnection connection = (PlatformURLConnection) urlHandler.openConnection( new URL( resUri.toString() ) ); 
					
				return connection.getResolvedURL().getPath();
			}
			catch ( final MalformedURLException p_ex ) {
				throw new IOException( p_ex );
			}
		}
		else if ( resUri.isFile() ) {
			if ( 	resUri.scheme() == null && 
					context != null &&
					context.eResource() != null ) {// Assume platform if no scheme
				final URI projectURI = context.eResource().getURI();
				final String projectName;
				
				if ( projectURI != null && projectURI.segmentCount() != 0 ) {
					projectName = getProjectName( projectURI );
				}
				else {
					if ( objectId == null ) {
						projectName = null;
					}
					else {
						projectName = objectId.substring( 0, objectId.substring( 1 ).indexOf( "/" ) + 1 );
					}
				}
				
				if ( projectName == null ) {
					return resUri.toString();
				}
				
				return getFullPath( 	context, 
										objectId,
										URI.createPlatformResourceURI( projectName + resUri.toString(), true ) );
			}

			return resUri.toFileString();
		}
		
		return resUri.toString();
	}
	
	protected static String getProjectName( final URI uri ) {
		if ( uri == null || !uri.isPlatform() || uri.segmentCount() < 2  ) {
			throw new IllegalArgumentException( "URI cannot be null and its scheme must be platform." );
		}
		
		return /*SEGMENT_SEPARATOR +*/ uri.segment( 1 );
	}
}