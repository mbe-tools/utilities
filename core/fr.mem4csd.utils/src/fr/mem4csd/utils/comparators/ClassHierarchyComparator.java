package fr.mem4csd.utils.comparators;

import java.util.Comparator;

public class ClassHierarchyComparator implements Comparator<Class<?>> {
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 * null is considered as the highest
	 */
	@Override
    public int compare(	final Class<?> class1, 
    					final Class<?> class2 ) {
		if( class1 == null ) {
            return class2 == null ? 0 : 1;
        }

        // At this point, we know that c1 and c2 are not null
        if ( class1.equals( class2 ) ) {
            return 0;
        }

        // At this point, c1 and c2 are not null and not equal, here we
        // compare them to see which is "higher" in the class hierarchy
        boolean c1Lower = class2.isAssignableFrom( class1 );
        boolean c2Lower = class1.isAssignableFrom( class2 );

        if ( c1Lower && !c2Lower ) {
            return - 1;
        } 
        
        if ( c2Lower && !c1Lower ) {
            return 1;
        } 

        // Doesn't matter, sort consistently on classname
        return class1.getName().compareTo( class2.getName() );
    }
}
