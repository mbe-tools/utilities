package fr.mem4csd.utils;

public class SystemUtil {

	public static String getOSName() {
		return System.getProperty( "os.name" );
	}
	
	public static boolean isLinuxLike() {
	    String osName = getOSName().toLowerCase();

	    return osName != null && osName.contains( "linux" );
	}

	public static boolean isWindowsLike() {
	    String osName = getOSName().toLowerCase();

	    return osName != null && osName.contains( "win" );
	}

	public static boolean isMacLike() {
	    final String osName = getOSName().toLowerCase();

	    return osName != null && osName.contains( "mac" );
	}

	public static boolean isJavaVersionLessThan( double version ) {
	    final String systemVersion = System.getProperty( "java.version" );

	    return systemVersion != null && Double.parseDouble( systemVersion ) >= version;
	}
	
	public static String getUserDir() {
		return System.getProperty( "user.dir" );
	}
	
	public static void setUserDir( final String p_dir ) {
		System.setProperty( "user.dir", p_dir );
	}
	
	public static String getTempDir() {
		return System.getProperty( "java.io.tmpdir" );
	}

	public static void setTempDir( final String p_dir ) {
		System.setProperty( "java.io.tmpdir", p_dir );
	}
	
	public static Integer getPermissionErrorCode() {
		if ( isLinuxLike() ) {
			return 126;
		}
		
		if ( isWindowsLike() ) {
			return 5;
		}
		
		return null;
	}
}
