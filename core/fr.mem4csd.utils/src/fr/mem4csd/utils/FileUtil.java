package fr.mem4csd.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class FileUtil {
	
	public static interface ICopyPolicy {
		
		boolean canOverride( final File targetFile );
		
		void postCopy( final File targetFile );
	};

	public static List<File> deleteFile( 	String filePath,
											boolean pb_recursive ) {
		List<File> deletedFiles = new ArrayList<File>();

		File file = new File( filePath );
		deleteFile( file, pb_recursive, deletedFiles );
		
		return deletedFiles;
	}
	
	public static void deleteFile( 	File file,
									boolean recursive,
									List<File> deletedFiles ) {
		if ( recursive && file.isDirectory() ) {
			File[] content = file.listFiles();
			
			for ( final File dirFile : content ) {
				deleteFile( dirFile, recursive, deletedFiles );
			}
		}

		deleteSingleFile( file, deletedFiles );
	}
	
	private static void deleteSingleFile( 	final File file,
											final List<File> deletedFiles ) {
		if ( file.isDirectory() ) {
			file.delete();
		}
		else if ( file.delete() ) {
			deletedFiles.add( file );
		}
	}
	
	public static boolean deleteSingleFile( final String fileName ) {
		return new File( fileName ).delete();
	}
	
	/**
	 * Copy the content of an input file into an output file. Also handles directories.
	 * @param inFile
	 * @param outFile
	 * @throws IOException
	 */
	public static void copyFile( 	final File inFile,
									final File outFile ) 
	throws IOException {
		copyFile( inFile, outFile, null );
	}
		
	public static void copyFile( 	final File inFile,
									final File outFile,
									final ICopyPolicy copyPolicy )
	throws IOException {
		if ( inFile.isFile() ) {
			copyFile( inFile.getPath(), outFile.getPath(), copyPolicy );
		}
		else if ( inFile.isDirectory() ) {
			final boolean canCopyContent;
			
			if ( outFile.exists() ) {
				canCopyContent = copyPolicy == null || copyPolicy.canOverride( outFile );
    		}
			else {
				canCopyContent = outFile.mkdir();
			}
	 
			if ( canCopyContent ) {
	    		for ( final String file : inFile.list() ) {
	    		   final File srcFile = new File( inFile, file );
	    		   final File destFile = new File( outFile, file );
	    		   copyFile( srcFile, destFile, copyPolicy );
	    		}
			}
		}
	}

	public static void copyFile( 	final String inFileName,
									final String outFileName ) 
	throws IOException {
		copyFile( inFileName, outFileName, null );
	}

	public static void copyFile( 	final String inFileName,
									final String outFileName,
									final ICopyPolicy copyPolicy ) 
	throws IOException {
		final File outFile = new File( outFileName );

		if ( copyPolicy != null && copyPolicy.canOverride( outFile ) ) {
			if ( !outFile.canWrite() ) {
				outFile.setWritable( true );
			}
			
			FileReader inFileWriter = null;
			FileWriter outFileWriter  = null;

			try {
				inFileWriter = new FileReader( inFileName );
				outFileWriter = new FileWriter( outFile );
				int ch;
		    
			    while ( ( ch = inFileWriter.read() ) != -1 ) {
			    	outFileWriter.write( ch );
			    }
			}
			finally {
				try {
					closeFile( inFileWriter );
				}
				finally {
					closeFile( outFileWriter );
					
					if ( copyPolicy != null ) {
						copyPolicy.postCopy( outFile );
					}
				}
			}
		}
	}
	
	private static void closeFile( final Closeable closeable )
	throws IOException {
		if ( closeable != null ) {
			closeable.close();
		}
	}
	
	public static String getNameWithoutExtension( final String fileName ) {
		return removeExtension( new File( fileName ).getName() );
	}

	public static String removeExtension( final String fileName ) {
		return removeExtension( new File( fileName ) );
	}
	
	public static String removeExtension( final File file ) {
		final String rootName = file.getPath();
		final int indexExt = rootName.lastIndexOf( '.' );
		
		if ( indexExt > - 1 ) {
			return rootName.substring( 0, indexExt );
		}
		
		return rootName;
	}

	public static String replaceExtension( 	final String fileName,
											final String newExt ) {
		return fileName.substring( 0, fileName.lastIndexOf( '.' ) ) + "." + newExt;
	}
	
	public static String getFileContent( final String filename ) 
	throws IOException {
		BufferedReader reader = null;
		
		try {
			final int size = 1024;
			reader = new BufferedReader( new FileReader( filename ), size );
			char[] buffer = new char[ 1 ];
			final StringBuffer content = new StringBuffer();
			
		    while ( reader.read( buffer ) != -1 ) {
		    	content.append( buffer );
		    }
		    
		    return content.toString();
		}
		finally {
			if ( reader != null ) {
				reader.close();
			}
		}
	}
	
	public static String getFileContent( 	final String filename,
											final int pi_startPos,
											final int pi_length ) 
	throws IOException {
		return getContent( new FileInputStream( filename ), pi_startPos, pi_length );
	}
	
	public static String getContent( final InputStream inputStream ) 
	throws IOException {
		final Reader reader = new InputStreamReader( inputStream );
		
		try {
			char[] buffer = new char[ 1 ];
			final StringBuffer content = new StringBuffer();
			
		    while ( reader.read( buffer ) != -1 ) {
		    	content.append( buffer );
		    }
		    
		    return content.toString();
		}
		finally {
			if ( reader != null ) {
				reader.close();
			}
		}
	}

	public static String getContent( 	final InputStream inputStream,
										final int pi_startPos,
										final int pi_length ) 
	throws IOException {
		InputStreamReader reader = null;
		
		try {
			reader = new InputStreamReader( inputStream );
			char[] bytes = new char[ pi_length ];
			
			skip( reader, pi_startPos );
			
			read( reader, bytes, pi_length );
			
			return new String( bytes );
		}
		finally {
			if ( reader != null ) {
				reader.close();
			}
		}
	}

	public static void skip(	final InputStreamReader reader,
								int pi_startPos )
	throws IOException {
		long skipped = 0;
		
		while( skipped >= 0 && pi_startPos > 0 && reader.ready() ){
			skipped = reader.skip( pi_startPos );
			
			if( skipped > 0 ){
				pi_startPos -= skipped;
			}
		}
	}
	
	public static void read( 	final InputStreamReader reader,
								final char[] pc_bytes,
								int pi_length )
	throws IOException {
		int bufferOffset = 0;
		int charactersRead = 0;
		
		while( charactersRead >= 0 && pi_length > 0 ){
			charactersRead = reader.read( pc_bytes, bufferOffset, pi_length );
			
			if ( charactersRead > 0 ){
				bufferOffset += charactersRead;
				pi_length -= charactersRead;
			}
		}
	}

	/**
	 * Returns a list of all files of the specified type (extension) in the specified directory and its child directories.
	 * @param directoryName The root directory to be searched. 
	 * @param type The file extension without the dot.
	 * @return
	 */
	public static List<File> findFilesOfTypeInDirectory( 	final String directoryName,
															final String type ) {
		return findFilesOfTypeInDirectory( directoryName, type, false );
	}
	
	/**
	 * Returns a list of all files of the specified type (extension) in the specified directory.
	 * @param directoryName The root directory to be searched. 
	 * @param type The file extension without the dot.
	 * @param pb_recursive If true, subdirectories will also be searched.
	 * @return
	 */
	public static List<File> findFilesOfTypeInDirectory( 	final String directoryName,
															final String type,
															final boolean pb_recursive ) {
		final List<File> files = new ArrayList<File>();
		
		for ( final File foundFile : findFilesOfTypeAndDirsInDirectory( directoryName, type, pb_recursive ) ) {
			if ( foundFile.isFile() ) {
				files.add( foundFile );
			}
		}
		
		return files;
	}
	
	private static File[] findFilesOfTypeAndDirsInDirectory( 	final String directoryName,
																final String type,
																final boolean pb_recursive ) {
		final File srcDir = new File( directoryName );
		
		if ( !srcDir.isDirectory() ) {
			throw new IllegalArgumentException( "File " + directoryName + " is not a directory." );
		}
		
		final File[] files;
		
		if ( type == null ) {
			files = srcDir.listFiles();
		}
		else {
			final String extension = "." + type;
			files = srcDir.listFiles( new FileFilter() {
				
				@Override
				public boolean accept( final File pathname ) {
					return pathname.isDirectory() || pathname.getName().endsWith( extension ) ;
				}
			} );
		}
		
		final Collection<File> foundFiles = new HashSet<File>( Arrays.asList( files ) );
		
		if ( pb_recursive ) {
			for ( final File file : files ) {
				if ( file.isDirectory() ) {
					foundFiles.addAll( Arrays.asList( findFilesOfTypeAndDirsInDirectory( file.getPath(), type, pb_recursive ) ) );
				}
			}
		}
		
		return foundFiles.toArray( new File[ foundFiles.size() ] );
	}
	
	public static void writeToFile( final String fileName,
									final String content )
	throws IOException {
		final FileWriter writer = new FileWriter( fileName );
		
		try {
			writer.write( content );
		}
		finally {
			writer.close();
		}
	}
	
	public static void writeToFile( final String fileName,
									final byte[] content )
	throws IOException {
		final FileOutputStream writer = new FileOutputStream( fileName );
		
		try {
			writer.write( content );
		}
		finally {
			writer.close();
		}
	}
	
	public static String getDevice( final File file ) {
		final String path = file.getPath();
		
		int index = path.indexOf( ":" );
		
		if ( index == - 1 ) {
			return null;
		}
		
		return path.substring( 0, index + 1);
	}
}
