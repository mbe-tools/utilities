package fr.mem4csd.utils.systemcommand;

public interface ISystemCommandExceptionHandler {
	
	void handleReturnCode( 	int returnCode,
							String command ) 
	throws SystemCommandException;
	
	void handleException( 	Throwable th,
							String command ) 
	throws SystemCommandException;
}
