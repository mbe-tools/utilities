/*******************************************************************************
 * Copyright (c) 2011 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
package fr.mem4csd.utils.systemcommand;

public class SystemCommandException extends Exception {

	private static final long serialVersionUID = 8653466824403695562L;
	
	private final Integer errorCode;

	public SystemCommandException( final Integer errorCode ) {
		this( errorCode, null );
	}

	public SystemCommandException( 	final Integer errorCode,
									final String message ) {
		super( message );
		
		this.errorCode = errorCode;
	}

	public SystemCommandException( final String message ) {
		this( message, (Throwable) null );
	}

	public SystemCommandException( final Throwable cause ) {
		this( null, cause );
	}

	public SystemCommandException(	final String message, 
									final Throwable cause ) {
		super( message, cause );
		
		errorCode = null;
	}

	public Integer getErrorCode() {
		return errorCode;
	}
}
