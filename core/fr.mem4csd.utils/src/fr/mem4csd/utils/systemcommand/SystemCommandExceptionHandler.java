/*******************************************************************************
 * Copyright (c) 2011 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
package fr.mem4csd.utils.systemcommand;


public class SystemCommandExceptionHandler implements ISystemCommandExceptionHandler {

	private final int okRetCode;

	public SystemCommandExceptionHandler( final int okRetCode ) {
		super();
		
		this.okRetCode = okRetCode;
	}
	
	@Override
	public void handleReturnCode( 	final int returnCode,
									final String command )
	throws SystemCommandException {
		if ( returnCode != getOkRetCode() ) {
			final StringBuffer errorMessage = new StringBuffer( "Error executing system command " + command + "." );
			errorMessage.append( " Return code : " + returnCode );
				
			throw new SystemCommandException( returnCode, errorMessage.toString() );
		}
	}
	
	@Override
	public void handleException( 	final Throwable th,
									final String command ) 
	throws SystemCommandException {
	      if ( !( th instanceof SystemCommandException ) ) {
	    	  throw new SystemCommandException( "Error executing command " + command, th );
	      } 
	      
	      throw (SystemCommandException) th;
	}

	protected int getOkRetCode() {
		return okRetCode;
	}
}
