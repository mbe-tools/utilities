/*******************************************************************************
 * Copyright (c) 2011 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
package fr.mem4csd.utils.systemcommand;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import fr.mem4csd.utils.SystemUtil;

/**
 * This class is used to execute a process that is external to the Java runtime VM. 
 */
public class SystemCommand {

	protected static final Level logLevelDefault = Level.INFO;

	protected static final String LINE_SEPARATOR = System.lineSeparator();

	private String command;

	private String commandArgs = "";

	private final String[] envParams;
	
	private File workingDir = null;
	
	private final PrintStream outStream;

	private final PrintStream errStream;

	private Level logLevel = logLevelDefault;
	
	private boolean loggingLineBreaks;

	private Process process;

	private boolean destroyed = false;

	private ProcessIO processOutStream = null;
	private ProcessIO processErrStream = null;

	private PrintWriter console = null;
	
	private final ISystemCommandExceptionHandler exceptionHandler;

	public SystemCommand( 	final String command,
							final String commandArgs,
							final PrintStream printStream,
							final PrintStream errStream  ) {
		this( command, commandArgs, (String) null, printStream, errStream );
	}

	public SystemCommand( 	final String command, 
							final String commandArgs,
							final String workingDir,
							final PrintStream printStream,
							final PrintStream errStream ) {
		this( command, commandArgs, null, workingDir, printStream, errStream, logLevelDefault );
	}

	public SystemCommand( 	final String command, 
							final String commandArgs,
							final String[] envParams,
							final PrintStream printStream,
							final PrintStream errStream,
							final Level logLevel ) {
		this( command, commandArgs, envParams, null, printStream, errStream, logLevel );
	}

	public SystemCommand( 	final String command, 
							final String commandArgs,
							final String[] envParams,
							final String workingDir,
							final PrintStream outStream,
							final PrintStream errStream,
							final Level logLevel ) {
		this.command = command;
		this.commandArgs = commandArgs;
		this.workingDir = new File( workingDir );
		this.outStream = outStream;
		this.errStream = errStream;
		loggingLineBreaks = false;

		// Adds the working directory to the if some environment parameters are passed, do nothing 
		if ( SystemUtil.isLinuxLike() && envParams == null ) {
			final List<String> params = new ArrayList<String>();
			
			for ( final Map.Entry<String, String> entry : System.getenv().entrySet() ) {
				params.add( entry.getKey() + "=" + entry.getValue() );
			}

			final String execDir = new File( command ).getParent();
			final String extraDirs = execDir + ":" + workingDir;
			final String exist = System.getenv( "LD_LIBRARY_PATH" );
			params.add( "LD_LIBRARY_PATH=" + ( exist == null ? extraDirs : exist + ":" + extraDirs ) );
			this.envParams = params.toArray( new String[ params.size() ] );
		}
		else {
			this.envParams = envParams;
		}

		exceptionHandler = createExceptionHandler();
	}
	
	protected ISystemCommandExceptionHandler createExceptionHandler() {
		return new SystemCommandExceptionHandler( 0 );
	}
	

	/**
	 * @param waitToFinish True if the thread must wait for the external process
	 * to finish before returning, False otherwise
	 * @return status returned from the external process or 0(successful)
	 */
	public Object execute( boolean waitToFinish ) 
	throws InterruptedException, SystemCommandException {
		final String fullCommand = getFullCommand();
		Integer retCode = null;

		if ( outStream != null ) {
			outStream.println( "Executing system command " + fullCommand );
		}

		try {
			final Runtime rt = Runtime.getRuntime();

			if ( SystemUtil.isLinuxLike() ) {
				process = rt.exec( "/bin/bash", envParams, workingDir );
				startIOs( process );
				console = new PrintWriter( 	new BufferedWriter( new OutputStreamWriter( process.getOutputStream() ) ), 
											true ); 
				console.println( "cd " + workingDir );
				console.println( fullCommand );
				console.println( "exit" );
			}
			else {
				console = null;
				process = rt.exec( fullCommand, envParams, workingDir );
				startIOs( process );
			}

			retCode = waitToFinish ? process.waitFor() : process.exitValue();

			while( areIOsProcessesAlive() ) {
				Thread.sleep( 10 );
			}

			if ( destroyed ) {
				destroyed = false;

				throw new InterruptedException( "Process " + command + " stopped successfully." );
			}
			
			handleReturnCode( retCode );
		}
		catch ( final InterruptedException ex ) {
			throw ex;
		}
		catch ( final IOException ex ) {
			throw new SystemCommandException( ex );
		}

		return retCode;
	}
	
	protected void handleReturnCode( final int retCode ) 
	throws SystemCommandException {
		getExceptionHandler().handleReturnCode( retCode, getFullCommand() );
	}

	public void stop() {
		if ( process != null ) {
			if ( console != null && SystemUtil.isWindowsLike() ) {
				console.println( (char)3 );
			}
			
			destroyed = true;
			process.destroy();
			stopIOs();
			outStream.println( "Process " + command + " killed upon user requests." );
		}
	}

	protected void startIOs( final Process process ) {
		processErrStream = buildProcessIO( process.getErrorStream(), Level.SEVERE );
		processOutStream = buildProcessIO( process.getInputStream(), Level.INFO );
		processErrStream.start(); // process any errors from the command
		processOutStream.start(); // process any output from the command
	}

	protected void stopIOs() {
		if ( processErrStream != null ) {
			processErrStream.end();
		}

		if ( processOutStream != null ) {
			processOutStream.end();
		}
	}

	private boolean areIOsProcessesAlive() {
		if ( processErrStream == null ) {
			return processOutStream != null && processOutStream.isAlive();
		}

		if ( outStream == null ) {
			return processErrStream != null && processErrStream.isAlive();
		}

		return processErrStream.isAlive() || processOutStream.isAlive();
	}

	protected class ProcessIO extends Thread {
		private final InputStream inputStr;
		private final Level streamType;
		private boolean stopRequested = false;

		protected ProcessIO( 	final InputStream inputStr, 
								final Level type ) {
			this.inputStr = inputStr;
			this.streamType = type;
		}

		@Override
		public void run() {
			try {
				final InputStreamReader reader = new InputStreamReader( inputStr );
				final BufferedReader buffReader = new BufferedReader( reader );
				String streamData = null;

				while( !stopRequested && ( streamData = buffReader.readLine() ) != null ) {
					log( streamType, streamData );
				}
			}
			catch( final IOException ex ) {
				errStream.println( "Error processing " + streamType + " stream" );
				
				ex.printStackTrace();
			}
		}

		public void end() {
			stopRequested = true;
		}
	}

	protected void log( final Level level,
						final String line ) {
		if ( !"".equals( line ) ) {
			final int i_level = level.intValue();

			if ( i_level >= logLevel.intValue() ) {
				if ( i_level < Level.OFF.intValue() ) {
					if ( i_level < Level.SEVERE.intValue() ) {
						if ( i_level < Level.WARNING.intValue() ) {
							outStream.println( line );
						}
						else {
							outStream.println( line );
						}
					}
					else {
						errStream.println( line );
					}
				}
			}
		}
	}

	protected ProcessIO buildProcessIO( final InputStream inputStr, 
										final Level logLevel ) {
		return new ProcessIO( inputStr, logLevel );
	}

	public Level getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(Level logLevel) {
		this.logLevel = logLevel;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getFullCommand() {
		return command + ( commandArgs == null ? "" : ( " " + commandArgs ) );
	}

	public File getWorkingDir() {
		return workingDir;
	}

	public void setWorkingDir(File workingDir) {
		this.workingDir = workingDir;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer( "System Command: " );
		buffer.append( getCommand() );
		buffer.append( LINE_SEPARATOR );
		buffer.append( "Arguments: " );
		buffer.append( getCommandArgs() );

		return buffer.toString();
	}

	protected String getCommandArgs() {
		return commandArgs;
	}

	protected void setCommandArgs(String commandArgs) {
		this.commandArgs = commandArgs;
	}
	
	public boolean isLoggingLineBreaks() {
		return loggingLineBreaks;
	}
	
	protected void setLoggingLineBreaks( final boolean pb_loggingLineBreaks ) {
		loggingLineBreaks = pb_loggingLineBreaks;
	}

	public ISystemCommandExceptionHandler getExceptionHandler() {
		return exceptionHandler;
	}
}
