package fr.mem4csd.util.exception;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseExceptionHandler implements IExceptionHandler {

	private final transient Logger logger;

	public BaseExceptionHandler( final Logger logger ) {
		this.logger = logger == null ? Logger.getGlobal() : logger;
	}
	 
	@Override
	public void handleException( 	final Throwable p_th, 
									final Object p_info) {
		getLogger().log( Level.SEVERE, String.valueOf( p_info ), p_th );
	}
	
	protected Logger getLogger() {
		return logger;
	}

	@Override
	public Object handleExceptionWithReturn(	final Throwable p_th,
												final Object p_info ) {
		handleException( p_th, p_info );
		
		return p_th.getLocalizedMessage();
	}
}
