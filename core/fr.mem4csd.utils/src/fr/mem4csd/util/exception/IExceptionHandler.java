package fr.mem4csd.util.exception;

public interface IExceptionHandler {
	
	void handleException( 	Throwable p_th,
							Object p_info );
	
	Object handleExceptionWithReturn( 	Throwable p_th,
										Object p_info );
}
