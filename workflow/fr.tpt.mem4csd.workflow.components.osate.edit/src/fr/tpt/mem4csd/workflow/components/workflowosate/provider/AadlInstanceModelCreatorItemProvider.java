/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.mdelab.workflow.components.provider.WorkflowComponentItemProvider;
import fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator;
import fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage;

/**
 * This is the item provider adapter for a {@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AadlInstanceModelCreatorItemProvider extends WorkflowComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AadlInstanceModelCreatorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSystemImplementationNamePropertyDescriptor(object);
			addPackageModelSlotPropertyDescriptor(object);
			addSystemInstanceModelSlotPropertyDescriptor(object);
			addSavePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the System Implementation Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystemImplementationNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AadlInstanceModelCreator_systemImplementationName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AadlInstanceModelCreator_systemImplementationName_feature", "_UI_AadlInstanceModelCreator_type"),
				 WorkflowosatePackage.Literals.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Package Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPackageModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AadlInstanceModelCreator_packageModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AadlInstanceModelCreator_packageModelSlot_feature", "_UI_AadlInstanceModelCreator_type"),
				 WorkflowosatePackage.Literals.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the System Instance Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystemInstanceModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AadlInstanceModelCreator_systemInstanceModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AadlInstanceModelCreator_systemInstanceModelSlot_feature", "_UI_AadlInstanceModelCreator_type"),
				 WorkflowosatePackage.Literals.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Save feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSavePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AadlInstanceModelCreator_save_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AadlInstanceModelCreator_save_feature", "_UI_AadlInstanceModelCreator_type"),
				 WorkflowosatePackage.Literals.AADL_INSTANCE_MODEL_CREATOR__SAVE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns AadlInstanceModelCreator.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AadlInstanceModelCreator"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		return "AADL Instance Model Creator " + ( (AadlInstanceModelCreator) object ).getName();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AadlInstanceModelCreator.class)) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME:
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT:
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT:
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
