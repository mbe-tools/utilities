/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate.impl;

import de.mdelab.workflow.WorkflowPackage;

import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.helpers.HelpersPackage;
import fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator;
import fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosateFactory;
import fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowosatePackageImpl extends EPackageImpl implements WorkflowosatePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aadlInstanceModelCreatorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowosatePackageImpl() {
		super(eNS_URI, WorkflowosateFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowosatePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowosatePackage init() {
		if (isInited) return (WorkflowosatePackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowosatePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowosatePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowosatePackageImpl theWorkflowosatePackage = registeredWorkflowosatePackage instanceof WorkflowosatePackageImpl ? (WorkflowosatePackageImpl)registeredWorkflowosatePackage : new WorkflowosatePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		WorkflowPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowosatePackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowosatePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWorkflowosatePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowosatePackage.eNS_URI, theWorkflowosatePackage);
		return theWorkflowosatePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAadlInstanceModelCreator() {
		return aadlInstanceModelCreatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInstanceModelCreator_SystemImplementationName() {
		return (EAttribute)aadlInstanceModelCreatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInstanceModelCreator_PackageModelSlot() {
		return (EAttribute)aadlInstanceModelCreatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInstanceModelCreator_SystemInstanceModelSlot() {
		return (EAttribute)aadlInstanceModelCreatorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAadlInstanceModelCreator_Save() {
		return (EAttribute)aadlInstanceModelCreatorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowosateFactory getWorkflowosateFactory() {
		return (WorkflowosateFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		aadlInstanceModelCreatorEClass = createEClass(AADL_INSTANCE_MODEL_CREATOR);
		createEAttribute(aadlInstanceModelCreatorEClass, AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME);
		createEAttribute(aadlInstanceModelCreatorEClass, AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT);
		createEAttribute(aadlInstanceModelCreatorEClass, AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT);
		createEAttribute(aadlInstanceModelCreatorEClass, AADL_INSTANCE_MODEL_CREATOR__SAVE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aadlInstanceModelCreatorEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());

		// Initialize classes, features, and operations; add parameters
		initEClass(aadlInstanceModelCreatorEClass, AadlInstanceModelCreator.class, "AadlInstanceModelCreator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAadlInstanceModelCreator_SystemImplementationName(), ecorePackage.getEString(), "systemImplementationName", null, 1, 1, AadlInstanceModelCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAadlInstanceModelCreator_PackageModelSlot(), ecorePackage.getEString(), "packageModelSlot", null, 1, 1, AadlInstanceModelCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAadlInstanceModelCreator_SystemInstanceModelSlot(), ecorePackage.getEString(), "systemInstanceModelSlot", null, 1, 1, AadlInstanceModelCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAadlInstanceModelCreator_Save(), ecorePackage.getEBoolean(), "save", "true", 0, 1, AadlInstanceModelCreator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WorkflowosatePackageImpl
