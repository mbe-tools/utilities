/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aadl Instance Model Creator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemImplementationName <em>System Implementation Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getPackageModelSlot <em>Package Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemInstanceModelSlot <em>System Instance Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#isSave <em>Save</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getAadlInstanceModelCreator()
 * @model
 * @generated
 */
public interface AadlInstanceModelCreator extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>System Implementation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Implementation Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Implementation Name</em>' attribute.
	 * @see #setSystemImplementationName(String)
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getAadlInstanceModelCreator_SystemImplementationName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemImplementationName();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemImplementationName <em>System Implementation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Implementation Name</em>' attribute.
	 * @see #getSystemImplementationName()
	 * @generated
	 */
	void setSystemImplementationName(String value);

	/**
	 * Returns the value of the '<em><b>Package Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Model Slot</em>' attribute.
	 * @see #setPackageModelSlot(String)
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getAadlInstanceModelCreator_PackageModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getPackageModelSlot();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getPackageModelSlot <em>Package Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Model Slot</em>' attribute.
	 * @see #getPackageModelSlot()
	 * @generated
	 */
	void setPackageModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>System Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Instance Model Slot</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Instance Model Slot</em>' attribute.
	 * @see #setSystemInstanceModelSlot(String)
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getAadlInstanceModelCreator_SystemInstanceModelSlot()
	 * @model required="true"
	 * @generated
	 */
	String getSystemInstanceModelSlot();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemInstanceModelSlot <em>System Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Instance Model Slot</em>' attribute.
	 * @see #getSystemInstanceModelSlot()
	 * @generated
	 */
	void setSystemInstanceModelSlot(String value);

	/**
	 * Returns the value of the '<em><b>Save</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Save</em>' attribute.
	 * @see #setSave(boolean)
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getAadlInstanceModelCreator_Save()
	 * @model default="true"
	 * @generated
	 */
	boolean isSave();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#isSave <em>Save</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Save</em>' attribute.
	 * @see #isSave()
	 * @generated
	 */
	void setSave(boolean value);

} // AadlInstanceModelCreator
