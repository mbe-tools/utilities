/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate;

import de.mdelab.workflow.components.WorkflowComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Osate Setup</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage#getOsateSetup()
 * @model
 * @generated
 */
public interface OsateSetup extends WorkflowComponent {
} // OsateSetup
