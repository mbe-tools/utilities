/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate.impl;

import fr.tpt.mem4csd.workflow.components.workflowosate.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowosateFactoryImpl extends EFactoryImpl implements WorkflowosateFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowosateFactory init() {
		try {
			WorkflowosateFactory theWorkflowosateFactory = (WorkflowosateFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowosatePackage.eNS_URI);
			if (theWorkflowosateFactory != null) {
				return theWorkflowosateFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowosateFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowosateFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR: return createAadlInstanceModelCreator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AadlInstanceModelCreator createAadlInstanceModelCreator() {
		AadlInstanceModelCreatorImpl aadlInstanceModelCreator = new AadlInstanceModelCreatorImpl();
		return aadlInstanceModelCreator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowosatePackage getWorkflowosatePackage() {
		return (WorkflowosatePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowosatePackage getPackage() {
		return WorkflowosatePackage.eINSTANCE;
	}

} //WorkflowosateFactoryImpl
