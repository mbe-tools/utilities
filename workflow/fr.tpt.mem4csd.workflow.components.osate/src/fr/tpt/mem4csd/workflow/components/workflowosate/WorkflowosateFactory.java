/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage
 * @generated
 */
public interface WorkflowosateFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowosateFactory eINSTANCE = fr.tpt.mem4csd.workflow.components.workflowosate.impl.WorkflowosateFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Aadl Instance Model Creator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aadl Instance Model Creator</em>'.
	 * @generated
	 */
	AadlInstanceModelCreator createAadlInstanceModelCreator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowosatePackage getWorkflowosatePackage();

} //WorkflowosateFactory
