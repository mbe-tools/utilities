/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate;

import de.mdelab.workflow.components.ComponentsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosateFactory
 * @model kind="package"
 * @generated
 */
public interface WorkflowosatePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowosate";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowosate";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowosate";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowosatePackage eINSTANCE = fr.tpt.mem4csd.workflow.components.workflowosate.impl.WorkflowosatePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl <em>Aadl Instance Model Creator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.impl.WorkflowosatePackageImpl#getAadlInstanceModelCreator()
	 * @generated
	 */
	int AADL_INSTANCE_MODEL_CREATOR = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>System Implementation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Package Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>System Instance Model Slot</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Save</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR__SAVE = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Aadl Instance Model Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;


	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int AADL_INSTANCE_MODEL_CREATOR___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Aadl Instance Model Creator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL_INSTANCE_MODEL_CREATOR_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator <em>Aadl Instance Model Creator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aadl Instance Model Creator</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator
	 * @generated
	 */
	EClass getAadlInstanceModelCreator();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemImplementationName <em>System Implementation Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Implementation Name</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemImplementationName()
	 * @see #getAadlInstanceModelCreator()
	 * @generated
	 */
	EAttribute getAadlInstanceModelCreator_SystemImplementationName();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getPackageModelSlot <em>Package Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Package Model Slot</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getPackageModelSlot()
	 * @see #getAadlInstanceModelCreator()
	 * @generated
	 */
	EAttribute getAadlInstanceModelCreator_PackageModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemInstanceModelSlot <em>System Instance Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Instance Model Slot</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#getSystemInstanceModelSlot()
	 * @see #getAadlInstanceModelCreator()
	 * @generated
	 */
	EAttribute getAadlInstanceModelCreator_SystemInstanceModelSlot();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#isSave <em>Save</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Save</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator#isSave()
	 * @see #getAadlInstanceModelCreator()
	 * @generated
	 */
	EAttribute getAadlInstanceModelCreator_Save();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowosateFactory getWorkflowosateFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl <em>Aadl Instance Model Creator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl
		 * @see fr.tpt.mem4csd.workflow.components.workflowosate.impl.WorkflowosatePackageImpl#getAadlInstanceModelCreator()
		 * @generated
		 */
		EClass AADL_INSTANCE_MODEL_CREATOR = eINSTANCE.getAadlInstanceModelCreator();
		/**
		 * The meta object literal for the '<em><b>System Implementation Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME = eINSTANCE.getAadlInstanceModelCreator_SystemImplementationName();
		/**
		 * The meta object literal for the '<em><b>Package Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT = eINSTANCE.getAadlInstanceModelCreator_PackageModelSlot();
		/**
		 * The meta object literal for the '<em><b>System Instance Model Slot</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT = eINSTANCE.getAadlInstanceModelCreator_SystemInstanceModelSlot();
		/**
		 * The meta object literal for the '<em><b>Save</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AADL_INSTANCE_MODEL_CREATOR__SAVE = eINSTANCE.getAadlInstanceModelCreator_Save();

	}

} //WorkflowosatePackage
