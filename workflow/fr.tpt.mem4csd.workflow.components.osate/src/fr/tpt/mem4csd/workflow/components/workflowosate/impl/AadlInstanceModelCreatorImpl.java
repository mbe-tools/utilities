/**
 */
package fr.tpt.mem4csd.workflow.components.workflowosate.impl;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.errorreporting.AnalysisErrorReporterManager;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.tpt.mem4csd.workflow.components.workflowosate.AadlInstanceModelCreator;
import fr.tpt.mem4csd.workflow.components.workflowosate.WorkflowosatePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aadl Instance Model Creator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl#getSystemImplementationName <em>System Implementation Name</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl#getPackageModelSlot <em>Package Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl#getSystemInstanceModelSlot <em>System Instance Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.workflowosate.impl.AadlInstanceModelCreatorImpl#isSave <em>Save</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AadlInstanceModelCreatorImpl extends WorkflowComponentImpl implements AadlInstanceModelCreator {
	/**
	 * The default value of the '{@link #getSystemImplementationName() <em>System Implementation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemImplementationName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_IMPLEMENTATION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemImplementationName() <em>System Implementation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemImplementationName()
	 * @generated
	 * @ordered
	 */
	protected String systemImplementationName = SYSTEM_IMPLEMENTATION_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPackageModelSlot() <em>Package Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKAGE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPackageModelSlot() <em>Package Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String packageModelSlot = PACKAGE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSystemInstanceModelSlot() <em>System Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_INSTANCE_MODEL_SLOT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemInstanceModelSlot() <em>System Instance Model Slot</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemInstanceModelSlot()
	 * @generated
	 * @ordered
	 */
	protected String systemInstanceModelSlot = SYSTEM_INSTANCE_MODEL_SLOT_EDEFAULT;

	/**
	 * The default value of the '{@link #isSave() <em>Save</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSave()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isSave() <em>Save</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSave()
	 * @generated
	 * @ordered
	 */
	protected boolean save = SAVE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AadlInstanceModelCreatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowosatePackage.Literals.AADL_INSTANCE_MODEL_CREATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSystemImplementationName() {
		return systemImplementationName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSystemImplementationName(String newSystemImplementationName) {
		String oldSystemImplementationName = systemImplementationName;
		systemImplementationName = newSystemImplementationName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME, oldSystemImplementationName, systemImplementationName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPackageModelSlot() {
		return packageModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPackageModelSlot(String newPackageModelSlot) {
		String oldPackageModelSlot = packageModelSlot;
		packageModelSlot = newPackageModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT, oldPackageModelSlot, packageModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSystemInstanceModelSlot() {
		return systemInstanceModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSystemInstanceModelSlot(String newSystemInstanceModelSlot) {
		String oldSystemInstanceModelSlot = systemInstanceModelSlot;
		systemInstanceModelSlot = newSystemInstanceModelSlot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT, oldSystemInstanceModelSlot, systemInstanceModelSlot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSave() {
		return save;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSave(boolean newSave) {
		boolean oldSave = save;
		save = newSave;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE, oldSave, save));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME:
				return getSystemImplementationName();
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT:
				return getPackageModelSlot();
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT:
				return getSystemInstanceModelSlot();
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE:
				return isSave();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME:
				setSystemImplementationName((String)newValue);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT:
				setPackageModelSlot((String)newValue);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT:
				setSystemInstanceModelSlot((String)newValue);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE:
				setSave((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME:
				setSystemImplementationName(SYSTEM_IMPLEMENTATION_NAME_EDEFAULT);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT:
				setPackageModelSlot(PACKAGE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT:
				setSystemInstanceModelSlot(SYSTEM_INSTANCE_MODEL_SLOT_EDEFAULT);
				return;
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE:
				setSave(SAVE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_IMPLEMENTATION_NAME:
				return SYSTEM_IMPLEMENTATION_NAME_EDEFAULT == null ? systemImplementationName != null : !SYSTEM_IMPLEMENTATION_NAME_EDEFAULT.equals(systemImplementationName);
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__PACKAGE_MODEL_SLOT:
				return PACKAGE_MODEL_SLOT_EDEFAULT == null ? packageModelSlot != null : !PACKAGE_MODEL_SLOT_EDEFAULT.equals(packageModelSlot);
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SYSTEM_INSTANCE_MODEL_SLOT:
				return SYSTEM_INSTANCE_MODEL_SLOT_EDEFAULT == null ? systemInstanceModelSlot != null : !SYSTEM_INSTANCE_MODEL_SLOT_EDEFAULT.equals(systemInstanceModelSlot);
			case WorkflowosatePackage.AADL_INSTANCE_MODEL_CREATOR__SAVE:
				return save != SAVE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (systemImplementationName: ");
		result.append(systemImplementationName);
		result.append(", packageModelSlot: ");
		result.append(packageModelSlot);
		result.append(", systemInstanceModelSlot: ");
		result.append(systemInstanceModelSlot);
		result.append(", save: ");
		result.append(save);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean checkConfiguration( final WorkflowExecutionContext context )
	throws IOException {
		boolean success = true;

		if ( getSystemImplementationName() == null || getSystemImplementationName().isEmpty() ) {
			context.getLogger().addError( "System implementation name is not set.", null, this );
			success = false;
		}

		if ( getPackageModelSlot() == null || getPackageModelSlot().isEmpty() ) {
			context.getLogger().addError( "AADL package model slot is not set.", null, this );
			success = false;
		}

		if ( getSystemInstanceModelSlot() == null || getSystemInstanceModelSlot().isEmpty() ) {
			context.getLogger().addError( "System instance model slot is not set.", null, this );
			success = false;
		}
		
		return success;
	}
	
	private SystemImplementation findSystemImplementation( final WorkflowExecutionContext context ) {
		final AadlPackage res = (AadlPackage) context.getModelSlots().get( getPackageModelSlot() );
		final String name = getSystemImplementationName();
		
		for ( final Classifier classifier : res.getOwnedPublicSection().getOwnedClassifiers() ) {
			if (classifier.getName().equals(name)) {
				return (SystemImplementation) classifier;
			}
		}
		
		return null;
	}

	@Override
	public void execute(final WorkflowExecutionContext context, final IProgressMonitor monitor)
	throws WorkflowExecutionException, IOException {
		final SystemImplementation systemImpl = findSystemImplementation( context );

		if ( systemImpl == null || systemImpl.eResource() == null ) {
			final String message = "System implementation '" + getSystemImplementationName() + "' could not be found in AADL package of model slot '" + getPackageModelSlot() + "'.";
			context.getLogger().addError( message, null, this );

			throw new WorkflowExecutionException( message );
		}

		
		try {
			final URI instanceURI = InstantiateModel.getInstanceModelURI( systemImpl );
			final ResourceSet resSet = systemImpl.eResource().getResourceSet();
			final Resource instanceRes = resSet.getResource( instanceURI, false );
			
			if ( instanceRes != null ) {
				instanceRes.unload();
				resSet.getResources().remove( instanceRes );
			}

			final SystemInstance systIns = InstantiateModel.instantiate( systemImpl, AnalysisErrorReporterManager.NULL_ERROR_MANANGER, monitor );
			final String errorMessage = InstantiateModel.getErrorMessage();
			
			if ( errorMessage != null ) {
				throw new WorkflowExecutionException( errorMessage );
			}
			
			//systIns.eResource()
			context.getModelSlots().put( getSystemInstanceModelSlot(), systIns );
			
			if ( isSave() ) {
				systIns.eResource().save( null );
			}
		}
		catch ( final IOException ex ) {
			throw ex;
		}
		catch ( final Exception ex ) {
			throw new WorkflowExecutionException( ex );
		}
	}
} //AadlInstanceModelCreatorImpl
