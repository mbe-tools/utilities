/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm;

import de.mdelab.workflow.components.ComponentsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import workflow.components='../../../plugin/de.mdelab.workflow/model/workflow.ecore#//components' workflow.components.atlTransformer='../../../plugin/de.mdelab.workflow.components.atlTransformer/model/atlTransformer.ecore#/'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface WorkflowemftvmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workflowemftvm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paristech.fr/utilities/workflow/workflowemftvm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workflowemftvm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowemftvmPackage eINSTANCE = fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.WorkflowemftvmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl <em>Emft Vm Transformer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.WorkflowemftvmPackageImpl#getEmftVmTransformer()
	 * @generated
	 */
	int EMFT_VM_TRANSFORMER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__NAME = ComponentsPackage.WORKFLOW_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__DESCRIPTION = ComponentsPackage.WORKFLOW_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__ENABLED = ComponentsPackage.WORKFLOW_COMPONENT__ENABLED;

	/**
	 * The feature id for the '<em><b>Rules Model Slot</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__INPUT_MODELS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Output Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__OUTPUT_MODELS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Output Models</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Register Dependency Models</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Debug Output</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__DEBUG_OUTPUT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Discard Extra Root Elements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Emft Vm Transformer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER_FEATURE_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_FEATURE_COUNT + 7;


	/**
	 * The operation id for the '<em>Check Configuration</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CONFIGURATION__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link de.mdelab.workflow.components.WorkflowComponent#execute(de.mdelab.workflow.WorkflowExecutionContext) model documentation} for details.
	 * @generated
	 * @ordered
	 */
	@Deprecated
	int EMFT_VM_TRANSFORMER___EXECUTE__WORKFLOWEXECUTIONCONTEXT = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT;

	/**
	 * The operation id for the '<em>Execute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___EXECUTE__WORKFLOWEXECUTIONCONTEXT_IPROGRESSMONITOR;

	/**
	 * The operation id for the '<em>Check Canceled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER___CHECK_CANCELED__IPROGRESSMONITOR = ComponentsPackage.WORKFLOW_COMPONENT___CHECK_CANCELED__IPROGRESSMONITOR;

	/**
	 * The number of operations of the '<em>Emft Vm Transformer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMFT_VM_TRANSFORMER_OPERATION_COUNT = ComponentsPackage.WORKFLOW_COMPONENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer <em>Emft Vm Transformer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Emft Vm Transformer</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer
	 * @generated
	 */
	EClass getEmftVmTransformer();

	/**
	 * Returns the meta object for the attribute list '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getRulesModelSlot <em>Rules Model Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Rules Model Slot</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getRulesModelSlot()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EAttribute getEmftVmTransformer_RulesModelSlot();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputModels <em>Input Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Models</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputModels()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EReference getEmftVmTransformer_InputModels();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getOutputModels <em>Output Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Models</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getOutputModels()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EReference getEmftVmTransformer_OutputModels();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputOutputModels <em>Input Output Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Output Models</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputOutputModels()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EReference getEmftVmTransformer_InputOutputModels();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isRegisterDependencyModels <em>Register Dependency Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Register Dependency Models</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isRegisterDependencyModels()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EAttribute getEmftVmTransformer_RegisterDependencyModels();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDebugOutput <em>Debug Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Debug Output</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDebugOutput()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EAttribute getEmftVmTransformer_DebugOutput();

	/**
	 * Returns the meta object for the attribute '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDiscardExtraRootElements <em>Discard Extra Root Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discard Extra Root Elements</em>'.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDiscardExtraRootElements()
	 * @see #getEmftVmTransformer()
	 * @generated
	 */
	EAttribute getEmftVmTransformer_DiscardExtraRootElements();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkflowemftvmFactory getWorkflowemftvmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl <em>Emft Vm Transformer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl
		 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.WorkflowemftvmPackageImpl#getEmftVmTransformer()
		 * @generated
		 */
		EClass EMFT_VM_TRANSFORMER = eINSTANCE.getEmftVmTransformer();

		/**
		 * The meta object literal for the '<em><b>Rules Model Slot</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT = eINSTANCE.getEmftVmTransformer_RulesModelSlot();

		/**
		 * The meta object literal for the '<em><b>Input Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMFT_VM_TRANSFORMER__INPUT_MODELS = eINSTANCE.getEmftVmTransformer_InputModels();

		/**
		 * The meta object literal for the '<em><b>Output Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMFT_VM_TRANSFORMER__OUTPUT_MODELS = eINSTANCE.getEmftVmTransformer_OutputModels();

		/**
		 * The meta object literal for the '<em><b>Input Output Models</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS = eINSTANCE.getEmftVmTransformer_InputOutputModels();

		/**
		 * The meta object literal for the '<em><b>Register Dependency Models</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS = eINSTANCE.getEmftVmTransformer_RegisterDependencyModels();

		/**
		 * The meta object literal for the '<em><b>Debug Output</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMFT_VM_TRANSFORMER__DEBUG_OUTPUT = eINSTANCE.getEmftVmTransformer_DebugOutput();

		/**
		 * The meta object literal for the '<em><b>Discard Extra Root Elements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS = eINSTANCE.getEmftVmTransformer_DiscardExtraRootElements();

	}

} //WorkflowemftvmPackage
