/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm;

import de.mdelab.workflow.components.WorkflowComponent;

import de.mdelab.workflow.components.atlTransformer.ATLTransformerModel;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Emft Vm Transformer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getRulesModelSlot <em>Rules Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputModels <em>Input Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getOutputModels <em>Output Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#getInputOutputModels <em>Input Output Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isRegisterDependencyModels <em>Register Dependency Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDebugOutput <em>Debug Output</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDiscardExtraRootElements <em>Discard Extra Root Elements</em>}</li>
 * </ul>
 *
 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='atLeastOneOutputModel'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot atLeastOneOutputModel='( not outputModels-&gt;isEmpty() ) or ( not inputOutputModels-&gt;isEmpty() )'"
 * @generated
 */
public interface EmftVmTransformer extends WorkflowComponent {
	/**
	 * Returns the value of the '<em><b>Rules Model Slot</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules Model Slot</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules Model Slot</em>' attribute list.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_RulesModelSlot()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getRulesModelSlot();

	/**
	 * Returns the value of the '<em><b>Input Models</b></em>' containment reference list.
	 * The list contents are of type {@link de.mdelab.workflow.components.atlTransformer.ATLTransformerModel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The input models of the transformation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Input Models</em>' containment reference list.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_InputModels()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ATLTransformerModel> getInputModels();

	/**
	 * Returns the value of the '<em><b>Output Models</b></em>' containment reference list.
	 * The list contents are of type {@link de.mdelab.workflow.components.atlTransformer.ATLTransformerModel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The output models of the transformation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Output Models</em>' containment reference list.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_OutputModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<ATLTransformerModel> getOutputModels();

	/**
	 * Returns the value of the '<em><b>Input Output Models</b></em>' containment reference list.
	 * The list contents are of type {@link de.mdelab.workflow.components.atlTransformer.ATLTransformerModel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The output models of the transformation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Input Output Models</em>' containment reference list.
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_InputOutputModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<ATLTransformerModel> getInputOutputModels();

	/**
	 * Returns the value of the '<em><b>Register Dependency Models</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Register Dependency Models</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Register Dependency Models</em>' attribute.
	 * @see #setRegisterDependencyModels(boolean)
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_RegisterDependencyModels()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isRegisterDependencyModels();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isRegisterDependencyModels <em>Register Dependency Models</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Register Dependency Models</em>' attribute.
	 * @see #isRegisterDependencyModels()
	 * @generated
	 */
	void setRegisterDependencyModels(boolean value);

	/**
	 * Returns the value of the '<em><b>Debug Output</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug Output</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug Output</em>' attribute.
	 * @see #setDebugOutput(boolean)
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_DebugOutput()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isDebugOutput();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDebugOutput <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug Output</em>' attribute.
	 * @see #isDebugOutput()
	 * @generated
	 */
	void setDebugOutput(boolean value);

	/**
	 * Returns the value of the '<em><b>Discard Extra Root Elements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Extra Root Elements</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Extra Root Elements</em>' attribute.
	 * @see #setDiscardExtraRootElements(boolean)
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#getEmftVmTransformer_DiscardExtraRootElements()
	 * @model required="true"
	 * @generated
	 */
	boolean isDiscardExtraRootElements();

	/**
	 * Sets the value of the '{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer#isDiscardExtraRootElements <em>Discard Extra Root Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Extra Root Elements</em>' attribute.
	 * @see #isDiscardExtraRootElements()
	 * @generated
	 */
	void setDiscardExtraRootElements(boolean value);

} // EmftVmTransformer
