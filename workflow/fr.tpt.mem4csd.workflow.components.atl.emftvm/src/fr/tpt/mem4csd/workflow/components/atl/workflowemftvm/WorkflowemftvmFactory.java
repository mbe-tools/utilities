/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage
 * @generated
 */
public interface WorkflowemftvmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkflowemftvmFactory eINSTANCE = fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.WorkflowemftvmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Emft Vm Transformer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Emft Vm Transformer</em>'.
	 * @generated
	 */
	EmftVmTransformer createEmftVmTransformer();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WorkflowemftvmPackage getWorkflowemftvmPackage();

} //WorkflowemftvmFactory
