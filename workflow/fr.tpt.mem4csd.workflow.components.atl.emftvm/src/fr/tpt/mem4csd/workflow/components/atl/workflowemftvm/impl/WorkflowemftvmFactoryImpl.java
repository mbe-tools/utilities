/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl;

import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowemftvmFactoryImpl extends EFactoryImpl implements WorkflowemftvmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WorkflowemftvmFactory init() {
		try {
			WorkflowemftvmFactory theWorkflowemftvmFactory = (WorkflowemftvmFactory)EPackage.Registry.INSTANCE.getEFactory(WorkflowemftvmPackage.eNS_URI);
			if (theWorkflowemftvmFactory != null) {
				return theWorkflowemftvmFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WorkflowemftvmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowemftvmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER: return createEmftVmTransformer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EmftVmTransformer createEmftVmTransformer() {
		EmftVmTransformerImpl emftVmTransformer = new EmftVmTransformerImpl();
		return emftVmTransformer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowemftvmPackage getWorkflowemftvmPackage() {
		return (WorkflowemftvmPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WorkflowemftvmPackage getPackage() {
		return WorkflowemftvmPackage.eINSTANCE;
	}

} //WorkflowemftvmFactoryImpl
