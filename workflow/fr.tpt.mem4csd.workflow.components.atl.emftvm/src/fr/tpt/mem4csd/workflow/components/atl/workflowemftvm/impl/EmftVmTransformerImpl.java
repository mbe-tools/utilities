/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.Module;
import org.eclipse.m2m.atl.emftvm.profiler.Profiler;
import org.eclipse.m2m.atl.emftvm.profiler.Profiler.ProfilingData;
import org.eclipse.m2m.atl.emftvm.util.ModuleNotFoundException;
import org.eclipse.m2m.atl.emftvm.util.ModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.TimingData;

import de.mdelab.workflow.WorkflowExecutionContext;
import de.mdelab.workflow.WorkflowExecutionLogger;
import de.mdelab.workflow.components.atlTransformer.ATLTransformerModel;
import de.mdelab.workflow.components.impl.WorkflowComponentImpl;
import de.mdelab.workflow.impl.WorkflowExecutionException;
import fr.mem4csd.utils.emf.EMFUtil;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Emft Vm Transformer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#getRulesModelSlot <em>Rules Model Slot</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#getInputModels <em>Input Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#getOutputModels <em>Output Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#getInputOutputModels <em>Input Output Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#isRegisterDependencyModels <em>Register Dependency Models</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#isDebugOutput <em>Debug Output</em>}</li>
 *   <li>{@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl.EmftVmTransformerImpl#isDiscardExtraRootElements <em>Discard Extra Root Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmftVmTransformerImpl extends WorkflowComponentImpl implements EmftVmTransformer {
	/**
	 * The cached value of the '{@link #getRulesModelSlot() <em>Rules Model Slot</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRulesModelSlot()
	 * @generated
	 * @ordered
	 */
	protected EList<String> rulesModelSlot;

	/**
	 * The cached value of the '{@link #getInputModels() <em>Input Models</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputModels()
	 * @generated
	 * @ordered
	 */
	protected EList<ATLTransformerModel> inputModels;

	/**
	 * The cached value of the '{@link #getOutputModels() <em>Output Models</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputModels()
	 * @generated
	 * @ordered
	 */
	protected EList<ATLTransformerModel> outputModels;

	/**
	 * The cached value of the '{@link #getInputOutputModels() <em>Input Output Models</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputOutputModels()
	 * @generated
	 * @ordered
	 */
	protected EList<ATLTransformerModel> inputOutputModels;

	/**
	 * The default value of the '{@link #isRegisterDependencyModels() <em>Register Dependency Models</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRegisterDependencyModels()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REGISTER_DEPENDENCY_MODELS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRegisterDependencyModels() <em>Register Dependency Models</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRegisterDependencyModels()
	 * @generated
	 * @ordered
	 */
	protected boolean registerDependencyModels = REGISTER_DEPENDENCY_MODELS_EDEFAULT;

	/**
	 * The default value of the '{@link #isDebugOutput() <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebugOutput()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEBUG_OUTPUT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDebugOutput() <em>Debug Output</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDebugOutput()
	 * @generated
	 * @ordered
	 */
	protected boolean debugOutput = DEBUG_OUTPUT_EDEFAULT;

	/**
	 * The default value of the '{@link #isDiscardExtraRootElements() <em>Discard Extra Root Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDiscardExtraRootElements()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISCARD_EXTRA_ROOT_ELEMENTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDiscardExtraRootElements() <em>Discard Extra Root Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDiscardExtraRootElements()
	 * @generated
	 * @ordered
	 */
	protected boolean discardExtraRootElements = DISCARD_EXTRA_ROOT_ELEMENTS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmftVmTransformerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getRulesModelSlot() {
		if (rulesModelSlot == null) {
			rulesModelSlot = new EDataTypeUniqueEList<String>(String.class, this, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT);
		}
		return rulesModelSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ATLTransformerModel> getInputModels() {
		if (inputModels == null) {
			inputModels = new EObjectContainmentEList<ATLTransformerModel>(ATLTransformerModel.class, this, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS);
		}
		return inputModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ATLTransformerModel> getOutputModels() {
		if (outputModels == null) {
			outputModels = new EObjectContainmentEList<ATLTransformerModel>(ATLTransformerModel.class, this, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS);
		}
		return outputModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ATLTransformerModel> getInputOutputModels() {
		if (inputOutputModels == null) {
			inputOutputModels = new EObjectContainmentEList<ATLTransformerModel>(ATLTransformerModel.class, this, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS);
		}
		return inputOutputModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isRegisterDependencyModels() {
		return registerDependencyModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRegisterDependencyModels(boolean newRegisterDependencyModels) {
		boolean oldRegisterDependencyModels = registerDependencyModels;
		registerDependencyModels = newRegisterDependencyModels;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS, oldRegisterDependencyModels, registerDependencyModels));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isDebugOutput() {
		return debugOutput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDebugOutput(boolean newDebugOutput) {
		boolean oldDebugOutput = debugOutput;
		debugOutput = newDebugOutput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT, oldDebugOutput, debugOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isDiscardExtraRootElements() {
		return discardExtraRootElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDiscardExtraRootElements(boolean newDiscardExtraRootElements) {
		boolean oldDiscardExtraRootElements = discardExtraRootElements;
		discardExtraRootElements = newDiscardExtraRootElements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS, oldDiscardExtraRootElements, discardExtraRootElements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
				return ((InternalEList<?>)getInputModels()).basicRemove(otherEnd, msgs);
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
				return ((InternalEList<?>)getOutputModels()).basicRemove(otherEnd, msgs);
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				return ((InternalEList<?>)getInputOutputModels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT:
				return getRulesModelSlot();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
				return getInputModels();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
				return getOutputModels();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				return getInputOutputModels();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS:
				return isRegisterDependencyModels();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT:
				return isDebugOutput();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS:
				return isDiscardExtraRootElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT:
				getRulesModelSlot().clear();
				getRulesModelSlot().addAll((Collection<? extends String>)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
				getInputModels().clear();
				getInputModels().addAll((Collection<? extends ATLTransformerModel>)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
				getOutputModels().clear();
				getOutputModels().addAll((Collection<? extends ATLTransformerModel>)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				getInputOutputModels().clear();
				getInputOutputModels().addAll((Collection<? extends ATLTransformerModel>)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS:
				setRegisterDependencyModels((Boolean)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT:
				setDebugOutput((Boolean)newValue);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS:
				setDiscardExtraRootElements((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT:
				getRulesModelSlot().clear();
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
				getInputModels().clear();
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
				getOutputModels().clear();
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				getInputOutputModels().clear();
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS:
				setRegisterDependencyModels(REGISTER_DEPENDENCY_MODELS_EDEFAULT);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT:
				setDebugOutput(DEBUG_OUTPUT_EDEFAULT);
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS:
				setDiscardExtraRootElements(DISCARD_EXTRA_ROOT_ELEMENTS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT:
				return rulesModelSlot != null && !rulesModelSlot.isEmpty();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
				return inputModels != null && !inputModels.isEmpty();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
				return outputModels != null && !outputModels.isEmpty();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				return inputOutputModels != null && !inputOutputModels.isEmpty();
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS:
				return registerDependencyModels != REGISTER_DEPENDENCY_MODELS_EDEFAULT;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT:
				return debugOutput != DEBUG_OUTPUT_EDEFAULT;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS:
				return discardExtraRootElements != DISCARD_EXTRA_ROOT_ELEMENTS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (rulesModelSlot: ");
		result.append(rulesModelSlot);
		result.append(", registerDependencyModels: ");
		result.append(registerDependencyModels);
		result.append(", debugOutput: ");
		result.append(debugOutput);
		result.append(", discardExtraRootElements: ");
		result.append(discardExtraRootElements);
		result.append(')');
		return result.toString();
	}

	@Override
	public void execute(	final WorkflowExecutionContext context,
							final IProgressMonitor monitor )
	throws WorkflowExecutionException, IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 2);
		
		try {
			final ExecEnv env = createExecutionEnvironment( context );
			subMonitor.worked( 1 );
			
			doTransformation( env, context.getLogger() );
			
			setOutputModels( context, env.getInoutModels(), getInputOutputModels() );
			setOutputModels( context, env.getOutputModels(), getOutputModels() );

			subMonitor.worked( 1 );
		}
		catch ( final WorkflowExecutionException p_ex ) {
			throw p_ex;
		}
		catch ( RuntimeException p_ex ) {
			context.getLogger().addError( "Error when executing workflow component " + getName(), p_ex, this );
			
			throw p_ex;
		}
	}
	
	private ExecEnv createExecutionEnvironment( final WorkflowExecutionContext context )
	throws WorkflowExecutionException {
		final ExecEnv env = EmftvmFactory.eINSTANCE.createExecEnv();
		loadMetaModels( env, getInputModels(), context.getLogger() );
		loadMetaModels( env, getOutputModels(), context.getLogger() );
		loadInputModels( context, env );
		loadOutputModels( env, context.getGlobalResourceSet() );
		loadInputOutputModels( context, env );
		
		loadModules( context, env );
		
		return env;
	}
	
	private void doTransformation( 	final ExecEnv env,
									final WorkflowExecutionLogger logger ) {
		final Profiler profiler;
		
		if ( isDebugOutput() ) {
			profiler = new Profiler();
			env.setMonitor( profiler );
		}
		else {
			profiler = null;
		}
		
		// Run transformation
		final TimingData td = new TimingData();
		td.finishLoading();

		env.run( td );
		
		if ( profiler != null ) {
			for ( final ProfilingData data : profiler.getResults() ) {
				logger.addInfo( data.toString(), this );
			}
		}
	}
	
	private void setOutputModels( 	final WorkflowExecutionContext context,
									final Map<String, Model> outEnvModels,
									final Collection<ATLTransformerModel> outWorkflowAtlModels) {
		for ( final ATLTransformerModel transModel : outWorkflowAtlModels ) {
			final String slotName = transModel.getModelSlot();

			if ( slotName != null && !slotName.isEmpty() ) {
				final Model model = outEnvModels.get( transModel.getModelName() );
				
				if ( model != null ) {
					final Object retValue;
					
					if ( isDiscardExtraRootElements() && !model.getResource().getContents().isEmpty() ) {
						retValue = model.getResource().getContents().get( 0 );
					}
					else {
						retValue =  model.getResource().getContents();
					}

					context.getModelSlots().put( slotName, retValue );
				}
			}
		}
	}

	public void loadModules( 	final WorkflowExecutionContext context,
								final ExecEnv env )
	throws WorkflowExecutionException {
		final ModuleResolver moduleResolver = new ModuleResolver() {
			
			@Override
			public Module resolveModule( String moduleSlotName )
			throws ModuleNotFoundException {
				final Object module = context.getModelSlots().get( moduleSlotName );
				
				if ( module instanceof Module ) {
					return (Module) module;
				}
				else if ( module instanceof Resource ) {
					return (Module) ( (Resource) module ).getContents().get( 0 );
				}
				else {
					final String message = "Cannot find an EMFTVM Module in module slot '" + moduleSlotName + "'.";
					context.getLogger().addError( message, null, EmftVmTransformerImpl.this );
					
					throw new ModuleNotFoundException( message );
				}
			}
		};
		
		for( final String moduleSlot : getRulesModelSlot() ) {
			env.loadModule( moduleResolver, moduleSlot, true );
		}
	}
	
	private void loadMetaModels( 	final ExecEnv env,
									final Collection<ATLTransformerModel> transModels,
									final WorkflowExecutionLogger logger ) 
	throws WorkflowExecutionException {
		for ( final ATLTransformerModel transModel : transModels ) {
			final String mmName = transModel.getMetamodelName();
			
			if ( !env.getMetaModels().containsKey( mmName ) ) {
				final Metamodel metaModel = EmftvmFactory.eINSTANCE.createMetamodel();
				final String metamodelUri = transModel.getMetamodelURI();
				final EPackage ePackage = EPackage.Registry.INSTANCE.getEPackage( metamodelUri );
				
				if ( ePackage == null ) {
					final String message = "Metamodel package '" + metamodelUri + "' not found in registry.";
					logger.addError( message, null, this );
					
					throw new WorkflowExecutionException( message );
				}
				
				metaModel.setResource( ePackage.eResource() );
				env.registerMetaModel( mmName, metaModel );
			}
		}
	}
	
	private void loadInputModels( 	final WorkflowExecutionContext context,
									final ExecEnv env ) 
	throws WorkflowExecutionException {
		for ( final ATLTransformerModel transModel : getInputModels() ) {
			final String slotName = transModel.getModelSlot();
			
			if ( slotName != null && !slotName.isEmpty() ) {
				final String metamodelUri = transModel.getMetamodelURI();
				final EPackage ePackage = EPackage.Registry.INSTANCE.getEPackage( metamodelUri );

				final Resource resource = getModelResource( context, slotName, ePackage.getNsPrefix(), true );
			
				final List<Resource> addedResourceList = new ArrayList<Resource>();
				loadInputResource (resource, transModel, env, addedResourceList );
			}
		}
	}
	
	private void loadInputResource( final Resource resource,
									final String modelId,
									final ExecEnv env,
									final List<Resource> addedResourceList )	{
		if ( !addedResourceList.contains( resource ) ) {
			addedResourceList.add( resource );

			final Model model = EmftvmFactory.eINSTANCE.createModel();
			model.setResource( resource );
			env.registerInputModel( modelId, model );

			if ( isRegisterDependencyModels() ) {
				for ( final Resource res : EMFUtil.allReferencedExtents( resource ) ) {
					if ( res != resource ) {
						final String id;
						
						final EObject eObj = res.getContents().get(0);
						final EStructuralFeature nameFeat = eObj.eClass().getEStructuralFeature( "name" );
						
						if ( nameFeat == null ) {
							id = res.getURI().trimFileExtension().lastSegment().toUpperCase();
						}
						else {
							final Object featValue = eObj.eGet( nameFeat );
							
							if ( featValue instanceof String ) {
								id = ( (String) featValue ).toUpperCase();
							}
							else {
								id = res.getURI().trimFileExtension().lastSegment().toUpperCase();
							}
						}
	//					List<EAttribute> attrList = eObj.eClass().getEAllAttributes();
	//					for(EAttribute attr : attrList)
	//					{
	//						if(attr.getName().equalsIgnoreCase("name"))
	//						{
	//							Object featValue = eObj.eGet(attr);
	//							if(featValue instanceof String)
	//							{
	//								id = (String) featValue;
	//								id = id.toUpperCase();
	//								break;
	//							}
	//						}
	//					}
	//					if(id==null)
	//						id = res.getURI().trimFileExtension().lastSegment().toUpperCase();
	
						loadInputResource(res, id, env, addedResourceList );
					}
				}
			}
		}
	}
	
	private void loadInputResource( final Resource resource,
									final ATLTransformerModel transModel,
									final ExecEnv env,
									final List<Resource> addedResourceList ) {
		final String modelId = transModel.getModelName();
		loadInputResource( resource, modelId, env, addedResourceList );
	}
	
	private void loadOutputModels( 	final ExecEnv env,
									final ResourceSet resSet ) 
	throws WorkflowExecutionException {
		for ( final ATLTransformerModel transModel : getOutputModels() ) {
			final String slotName = transModel.getModelSlot();
			
			if ( slotName != null && !slotName.isEmpty() ) {
				final Model model = EmftvmFactory.eINSTANCE.createModel();
				final String metamodelUri = transModel.getMetamodelURI();
				final EPackage ePackage = EPackage.Registry.INSTANCE.getEPackage( metamodelUri );
				final Resource resource = createResource( resSet, ePackage.getNsPrefix() );
				model.setResource( resource );
				env.registerOutputModel( transModel.getModelName(), model );
			}
		}
	}
	
	private void loadInputOutputModels( final WorkflowExecutionContext context,
										final ExecEnv env ) 
	throws WorkflowExecutionException {
		for ( final ATLTransformerModel transModel : getInputOutputModels() ) {
			final String slotName = transModel.getModelSlot();
			
			if ( slotName != null && !slotName.isEmpty() ) {
				final Model model = EmftvmFactory.eINSTANCE.createModel();
				
				final String fileExtension = fileExtension( transModel );
				final Resource resource = getModelResource( context,  slotName, fileExtension, false );
				model.setResource( resource );
				env.registerInOutModel( transModel.getModelName(), model );
			}
		}
	}
	
	private String fileExtension( final ATLTransformerModel transModel ) {
		if ( transModel.getExtension() != null ) {
			return transModel.getExtension();
		}
		
		final String metamodelUri = transModel.getMetamodelURI();
		final EPackage ePackage = EPackage.Registry.INSTANCE.getEPackage( metamodelUri );
		
		return ePackage.getNsPrefix();
	}
} //EmftVmTransformerImpl
