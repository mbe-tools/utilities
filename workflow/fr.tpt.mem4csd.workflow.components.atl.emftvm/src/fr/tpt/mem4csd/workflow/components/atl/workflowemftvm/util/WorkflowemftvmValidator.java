/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.util;

import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage
 * @generated
 */
public class WorkflowemftvmValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final WorkflowemftvmValidator INSTANCE = new WorkflowemftvmValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "fr.tpt.mem4csd.workflow.components.atl.workflowemftvm";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkflowemftvmValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return WorkflowemftvmPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER:
				return validateEmftVmTransformer((EmftVmTransformer)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEmftVmTransformer(EmftVmTransformer emftVmTransformer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(emftVmTransformer, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(emftVmTransformer, diagnostics, context);
		if (result || diagnostics != null) result &= validateEmftVmTransformer_atLeastOneOutputModel(emftVmTransformer, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the atLeastOneOutputModel constraint of '<em>Emft Vm Transformer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String EMFT_VM_TRANSFORMER__AT_LEAST_ONE_OUTPUT_MODEL__EEXPRESSION = "( not outputModels->isEmpty() ) or ( not inputOutputModels->isEmpty() )";

	/**
	 * Validates the atLeastOneOutputModel constraint of '<em>Emft Vm Transformer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEmftVmTransformer_atLeastOneOutputModel(EmftVmTransformer emftVmTransformer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER,
				 emftVmTransformer,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "atLeastOneOutputModel",
				 EMFT_VM_TRANSFORMER__AT_LEAST_ONE_OUTPUT_MODEL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //WorkflowemftvmValidator
