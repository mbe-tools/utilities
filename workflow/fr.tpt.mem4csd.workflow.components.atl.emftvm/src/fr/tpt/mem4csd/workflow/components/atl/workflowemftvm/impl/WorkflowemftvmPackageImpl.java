/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.impl;

import de.mdelab.workflow.WorkflowPackage;
import de.mdelab.workflow.components.ComponentsPackage;

import de.mdelab.workflow.components.atlTransformer.AtlTransformerPackage;
import de.mdelab.workflow.helpers.HelpersPackage;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmFactory;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage;

import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.util.WorkflowemftvmValidator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkflowemftvmPackageImpl extends EPackageImpl implements WorkflowemftvmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emftVmTransformerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkflowemftvmPackageImpl() {
		super(eNS_URI, WorkflowemftvmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkflowemftvmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkflowemftvmPackage init() {
		if (isInited) return (WorkflowemftvmPackage)EPackage.Registry.INSTANCE.getEPackage(WorkflowemftvmPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkflowemftvmPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkflowemftvmPackageImpl theWorkflowemftvmPackage = registeredWorkflowemftvmPackage instanceof WorkflowemftvmPackageImpl ? (WorkflowemftvmPackageImpl)registeredWorkflowemftvmPackage : new WorkflowemftvmPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		AtlTransformerPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		ComponentsPackage.eINSTANCE.eClass();
		HelpersPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkflowemftvmPackage.createPackageContents();

		// Initialize created meta-data
		theWorkflowemftvmPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theWorkflowemftvmPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return WorkflowemftvmValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theWorkflowemftvmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkflowemftvmPackage.eNS_URI, theWorkflowemftvmPackage);
		return theWorkflowemftvmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEmftVmTransformer() {
		return emftVmTransformerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEmftVmTransformer_RulesModelSlot() {
		return (EAttribute)emftVmTransformerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEmftVmTransformer_InputModels() {
		return (EReference)emftVmTransformerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEmftVmTransformer_OutputModels() {
		return (EReference)emftVmTransformerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEmftVmTransformer_InputOutputModels() {
		return (EReference)emftVmTransformerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEmftVmTransformer_RegisterDependencyModels() {
		return (EAttribute)emftVmTransformerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEmftVmTransformer_DebugOutput() {
		return (EAttribute)emftVmTransformerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEmftVmTransformer_DiscardExtraRootElements() {
		return (EAttribute)emftVmTransformerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkflowemftvmFactory getWorkflowemftvmFactory() {
		return (WorkflowemftvmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		emftVmTransformerEClass = createEClass(EMFT_VM_TRANSFORMER);
		createEAttribute(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT);
		createEReference(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__INPUT_MODELS);
		createEReference(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__OUTPUT_MODELS);
		createEReference(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS);
		createEAttribute(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS);
		createEAttribute(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__DEBUG_OUTPUT);
		createEAttribute(emftVmTransformerEClass, EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		AtlTransformerPackage theAtlTransformerPackage = (AtlTransformerPackage)EPackage.Registry.INSTANCE.getEPackage(AtlTransformerPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		emftVmTransformerEClass.getESuperTypes().add(theComponentsPackage.getWorkflowComponent());

		// Initialize classes, features, and operations; add parameters
		initEClass(emftVmTransformerEClass, EmftVmTransformer.class, "EmftVmTransformer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmftVmTransformer_RulesModelSlot(), ecorePackage.getEString(), "rulesModelSlot", null, 1, -1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmftVmTransformer_InputModels(), theAtlTransformerPackage.getATLTransformerModel(), null, "inputModels", null, 1, -1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmftVmTransformer_OutputModels(), theAtlTransformerPackage.getATLTransformerModel(), null, "outputModels", null, 0, -1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEmftVmTransformer_InputOutputModels(), theAtlTransformerPackage.getATLTransformerModel(), null, "inputOutputModels", null, 0, -1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmftVmTransformer_RegisterDependencyModels(), ecorePackage.getEBoolean(), "registerDependencyModels", "false", 1, 1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmftVmTransformer_DebugOutput(), ecorePackage.getEBoolean(), "debugOutput", "false", 1, 1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEmftVmTransformer_DiscardExtraRootElements(), ecorePackage.getEBoolean(), "discardExtraRootElements", null, 1, 1, EmftVmTransformer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/OCL/Import
		createImportAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createImportAnnotations() {
		String source = "http://www.eclipse.org/OCL/Import";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "workflow.components", "../../../plugin/de.mdelab.workflow/model/workflow.ecore#//components",
			   "workflow.components.atlTransformer", "../../../plugin/de.mdelab.workflow.components.atlTransformer/model/atlTransformer.ecore#/"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			   "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			   "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });
		addAnnotation
		  (emftVmTransformerEClass,
		   source,
		   new String[] {
			   "constraints", "atLeastOneOutputModel"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
		addAnnotation
		  (emftVmTransformerEClass,
		   source,
		   new String[] {
			   "atLeastOneOutputModel", "( not outputModels->isEmpty() ) or ( not inputOutputModels->isEmpty() )"
		   });
	}

} //WorkflowemftvmPackageImpl
