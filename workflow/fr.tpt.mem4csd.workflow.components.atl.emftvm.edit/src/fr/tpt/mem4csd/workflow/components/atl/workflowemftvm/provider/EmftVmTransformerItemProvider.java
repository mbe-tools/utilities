/**
 */
package fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.mdelab.workflow.components.atlTransformer.AtlTransformerFactory;
import de.mdelab.workflow.components.provider.WorkflowComponentItemProvider;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer;
import fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.WorkflowemftvmPackage;

/**
 * This is the item provider adapter for a {@link fr.tpt.mem4csd.workflow.components.atl.workflowemftvm.EmftVmTransformer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EmftVmTransformerItemProvider extends WorkflowComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmftVmTransformerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRulesModelSlotPropertyDescriptor(object);
			addRegisterDependencyModelsPropertyDescriptor(object);
			addDebugOutputPropertyDescriptor(object);
			addDiscardExtraRootElementsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Rules Model Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRulesModelSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EmftVmTransformer_rulesModelSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EmftVmTransformer_rulesModelSlot_feature", "_UI_EmftVmTransformer_type"),
				 WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Register Dependency Models feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRegisterDependencyModelsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EmftVmTransformer_registerDependencyModels_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EmftVmTransformer_registerDependencyModels_feature", "_UI_EmftVmTransformer_type"),
				 WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Debug Output feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDebugOutputPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EmftVmTransformer_debugOutput_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EmftVmTransformer_debugOutput_feature", "_UI_EmftVmTransformer_type"),
				 WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Discard Extra Root Elements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDiscardExtraRootElementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EmftVmTransformer_discardExtraRootElements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EmftVmTransformer_discardExtraRootElements_feature", "_UI_EmftVmTransformer_type"),
				 WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_MODELS);
			childrenFeatures.add(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__OUTPUT_MODELS);
			childrenFeatures.add(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EmftVmTransformer.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EmftVmTransformer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		final String label = "ATL EMFTVM Transformer "	+ ( (EmftVmTransformer) object ).getName();
		
		return label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EmftVmTransformer.class)) {
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__RULES_MODEL_SLOT:
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__REGISTER_DEPENDENCY_MODELS:
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DEBUG_OUTPUT:
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__DISCARD_EXTRA_ROOT_ELEMENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_MODELS:
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__OUTPUT_MODELS:
			case WorkflowemftvmPackage.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_MODELS,
				 AtlTransformerFactory.eINSTANCE.createATLTransformerModel()));

		newChildDescriptors.add
			(createChildParameter
				(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__OUTPUT_MODELS,
				 AtlTransformerFactory.eINSTANCE.createATLTransformerModel()));

		newChildDescriptors.add
			(createChildParameter
				(WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS,
				 AtlTransformerFactory.eINSTANCE.createATLTransformerModel()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_MODELS ||
			childFeature == WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__OUTPUT_MODELS ||
			childFeature == WorkflowemftvmPackage.Literals.EMFT_VM_TRANSFORMER__INPUT_OUTPUT_MODELS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
